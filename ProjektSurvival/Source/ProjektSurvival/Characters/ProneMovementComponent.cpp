// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "ProneMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"
#include "HumanCharacter.h"
#include "Kismet/KismetMathLibrary.h"

UProneMovementComponent::UProneMovementComponent()
{
	MaxSpeed = 16;
	Acceleration = 2048;
	Deceleration = 2048;
	TurningBoost = 20;
	bAutoActivate = false;
	Deactivate();

	RotationMode = ERotationMode::OrientToMovement;
	Owner = Cast<AHumanCharacter>(GetOwner());
}

void UProneMovementComponent::BeginPlay()
{
	World = GetWorld();

	CharacterMovement = Owner->GetCharacterMovement();
	OwnerRoot = Cast<UPrimitiveComponent>(Owner->GetRootComponent());

	CapsuleRadius = Owner->GetSimpleCollisionRadius();
	CapsuleHalfHeight = Owner->GetSimpleCollisionHalfHeight();
}

void UProneMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Rotation synchronizing with surface	
	SynchronizeRotationWithSurface();

	if (bDeactivate) return;
	
	CurrentSpeed = Velocity.Size();

	// In order to fix an engine glitch that physics won't get updated and also prone collision smooth transition.
	if (Alpha != 1)
	{
		// Setting Alpha and Clamping Alpha (0-1)
		Alpha = FMath::FInterpTo(Alpha, Alpha + 1, World->DeltaTimeSeconds, 1);
		if (Alpha > 1) Alpha = 1;
		// Setting up ProneCollision dimensions
		ProneCollision->SetCapsuleHalfHeight(FMath::Lerp<float>(CapsuleRadius, CapsuleHalfHeight, Alpha));
		ProneCollision->SetRelativeLocation(FVector(0, 0, FMath::Lerp<float>(10, 0, Alpha)));
	}

	// Rotation modes
	switch (RotationMode)
	{
	case ERotationMode::OrientToMovement:
		OrientToMovement();
		break;

	case ERotationMode::AlwaysFaceControlRotation:
		FaceControlRotation();
		break;
	}

	// Checks whether player is falling or not by line tracing and goes to ragdoll if player was falling
	CheckFalling();

	// It will manage all of blocking obstacles by stopping the movement or adding force to the obstacle
	CheckObstaclesBlocking();
}

// It will manage all of blocking obstacles by stopping the movement or adding force to the obstacle
void UProneMovementComponent::CheckObstaclesBlocking()
{
	constexpr float BlockLength = 15;
	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Blockage", false, Owner);
	// Desired OwnerMovement Direction of Player
	FVector MovementDirection = GetLastInputVector().GetSafeNormal();
	// Trying to calculate the angle somehow
	float DotProduct = MovementDirection | Owner->GetActorForwardVector().GetSafeNormal();
	FVector StartLocation = ProneCollision->GetComponentLocation() + (MovementDirection * (FMath::Abs(DotProduct) < 0.8 ? CapsuleRadius : CapsuleHalfHeight));
	FVector EndLocation = StartLocation + (MovementDirection * BlockLength);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
	{
		if (EnablePhysicalInteraction && Hit.Component->IsSimulatingPhysics() && Hit.Component->GetClass()->IsChildOf<UPrimitiveComponent>())
		{
			if (Hit.Distance < 5)
			{
				FVector ForceDir = -Hit.ImpactNormal;
				const float ComponentSpeed = Hit.Component->GetPhysicsLinearVelocity().Size();
				if (ComponentSpeed < CurrentSpeed) // F = ma | we try to sync obstacle velocity with player's velocity
					Hit.Component->AddImpulseAtLocation(ForceDir * (CurrentSpeed - ComponentSpeed) * Hit.Component->BodyInstance.GetBodyMass(), Hit.Location);
			}
		}
		// Stopping the movement
		else
			Velocity = FVector(0),
			MaxSpeed = 0;
	}
	// Turning normal movement back on
	else
		MaxSpeed = DefaultMaxSpeed;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Red, false, -1, '\000', 1);
}

void UProneMovementComponent::Setup(UCapsuleComponent* InProneCollision)
{
	ProneCollision = InProneCollision;
}

void UProneMovementComponent::Prone()
{
	if (!CharacterMovement) return;

	if (!DefaultMaxSpeed)
		DefaultMaxSpeed = MaxSpeed;

	// Disabling Character OwnerMovement	
	CharacterMovement->Velocity = FVector(0);
	CharacterMovement->SetMovementMode(EMovementMode::MOVE_Custom);
	CharacterMovement->Deactivate();

	bDeactivate = false;
	Activate();
	OwnerRoot->SetLinearDamping(99999);
	OwnerRoot->SetSimulatePhysics(true);

	ProneCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	// Triggers prone collision size change and fixes and engine's bug
	Alpha = 0;
	ProneCollision->SetCapsuleHalfHeight(CapsuleRadius);

	bFirstSync = true;
}

void UProneMovementComponent::UnProne()
{
	ProneCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	OwnerRoot->SetLinearDamping(0.1f);
	OwnerRoot->SetSimulatePhysics(false);

	// Deactivating prone movement
	Velocity = FVector(0);
	bDeactivate = true;

	// Enabling Character OwnerMovement
	CharacterMovement->Activate();
	CharacterMovement->Velocity = FVector(0);
	CharacterMovement->SetMovementMode(EMovementMode::MOVE_Walking);
}

// Checks whether player is falling or not by line and capsule tracing and goes to ragdoll if player was falling
void UProneMovementComponent::CheckFalling()
{
	FVector OwnerLocation = Owner->GetActorLocation()
		, OwnerUpVector = Owner->GetActorUpVector().GetSafeNormal();

	FHitResult LineHit, CapsuleHit;
	FVector StartLocation = OwnerLocation + Owner->GetActorForwardVector().GetSafeNormal() * (CapsuleHalfHeight * 0.25)
		, CapsuleTraceEndLocation = StartLocation + OwnerUpVector * -15
		, LineTraceEndLocation = StartLocation + FVector::DownVector * 90;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Falling", false, Owner);
	FCollisionShape CapsuleShape = FCollisionShape::MakeCapsule(0.75 * CapsuleRadius, 0.75 * CapsuleHalfHeight);

	bIsFalling = !World->SweepSingleByChannel(CapsuleHit, StartLocation, CapsuleTraceEndLocation, ProneCollision->GetComponentQuat(), ECollisionChannel::ECC_Visibility, CapsuleShape, QueryParams);
	bool bLineTrace = World->LineTraceSingleByChannel(LineHit, StartLocation, LineTraceEndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

	if (bIsFalling)
	{
		if (!bLineTrace)
			Owner->ActivateRagdoll();
	}
	else
	{
		float DotProduct = FVector(0, 0, CapsuleHit.ImpactNormal.Z) | FVector(0, 0, 1);
		if (DotProduct < 0.4)
			Owner->ActivateRagdoll();
	}

	// Computing Gravity Offset	
	float GravityDeltaLocation = 1;
	if (!bIsFalling)
	{
		// Computing Capsule Bottom
		FVector CapsuleBottom = OwnerLocation + OwnerUpVector * -CapsuleRadius;
		// Computing Capsule Bottom And Floor Distance
		float FloorDist = CapsuleHit.Location.Z - CapsuleBottom.Z;
		// Clamping
		if (FloorDist < GravityDeltaLocation)  GravityDeltaLocation = FloorDist;

	}
	// Applying Gravity	
	FHitResult GravityHit;
	SafeMoveUpdatedComponent(FVector::DownVector * GravityDeltaLocation, UpdatedComponent->GetComponentQuat(), true, GravityHit);

	if (bDrawDebugHelpers)
	{
		DrawDebugCapsule(World, CapsuleTraceEndLocation, 0.75 * CapsuleHalfHeight, CapsuleRadius, ProneCollision->GetComponentQuat(), FColor::Red, false, -1, '\000', 2);
		DrawDebugLine(World, StartLocation, LineTraceEndLocation, FColor::Red, false, -1, '\000', 1);
	}
}

// Rotates the character towards the movement direction
void UProneMovementComponent::OrientToMovement()
{
	if (!CurrentSpeed) return;

	float DesiredYaw = Owner->GetControlRotation().Yaw;
	float CurrentYaw = Owner->GetActorRotation().Yaw;

	if (!FMath::IsNearlyEqual(CurrentYaw, DesiredYaw, 1.0f))
	{
		constexpr float TURNING_COEFFICIENT = 10;
		FRotator DesiredRotation = Owner->GetActorRotation();
		DesiredRotation.Yaw = FMath::FixedTurn(CurrentYaw, DesiredYaw, World->DeltaTimeSeconds * TURNING_COEFFICIENT * TurningBoost);

		UpdatedComponent->MoveComponent(FVector(0), DesiredRotation, true);
	}
}

// Faces control rotation of player all the time, can be use in aiming situations...
void UProneMovementComponent::FaceControlRotation()
{
	float DesiredYaw = Owner->GetControlRotation().Yaw;
	float CurrentYaw = Owner->GetActorRotation().Yaw;

	if (!FMath::IsNearlyEqual(CurrentYaw, DesiredYaw, 0.1f))
	{
		FRotator DesiredRotation = Owner->GetActorRotation();
		DesiredRotation.Yaw = DesiredYaw;

		UpdatedComponent->MoveComponent(FVector(0), DesiredRotation, true);
	}
}

void UProneMovementComponent::SynchronizeRotationWithSurface()
{
	// Default Rotation
	FRotator SurfaceRotation = Owner->GetActorRotation();

	if (!bDeactivate)
		SurfaceRotation.Roll = CalculateSurfaceRoll()
		, SurfaceRotation.Pitch = CalculateSurfacePitch();
	else
		SurfaceRotation.Roll = 0, SurfaceRotation.Pitch = 0;

	if (!Owner->GetActorRotation().Equals(SurfaceRotation, 1))
	{
		FRotator InterpolatedRot = FMath::RInterpTo(UpdatedComponent->GetComponentRotation(), SurfaceRotation, World->DeltaTimeSeconds, bDeactivate ? 2 : 5);
		UpdatedComponent->MoveComponent(FVector(0), InterpolatedRot, true);
		bFirstSync = false;
	}
	else if (bDeactivate)
		Deactivate();
}

// Returns New roll or the last calculated pitch
float UProneMovementComponent::CalculateSurfacePitch()
{
	FHitResult FHit, BHit;
	FVector StartLocation, EndLocation;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, false, Owner);
	constexpr float TraceHeightCoefficient = 4;
	// this will basically trace more towards center of capsule to make sure it's tracing the surface player's standing on!!
	constexpr float InsuranceOffset = 5;
	// in order to avoid climbing up walls and etc...
	constexpr float InsuranceAngle = 25;

	// Back Trace
	StartLocation = ProneCollision->GetComponentLocation() + (ProneCollision->GetUpVector().GetSafeNormal() * (CapsuleHalfHeight - InsuranceOffset));
	EndLocation = StartLocation + (Owner->GetActorUpVector().GetSafeNormal() * (-CapsuleRadius * TraceHeightCoefficient));
	EndLocation += Owner->GetActorForwardVector().GetSafeNormal() * -InsuranceAngle;
	// Return if not hit
	if (!World->LineTraceSingleByChannel(BHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
		return SurfacePitch;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Orange, false, -1, '\000', 1);

	// Front Trace
	StartLocation = ProneCollision->GetComponentLocation() + (-ProneCollision->GetUpVector().GetSafeNormal() * (CapsuleHalfHeight - InsuranceOffset));
	EndLocation = StartLocation + (Owner->GetActorUpVector().GetSafeNormal() * (-CapsuleRadius * TraceHeightCoefficient));
	EndLocation += Owner->GetActorForwardVector().GetSafeNormal() * InsuranceAngle;
	// Return if not hit
	if (!World->LineTraceSingleByChannel(FHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
		return SurfacePitch;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Orange, false, -1, '\000', 1.5);

	FVector GroundDirection = FHit.Location - BHit.Location;
	float NewSurfacePitch = Owner->CalculateDeltaAngle(
		GroundDirection
		, FVector(GroundDirection.X, GroundDirection.Y, 0)
		, true);

	// Check if the new surface pitch is valid...
	if (FMath::Abs(SurfacePitch - NewSurfacePitch) < 10 || bFirstSync)
		SurfacePitch = NewSurfacePitch;

	return SurfacePitch;

}

// Returns New roll or the last calculated roll
float UProneMovementComponent::CalculateSurfaceRoll()
{
	FHitResult LHit, RHit;
	FVector StartLocation, EndLocation;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, false, Owner);
	constexpr float TraceHeightCoefficient = 3;
	// this will basically trace more towards center of capsule to make sure it's tracing the surface player's standing on!!
	constexpr float InsuranceOffset = 5;
	// in order to avoid climbing up walls and etc...
	constexpr float InsuranceAngle = 10;

	// Left Trace
	StartLocation = ProneCollision->GetComponentLocation() + (-ProneCollision->GetRightVector().GetSafeNormal() * (CapsuleRadius - InsuranceOffset));
	EndLocation = StartLocation + (Owner->GetActorUpVector().GetSafeNormal() * -CapsuleRadius * TraceHeightCoefficient);
	EndLocation += Owner->GetActorRightVector() * -InsuranceAngle;
	if (!World->LineTraceSingleByChannel(LHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
		return SurfaceRoll;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Silver, false, -1, '\000', 1.5);

	// Right Trace
	StartLocation = ProneCollision->GetComponentLocation() + (ProneCollision->GetRightVector().GetSafeNormal() * (CapsuleRadius - InsuranceOffset));
	EndLocation = StartLocation + (Owner->GetActorUpVector().GetSafeNormal() * -CapsuleRadius * TraceHeightCoefficient);
	EndLocation += Owner->GetActorRightVector() * InsuranceAngle;
	if (!World->LineTraceSingleByChannel(RHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
		return SurfaceRoll;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Silver, false, -1, '\000', 1.5);

	FVector GroundDirection = LHit.Location - RHit.Location;
	return SurfaceRoll = Owner->CalculateDeltaAngle(
		GroundDirection
		, FVector(GroundDirection.X, GroundDirection.Y, 0)
		, true);
}