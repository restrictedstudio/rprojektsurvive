// Fill out your copyright notice in the Description page of Project Settings.

#include "HumanCombatComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "Components/WidgetComponent.h"
#include "Components/CapsuleComponent.h"
#include "Animation/SkeletalMeshActor.h"

#include "HumanCharacter.h"
#include "HumanDataComponent.h"
#include "HumanMovementComponent.h"
#include "HumanCameraManagerComponent.h"
#include "../UI/InventoryWidget.h"
#include "../Items/Item_Weapon.h"
#include "../Weapons/Bow.h"
#include "../Weapons/Gun.h"
#include "../Weapons/Throwable.h"
#include "../Weapons/MeleeWeapon.h"
#include "../Controllers/PController.h"
//TODO CHECK THESE GUYS USABILITY

// Sets default values for this component's properties
UHumanCombatComponent::UHumanCombatComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UHumanCombatComponent::SetAimAdjuster(float Value)
{
	// -85 is the default Yaw of the aim adjuster that works fine with most of the weapons...
	AimAdjuster->SetRelativeRotation(FRotator(0, -85 + Value, 0));
}

// Called when the game starts
void UHumanCombatComponent::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	Owner = Cast<AHumanCharacter>(GetOwner());
	OwnerData = Owner->GetData();
	OwnerMovement = Owner->GetMovement();
	OwnerCamera = Owner->GetCameraManager();
	SKMesh = Owner->GetMesh();

	SetAimAdjuster(0);

	OwnerAnimInst = SKMesh->GetAnimInstance();
	// Events	
	OwnerAnimInst->OnMontageEnded.AddDynamic(this, &UHumanCombatComponent::OnMontageEnded);
	OwnerAnimInst->OnMontageBlendingOut.AddDynamic(this, &UHumanCombatComponent::OnMontageBlendingOut);
	OwnerAnimInst->OnMontageStarted.AddDynamic(this, &UHumanCombatComponent::OnMontageStarted);

	// UnArmed Combat Collisions hit response
	RightHandCollision->OnComponentHit.AddDynamic(this, &UHumanCombatComponent::OnMeleeHit);
	LeftHandCollision->OnComponentHit.AddDynamic(this, &UHumanCombatComponent::OnMeleeHit);
	RightFootCollision->OnComponentHit.AddDynamic(this, &UHumanCombatComponent::OnMeleeHit);
	LeftFootCollision->OnComponentHit.AddDynamic(this, &UHumanCombatComponent::OnMeleeHit);

	RootComp = Owner->GetRootComponent();

	SetCombatState(ECombatState::NONE);
	AttackType = EAttackType::NONE;

	Controller = Owner->GetController<APController>();
}

void UHumanCombatComponent::SetupComponents(USceneComponent* AimAdjusterRef, UWidgetComponent* BlockedWidgetRef, UCapsuleComponent* InRightHandCollision, UCapsuleComponent* InLeftHandCollision, UCapsuleComponent* InRightFootCollision, UCapsuleComponent* InLeftFootCollision)
{
	AimAdjuster = AimAdjusterRef, BlockedWidget = BlockedWidgetRef;
	RightHandCollision = InRightHandCollision, LeftHandCollision = InLeftHandCollision;
	RightFootCollision = InRightFootCollision, LeftFootCollision = InLeftFootCollision;
}

// Called every frame
void UHumanCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CheckHits();
	CheckWeaponBlockage();
	Timer(DeltaTime);
	UpdateHitHumanPhysicsWeight();
}

void UHumanCombatComponent::Attack()
{
	bIsAttackKeyDown = true;

	if (CombatState == ECombatState::MeleeAttacking || CombatState == ECombatState::Blocked
		|| CombatState == ECombatState::ReactingToHit || OwnerMovement->IsRagdoll()) return;

	// Combat Mode	
	if (!bCombatMode)
		SetCombatMode(true);
	PauseCombatModeTimer();

	// Weapon Attack
	if (ActiveSlot && ActiveSlot->WeaponActor)
		ActiveSlot->WeaponActor->Attack();
	// Melee Attack | Punch and Kick
	else
	{
		AttackType = FindBestUnArmedAttackType();

		// Enable Melee Weapon's Collision and Hit Evenets		
		SetMeleeCollisions(true, AttackType);

		bDidCollisionHit = false;
		bDidTraceHit = false;

		//TODO play hit reaction randomly with low chance...

		OwnerMovement->StopForcedMovement();
		if (AttackType == EAttackType::RightPunch || AttackType == EAttackType::LeftPunch)
			bIsUpperBodySlot = true;
		else if (AttackType == EAttackType::RightKick || AttackType == EAttackType::LeftKick)
			bIsUpperBodySlot = false, OwnerMovement->StopMovementForcefully();

		PlayMontage();
	}
}

void UHumanCombatComponent::AttackKeyUp()
{
	bIsAttackKeyDown = false;

	ResetCombatModeTimer();

	if (ActiveSlot && ActiveSlot->WeaponActor)
		ActiveSlot->WeaponActor->OnAttackKeyRelease();
}

void UHumanCombatComponent::Reload()
{
	//if (AttackType == EAttackType::Gun)
	if (!ActiveSlot || !ActiveSlot->WeaponActor || AttackType != EAttackType::Gun) return;

	if (ActiveSlot->WeaponActor->Reload())
		OwnerMovement->PauseSprint();
}

void UHumanCombatComponent::PrimarySlot1()
{
	EquipSlot(EHotbarSlotLocation::HeavyWeapon1);
}

void UHumanCombatComponent::PrimarySlot2()
{
	EquipSlot(EHotbarSlotLocation::HeavyWeapon2);
}

void UHumanCombatComponent::SecondarySlot1()
{
	EquipSlot(EHotbarSlotLocation::LightWeapon1);
}

void UHumanCombatComponent::SecondarySlot2()
{
	EquipSlot(EHotbarSlotLocation::LightWeapon2);
}

// Cycles Between Valid Throwable Slots
void UHumanCombatComponent::ThrowableSlotsCycle()
{
	if (!OwnerData->HasThrowables())
		return;

	if (ActiveSlot && ActiveSlot->WeaponActor) // Ensuring pointers	
		if (ActiveSlot->WeaponActor->GetClass()->IsChildOf<AThrowable>())
			if (++ActiveThrowableSlot == 5)
				ActiveThrowableSlot = 1;

	switch (ActiveThrowableSlot)
	{
	case 1:
		EquipSlot(EHotbarSlotLocation::Throwable1);
		if (ActiveSlot && ActiveSlot->Item)
			break;

	case 2:
		EquipSlot(EHotbarSlotLocation::Throwable2);
		if (ActiveSlot && ActiveSlot->Item)
			break;

	case 3:
		EquipSlot(EHotbarSlotLocation::Throwable3);
		if (ActiveSlot && ActiveSlot->Item)
			break;

	case 4:
		EquipSlot(EHotbarSlotLocation::Throwable4);
		if (!ActiveSlot || !ActiveSlot->Item)
		{
			ActiveThrowableSlot = 1, ThrowableSlotsCycle();
			return;
		}
		break;
	}

	/*if (ActiveSlot && ActiveSlot->Item)
		ThrowableActor = Cast<AThrowable>(ActiveSlot->WeaponActor);*/
}

// Melee attack for guns...
void UHumanCombatComponent::MeleeAttack()
{
	if (!ActiveSlot || !ActiveSlot->WeaponActor || AttackType != EAttackType::Gun /*!ActiveSlot->WeaponActor->GetClass()->IsChildOf<AGun>()*/) return;

	if (Cast<AGun>(ActiveSlot->WeaponActor)->MeleeAttack())
		OwnerMovement->PauseSprint();
}

void UHumanCombatComponent::AimKeyPressed()
{
	bIsAimKeyDown = true;

	Aim();
}

void UHumanCombatComponent::AimKeyReleased()
{
	bIsAimKeyDown = false;

	UnAim();
}

void UHumanCombatComponent::Aim()
{
	if (CombatState == ECombatState::Reloading || !ActiveSlot || !ActiveSlot->Item)
		return;

	if (!ActiveSlot->WeaponActor->Aim()) // Weapon Aim()	
		return;

	SetCombatMode(true);
	SetCombatState(ECombatState::Aiming);
	PauseCombatModeTimer();
	Owner->DisableNeckRotation(true);
	// just to make sure player ain't running
	OwnerMovement->PauseSprint();
}

void UHumanCombatComponent::UnAim()
{
	if (!IsAiming()) return;

	ResetCombatModeTimer();

	Owner->DisableNeckRotation(false);

	if (!ActiveSlot || !ActiveSlot->Item) return;

	// Resets CombatState after a short delay
	if (CombatState != ECombatState::MovementState && CombatState != ECombatState::Reloading && !bIsWeaponBlocked)
		CombatStateResetTimer = 0;

	ActiveSlot->WeaponActor->UnAim();

	// If was not spriting go quickaiming
	if (!OwnerMovement->ResumeSprint())
		StartQuickAiming();
}

void UHumanCombatComponent::CheckAiming()
{
	if (bIsAimKeyDown)
		Aim();
}

void UHumanCombatComponent::StartQuickAiming()
{
	if (!IsUsingGun() || bIsAimKeyDown || IsAiming()) return;

	AGun* GunActor = Cast<AGun>(ActiveSlot->WeaponActor);

	bUseAimPose = true;
	PlayMontage(ECombatState::QuickAiming, GunActor->GetAnimData().AnimMontage, GunActor->GetAnimData().QuickAimingSectionName);
}

void UHumanCombatComponent::ChangeGunFireMode()
{
	if (!ActiveSlot || !ActiveSlot->WeaponActor || AttackType != EAttackType::Gun) return;

	Cast<AGun>(ActiveSlot->WeaponActor)->ChangeGunFiringMode();
}

/* Plays a hit reaction montage
@HitDirection it can be only (R,F,B,L) unless it wont work properly. */
void UHumanCombatComponent::PlayHitReactionMontage(FName HitDirection)
{
	FHitReactionAnimStruct MontageData;
	FVector MovementDirection;
	// (L for Left) or (R for Right) or (F for Forward) or (B for Backward)	
	if (HitDirection == TEXT("F"))
	{
		MontageData = FrontHitReactionAnimations[UKismetMathLibrary::RandomInteger(FrontHitReactionAnimations.Num())];
		MovementDirection = RootComp->GetForwardVector() * -1;
	}
	else if (HitDirection == TEXT("B"))
	{
		MontageData = BehindHitReactionAnimations[UKismetMathLibrary::RandomInteger(BehindHitReactionAnimations.Num())];
		MovementDirection = RootComp->GetForwardVector();
	}
	else if (HitDirection == TEXT("R"))
	{
		MontageData = RightHitReactionAnimations[UKismetMathLibrary::RandomInteger(RightHitReactionAnimations.Num())];
		MovementDirection = RootComp->GetRightVector() * -1;
	}
	else // Left on default
	{
		MontageData = LeftHitReactionAnimations[UKismetMathLibrary::RandomInteger(LeftHitReactionAnimations.Num())];
		MovementDirection = RootComp->GetRightVector();
	}

	PlayMontage(ECombatState::ReactingToHit, HitReactionMontage, MontageData.SectionName);
	OwnerMovement->MoveForDuration(MovementDirection, MontageData.MovementSpeed, MontageData.MovingDuration, false, true);
}

void UHumanCombatComponent::CancelCurrentAction()
{
	if (!ActiveSlot || !ActiveSlot->WeaponActor) return;

	switch (AttackType)
	{
	case EAttackType::Gun:
		if (CombatState == ECombatState::Reloading)
			Cast<AGun>(ActiveSlot->WeaponActor)->CancelReloading();
		break;

		// *** MOVE TO DATA COMPONENT ***
		//
	/*case EAttackType::Throwable:
		if (!bHasThrowable)
		{
			ThrowableEquipTimer = 50;
			ThrowableDelayTimer = 50;
		}
		break;*/

	case EAttackType::Bow:
		Cast<ABow>(ActiveSlot->WeaponActor)->CancelActions();
		break;
	}
}

void UHumanCombatComponent::Timer(float DeltaTime)
{
	if (CombatModeTimer < CombatModeTimeout && !OwnerCamera->IsFirstPerson())
	{
		CombatModeTimer += DeltaTime;

		// Executes once after delay ended.
		if ((CombatModeTimer >= CombatModeTimeout) && bCombatMode)
			SetCombatMode(false);
	}

	if (CombatStateResetTimer <= CombatStateResetTime)
	{
		CombatStateResetTimer += DeltaTime;

		if (CombatStateResetTimer > CombatStateResetTime)
			SetCombatState(ECombatState::NONE);
	}

	if (AttackType == EAttackType::Gun)
	{
		// First Fire Delay
		if (FireTimer < FirstFireDelay)
		{
			FireTimer += DeltaTime;

			if (FireTimer >= FirstFireDelay && FireAfterDelay)
			{
				FireAfterDelay = false;
				Cast<AGun>(ActiveSlot->WeaponActor)->Fire();
			}
		}

		// Gun Blockage
		if (WeaponBlockageTimer < WeaponBlockageCheckDelay)
			WeaponBlockageTimer += DeltaTime;

	}
	//
	//  *** THESE CODES NEED TO BE MOVED TO DATA AND INVENTORY ***
	//
	// These codes dont have any usage for now, but in the future should be moved to Human Data Component and be managed there
	// and they basically use the next throwable...
	//else if (AttackType == EAttackType::Throwable && false)
	//{
	//	if (ThrowableDelayTimer < ThrowableActor->GetNextThrowingDelay())
	//	{
	//		ThrowableDelayTimer += DeltaTime;

	//		if (ThrowableDelayTimer >= ThrowableActor->GetNextThrowingDelay())
	//		{
	//			ThrowableEquipTimer = 0;
	//			PlayMontage(ECombatState::NONE, ThrowableActor->GetAnimData().AnimMontage, ThrowableActor->GetAnimData().EquipSectionName);
	//		}
	//	}

	//	// Respawining Throwable
	//	if (ThrowableEquipTimer < ThrowableActor->GetAnimData().WeaponAttachTimeNotifier)
	//	{
	//		ThrowableEquipTimer += DeltaTime;

	//		//TODO UPDATE THIS WITH NEW INV CODE!
	//		/*if (ThrowableEquipTimer >= ThrowableActor->GetAnimData().WeaponAttachTimeNotifier)
	//			SetupThrowable(ThrowableBlueprint);*/
	//	}
	//}
	else if (AttackType == EAttackType::Bow)
		// Bow Blockage | Does nothing yet =)
		if (WeaponBlockageTimer < WeaponBlockageCheckDelay)
			WeaponBlockageTimer += DeltaTime;

}

// All of the line traces of this class	
void UHumanCombatComponent::CheckHits()
{
	if (bDidTraceHit || CombatState == ECombatState::NONE) return;
	/*
	NOTE - ObjectTypes #Notes #Info #TraceChannel #Collision

	ECC_GameTraceChannel1 = AliveCreature
	ECC_GameTraceChannel2 = Thing
	ECC_GameTraceChannel3 = MeleeWeapon
	ECC_GameTraceChannel4 = Projectile Trace
	*/

	FVector StartLocation, EndLocation;
	// Setting Start And End Locations
	switch (AttackType)
	{
	case EAttackType::RightPunch:
		StartLocation = SKMesh->GetSocketLocation(TEXT("RightHand"));
		EndLocation = StartLocation + (UKismetMathLibrary::GetRightVector(SKMesh->GetSocketRotation(TEXT("RightHand"))) * -25);
		break;

	case EAttackType::LeftPunch:
		//TODO Add this feature
		break;

	case EAttackType::RightKick:
		StartLocation = SKMesh->GetSocketLocation(TEXT("RightFoot"));
		EndLocation = StartLocation + (UKismetMathLibrary::GetRightVector(SKMesh->GetSocketRotation(TEXT("RightFoot"))) * -25);
		break;

	case EAttackType::LeftKick:
		//TODO Add this feature
		break;
	}

	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Physical Animation", true, Owner);
	FCollisionObjectQueryParams ObjectParams = FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel1 /* Alive Creature */);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Red, false, 2, '\000', 1);

	if (PhysicalAnimActiveIndexes.Num() < 100 && World->LineTraceSingleByObjectType(Hit, StartLocation, EndLocation, ObjectParams, QueryParams))
	{
		AActor* HitActor = Hit.GetActor();
		if (HitActor->GetClass()->IsChildOf<AHumanCharacter>())
		{
			AHumanCharacter* HitHuman = Cast<AHumanCharacter>(HitActor);
			HitHuman->GetCombat()->DealDamage(FDamage(UnArmedCombatDamage), TEXT("Head"));
			if (!HitHuman->GetMovement()->IsRagdoll())
			{
				PlayUnArmedEndMontage();
				ApplyPhysicalAnimation(Hit.GetActor(), HitHuman->GetMesh(), Hit.BoneName);
				SetMeleeCollisions(false, AttackType);
				bDidTraceHit = true;
			}
		}
	}

	// Setting Start and End Location of the trace
	switch (AttackType)
	{
	case EAttackType::RightKick:
		StartLocation = SKMesh->GetSocketLocation(TEXT("RightFoot")) + UKismetMathLibrary::GetUpVector(SKMesh->GetSocketRotation(TEXT("RightFoot"))) * 10;
		EndLocation = StartLocation + UKismetMathLibrary::GetRightVector(SKMesh->GetSocketRotation(TEXT("RightFoot"))) * -25 + UKismetMathLibrary::GetUpVector(SKMesh->GetSocketRotation("RightFoot")) * 10;
		break;

	case EAttackType::LeftKick:
		//TODO Add this feature
		break;
	}

	ObjectParams = FCollisionObjectQueryParams();
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldDynamic);
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldStatic);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Green, false, 2, '\000', 1);

	if (World->LineTraceSingleByObjectType(Hit, StartLocation, EndLocation, ObjectParams, QueryParams))
	{
		PlayUnArmedAttackBlockMontage();
		bDidTraceHit = true;
		SetMeleeCollisions(false, AttackType);
	}
}

// checks if gun's blocked plays montage an stuff (Projectile Based Weapons)
void UHumanCombatComponent::CheckWeaponBlockage()
{
	if (!ActiveSlot || !ActiveSlot->WeaponActor) return;

	AGun* GunActor = nullptr;
	if (AttackType == EAttackType::Gun)
	{
		GunActor = Cast<AGun>(ActiveSlot->WeaponActor);
		// Won't Check blockage on melee attacking
		if (GunActor->IsMeleeAttacking())
			return;
	}

	FHitResult Hit;
	FVector StartLocation = RootComp->GetComponentLocation() + RootComp->GetForwardVector().GetSafeNormal() * OwnerMovement->GetDefaultCapsuleSize().X
		, EndLocation = StartLocation + RootComp->GetForwardVector().GetSafeNormal() * MinAllowedRangeForUsingGun;

	if (bIsAimKeyDown && World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility) && Hit.Actor != Owner)
	{
		if (!bIsWeaponBlocked)
		{
			//UnAim();
			PauseCombatModeTimer();

			if (AttackType == EAttackType::Gun)
				PlayMontage(ECombatState::Blocked, GunActor->GetAnimData().AnimMontage, GunActor->GetAnimData().BlockedSectionName);
			else if (AttackType == EAttackType::Bow)
			{
				ABow* BowActor = Cast<ABow>(ActiveSlot->WeaponActor);
				PlayMontage(ECombatState::Blocked, BowActor->GetAnimData().AnimMontage, BowActor->GetAnimData().BlockedSectionName);
			}
		}
		bIsWeaponBlocked = true;
	}
	else if (bIsWeaponBlocked && WeaponBlockageTimer > WeaponBlockageCheckDelay)
	{		
		StopMontage();
		WeaponBlockageTimer = 0;
		bIsWeaponBlocked = false;
		ResetCombatModeTimer();
		// Getting Aim back on
		CheckAiming();
	}
}

//Interps Hit human physics blend weight to HitHumanPhysicsBlendWeightTarget all the time
void UHumanCombatComponent::UpdateHitHumanPhysicsWeight()
{
	if (!PhysicalAnimActiveIndexes.Num()) return;

	// Iterates through all of simulating physics bones
	for (int i = 0; i < PhysicalAnimActiveIndexes.Num(); i++)
	{
		if (!HitHumen[PhysicalAnimActiveIndexes[i]]->GetMovement()->IsRagdoll())
		{
			// Reverses the blend weight as soon as reached to the target!
			if (HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] != 0 && CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] == HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]])
				HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] = 0;

			if (CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] == HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]]) return;

			if (CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] < HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] + 0.03 && HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] < CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]])
				CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] = HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]];
			else if (CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] > HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] - 0.03 && HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] > CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]])
				CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] = HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]];

			// Decreasing
			if (HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] < CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]])
				CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] = FMath::FInterpTo(CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]], CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] - 1, World->DeltaTimeSeconds, 3);
			// Increasing
			else if (HitHumanPhysicsBlendWeightTargets[PhysicalAnimActiveIndexes[i]] > CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]])
				CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] = FMath::FInterpTo(CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]], CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] + 1, World->DeltaTimeSeconds, 10);

			HitSKMeshes[PhysicalAnimActiveIndexes[i]]->SetAllBodiesBelowPhysicsBlendWeight(HitBones[PhysicalAnimActiveIndexes[i]], CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]]);

			if (CurrentHitHumanPhysicsBlendWeights[PhysicalAnimActiveIndexes[i]] == 0)
			{
				// Stops physics simulation if no parent bone were simulating physics
				if (!AreBoneParentsSimulatingPhysics(HitSKMeshes[PhysicalAnimActiveIndexes[i]], HitSKMeshes[PhysicalAnimActiveIndexes[i]]->GetParentBone(HitBones[PhysicalAnimActiveIndexes[i]])))
					HitSKMeshes[PhysicalAnimActiveIndexes[i]]->SetAllBodiesBelowSimulatePhysics(HitBones[PhysicalAnimActiveIndexes[i]], false);
				// Sets the collision to query only for optimization purposes after no bones were simulating!
				if (!HitSKMeshes[PhysicalAnimActiveIndexes[i]]->IsAnySimulatingPhysics())
					HitSKMeshes[PhysicalAnimActiveIndexes[i]]->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

				// Remove the expired index from the array									
				PhysicalAnimActiveIndexes.RemoveAt(PhysicalAnimActiveIndexes.Find(PhysicalAnimActiveIndexes[i]));
			}
		}
		else
			PhysicalAnimActiveIndexes.RemoveAt(PhysicalAnimActiveIndexes.Find(PhysicalAnimActiveIndexes[i]));
	}
}

// Plays the given montage
void UHumanCombatComponent::PlayMontage(ECombatState AnimCombatState, UAnimMontage* Montage, FName SectionName, bool bIsUpperBody)
{
	if (!SectionName.IsEqual("Default") && !Montage->IsValidSectionName(SectionName))
	{
		UE_LOG(LogTemp, Error, TEXT("%s is not a valid section name."), *SectionName.ToString());
		return;
	}	

	bIsUpperBodySlot = bIsUpperBody;
	OwnerAnimInst->Montage_Play(Montage);
	if (!SectionName.IsEqual("Default"))
		OwnerAnimInst->Montage_JumpToSection(SectionName, Montage);
	PlayingMontage = Montage;
	PlayingMontageSection = SectionName;

	SetCombatState(AnimCombatState);

	// Setting the aim offsets...
	if (ActiveSlot && ActiveSlot->WeaponActor)
		if (AnimCombatState == ECombatState::Aiming)
			AimingType = AimType;
		else if (AnimCombatState == ECombatState::QuickAiming)
			AimingType = QuickAimType;
}

void UHumanCombatComponent::StopMontage(float BlendTime)
{
	// If there was any combat montages playing then return
	for (FAnimMontageInstance* Instance : OwnerAnimInst->MontageInstances)
		if (Instance->Montage && !Instance->Montage->GetGroupName().IsEqual("Movement"))
			OwnerAnimInst->Montage_Stop(BlendTime, Instance->Montage);

	PlayingMontage = nullptr;
	PlayingMontageSection = TEXT("");
	// CombatState to NONE
	CombatStateResetTimer = 0;
}

AWeapon* UHumanCombatComponent::SetupWeapon(TSubclassOf<AActor> WeaponBlueprint, EHotbarSlotLocation Slot, UItem_Weapon* Item)
{
	if (WeaponBlueprint->GetDefaultObject()->GetClass()->IsChildOf<AWeapon>())
	{
		AWeapon* WeaponActor =
			World->SpawnActor<AWeapon>(WeaponBlueprint);

		WeaponActor->SetActorLabel(Item->GetName().ToString());
		WeaponActor->AttachToComponent(SKMesh, FAttachmentTransformRules::KeepWorldTransform, WeaponHolderSockets.FindChecked(Slot));
		WeaponActor->SetActorRelativeTransform(Item->GetSocketTransform());
		WeaponActor->Setup(FPlayerData(Owner, this, Owner->GetCameraManager(), Owner->GetMovement(), OwnerAnimInst, Controller));

		return WeaponActor;
	}

	return nullptr;
}

void UHumanCombatComponent::ApplyPhysicalAnimation(AActor* HumanActor, USkeletalMeshComponent* HitHumanSKMesh, FName HitBone)
{
	if (PhysicalAnimActiveIndexes.Num() >= 100 || HitHumanSKMesh->IsSimulatingPhysics(HitBone)) return;
	// if Hips bone get physical animation i'll be super awkward so we direct it to Spine bone!
	if (HitBone == TEXT("Hips")) HitBone = TEXT("Spine");

	PhysicalAnimationFreeIndex = FindFreePhysicalAnimArrayIndex();
	PhysicalAnimActiveIndexes.Add(PhysicalAnimationFreeIndex);

	HitBones[PhysicalAnimationFreeIndex] = HitBone;
	HitSKMeshes[PhysicalAnimationFreeIndex] = HitHumanSKMesh;

	AHumanCharacter* HitHuman = Cast<AHumanCharacter>(HumanActor);
	HitHumen[PhysicalAnimationFreeIndex] = HitHuman;

	// Setting Physical Animation	
	HitHuman->GetPhysicalAnimation()->SetSkeletalMeshComponent(HitHumanSKMesh);
	HitHuman->GetPhysicalAnimation()->ApplyPhysicalAnimationProfileBelow(HitBone, TEXT("PhysicalAnim"));
	HitHumanPhysicsBlendWeightTargets[PhysicalAnimationFreeIndex] = 0.6;
	CurrentHitHumanPhysicsBlendWeights[PhysicalAnimationFreeIndex] = 0.3;
	// Preparing SKMesh for Physical Animation
	HitHumanSKMesh->SetConstraintProfile(HitBone, TEXT("PhysicalAnim"));
	HitHumanSKMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	HitHumanSKMesh->SetAllBodiesBelowSimulatePhysics(HitBone, true);

}

// Linear Damage
void UHumanCombatComponent::DealDamage(FDamage DamageData, FName HitBone)
{
	if (HitBone.IsEqual(TEXT("Head")))
		OwnerData->AddHealth(-DamageData.Head);
	else if (IsBoneChildOf(HitBone, TEXT("LeftShoulder")) || IsBoneChildOf(HitBone, TEXT("RightShoulder")))
		OwnerData->AddHealth(-DamageData.Arms);
	else if (IsBoneChildOf(HitBone, TEXT("Spine")))
		OwnerData->AddHealth(-DamageData.Body);
	else if (IsBoneChildOf(HitBone, TEXT("LeftUpLeg")) || IsBoneChildOf(HitBone, TEXT("RightUpLeg")))
		OwnerData->AddHealth(-DamageData.Legs);

	if (OwnerData->GetHealth() <= 0)
		Owner->Die();
}

// Radial Damage
void UHumanCombatComponent::DealDamage(FVector DamageOrigin, float DamageRadius, float Damage)
{
	float PlayerDistanceFromOrigin = (Owner->GetActorLocation() - DamageOrigin).Size();

	OwnerData->AddHealth(
		-Damage * ((DamageRadius - PlayerDistanceFromOrigin) / DamageRadius));

	if (OwnerData->GetHealth() <= 0)
		Owner->Die();
}

void UHumanCombatComponent::EquipSlot(EHotbarSlotLocation SlotLocation)
{
	if (!ActiveSlot || !ActiveSlot->Item) // Equiping weapon from unarmed state
	{
		ActiveSlot = OwnerData->GetHotbarSlot(SlotLocation);

		if (ActiveSlot->Item)
		{
			ActiveSlot->WeaponActor->Equip();
			Controller->GetInventoryUI()->SelectHotbarSlot(ActiveSlot->SlotLocation);
		}
	}
	else // Switching between weapons
	{
		if (!ActiveSlot->WeaponActor) return;

		// Deselecting EX-Weapon in UI
		if (ActiveSlot->SlotLocation == SlotLocation)
		{
			UnEquipActiveSlot();
			return;
		}

		// Unequiping the EX-Weapon from character
		if (ActiveSlot->Item)
			UnEquipActiveSlot();

		ActiveSlot = OwnerData->GetHotbarSlot(SlotLocation);

		// Equiping the (new) selected weapon
		if (ActiveSlot->Item)
		{
			ActiveSlot->WeaponActor->Equip();
			Controller->GetInventoryUI()->SelectHotbarSlot(ActiveSlot->SlotLocation);
		}
	}

	//TOOD Ditch this after equip animation added. cuz no longer should be needed...
	// Playing Movement Montage
	PlayMovementMontage(OwnerMovement->GetMovementState());
}

void UHumanCombatComponent::UnEquipActiveSlot()
{
	ActiveSlot->WeaponActor->UnEquip(ActiveSlot->Item, WeaponHolderSockets.FindChecked(ActiveSlot->SlotLocation));
	Controller->GetInventoryUI()->SelectHotbarSlot(EHotbarSlotLocation::NONE);
	ActiveSlot = nullptr;
}

// Finds the best unarmed attack type (punch or kick) depend on the situation of player and the world using raycast!
EAttackType UHumanCombatComponent::FindBestUnArmedAttackType()
{
	FHitResult Hit;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(Owner);
	FVector PunchTraceStartLocation, PunchTraceEndLocation, KickTraceStartLocation, KickTraceEndLocation;
	// Punch Trace FVectors
	PunchTraceStartLocation = RootComp->GetComponentLocation() + RootComp->GetForwardVector().GetSafeNormal() * 35 + RootComp->GetUpVector().GetSafeNormal() * 45;
	PunchTraceEndLocation = PunchTraceStartLocation + RootComp->GetForwardVector().GetSafeNormal() * 10;
	// Kick Trace FVectors
	KickTraceStartLocation = RootComp->GetComponentLocation() + RootComp->GetForwardVector().GetSafeNormal() * 35 + RootComp->GetUpVector().GetSafeNormal() * -45;
	KickTraceEndLocation = KickTraceStartLocation + RootComp->GetForwardVector().GetSafeNormal() * 10;

	// Adding differnet attack types so we can choose a random one while needed!
	TArray<EAttackType> DifferentAttackTypes;
	DifferentAttackTypes.Add(EAttackType::RightPunch);
	DifferentAttackTypes.Add(EAttackType::RightKick);

	FCollisionQueryParams QueryParams = FCollisionQueryParams("Punch or Kick Finder", false, Owner);
	FCollisionShape BoxShape = FCollisionShape::MakeBox(FVector(25, 25, 10));

	// Punch Trace
	bool PunchingTrace = World->SweepSingleByChannel(Hit, PunchTraceStartLocation, PunchTraceEndLocation, Owner->GetActorQuat(), ECollisionChannel::ECC_Visibility, BoxShape, QueryParams);
	if (bDrawDebugHelpers)
		DrawDebugBox(World, PunchTraceStartLocation, BoxShape.GetExtent(), FColor::Yellow, false, 2, '\000', 1);

	// Kick Trace
	BoxShape = FCollisionShape::MakeBox(FVector(60, 35, 40));
	bool KickingTrace = World->SweepSingleByChannel(Hit, KickTraceStartLocation, KickTraceEndLocation, Owner->GetActorQuat(), ECollisionChannel::ECC_Visibility, BoxShape, QueryParams);
	if (bDrawDebugHelpers)
		DrawDebugBox(World, KickTraceStartLocation, BoxShape.GetExtent(), FColor::Orange, false, 2, '\000', 1);

	// Punch Detector Trace
	if (PunchingTrace)
	{
		if ((AttackType == EAttackType::RightPunch) && KickingTrace)
			return DifferentAttackTypes[UKismetMathLibrary::RandomInteger(DifferentAttackTypes.Num())];

		//TODO add left punch aw
		return EAttackType::RightPunch;
	}
	// Kick Detector Trace
	else if (KickingTrace)
	{
		//TODO add left kick aw
		return EAttackType::RightKick;
	}

	return DifferentAttackTypes[UKismetMathLibrary::RandomInteger(DifferentAttackTypes.Num())];
}

// Manages the array and sets the CombatState variable
void UHumanCombatComponent::SetCombatState(ECombatState State)
{
	// Prevents CombatState to become NONE while it's not
	if (State != ECombatState::NONE) CombatStateResetTimer = 50;

	if (State == CombatState) return;

	// If array was hitting the maximum count
	if (CSLogsCount == CS_MAX_LOGS_COUNT)
		// Shifting the Array
		for (int i = 1; i < CS_MAX_LOGS_COUNT; i++)
			CombatStatesLog[i - 1] = CombatStatesLog[i];
	else
		++CSLogsCount;

	CombatState = State;
	CombatStatesLog[CSLogsCount - 1] = State;

	PrintCSLog();

	// Updating Rotation Mode
	if (IsAiming())
	{
		if (!bRevertRotationMode)
			OwnerMovement->SetRotationMode(ERotationMode::AlwaysFaceControlRotation)
			, bRevertRotationMode = true;
	}
	else if (bRevertRotationMode && State != ECombatState::Blocked && State != ECombatState::Reloading)
		OwnerMovement->RevertRotationMode()
		, bRevertRotationMode = false;
}

void UHumanCombatComponent::PrintCSLog()
{
	if (!bPrintCombatStatesLog) return;

	UE_LOG(LogTemp, Error, TEXT(" --- COMBAT STATES LOG --- "));

	for (int i = 0; i < CSLogsCount; i++)
		switch (CombatStatesLog[i])
		{
		case ECombatState::Aiming: UE_LOG(LogTemp, Warning, TEXT("Aiming")); break;
		case ECombatState::Blocked: UE_LOG(LogTemp, Warning, TEXT("Blocked")); break;
		case ECombatState::MeleeAttacking: UE_LOG(LogTemp, Warning, TEXT("Melee Attacking")); break;
		case ECombatState::MovementState: UE_LOG(LogTemp, Warning, TEXT("Movement State")); break;
		case ECombatState::NONE: UE_LOG(LogTemp, Warning, TEXT("NONE")); break;
		case ECombatState::QuickAiming: UE_LOG(LogTemp, Warning, TEXT("Quick Aiming")); break;
		case ECombatState::Reloading: UE_LOG(LogTemp, Warning, TEXT("Reloading")); break;
		default: UE_LOG(LogTemp, Warning, TEXT("Unknown State")) break;
		}

	UE_LOG(LogTemp, Error, TEXT(" --- LOG END --- "));
}

void UHumanCombatComponent::OnMeleeHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (CombatState != ECombatState::MeleeAttacking || bDidCollisionHit) return;

	bool bRagdollHit = false;
	if (!OtherActor->GetClass()->IsChildOf<AHumanCharacter>()) // Not Hitting Human
	{
		int ImpulseMultiplier = 10;
		if (AttackType == EAttackType::RightKick || AttackType == EAttackType::LeftKick)
			ImpulseMultiplier *= 5;

		Cast<UPrimitiveComponent>(OtherActor->GetRootComponent())->AddImpulseAtLocation(NormalImpulse.GetSafeNormal() * ImpulseMultiplier, Hit.Location);

		if (bDrawDebugHelpers)
			DrawDebugLine(World, Hit.Location, Hit.Location + (NormalImpulse.GetSafeNormal() * ImpulseMultiplier), FColor::Orange, false, 2, '\000', 2);
	}
	else
	{
		// Enemy's Combat Component
		UHumanCombatComponent* CombatComp = OtherActor->FindComponentByClass<UHumanCombatComponent>();
		// Dealing Damage
		CombatComp->DealDamage(FDamage(UnArmedCombatDamage), TEXT("Head"));
		bRagdollHit = CombatComp->IsRagdoll();
	}

	bool bIsHitObjectMovable = OtherActor->GetRootComponent()->Mobility == EComponentMobility::Movable;
	// if trace was hit and the montage end was not being played already!	
	if (!bIsHitObjectMovable && !bDidTraceHit && !bRagdollHit)
		PlayUnArmedEndMontage();

	bDidCollisionHit = true;
	SetMeleeCollisions(false, AttackType);
}

void UHumanCombatComponent::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage->GetGroupName().IsEqual("Movement")) return;

	// If there was any combat montages playing then return
	for (FAnimMontageInstance* Instance : OwnerAnimInst->MontageInstances)
		if (Instance->Montage && !Instance->Montage->GetGroupName().IsEqual("Movement")) return;

	// When the last montage is blending out			
	PlayMovementMontage(OwnerMovement->GetMovementState());
}
// Acts like all montages ended...
void UHumanCombatComponent::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage->GetGroupName().IsEqual("Movement")) return;

	// If there was any combat montages playing then return
	for (FAnimMontageInstance* Instance : OwnerAnimInst->MontageInstances)
		if (Instance->Montage && !Instance->Montage->GetGroupName().IsEqual("Movement")) return;

	if (AttackType == EAttackType::RightKick || AttackType == EAttackType::RightPunch || AttackType == EAttackType::LeftKick || AttackType == EAttackType::LeftPunch)
	{
		SetMeleeCollisions(false, AttackType);
		if (AttackType == EAttackType::RightKick || AttackType == EAttackType::LeftKick)
			OwnerMovement->StopForcedMovement();
	}

	// Starts Sprinting if player was sprinting before...
	OwnerMovement->ResumeSprint();

	// On last montage ending
	PlayMovementMontage(OwnerMovement->GetMovementState());

	if (IsAiming() || CombatState == ECombatState::MovementState) return;

	// Sets CombatState to NONE after a small delay...
	CombatStateResetTimer = 0;
}

void UHumanCombatComponent::OnMontageStarted(UAnimMontage* Montage)
{
	if (Montage->GetGroupName().IsEqual("Movement") || Montage == MovementAnimationData.AnimMontage) return;

	OwnerMovement->PauseSprint();
}

// Decides which montage to play automaticly
void UHumanCombatComponent::PlayMontage()
{
	if (AttackType == EAttackType::RightPunch) PlayMontage(ECombatState::MeleeAttacking, PunchMontage, RightPunchSectionName);
	else if (AttackType == EAttackType::LeftPunch) { /*NO CODES YET*/ }
	else if (AttackType == EAttackType::RightKick) PlayMontage(ECombatState::MeleeAttacking, KickMontage, RightKickSectionName, false);
	else if (AttackType == EAttackType::LeftKick) { /*NO CODES YET*/ }
}

/*
Finds out the direction of force after hit so that we can play the correct hit animation!
@return (L for Left) or (R for Right) or (F for Forward) or (B for Backward)
*/
FName UHumanCombatComponent::FindHitDirection(FVector HitActorForward, FVector HitImpactNormal)
{
	// In Order to compute the right angle! (XY Plane)
	HitActorForward.Z = 0;
	HitImpactNormal.Z = 0;

	// Normalizing
	HitActorForward.Normalize();
	HitImpactNormal.Normalize();

	//Gets the angle between HitActorForward & HitImpactNormal
	float DotProduct =
		FVector::DotProduct(
			HitActorForward,
			HitImpactNormal
		);

	// Forward
	if (DotProduct > 0.5)
		return TEXT("B");
	// Backward
	else if (DotProduct < -0.5)
		return TEXT("F");
	// Left/Right | Figuring out whether it's left or right		
	else if (FMath::Abs(DotProduct) <= 0.5 && FVector::CrossProduct(HitActorForward, HitImpactNormal).Z < 0)
		return TEXT("L");
	return TEXT("R");
}

// Decides which UnArmed montage end to play automaticly
void UHumanCombatComponent::PlayUnArmedEndMontage()
{
	if (AttackType == EAttackType::RightPunch) PlayMontage(ECombatState::NONE, PunchMontage, RightPunchEndSectionName);
	else if (AttackType == EAttackType::LeftPunch) { /*NO CODES YET*/ }
	else if (AttackType == EAttackType::RightKick) PlayMontage(ECombatState::NONE, KickMontage, RightKickEndSectionName);
	else if (AttackType == EAttackType::LeftKick) { /*NO CODES YET*/ }
}

// Decides which UnArmed montage block to play automaticly
void UHumanCombatComponent::PlayUnArmedAttackBlockMontage()
{
	if (AttackType == EAttackType::RightPunch) PlayMontage(ECombatState::NONE, PunchMontage, RightPunchBlockedSectionName);
	else if (AttackType == EAttackType::LeftPunch) { /*NO CODES YET*/ }
	else if (AttackType == EAttackType::RightKick) PlayMontage(ECombatState::NONE, KickMontage, RightKickBlockedSectionName);
	else if (AttackType == EAttackType::LeftKick) { /*NO CODES YET*/ }
}

// Finds the index that HitActors,HitSKMeshes and etc... are not using.
int UHumanCombatComponent::FindFreePhysicalAnimArrayIndex()
{
	for (int i = 0; i < 100; i++)
		if (!PhysicalAnimActiveIndexes.Contains(i))
			return i;
	return 0;
}

bool UHumanCombatComponent::AreBoneParentsSimulatingPhysics(USkeletalMeshComponent* SkeletonMesh, FName BoneName)
{
	if (SkeletonMesh->GetParentBone(BoneName) != TEXT("None"))
		return (bool)(FMath::Clamp<int>(
			AreBoneParentsSimulatingPhysics(SkeletonMesh, SkeletonMesh->GetParentBone(BoneName))
			+
			SkeletonMesh->IsSimulatingPhysics(BoneName), 0, 1));
	else
		return false;
}

bool UHumanCombatComponent::IsBoneChildOf(FName BoneName, FName TargetParent)
{
	if (BoneName == TargetParent)
		return true;

	FName ParentBone = SKMesh->GetParentBone(BoneName);
	if (ParentBone != TEXT("None"))
		return IsBoneChildOf(ParentBone, TargetParent);
	else
		return false;
}

bool UHumanCombatComponent::IsReloading()
{
	if (!ActiveSlot || ActiveSlot->WeaponActor) return false;

	return Cast<AGun>(ActiveSlot->WeaponActor)->IsReloading();
}

float UHumanCombatComponent::GetMinHitSpeedToGoToRagdoll()
{
	//TODO Update it
	//return Owner->GetMinHitSpeedToGoToRagdoll();
	return 99999;
}

void UHumanCombatComponent::GoRagdoll()
{
	Owner->ActivateRagdoll(true);

	/*if (ensure(MeleeWeaponActor))
		MeleeWeaponActor->SetCollision(false);*/
}

bool UHumanCombatComponent::IsRagdoll()
{
	return OwnerMovement->IsRagdoll();
}

// Checks the movment state and decides whether to play movement montage for gun or not
void UHumanCombatComponent::PlayMovementMontage(EMovementState State)
{
	if (!MovementAnimationData.AnimMontage) return;

	bool bIsMovementStateActive = (CombatState == ECombatState::MovementState);

	// Returnings
	if (CombatMovementState == State && bIsMovementStateActive || OwnerAnimInst->GetActiveMontageInstance() && !bIsMovementStateActive)	return;
	if (bIsWeaponBlocked || IsReloading() || IsAiming()) return;
	// If there was any combat montages playing then return
	for (FAnimMontageInstance* Instance : OwnerAnimInst->MontageInstances)
		if (Instance->Montage
			&& Instance->Montage != MovementAnimationData.AnimMontage
			&& !Instance->Montage->GetGroupName().IsEqual("Movement"))
			return;


	switch (State)
	{
	case EMovementState::Idle:
		if (MovementAnimationData.bOverrideIdle)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.IdleSectionName)
			, CombatMovementState = State;
		break;

	case EMovementState::Walking:
		if (MovementAnimationData.bOverrideWalking)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.WalkingSectionName)
			, CombatMovementState = State;
		break;

	case EMovementState::Jogging:
		if (MovementAnimationData.bOverrideJoging)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.JoggingSectionName)
			, CombatMovementState = State;
		break;

	case EMovementState::Running:
		if (MovementAnimationData.bOverrideRunning)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.RunningSectionName)
			, CombatMovementState = State;
		break;

	case EMovementState::InAir:
		if (MovementAnimationData.bOverrideJumping)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.JumpingSectionName)
			, CombatMovementState = State;
		break;

	case EMovementState::Falling:
		if (MovementAnimationData.bOverrideFalling)
			PlayMontage(ECombatState::MovementState, MovementAnimationData.AnimMontage, MovementAnimationData.FallingSectionName)
			, CombatMovementState = State;
		break;

	default: // Stops if unsupported movement states play...
		StopMontage(0.5);		
		break;
	}
}

// Swaps 2 weapon actors only
void UHumanCombatComponent::SwapWeapons(FHotbarSlot* Slot1, FHotbarSlot* Slot2)
{
	if (Slot1->WeaponActor)
		if (!ActiveSlot || ActiveSlot->ID != Slot1->ID)
			Slot1->WeaponActor->AttachToComponent(SKMesh, FAttachmentTransformRules::KeepRelativeTransform, WeaponHolderSockets.FindChecked(Slot2->SlotLocation));
		else
			ActiveSlot = Slot2;

	if (Slot2->WeaponActor)
		if (!ActiveSlot || ActiveSlot->ID != Slot2->ID)
			Slot2->WeaponActor->AttachToComponent(SKMesh, FAttachmentTransformRules::KeepRelativeTransform, WeaponHolderSockets.FindChecked(Slot1->SlotLocation));
		else
			ActiveSlot = Slot1;
}

void UHumanCombatComponent::SetCombatMode(bool bEnabled)
{	
	if (bEnabled)
		ResetCombatModeTimer();

	if (bCombatMode == bEnabled) return;
	bCombatMode = bEnabled;

	OwnerCamera->SetAutoPilotModeEnabled(!bCombatMode);

	if (bCombatMode)
		OwnerCamera->SetCameraFollowSpeed(30);
	else
	{
		// Reset Camera follow speed to default
		OwnerCamera->SetCameraFollowSpeed(0);
		CombatModeTimer = 50;

		UnAim();

		if (AttackType != EAttackType::Gun && AttackType != EAttackType::Throwable) return;

		if (CombatState != ECombatState::MovementState)		
			SetCombatState(ECombatState::NONE), StopMontage();					
	}
}

// Sets the widget that shows gun or any other weapons blockage by obstacle
void UHumanCombatComponent::SetBlockedWidget(bool Visibility, FVector Location)
{
	BlockedWidget->SetVisibility(Visibility);

	FRotator FaceCameraRotation = (OwnerCamera->GetViewportLocation() - Location).Rotation();
	BlockedWidget->SetWorldLocationAndRotation(Location, FaceCameraRotation);
}

void UHumanCombatComponent::ResumeCurrentMontage()
{
	OwnerAnimInst->Montage_Resume(PlayingMontage);
}

// Returns aim adjuster's pitch
float UHumanCombatComponent::GetAimAdjuster()
{
	return AimAdjuster->GetComponentRotation().Pitch;
}

void UHumanCombatComponent::ResetCombatModeTimer()
{
	if (bIsAimKeyDown || bIsAttackKeyDown) return;

	CombatModeTimer = 0;	
}

void UHumanCombatComponent::SetMeleeCollisions(bool bEnabled, EAttackType InAttackType)
{
	if (AttackType == EAttackType::RightPunch)
		RightHandCollision->SetCollisionEnabled(bEnabled ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision)
		, RightHandCollision->SetNotifyRigidBodyCollision(bEnabled);
	else if (AttackType == EAttackType::LeftPunch)
		LeftHandCollision->SetCollisionEnabled(bEnabled ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision)
		, LeftHandCollision->SetNotifyRigidBodyCollision(bEnabled);
	else if (AttackType == EAttackType::RightKick)
		RightFootCollision->SetCollisionEnabled(bEnabled ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision)
		, RightFootCollision->SetNotifyRigidBodyCollision(bEnabled);
	else if (AttackType == EAttackType::LeftKick)
		LeftFootCollision->SetCollisionEnabled(bEnabled ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision)
		, LeftFootCollision->SetNotifyRigidBodyCollision(bEnabled);
}