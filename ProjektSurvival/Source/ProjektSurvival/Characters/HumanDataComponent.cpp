// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "HumanDataComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "HumanCombatComponent.h"
#include "HumanCameraManagerComponent.h"
#include "HumanCharacter.h"
#include "HumanMovementComponent.h"
#include "../Weapons/Weapon.h"
#include "../Items/Item.h"
#include "../Items/Item_Consumable.h"
#include "../Items/Item_Wearable.h"
#include "../Items/Item_Weapon.h"
#include "../Controllers/PController.h"
#include "../UI/InventoryWidget.h"
#include "../UI/PlayerHUDWidget.h"

// Sets default values for this component's properties
UHumanDataComponent::UHumanDataComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHumanDataComponent::OnInteractPressed()
{
	bIsInteractedKeyDown = true;
	InteractedItem = nullptr;

	// Set Timer for Interation Holding Event	
	World->GetTimerManager().ClearTimer(InteractionTimer);
	World->GetTimerManager().SetTimer(InteractionTimer, this, &UHumanDataComponent::OnInteractHeld, InteractionHoldTime);
}

void UHumanDataComponent::OnInteractHeld()
{
	// Pickup System
	FHitResult ItemHit;
	FVector ItemTraceStart, LookDirection;
	Controller->GetItemTraceLine(ItemTraceStart, LookDirection);
	FVector ItemTraceEnd = ItemTraceStart + LookDirection * ItemsDetectorTraceLength;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, ItemTraceStart, ItemTraceEnd, FColor::Orange, false, 2, 0, 1);

	if (World->LineTraceSingleByChannel(ItemHit, ItemTraceStart, ItemTraceEnd, ECollisionChannel::ECC_Visibility) && ItemHit.Actor->GetClass()->IsChildOf<APhysicalItem>())
	{
		InteractedItem = Cast<APhysicalItem>(ItemHit.GetActor());

		if (InteractedItem->IsTaken()) return;

		InteractedItem->Setup(Owner, Controller);
		InteractedItem->ShowWidget();
		InteractedItem->SetIsTaken(true);

		OwnerCamera->SetUIMode(true);
		OwnerCamera->SetAutoPilotModeEnabled(false);
		OwnerMovement->StopMovementForcefully();
	}
}

void UHumanDataComponent::OnInteractReleased()
{
	World->GetTimerManager().ClearTimer(InteractionTimer);
	OwnerCamera->SetAutoPilotModeEnabled(true);
	bIsInteractedKeyDown = false;

	if (InteractedItem) // Holding
	{
		InteractedItem->HideWidget();
		OwnerMovement->StopForcedMovement();
		OwnerCamera->SetUIMode(false);
		Owner->GetCameraManager()->SetUIMode(false);
		//Owner->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
	}
	else // Release
	{
		// Pickup System
		FHitResult ItemHit;

		FVector ItemTraceStart, LookDirection;
		Controller->GetItemTraceLine(ItemTraceStart, LookDirection);
		FVector ItemTraceEnd = ItemTraceStart + LookDirection * ItemsDetectorTraceLength;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, ItemTraceStart, ItemTraceEnd, FColor::Yellow, false, 2, 0, 1);

		if (World->LineTraceSingleByChannel(ItemHit, ItemTraceStart, ItemTraceEnd, ECollisionChannel::ECC_Visibility)
			&& ItemHit.Actor->GetClass()->IsChildOf<APhysicalItem>())
		{
			APhysicalItem* PhItem = Cast<APhysicalItem>(ItemHit.GetActor());

			if (PhItem->IsTaken()) return;

			PhItem->Setup(Owner, Controller);
			PhItem->Take();
		}
	}
}

// AmountToAdd : 0 means add max possible amount to inventory
bool UHumanDataComponent::AddItemToInventoryFromProximity(int ID, int AmountToAdd)
{
	if (ID == -1) return false;

	int SlotIndex = -1;
	GetProximitySlotByID(ID, SlotIndex);
	if (SlotIndex == -1) return false;

	UItem* Item = ProximitySlots[SlotIndex].Item;

	int MaxAllowedAmount = CheckInventorySpaceForItem(Item);
	if (!MaxAllowedAmount) return false;

	APhysicalItem* PhysicalItem = ProximitySlots[SlotIndex].PhItem;
	int Amount = FMath::Clamp<int>(AmountToAdd ? AmountToAdd : PhysicalItem->GetAmount(), 1, MaxAllowedAmount);

	AddItemToInventory(Item, Amount);

	UInventoryWidget* InventoryUI = Controller->GetInventoryUI();
	// Updating the Amount of Physical Item or Destroying it
	int PhItemAmount = PhysicalItem->GetAmount() - Amount;

	if (PhItemAmount)
	{
		PhysicalItem->SetAmount(PhItemAmount);
		// Updating Proximity UI	
		if (bIsInventoryOpen)
			InventoryUI->UpdateProximitySlot(Controller, FProximitySlot(ID, Item, PhysicalItem, PhysicalItem->GetAmount()));
	}
	else
	{
		PhysicalItem->Destroy();
		// Updating Proximity UI	
		if (bIsInventoryOpen)
			InventoryUI->RemoveProximitySlot(Controller, ID);
	}

	return true;
}

bool UHumanDataComponent::AddItemToInventoryFromProximity(APhysicalItem* PhItem, int AmountToAdd)
{
	if (!PhItem) return false;

	UItem* Item = PhItem->GetItem();

	int MaxAllowedAmount = CheckInventorySpaceForItem(Item);
	if (!MaxAllowedAmount) return false;

	int Amount = FMath::Clamp<int>(AmountToAdd ? AmountToAdd : PhItem->GetAmount(), 1, MaxAllowedAmount);

	AddItemToInventory(Item, Amount);

	// Updating the Amount of Physical Item or Destroying it
	int PhItemAmount = PhItem->GetAmount() - Amount;
	if (PhItemAmount)
		PhItem->SetAmount(PhItemAmount);
	else
		PhItem->Destroy();

	return true;
}

bool UHumanDataComponent::AddItemToInventoryFromHotbar(int ID)
{
	if (ID == -1) return false;

	const int AmountToAdd = 1;

	UItem* Item = HotbarSlots[ID].Item;

	int MaxAllowedAmount = CheckInventorySpaceForItem(Item);
	if (!MaxAllowedAmount) return false;

	RemoveHotbarSlot(ID);

	int Amount = AmountToAdd;
	AddItemToInventory(Item, Amount);

	return true;
}

bool UHumanDataComponent::AddItemToInventoryFromWeared(int ID)
{
	if (ID == -1) return false;

	const int AmountToAdd = 1;

	int SlotIndex = GetWearableSlotIndex(ID);
	if (SlotIndex == -1) return false;

	UItem* Item = WearedSlots[SlotIndex].Item;

	int MaxAllowedAmount = CheckInventorySpaceForItem(Item);
	if (!MaxAllowedAmount) return false;

	RemoveWearableSlot(WearedSlots[SlotIndex].Item->GetCompatibleSlot());
	AddItemToInventory(Item, AmountToAdd);

	return true;
}

int UHumanDataComponent::FindEmptyHotbarSlot(EWeaponSlotType SlotType)
{
	for (int i = 0; i < HotbarSlotsCount; ++i)
		if (SlotType == EWeaponSlotType::HeavyWeapon)
		{
			if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::HeavyWeapon1 && !HotbarSlots[i].Item)
				return i;
			else if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::HeavyWeapon2 && !HotbarSlots[i].Item)
				return i;
		}
		else if (SlotType == EWeaponSlotType::LightWeapon)
		{
			if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::LightWeapon1 && !HotbarSlots[i].Item)
				return i;
			else if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::LightWeapon2 && !HotbarSlots[i].Item)
				return i;
		}
		else if (SlotType == EWeaponSlotType::Throwable)
		{
			if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable1 && !HotbarSlots[i].Item)
				return i;
			else if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable2 && !HotbarSlots[i].Item)
				return i;
			else if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable3 && !HotbarSlots[i].Item)
				return i;
			else if (HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable4 && !HotbarSlots[i].Item)
				return i;
		}

	return -1;
}

// checks if player is carrying any sorta throwables
bool UHumanDataComponent::HasThrowables()
{
	for (int i = 0; i < HotbarSlotsCount; ++i)
		if ((HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable1 || HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable2
			|| HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable3 || HotbarSlots[i].SlotLocation == EHotbarSlotLocation::Throwable4)
			&& HotbarSlots[i].Item)
			return true;

	return false;
}

void UHumanDataComponent::ReplaceHotbarSlot(EHotbarSlotLocation SlotLocation, FHotbarSlot HSlot)
{
	FHotbarSlot* SlotToReplace = GetHotbarSlot(SlotLocation);

	InventoryUsedCapacity -= HSlot.Item->GetCapacity();
	if (SlotToReplace->Item && !AddItemToInventoryFromHotbar(SlotToReplace->ID))
		SlotToReplace->Item->Drop(Controller, SlotToReplace->ID, ESlotType::Hotbar, 1);
	InventoryUsedCapacity += HSlot.Item->GetCapacity();

	SlotToReplace->Item = HSlot.Item;
	SlotToReplace->WeaponActor = HSlot.WeaponActor;
}

// Toggles mouse cursor and set the input mode. and also bIsInventoryOpen
void UHumanDataComponent::SetIsInventoryOpen(bool bValue)
{
	bIsInventoryOpen = bValue;

	// Setting up UI	
	Controller->bShowMouseCursor = bValue;
	Owner->GetCameraManager()->SetUIMode(bValue);

	if (bValue)
		UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(Controller, nullptr, EMouseLockMode::LockAlways, false);
	else
		UWidgetBlueprintLibrary::SetInputMode_GameOnly(Controller);
}

int UHumanDataComponent::FindInventorySlotIndexByItem(UItem* Item)
{
	for (int i = 0; i < InventorySlots.Num(); ++i)
		if (InventorySlots[i].Item == Item)
			return i;
	return -1;
}

int32 UHumanDataComponent::FindEmptyInventoryID()
{
	if (InventorySlots.Num() == 0)
		return 0;

	if (InventorySlots.Last(0).ID + 1 >= INT_MAX) // If max in exceeded then we fill out the empty indexes... | explensive cost
	{
		InventorySlots.Sort([](const FInventorySlot& a, const FInventorySlot& b) { return a.ID < b.ID; });

		for (int i = 0; i < InventorySlots.Num(); ++i)
			if (i != InventorySlots[i].ID)
				return i;
	}
	// othervise | cheap cost
	return (InventorySlots.Last(0).ID + 1);
}

// calls while adding an item and shows message if inventory had no space
int UHumanDataComponent::CheckInventorySpaceForItem(UItem* Item)
{
	// Return if didnt have capacity for even 1 amount of this item
	if (Item->GetCapacity() + InventoryUsedCapacity > InventoryMaxCapacity)
	{
		ShowMessage(FText::FromString("No inventory space for this item!"));
		return 0;
	}

	// calculating the allowed amount to add to inventory
	return (InventoryMaxCapacity - InventoryUsedCapacity) / Item->GetCapacity();
}

// DO NOT CALL IT MANUALLY, USE AddItemToInventoryFrom... functions
// Adds Item to slots, UI, updates InventoryCapacity
void UHumanDataComponent::AddItemToInventory(UItem* Item, int Amount)
{
	UInventoryWidget* InventoryUI = Controller->GetInventoryUI();

	// Updating inventory capacity
	InventoryUsedCapacity += Item->GetCapacity() * Amount;

	// figuring out whether items exists in the inventory already or not
	int FoundIndex = FindInventorySlotIndexByItem(Item);

	// Adding brand new item to inventory
	if (FoundIndex == -1)
	{
		FInventorySlot ISlot = FInventorySlot(FindEmptyInventoryID(), Item, Amount);
		InventorySlots.Add(ISlot);
		// Updating UI
		if (bIsInventoryOpen)
			InventoryUI->AddInventorySlot(this, Controller, ISlot);
	}
	// Updating Amount
	else
	{
		InventorySlots[FoundIndex].Amount += Amount;
		// Updating UI
		if (bIsInventoryOpen)
			InventoryUI->UpdateInventorySlot(this, Controller, InventorySlots[FoundIndex]);
	}
}

void UHumanDataComponent::AddProximitySlot(FProximitySlot PSlot)
{
	ProximitySlots.Add(PSlot);

	// Adding To UI	
	Controller->GetInventoryUI()->AddProximitySlot(Controller, PSlot);
}

void UHumanDataComponent::RemoveProximitySlot(int ID)
{
	int Index;
	FProximitySlot PSlot = GetProximitySlotByID(ID, Index);

	Controller->GetInventoryUI()->RemoveProximitySlot(
		Controller,
		PSlot.ID,
		true);

	ProximitySlots.RemoveAt(Index);
}

// finds an ID for proximity slots that ain't occupied.
int32 UHumanDataComponent::FindEmptyProximityID()
{
	if (ProximitySlots.Num() == 0)
		return 0;

	if (ProximitySlots.Last(0).ID + 1 >= INT_MAX) // If max in exceeded then we fill out the empty indexes... | explensive cost
	{
		ProximitySlots.Sort([](const FProximitySlot& a, const FProximitySlot& b) { return a.ID < b.ID; });

		for (int i = 0; i < ProximitySlots.Num(); ++i)
			if (i != ProximitySlots[i].ID)
				return i;
	}
	// othervise | cheap cost
	return (ProximitySlots.Last(0).ID + 1);
}

// Show/Hide Inventory
void UHumanDataComponent::ToggleShowInventory()
{
	SetIsInventoryOpen(!bIsInventoryOpen);

	UInventoryWidget* InvUI = Controller->GetInventoryUI();
	if (bIsInventoryOpen)
	{
		InvUI->LoadInventorySlots(this, Controller);

		ProximitySlots.Empty();
		InvUI->LoadProximitySlots(this, Controller);

		World->GetTimerManager().SetTimer(ProximityDetectorTimer, this, &UHumanDataComponent::UpdateProximitySlots, InventoryProximityRefreshInterval, true, InventoryProximityRefreshInterval);
	}
	else
	{
		ProximityActorsToIgnore.Empty();
		World->GetTimerManager().ClearTimer(ProximityDetectorTimer);
	}

	InvUI->ToggleInventoryView();
}

void UHumanDataComponent::UpdateInventorySlot(int ID, int AmountToAdd)
{
	// figuring out whether items exists in the inventory already or not
	int FoundIndex = -1;
	for (int i = 0; i < InventorySlots.Num(); ++i)
		if (InventorySlots[i].ID == ID)
		{
			FoundIndex = i;
			break;
		}

	if (FoundIndex == -1) return;

	// Updating Item Amount
	InventorySlots[FoundIndex].Amount += AmountToAdd;
	// Updating Inventory Capacity
	InventoryUsedCapacity += InventorySlots[FoundIndex].Item->GetCapacity() * AmountToAdd;

	// Updating UI
	if (bIsInventoryOpen)
		Controller->GetInventoryUI()->UpdateInventorySlot(this, Controller, InventorySlots[FoundIndex]);

	// Removing if amount was 0
	if (!InventorySlots[FoundIndex].Amount)
		InventorySlots.RemoveAt(FoundIndex);
}

// Updates proximity slot UI (usually use for removing or updating amount!)
void UHumanDataComponent::UpdateProximitySlot(int ID, int AmountToAdd)
{
	int Index;
	FProximitySlot PSlot = GetProximitySlotByID(ID, Index);
	if (PSlot.ID == -1) return;

	PSlot.Amount += AmountToAdd;

	if (PSlot.Amount)
	{
		PSlot.PhItem->SetAmount(PSlot.Amount);
		Controller->GetInventoryUI()->UpdateProximitySlot(Controller, PSlot);
	}
	else
	{
		PSlot.PhItem->Destroy();
		RemoveProximitySlot(ID);
	}
}

FInventorySlot UHumanDataComponent::GetInventorySlotByID(int ID)
{
	for (int i = 0; i < InventorySlots.Num(); ++i)
		if (InventorySlots[i].ID == ID)
			return InventorySlots[i];

	return FInventorySlot();
}

FProximitySlot UHumanDataComponent::GetProximitySlotByID(int ID)
{
	for (int i = 0; i < ProximitySlots.Num(); ++i)
		if (ProximitySlots[i].ID == ID)
			return ProximitySlots[i];

	return FProximitySlot();
}

FProximitySlot UHumanDataComponent::GetProximitySlotByID(int ID, int& Index)
{
	for (int i = 0; i < ProximitySlots.Num(); ++i)
		if (ProximitySlots[i].ID == ID)
		{
			Index = i;
			return ProximitySlots[i];
		}

	return FProximitySlot();
}

int UHumanDataComponent::FindPhysicalItemId(APhysicalItem* PhItem)
{
	for (int i = 0; i < ProximitySlots.Num(); ++i)
		if (ProximitySlots[i].PhItem == PhItem)
			return ProximitySlots[i].ID;

	return -1;
}

void UHumanDataComponent::AddHealth(float Amount)
{
	Health += Amount;

	if (Health > MaxHealth)
		Health = MaxHealth;
}

void UHumanDataComponent::AddHunger(float Amount)
{
	Hunger += Amount;
	Hunger = FMath::Clamp<float>(Hunger, 0, 100);
}

void UHumanDataComponent::AddThirst(float Amount)
{
	Thirst += Amount;
	Thirst = FMath::Clamp<float>(Thirst, 0, 100);
}

void UHumanDataComponent::AddStamina(float Amount)
{
	Stamina += Amount;
	Stamina = FMath::Clamp<float>(Stamina, 0, 100);

	World->GetTimerManager().ClearTimer(StaminaRecoveryTimer);
	World->GetTimerManager().SetTimer(StaminaRecoveryTimer, this, &UHumanDataComponent::StartStaminaRecovering, StaminaRecoverDelay, false);
}

// Cancels Consuming Item for now.
void UHumanDataComponent::Cancel()
{
	if (!InUsePhItem && InUseItemID == -1) return;

	// UI
	SetHint(FText());
	Controller->GetHUD()->HideWaitBar();

	// PhysicalItem
	if (InUsePhItem)
		InUsePhItem->SetIsTaken(false);
	else if (InUseItemSlotType == ESlotType::Proximity)
		GetProximitySlotByID(InUseItemID).PhItem->SetIsTaken(false);

	// Stopping the consume timer
	World->GetTimerManager().ClearTimer(InUseItemTimer);

	SetInUseItem();
}

// Shows message and hides it after the ShowTime
void UHumanDataComponent::ShowMessage(FText Text, float ShowTime)
{	
	Controller->GetHUD()->SetScreenMessage(Text);
	World->GetTimerManager().SetTimer(MessageTimer, this, &UHumanDataComponent::ClearMessage, ShowTime, false);
}

void UHumanDataComponent::SetHint(FText Text)
{
	Controller->GetHUD()->SetHint(Text);
}

void UHumanDataComponent::ShowStackFrame()
{
	if (bIsInventoryOpen)
		Controller->GetInventoryUI()->OpenStackFrame();
}

// Consumes the item after the wait time exceeded ( this function triggers by Item_Consumable class)
void UHumanDataComponent::ConsumeInUseItem()
{
	UItem* InUseItem = nullptr;

	switch (InUseItemSlotType)
	{
	case ESlotType::Inventory:
		InUseItem = GetInventorySlotByID(InUseItemID).Item;
		UpdateInventorySlot(InUseItemID, -1);
		break;

	case ESlotType::Proximity:
		InUseItem = GetProximitySlotByID(InUseItemID).Item;
		UpdateProximitySlot(InUseItemID, -1);
		break;
	}

	if (!InUseItem)
		return;

	UItem_Consumable* Item = Cast<UItem_Consumable>(InUseItem);

	// Life Qualities
	AddHunger(Item->GetHungerRecoverAmount());
	AddThirst(Item->GetThirstRecoverAmount());
	AddHealth(Item->GetHealthRecoverAmount());
	AddStamina(Item->GetStaminaRecoverAmount());
	// Widget
	SetHint(FText());
	Controller->GetHUD()->HideWaitBar();

	// Clearing InUseItems
	SetInUseItem();
	OwnerMovement->CancelActionOnMovement(false);
}

// Using item directly from physical item
void UHumanDataComponent::ConsumeInUsePhItem()
{
	InUsePhItem->SetAmount(InUsePhItem->GetAmount() - 1);
	if (!InUsePhItem->GetAmount())
		InUsePhItem->Destroy();

	UItem_Consumable* Item = Cast<UItem_Consumable>(InUsePhItem->GetItem());

	// Life Qualities
	AddHunger(Item->GetHungerRecoverAmount());
	AddThirst(Item->GetThirstRecoverAmount());
	AddHealth(Item->GetHealthRecoverAmount());
	AddStamina(Item->GetStaminaRecoverAmount());
	// Widget
	SetHint(FText());
	Controller->GetHUD()->HideWaitBar();

	// Clearing InUseItems
	SetInUseItem();
}

bool UHumanDataComponent::EquipWearableItem(FWearableSlot WSlot)
{
	int WearedSlotIndex = GetWearableSlotIndex(WSlot.Item->GetCompatibleSlot());

	if (WearedSlotIndex != -1) // player is wearing sth in the same slot		
		// Replacing the existin item
		ReplaceWearableSlot(WearedSlots[WearedSlotIndex], WSlot);
	else
		// Adding New Slot
		WearedSlots.Add(WSlot);

	Controller->GetInventoryUI()->LoadWearedSlots(this, Controller);

	return true;
}

// @return index of WearableSlots if item was weared else -1 will be returned.
int UHumanDataComponent::GetWearableSlotIndex(EWearableSlot SlotType)
{
	for (int i = 0; i < WearedSlots.Num(); ++i)
		if (WearedSlots[i].Item->GetCompatibleSlot() == SlotType)
			return i;

	return -1;
}

int UHumanDataComponent::GetWearableSlotIndex(int ID)
{
	for (int i = 0; i < WearedSlots.Num(); ++i)
		if (WearedSlots[i].ID == ID)
			return i;

	return -1;
}

int UHumanDataComponent::FindEmptyWearingID()
{
	if (WearedSlots.Num() == 0)
		return 0;

	if (WearedSlots.Last(0).ID + 1 >= INT_MAX) // If max in exceeded then we fill out the empty indexes... | explensive cost
	{
		WearedSlots.Sort([](const FWearableSlot& a, const FWearableSlot& b) { return a.ID < b.ID; });

		for (int i = 0; i < WearedSlots.Num(); ++i)
			if (i != WearedSlots[i].ID)
				return i;
	}
	// othervise | cheap cost
	return (WearedSlots.Last(0).ID + 1
		);
}

FWearableSlot UHumanDataComponent::GetWearableSlotByID(int ID)
{
	for (int i = 0; i < WearedSlots.Num(); ++i)
		if (WearedSlots[i].ID == ID)
			return WearedSlots[i];

	return FWearableSlot();
}

void UHumanDataComponent::ClearMessage()
{
	Controller->GetHUD()->SetScreenMessage(FText());
}

void UHumanDataComponent::ReplaceWearableSlot(FWearableSlot SlotToReplace, FWearableSlot Replacement)
{
	InventoryUsedCapacity -= Replacement.Item->GetCapacity();
	if (!AddItemToInventoryFromWeared(SlotToReplace.ID))
	{
		SlotToReplace.Item->Drop(Controller, SlotToReplace.ID, ESlotType::Wearables, 1);
		RemoveWearableSlot(SlotToReplace.Item->GetCompatibleSlot());
	}
	InventoryUsedCapacity += Replacement.Item->GetCapacity();
	WearedSlots.Add(Replacement);
}

bool UHumanDataComponent::EquipHotbarSlot(FHotbarSlot HSlot)
{
	UItem_Weapon* WeaponItem;
	if (HSlot.Item->GetClass()->IsChildOf<UItem_Weapon>())
		WeaponItem = Cast<UItem_Weapon>(HSlot.Item);
	else
		return false;

	int HSlotIndex = GetHotbarSlot(HSlot.SlotLocation)->ID;

	if (HSlotIndex == -1)
		return false;

	if (HotbarSlots[HSlotIndex].Item)
		// Replacing the existing slot
		ReplaceHotbarSlot(HSlot.SlotLocation, HSlot);
	else
	{
		// Setting The New Slot
		HotbarSlots[HSlotIndex].Item = HSlot.Item;
		HotbarSlots[HSlotIndex].WeaponActor = HSlot.WeaponActor;
		// ID and SlotLocation are Static and will be set on initializing of the array
	}

	Controller->GetInventoryUI()->LoadHotbarSlots(this, Controller);

	return true;
}

// Removes hotbat slot from UI and remove weapon's actor as well (unless clear only 
bool UHumanDataComponent::RemoveHotbarSlot(int ID, bool ClearOnly)
{
	if (ID == -1) return false;

	if (HotbarSlots[ID].SlotLocation == OwnerCombat->GetActiveSlot()->SlotLocation)
		OwnerCombat->UnEquipActiveSlot();

	HotbarSlots[ID].Item = nullptr;	

	if (!ClearOnly && HotbarSlots[ID].WeaponActor)
		HotbarSlots[ID].WeaponActor->Destroy();
	HotbarSlots[ID].WeaponActor = nullptr;

	Controller->GetInventoryUI()->LoadHotbarSlots(this, Controller);

	return true;
}

// Returns hotbar slot refrence by SlotLocation
FHotbarSlot* UHumanDataComponent::GetHotbarSlot(EHotbarSlotLocation SlotLocation)
{
	switch (SlotLocation)
	{
	case EHotbarSlotLocation::HeavyWeapon1: return &HotbarSlots[0];
	case EHotbarSlotLocation::HeavyWeapon2: return &HotbarSlots[1];
	case EHotbarSlotLocation::LightWeapon1: return &HotbarSlots[2];
	case EHotbarSlotLocation::LightWeapon2: return &HotbarSlots[3];
	case EHotbarSlotLocation::Throwable1: return &HotbarSlots[4];
	case EHotbarSlotLocation::Throwable2: return &HotbarSlots[5];
	case EHotbarSlotLocation::Throwable3: return &HotbarSlots[6];
	case EHotbarSlotLocation::Throwable4: return &HotbarSlots[7];

	default: return &HotbarSlots[0]; // NEVER HAPPENS!
	}
}

// Swaps 2 hotbar slots and updates the UI
bool UHumanDataComponent::SwapHotbarSlots(EHotbarSlotLocation SlotLocation, int ID)
{
	FHotbarSlot* Slot1 = GetHotbarSlot(SlotLocation)
		, * Slot2 = &HotbarSlots[ID], temp;

	if (Slot1->ID == Slot2->ID) return false;

	// Swaps the Physical Weapons Actors
	Owner->FindComponentByClass<UHumanCombatComponent>()->SwapWeapons(Slot1, Slot2);

	// Swapping Data
	temp = *Slot1;
	Slot1->Item = Slot2->Item;
	Slot1->WeaponActor = Slot2->WeaponActor;

	Slot2->Item = temp.Item;
	Slot2->WeaponActor = temp.WeaponActor;

	Controller->GetInventoryUI()->LoadHotbarSlots(this, Controller);

	return true;
}

void UHumanDataComponent::RemoveWearableSlot(EWearableSlot SlotType)
{
	int Index = GetWearableSlotIndex(SlotType);
	if (Index == -1 || !WearedSlots[Index].WearableActor) return;
	WearedSlots[Index].WearableActor->Destroy();
	WearedSlots.RemoveAt(Index);

	// Updating UI
	Controller->GetInventoryUI()->LoadWearedSlots(this, Controller);
}

// Called when the game starts
void UHumanDataComponent::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	Owner = Cast<AHumanCharacter>(GetOwner());
	OwnerMovement = Owner->GetMovement();
	OwnerCamera = Owner->GetCameraManager();
	OwnerCombat = Owner->GetCombat();
	Health = MaxHealth;

	// Our custom tick event!
	FTimerHandle TimerHandle;
	World->GetTimerManager().SetTimer(TimerHandle, this, &UHumanDataComponent::CustomTick, 0.25, true);

	Controller = Owner->GetController<APController>();
}

// Ticks every second
void UHumanDataComponent::CustomTick()
{
	// Updating Hunger
	if (Hunger > 0) Hunger -= HungerDropRate;
	else Hunger = 0; // Clamping

	// Updating Thirst
	if (Thirst > 0) Thirst -= ThirstDropRate;
	else Thirst = 0; // Clamping

	// Updating Stamina	
	if (OwnerMovement->GetMovementState() == EMovementState::Running)
		Stamina -= StaminaDropRate, bRecoverStamina = false;
	else if (!bRecoverStamina && !World->GetTimerManager().IsTimerActive(StaminaRecoveryTimer))
		World->GetTimerManager().SetTimer(StaminaRecoveryTimer, this, &UHumanDataComponent::StartStaminaRecovering, StaminaRecoverDelay, false);
	else if (bRecoverStamina)
		Stamina += StaminaIncreaseRate;
	Stamina = FMath::Clamp<float>(Stamina, 0, 100);
}

void UHumanDataComponent::DetectProximitySlots()
{
	TArray<FHitResult> OutHits;
	FVector StartLocation = Owner->GetActorLocation() + Owner->GetActorUpVector().GetSafeNormal() * -5
		, EndLocation = StartLocation + FVector::UpVector * 1;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Proximity Items Detector", false, Owner);
	QueryParams.AddIgnoredActors(ProximityActorsToIgnore);

	FCollisionShape BoxShape = FCollisionShape::MakeBox(
		FVector(ProximityDetectionRadius, ProximityDetectionRadius, OwnerMovement->GetDefaultCapsuleSize().Y));		

	if (bDrawDebugHelpers)
		DrawDebugBox(World, StartLocation, BoxShape.GetExtent(), FColor::Silver, false, 2, '\000', 1);

	// Box trace for Interactable Item
	if (World->SweepMultiByChannel(OutHits, StartLocation, EndLocation, Owner->GetActorQuat(), ECollisionChannel::ECC_GameTraceChannel7 /* Interactable Item */, BoxShape, QueryParams))
		for (int i = 0; i < OutHits.Num(); ++i)
		{
			APhysicalItem* PhItem = Cast<APhysicalItem>(OutHits[i].GetActor());
			AddProximitySlot(FProximitySlot(FindEmptyProximityID(), PhItem->GetItem(), PhItem, PhItem->GetAmount()));

			ProximityActorsToIgnore.Add(PhItem);

			if (i > MaxItemsLoadPerFrame)
			{
				World->GetTimerManager().SetTimerForNextTick(this, &UHumanDataComponent::DetectProximitySlots);
				return;
			}
		}

	// Dont need it anymore!
	ProximityActorsToIgnore.Empty();
}

void UHumanDataComponent::UpdateProximitySlots()
{
	TArray<FHitResult> OutHits;
	FVector StartLocation = Owner->GetActorLocation() + Owner->GetActorUpVector().GetSafeNormal() * -5
		, EndLocation = StartLocation + FVector::UpVector * 1;	

	TArray<APhysicalItem*> AllInRangePhItems;
	int AddedItems = 0;
	int RemovedItems = 0;

	FCollisionQueryParams QueryParams = FCollisionQueryParams("Proximity Items Updator", false, Owner);
	QueryParams.AddIgnoredActors(ProximityActorsToIgnore);

	FCollisionShape BoxShape = FCollisionShape::MakeBox(
		FVector(ProximityDetectionRadius, ProximityDetectionRadius, OwnerMovement->GetDefaultCapsuleSize().Y));

	if (bDrawDebugHelpers)
		DrawDebugBox(World, StartLocation, BoxShape.GetExtent(), FColor::Purple, false, 0.5, '\000', 1);
		
	// Box trace for Interactable Item
	if (World->SweepMultiByChannel(OutHits, StartLocation, EndLocation, Owner->GetActorQuat(), ECollisionChannel::ECC_GameTraceChannel7 /* Interactable Item */, BoxShape, QueryParams))
		for (int i = 0; i < OutHits.Num(); ++i)
		{
			APhysicalItem* PhItem = Cast<APhysicalItem>(OutHits[i].GetActor());
			AllInRangePhItems.Add(PhItem);

			bool bIsNewItem = false;
			if (FindPhysicalItemId(PhItem) == -1)
				bIsNewItem = true;

			// Adding Proximity Slot
			if (bIsNewItem)
				++AddedItems, AddProximitySlot(FProximitySlot(FindEmptyProximityID(), PhItem->GetItem(), PhItem, PhItem->GetAmount()));

			// Break to go for the next frame | Optimization purposes
			if (AddedItems > MaxItemsLoadPerFrame) break;
		}

	UInventoryWidget* InventoryUI = Controller->GetInventoryUI();
	// Removing Gone Items From Proximity Slots UI
	for (int i = ProximitySlots.Num() - 1; i > -1; --i)
	{
		APhysicalItem* PhItem = ProximitySlots[i].PhItem;
		if (!AllInRangePhItems.Contains(PhItem))
		{
			InventoryUI->RemoveProximitySlot(
				Controller,
				ProximitySlots[i].ID,
				false);

			ProximitySlots.RemoveAt(i);

			++RemovedItems;
		}

		// Break to go for the next frame | Optimization purposes
		if (RemovedItems > MaxItemsLoadPerFrame) break;
	}

	if (RemovedItems)
		InventoryUI->SortProximitySlots();

	if (RemovedItems > MaxItemsLoadPerFrame || AddedItems > MaxItemsLoadPerFrame)
		World->GetTimerManager().SetTimerForNextTick(this, &UHumanDataComponent::UpdateProximitySlots);
}


FTransform UHumanDataComponent::FindBestDropLocation()
{
	if (!Owner) return FTransform();

	FHitResult Hit;
	FVector StartLocation;
	FVector EndLocation;
	float TraceHeight = 120;
	float LevelRadius = 50;

	// Check Forward Hit Trace
	for (int i = 1; i < 3; ++i)
	{
		FVector OwnerLocation = Owner->GetActorLocation()
			, OwnerForward = Owner->GetActorForwardVector().GetSafeNormal()
			, OwnerRight = Owner->GetActorRightVector().GetSafeNormal()
			, OwnerUp = Owner->GetActorUpVector().GetSafeNormal();

		// Checking If anything is in front of player like walls or something...
		StartLocation = OwnerLocation;
		EndLocation = StartLocation + OwnerForward * (LevelRadius * i);		
	
		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
		{
			Hit = FHitResult();
			break;
		}

		// Forward
		StartLocation = OwnerLocation + OwnerForward * (LevelRadius * i);
		EndLocation = StartLocation - OwnerUp * TraceHeight;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
			if (Hit.Actor->GetRootComponent()->GetCollisionObjectType() == ECollisionChannel::ECC_WorldStatic || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
				break;

		// Right + Forward
		StartLocation = OwnerLocation + (OwnerForward + OwnerRight).GetSafeNormal() * (LevelRadius * i);
		EndLocation = StartLocation - OwnerUp * TraceHeight;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
			if (Hit.Actor->GetRootComponent()->GetCollisionObjectType() == ECollisionChannel::ECC_WorldStatic || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
				break;

		// Left + Forward
		StartLocation = OwnerLocation + (OwnerForward - OwnerRight).GetSafeNormal() * (LevelRadius * i);
		EndLocation = StartLocation - OwnerUp * TraceHeight;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
			if (Hit.Actor->GetRootComponent()->GetCollisionObjectType() == ECollisionChannel::ECC_WorldStatic || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
				break;

		// Right
		StartLocation = OwnerLocation + OwnerRight * (LevelRadius * i);
		EndLocation = StartLocation - OwnerUp * TraceHeight;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
			if (Hit.Actor->GetRootComponent()->GetCollisionObjectType() == ECollisionChannel::ECC_WorldStatic || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
				break;

		// Left
		StartLocation = OwnerLocation - OwnerRight * (LevelRadius * i);
		EndLocation = StartLocation - OwnerUp * TraceHeight;

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);

		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
			if (Hit.Actor->GetRootComponent()->GetCollisionObjectType() == ECollisionChannel::ECC_WorldStatic || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
				break;

		// Reseting the hit if the hits were not valid (if they were valid this line of code would have never executed!)
		Hit = FHitResult();
	}

	if (!Hit.bBlockingHit)
	{
		StartLocation = Owner->GetActorLocation() + Owner->GetActorForwardVector().GetSafeNormal() * OwnerMovement->GetDefaultCapsuleSize().X;
		EndLocation = StartLocation - Owner->GetActorUpVector().GetSafeNormal() * TraceHeight;
		World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility);

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::MakeRandomColor(), false, 3, 0, 1);
	}

	return FTransform(Hit.Normal.RightVector.Rotation(), Hit.Location, FVector(1));
}