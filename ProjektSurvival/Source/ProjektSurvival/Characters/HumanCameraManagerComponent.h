// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HumanCameraManagerComponent.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UCameraShake;

enum class ECameraShakeType {
	Running,
	Hardlanding,
	Falling,
	Dodging,
	All
};

USTRUCT()
struct FAutoRotation
{
	GENERATED_BODY()

public:
	bool bIsRotatingToDesiredRotation;
	FRotator DesiredRotation;
	// X is Yaw Speed and Y is Pitch Rotation Rate.
	FVector2D RotationRate;
	bool* bEndIndicator;

	FAutoRotation()
	{
		bIsRotatingToDesiredRotation = false;
		DesiredRotation = FRotator();
		RotationRate = FVector2D();
		bEndIndicator = nullptr;
	}

	FAutoRotation(FRotator InDesiredRotation, FVector2D InRotationRate = FVector2D(2.5), bool* bInEndIndicator = nullptr)
	{
		bIsRotatingToDesiredRotation = true;
		DesiredRotation = InDesiredRotation;
		RotationRate = InRotationRate;
		bEndIndicator = bInEndIndicator;
	}
};

//TODO add descryption
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), hideCategories = ("Variable", "Sockets", "Component Replication", "Activation", "Cooking", "Events", "Asset User Data", "Collision"))
class PROJEKTSURVIVAL_API UHumanCameraManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHumanCameraManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Third person camera default height
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float CameraDefaultHeight = 50;
	// The interp speed that camera would follows the character
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float DefaultCameraFollowSpeed = 1.5;

	// Camera auto following mode switch interval
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Auto Pilot", meta = (ClampMin = "0"))
		float AutoPilotTriggerTime = 3;
	// Default pitch while in auto pilot mode
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Auto Pilot", meta = (ClampMin = "0"))
		FVector2D AutoPilotRotationRate = FVector2D(0.75, 0.25);
	// Default pitch while in auto pilot mode
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Auto Pilot", meta = (ClampMin = "-90", ClampMax = "90"))
		float AutoPilotDefaultPitch = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera Shakes")
		TSubclassOf<UCameraShake> CameraShakeOfRunning;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera Shakes")
		TSubclassOf<UCameraShake> CameraShakeOfFalling;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera Shakes")
		TSubclassOf<UCameraShake> CameraShakeOfHardLanding;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Camera Shakes")
		TSubclassOf<UCameraShake> CameraShakeOfDodging;

	// Total number of possible viewports
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Viewport")
		int ViewportsNum = 3;
	// Distance between each viewport
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Viewport")
		float ViewportsDelta = 75;

	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds")
		float Min3rdPCameraPitch = -85;
	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds")
		float Max3rdPCameraPitch = 50;
	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds")
		float Max1stPCameraPitch = 75;
	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds")
		float Min1stPCameraPitch = -75;
	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds")
		float Max1stPCameraYaw = 75;

	// Camera Limitation
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Bounds|Aiming")
		float Min3rdPCameraAimingPitch = -70;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ChangeViewport();

	void ToggleLookBehind();

	//Freelook
	void ActivateFreelook();
	void DeactivateFreelook();
	bool IsFreelooking() { return bIsFreelooking; }

	//Camera Navigation Functions which binds to mouse
	/**/
	void TwistCamera(float Value);
	void ElevateCamera(float Value);

	void SetCameras(USpringArmComponent* InTPSpringArm, UCameraComponent* InTPCamera, UCameraComponent* InFPCamera);

	int GetCameraDeltaAngle() { return CameraDeltaAngle; }

	// Rotates camera to the desired rotation
	// @bEndIndicator will be set to false after rotation ended
	// @CAUTION Roll of the rotation will be ignored!	
	void RotateTo(FAutoRotation DesiredRotation);

	void PlayCameraShake(ECameraShakeType ShakeType);
	void StopCameraShake(ECameraShakeType ShakeType);

	void SetAutoPilotModeEnabled(bool bEnabled) { bIsAutoPilotEnabled = bEnabled; }

	// 0 resets camera follow speed to default
	void SetCameraFollowSpeed(float FollowingSpeed) { CameraFollowSpeed = FollowingSpeed ? FollowingSpeed : DefaultCameraFollowSpeed; }

	// Lock/Unlock Camera Rotation
	void SetLockCameraRotation(bool bValue) { bLockCamera = bValue; }
	void SetUIMode(bool bValue) { bUIMode = bValue, SetLockCameraRotation(bValue); }
	bool IsUIMode() { return bUIMode; }

	// can be TPSpringArm or FPCamera
	USceneComponent* GetActiveCamera() { return ActiveCamera; }
	// @return Active camera location
	FVector GetViewportLocation();

	//TODO Encapsulate
	float MouseYValue = 0;

	UFUNCTION(BlueprintPure, Category = "Viewport")
		bool IsFirstPerson() { return false; }

	bool bUpdateAimPitch = true;
	UFUNCTION(BlueprintPure, Category = "Aiming")
		float GetAimingPitch() { return AimingPitch; }
	// Adds Custom Pitch to Camera
	void AddCamOffset(float Value);

	void SetFPCameraDriver(bool bEnabled, USceneComponent* DriverComponent = nullptr) { FPCameraDriver = DriverComponent, bIsFPCameraDriverActive = bEnabled; }

private:

	class UWorld* World = nullptr;
	class AHumanCharacter* Owner = nullptr;
	class UHumanMovementComponent* OwnerMovement = nullptr;
	class UHumanCombatComponent* OwnerCombat = nullptr;
	class APController* Controller = nullptr;
	class APlayerCameraManager* CameraManager = nullptr;

	// Syncs camera with desired rotation from RotateTo()
	void RotateToDesiredSynchronizer(float DeltaTime);

	// Third person camera boom
	USpringArmComponent* TPSpringArm = nullptr;
	// Syncs TP SpringArm Location with Character Location via Vector Interpolation
	void SyncTPCameraLocationWithCharacter(float DeltaTime);
	float TPSpringArmDefaultArmLength;

	// Third person camera
	UCameraComponent* TPCamera = nullptr;

	// First person camera
	UCameraComponent* FPCamera = nullptr;

	// Current Active Camera
	USceneComponent* ActiveCamera;

	float CameraFollowSpeed;

	void StopAutoRotation();
	void OnAutoRotationEnded();
	FAutoRotation AutoRotation = FAutoRotation();

	// Camera Shakers
	UCameraShake* RunningCameraShake = nullptr;
	UCameraShake* FallingCameraShake = nullptr;

	float CameraPitch = 0;

	// The pitch that AimOffsets would use!	
	float AimingPitch = 0;
	void UpdateAimingPitch(float DeltaTime);

	// First Person Camera Driver for Aiming.
	void UpdateFPCameraDriver();
	// Component
	USceneComponent* FPCameraDriver;
	FVector FPCameraDefaultLocation;
	bool bIsFPCameraDriverActive = false;
	// Use for Lerps
	float FPCameraDriverAlpha = 0;

	// Current Viewport
	int Viewport = 1;

	bool bIsLookingBehind = false;

	bool bIsFreelooking = false;
	float FreelookStartYaw = 0;	

	bool bLockCamera = false;
	bool bUIMode = false;

	void Timer();
	FTimerHandle MainTimer;
	float MainTimerRefreshInterval = 0.5;

	// Toggles the camera to auto pilot after timer ended.
	float CameraAutoPilotTimer = 50;
	bool bIsAutoPilotEnabled = true;
	// Manages Auto pilot related stuff
	void AutoPilotManager();

	// The angle between Camera and Character
	int CameraDeltaAngle = 0;

};
