// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "HumanCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/WidgetComponent.h"

#include "HumanMovementComponent.h"
#include "HumanCameraManagerComponent.h"
#include "HumanDataComponent.h"
#include "HumanCombatComponent.h"
#include "ProneMovementComponent.h"
//#include "../Controllers/PController.h"
#include "../Items/PhysicalItem.h"


// Sets default values
AHumanCharacter::AHumanCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Cast<UPrimitiveComponent>(RootComponent)->SetAngularDamping(99999);

	InitializeProneMovement();

	MovementComp = CreateDefaultSubobject<UHumanMovementComponent>(TEXT("MovementComp"));
	bUseControllerRotationYaw = false;	

	DataComp = CreateDefaultSubobject<UHumanDataComponent>(TEXT("DataComp"));

	// --- COMBAT ----
	SetupCombat();

	PhysicalAnimation = CreateDefaultSubobject<UPhysicalAnimationComponent>(FName(TEXT("PhysicalAnimation")));
	PhysicalAnimation->StrengthMultiplyer = 4;
	PhysicalAnimation->bAutoActivate = true;

	SetupCamera();
}

// Called when the game starts or when spawned
void AHumanCharacter::BeginPlay()
{
	Super::BeginPlay();	

	// Inialisations	
	World = GetWorld();

	// Possesing AI
	if (!Controller)
	{
		CameraManagerComp->Deactivate();
		CameraManagerComp->SetComponentTickEnabled(false);
		SpawnDefaultController();		
	}
}

void AHumanCharacter::Falling()
{
	MovementComp->OnFalling();
}

void AHumanCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);

	MovementComp->OnLanded(Hit);
}

// Called every frame
void AHumanCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!MovementComp) return;

	// Updates neck rotation to where player is actually looking at
	SetNeckRotation();

	// Updates waist (spine bone) rotation depend on the control rotation and movement direction
	SetWaistRotation();

	// Blink | Sets eyse morph target value
	Blink();
}

// Called to bind functionality to input
void AHumanCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement
	PlayerInputComponent->BindAxis<UHumanMovementComponent>(
		TEXT("MoveForward"), MovementComp, &UHumanMovementComponent::MoveForward);
	PlayerInputComponent->BindAxis<UHumanMovementComponent>(
		TEXT("MoveRight"), MovementComp, &UHumanMovementComponent::MoveRight);
	PlayerInputComponent->BindAxis<UHumanMovementComponent>(
		TEXT("MoveForward"), MovementComp, &UHumanMovementComponent::MoveForward);
	PlayerInputComponent->BindAxis<UHumanMovementComponent>(
		TEXT("MoveRight"), MovementComp, &UHumanMovementComponent::MoveRight);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("WalkModeToggle"), IE_Pressed, MovementComp, &UHumanMovementComponent::ToggleWalkMode);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Sprint"), IE_Pressed, MovementComp, &UHumanMovementComponent::Sprint);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Sprint"), IE_Released, MovementComp, &UHumanMovementComponent::UnSprint);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Crouch"), IE_Pressed, MovementComp, &UHumanMovementComponent::CrouchPressed);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Crouch"), IE_Released, MovementComp, &UHumanMovementComponent::CrouchReleased);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Prone"), IE_Pressed, MovementComp, &UHumanMovementComponent::PronePressed);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Jump"), IE_Pressed, MovementComp, &UHumanMovementComponent::Jump);
	PlayerInputComponent->BindAction(
		TEXT("Jump"), IE_Released, this, &AHumanCharacter::StopJumping);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("Ragdoll"), IE_Pressed, MovementComp, &UHumanMovementComponent::ToggleRagdoll);
	PlayerInputComponent->BindAction<UHumanMovementComponent>(
		TEXT("AutoMove"), IE_Pressed, MovementComp, &UHumanMovementComponent::ToggleAutoRun);

	//Camera
	PlayerInputComponent->BindAxis<UHumanCameraManagerComponent>(
		TEXT("ElevateCamera"), CameraManagerComp, &UHumanCameraManagerComponent::ElevateCamera);
	PlayerInputComponent->BindAxis<UHumanCameraManagerComponent>(
		TEXT("TwistCamera"), CameraManagerComp, &UHumanCameraManagerComponent::TwistCamera);
	PlayerInputComponent->BindAction<UHumanCameraManagerComponent>(
		TEXT("ChangeViewport"), EInputEvent::IE_Pressed, CameraManagerComp, &UHumanCameraManagerComponent::ChangeViewport);
	PlayerInputComponent->BindAction<UHumanCameraManagerComponent>( // Pressed
		TEXT("LookBehind"), EInputEvent::IE_Pressed, CameraManagerComp, &UHumanCameraManagerComponent::ToggleLookBehind);
	PlayerInputComponent->BindAction<UHumanCameraManagerComponent>( // Released
		TEXT("LookBehind"), EInputEvent::IE_Released, CameraManagerComp, &UHumanCameraManagerComponent::ToggleLookBehind);
	PlayerInputComponent->BindAction<UHumanCameraManagerComponent>(
		TEXT("FreeLook"), IE_Pressed, CameraManagerComp, &UHumanCameraManagerComponent::ActivateFreelook);
	PlayerInputComponent->BindAction<UHumanCameraManagerComponent>(
		TEXT("FreeLook"), IE_Released, CameraManagerComp, &UHumanCameraManagerComponent::DeactivateFreelook);

	// Data
	PlayerInputComponent->BindAction<UHumanDataComponent> // Pressed
		(TEXT("Interact"), IE_Pressed, DataComp, &UHumanDataComponent::OnInteractPressed);
	PlayerInputComponent->BindAction<UHumanDataComponent> // Released
		(TEXT("Interact"), IE_Released, DataComp, &UHumanDataComponent::OnInteractReleased);
	PlayerInputComponent->BindAction<UHumanDataComponent>
		(TEXT("Cancel"), IE_Pressed, DataComp, &UHumanDataComponent::Cancel);
	PlayerInputComponent->BindAction<UHumanDataComponent>
		(TEXT("ToggleInventory"), IE_Pressed, DataComp, &UHumanDataComponent::ToggleShowInventory);
	PlayerInputComponent->BindAction<UHumanDataComponent>
		(TEXT("Stack"), IE_Pressed, DataComp, &UHumanDataComponent::ShowStackFrame);

	// Combat
	PlayerInputComponent->BindAction
	(TEXT("Attack"), IE_Pressed, this, &AHumanCharacter::AttackKeyPressed);
	PlayerInputComponent->BindAction
	(TEXT("Attack"), IE_Released, this, &AHumanCharacter::AttackKeyReleased);
	PlayerInputComponent->BindAction
	(TEXT("Aim"), IE_Pressed, this, &AHumanCharacter::AimKeyPressed);
	PlayerInputComponent->BindAction
	(TEXT("Aim"), IE_Released, this, &AHumanCharacter::AimKeyReleased);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("Reload"), IE_Pressed, CombatComp, &UHumanCombatComponent::Reload);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("MeleeAttack"), IE_Pressed, CombatComp, &UHumanCombatComponent::MeleeAttack);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("ChangeGunFiringMode"), IE_Pressed, CombatComp, &UHumanCombatComponent::ChangeGunFireMode);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("PrimarySlot1"), IE_Pressed, CombatComp, &UHumanCombatComponent::PrimarySlot1);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("PrimarySlot2"), IE_Pressed, CombatComp, &UHumanCombatComponent::PrimarySlot2);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("SecondarySlot1"), IE_Pressed, CombatComp, &UHumanCombatComponent::SecondarySlot1);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("SecondarySlot2"), IE_Pressed, CombatComp, &UHumanCombatComponent::SecondarySlot2);
	PlayerInputComponent->BindAction<UHumanCombatComponent>
		(TEXT("ThrowableSlotsCycle"), IE_Pressed, CombatComp, &UHumanCombatComponent::ThrowableSlotsCycle);
}

void AHumanCharacter::SetupCombat()
{
	CombatComp = CreateDefaultSubobject<UHumanCombatComponent>(TEXT("CombatComp"));
	// -- Aim Adjuster
	AimAdjuster = CreateDefaultSubobject<USceneComponent>(FName(TEXT("AimAdjuster")));
	// Right hand : the hand which will hold the weapon
	AimAdjuster->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("RightHand"));
	// -- Blcokage UI Widget for waepons
	BlockedWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Blocked Widget"));
	BlockedWidget->SetCollisionProfileName(FName(TEXT("NoCollision")));
	BlockedWidget->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	BlockedWidget->SetWidgetSpace(EWidgetSpace::World);
	BlockedWidget->SetVisibility(false);
	// -- Melee Collisions
	// Hands Collision
	RightHandCollision = CreateDefaultSubobject<UCapsuleComponent>(FName(TEXT("RightHandCollision")));
	RightHandCollision->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("RightHand"));
	RightHandCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	RightHandCollision->SetGenerateOverlapEvents(false);
	LeftHandCollision = CreateDefaultSubobject<UCapsuleComponent>(FName(TEXT("LeftHandCollision")));
	LeftHandCollision->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("LeftHand"));
	LeftHandCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	LeftHandCollision->SetGenerateOverlapEvents(false);
	// Feet Collision
	RightFootCollision = CreateDefaultSubobject<UCapsuleComponent>(FName(TEXT("RightFootCollision")));
	RightFootCollision->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("RightFoot"));
	RightFootCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	RightFootCollision->SetGenerateOverlapEvents(false);
	LeftFootCollision = CreateDefaultSubobject<UCapsuleComponent>(FName(TEXT("LeftFootCollision")));
	LeftFootCollision->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("LeftFoot"));
	LeftFootCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	LeftFootCollision->SetGenerateOverlapEvents(false);
	// Setting References
	CombatComp->SetupComponents(AimAdjuster, BlockedWidget, RightHandCollision, LeftHandCollision, RightFootCollision, LeftFootCollision);
}

void AHumanCharacter::SetupCamera()
{
	// Camera Boom setup
	TPSpringArm = CreateDefaultSubobject<USpringArmComponent>(FName(TEXT("SpringArm")));
	TPSpringArm->bUsePawnControlRotation = true;
	TPSpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	TPSpringArm->TargetArmLength = 120;
	TPSpringArm->bInheritRoll = false, TPSpringArm->bInheritPitch = false, TPSpringArm->bInheritYaw = false;
	TPSpringArm->SetRelativeRotation(FRotator(-30, 0, 0));
	//TPSpringArm->SetRelativeLocation(FVector(0, 0, 50));

	// Third Person Camera	
	TPCamera = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("3rdPersonCamera")));
	TPCamera->AttachToComponent(TPSpringArm, FAttachmentTransformRules::KeepRelativeTransform);
	TPCamera->SetRelativeLocation(FVector(0, -35, 0));
	TPCamera->SetRelativeScale3D(FVector(0.7));

	// First Person Camera
	FPCamera = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("1stPersonCamera")));
	if (GetMesh())
		FPCamera->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("Camera"));
	FPCamera->SetAbsolute(false, true, false);
	FPCamera->SetRelativeScale3D(FVector(0.5));

	CameraManagerComp = CreateDefaultSubobject<UHumanCameraManagerComponent>(TEXT("CameraManagerComp"));
	CameraManagerComp->SetCameras(TPSpringArm, TPCamera, FPCamera);
	CameraManagerComp->SetComponentTickEnabled(true);
}


void AHumanCharacter::InitializeProneMovement()
{
	ProneCollision = CreateDefaultSubobject<UCapsuleComponent>(FName(TEXT("ProneCollision")));
	ProneCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	// Setting Prone collision default values
	ProneCollision->SetCollisionProfileName(TEXT("Pawn"));
	ProneCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProneCollision->SetNotifyRigidBodyCollision(false);
	ProneCollision->SetGenerateOverlapEvents(false);
	// Human Capsule Dimensions
	ProneCollision->SetCapsuleHalfHeight(88);
	ProneCollision->SetCapsuleRadius(34);
	ProneCollision->SetRelativeLocationAndRotation(FVector(0, 0, 0), FRotator(90, 0, 0));

	ProneMovementCmp = CreateDefaultSubobject<UProneMovementComponent>(FName(TEXT("ProneMovementCmp")));
	ProneMovementCmp->Setup(ProneCollision);
}

void AHumanCharacter::SetNeckRotation()
{
	if (bDisableNeckRotation)
	{
		NeckRotation.Pitch =
			FMath::FInterpTo(NeckRotation.Pitch, 0, World->GetDeltaSeconds(), NeckRotationInterpolatinSpeed);
		NeckRotation.Yaw =
			FMath::FInterpTo(NeckRotation.Yaw, 0, World->GetDeltaSeconds(), NeckRotationInterpolatinSpeed);
		NeckRotation.Roll =
			FMath::FInterpTo(NeckRotation.Roll, 0, World->GetDeltaSeconds(), NeckRotationInterpolatinSpeed);
	}
	else
	{
		const FRotator LookingRotation = TPCamera->GetComponentRotation();
		//GetMesh()->GetBoneQuaternion(TEXT("Hips")).GetUpVector()
		const float NeckYaw = CalculateDeltaAngle(GetActorForwardVector(), LookingRotation.Vector());
		const float NeckPitch = LookingRotation.Pitch - (LookingRotation.Pitch > 180 ? 360 : 0);

		float AngleDamper = 1;
		switch (MovementComp->GetMovementState())
		{
		case EMovementState::Crouching:
			AngleDamper = 0.75;
			break;
		case EMovementState::Proning:
			AngleDamper = 0.25;
			break;
		}
		/*else if (HumanCombatComponent->GetCombatMode())
			AngleDamper = 0.75;*/

		float InterpDamper = 1;
		if (FMath::Abs(NeckYaw) > 150)
			InterpDamper = 0.25;

		// Vertical Rotation
		const float PitchTarget = FMath::Clamp<float>(NeckPitch + 20, -MaxNeckRotaionPitchAngle,
			MaxNeckRotaionPitchAngle * AngleDamper);
		// Horizental Rotation
		const float YawTarget = FMath::Clamp<float>(NeckYaw, -MaxNeckRotaionYawAngle * AngleDamper,
			MaxNeckRotaionYawAngle * AngleDamper);

		NeckRotation.Pitch =
			FMath::FInterpTo(NeckRotation.Pitch, PitchTarget, World->GetDeltaSeconds(),
				NeckRotationInterpolatinSpeed * InterpDamper);
		NeckRotation.Yaw =
			FMath::FInterpTo(NeckRotation.Yaw, YawTarget, World->GetDeltaSeconds(),
				NeckRotationInterpolatinSpeed * InterpDamper);
		NeckRotation.Roll = 0;
		/*FMath::FInterpTo(NeckRotation.Roll, HumanCombatComponent->IsAiming() ? 15 : 0, World->GetDeltaSeconds(), NeckRotationInterpolatinSpeed);*/
	}
}

void AHumanCharacter::Blink()
{
	// Timer
	if (ElapsedTimeSinceLastBlink <= BlinkingInterval)
	{
		ElapsedTimeSinceLastBlink += World->DeltaTimeSeconds;
		return;
	}

	// Closes Eyes while character's in ragdoll mode
	bool bIsRagdoll = MovementComp->IsRagdoll(), bDirtyFlag = false;

	if (bAreEyesOpen || bIsRagdoll) // Closing Eyes
	{
		if (BlinkValue != 1)
			BlinkValue = FMath::Clamp<float>(FMath::FInterpTo(BlinkValue, BlinkValue + 5, World->DeltaTimeSeconds, 2.5), 0, 1)
			, bDirtyFlag = true;
		else if (!bIsRagdoll)
			bAreEyesOpen = false;
	}
	else // Opening Eyes
	{
		if (BlinkValue != 0)
			BlinkValue = FMath::Clamp<float>(FMath::FInterpTo(BlinkValue, BlinkValue - 5, World->DeltaTimeSeconds, 2.5), 0, 1)
			, bDirtyFlag = true;
		else
		{
			bAreEyesOpen = true;
			ElapsedTimeSinceLastBlink = 0;
			BlinkingInterval = FMath::FRandRange(1, MaxBlinkingInterval + 0.5);
		}
	}

	if (bDirtyFlag)
		GetMesh()->SetMorphTarget(TEXT("EyeLashes"), BlinkValue),
		GetMesh()->SetMorphTarget(TEXT("Eyes"), BlinkValue);
}

void AHumanCharacter::SetWaistRotation()
{
	constexpr float INTERP_SPEED = 4;

	if (bDisableWaistRotation)
	{
		WaistRotation.Yaw =
			FMath::FInterpTo(WaistRotation.Yaw, 0, World->GetDeltaSeconds(), INTERP_SPEED);
		WaistRotation.Pitch =
			FMath::FInterpTo(WaistRotation.Pitch, 0, World->GetDeltaSeconds(), INTERP_SPEED);
		WaistRotation.Roll =
			FMath::FInterpTo(WaistRotation.Roll, 0, World->GetDeltaSeconds(), INTERP_SPEED);
	}
	else
	{
		int CameraDeltaAngle = CameraManagerComp->GetCameraDeltaAngle();

		//Lean to the sides only if player is moving		
		if (MovementComp->GetSpeed() > 10)
			if (CameraDeltaAngle > 3)
				WaistRotation.Roll =
				FMath::FInterpTo(WaistRotation.Roll, MaxLeaningAngle, World->GetDeltaSeconds(), INTERP_SPEED);

			else if (CameraDeltaAngle < -3)
				WaistRotation.Roll =
				FMath::FInterpTo(WaistRotation.Roll, -MaxLeaningAngle, World->GetDeltaSeconds(), INTERP_SPEED);

			else
				WaistRotation.Roll =
				FMath::FInterpTo(WaistRotation.Roll, 0, World->GetDeltaSeconds(), INTERP_SPEED);
		else
			WaistRotation.Roll =
			FMath::FInterpTo(WaistRotation.Roll, 0, World->GetDeltaSeconds(), INTERP_SPEED);

		//Rotates waist depend on camera rotation and max degrees		
		float Damper = 1;
		switch (MovementComp->GetMovementState())
		{
		case EMovementState::Crouching:
			Damper = 0.5;
			break;
		case EMovementState::Proning:
			Damper = 0.25;
			break;
		}

		if (CameraDeltaAngle > 20)
			WaistRotation.Yaw =
			FMath::FInterpTo(WaistRotation.Yaw, (CameraDeltaAngle / 90) * MaxWaistRotationAngle * Damper,
				World->GetDeltaSeconds(), INTERP_SPEED / 2);
		else if (CameraDeltaAngle < -20)
			WaistRotation.Yaw =
			FMath::FInterpTo(WaistRotation.Yaw, -MaxWaistRotationAngle * Damper, World->GetDeltaSeconds(),
				INTERP_SPEED / 2);
		else
			WaistRotation.Yaw =
			FMath::FInterpTo(WaistRotation.Yaw, 0, World->GetDeltaSeconds(), INTERP_SPEED / 3);

		/*// Waist rotation for slopes (not realy visible but realistic!)
		if (SurfacePitch > 15 && CurrentSpeed > 10)
			WaistRotation.Pitch =
				FMath::FInterpTo(WaistRotation.Pitch, -10, World->GetDeltaSeconds(), INTERP_SPEED);
		else
			WaistRotation.Pitch =
				FMath::FInterpTo(WaistRotation.Pitch, 0, World->GetDeltaSeconds(), INTERP_SPEED);*/
	}
}

float AHumanCharacter::CalculateDeltaAngle(FVector Vec1, FVector Vec2, bool ContainZAxis)
{
	// No Z Offset
	if (!ContainZAxis)
	{
		Vec1.Z = 0;
		Vec2.Z = 0;
	}
	// Normalizing
	Vec1.Normalize();
	Vec2.Normalize();

	/* 1.The angle between Vec1 & Vec2 DotProduct */
	float DeltaAngle = UKismetMathLibrary::DegAcos(Vec1 | Vec2);

	// 2.Figuring out the sign of DeltaAngle (+/-)
	if (!ContainZAxis)
	{
		if (FVector::CrossProduct(Vec1, Vec2).Z < 0) DeltaAngle = -DeltaAngle;
	}
	else
		if (Vec1.Z < Vec2.Z) DeltaAngle = -DeltaAngle;

	return DeltaAngle;
}

// Disables All Components
void AHumanCharacter::Die()
{	
	// Combat
	CombatComp->Deactivate();
	CombatComp->SetComponentTickEnabled(false);
	RightHandCollision->Deactivate();
	LeftHandCollision->Deactivate();
	RightFootCollision->Deactivate();
	LeftFootCollision->Deactivate();
	// Movement	
	MovementComp->SetComponentTickEnabled(false);
	ProneMovementCmp->SetComponentTickEnabled(false);
	ProneCollision->Deactivate();
	// Ragdoll
	ActivateRagdoll(false);
	// Physics
	PhysicalAnimation->Deactivate();
	PhysicalAnimation->SetComponentTickEnabled(false);
	// Camera
	CameraManagerComp->Deactivate();
	CameraManagerComp->SetComponentTickEnabled(false);
	// Data	
	DataComp->Deactivate();
	DataComp->SetComponentTickEnabled(false);

	SetActorTickEnabled(false);	

	GetMesh()->SetMorphTarget(TEXT("EyeLashes"), 1);
	GetMesh()->SetMorphTarget(TEXT("Eyes"), 1);

	if (Controller)
		Controller->UnPossess();
}

void AHumanCharacter::AttackKeyPressed()
{
	if (CameraManagerComp->IsUIMode()) return;

	if (MovementComp->GetCarryMode())
		MovementComp->GetCarryingPhItem()->Throw();
	else
		CombatComp->Attack();
}

void AHumanCharacter::AttackKeyReleased()
{
	if (CameraManagerComp->IsUIMode()) return;

	if (!MovementComp->GetCarryMode())
		CombatComp->AttackKeyUp();
}

void AHumanCharacter::AimKeyPressed()
{
	if (CameraManagerComp->IsUIMode()) return;

	if (MovementComp->GetCarryMode())
		MovementComp->GetCarryingPhItem()->Drop();
	else
		CombatComp->AimKeyPressed();
}

void AHumanCharacter::AimKeyReleased()
{
	if (CameraManagerComp->IsUIMode()) return;

	if (!MovementComp->GetCarryMode())
		CombatComp->AimKeyReleased();
}

void AHumanCharacter::ActivateRagdoll(bool bAutoDeactivate)
{
	MovementComp->ActivateRagdoll(bAutoDeactivate);
}
