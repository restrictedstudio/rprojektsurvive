// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "HumanMovementComponent.h"
#include "HumanCombatComponent.h"
#include "HumanDataComponent.h"
#include "HumanCameraManagerComponent.h"
#include "HumanCharacter.h"
#include "ProneMovementComponent.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values for this component's properties
UHumanMovementComponent::UHumanMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	Owner = Cast<AHumanCharacter>(GetOwner());
	RotationMode = ERotationMode::OrientToMovement;
	ExRotationMode = ERotationMode::NONE;
}

// Called when the game starts
void UHumanMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	OwnerData = Owner->GetData();
	OwnerCombat = Owner->GetCombat();

	// Root Component (Capsule)
	OwnerRoot = Cast<UCapsuleComponent>(Owner->GetRootComponent());
	OwnerRoot->SetAngularDamping(99999);
	UpdateCapsuleSize();
	// -- Hit Event
	OwnerRoot->OnComponentHit.AddDynamic(this, &UHumanMovementComponent::OnCapsuleHit);

	// Setting default Character movement values
	CharacterMovement = Owner->GetCharacterMovement();
	// -- Turning
	CharacterMovement->bOrientRotationToMovement = true;
	CharacterMovement->RotationRate.Yaw = TurningRate / (bWalkMode ? 2 : 1);
	// -- Simple OwnerMovement
	CharacterMovement->SetMovementMode(EMovementMode::MOVE_Walking);
	CharacterMovement->MaxWalkSpeed = bWalkMode ? WalkingSpeed : JogginSpeed;
	CharacterMovement->MaxAcceleration = 750;
	CharacterMovement->GroundFriction = 20;
	CharacterMovement->BrakingDecelerationWalking = 10;
	CharacterMovement->BrakingFrictionFactor = 0.5f;
	// -- Jump
	CharacterMovement->JumpZVelocity = JumpVelocity.Y;
	// -- Crouching
	CharacterMovement->bCanWalkOffLedgesWhenCrouching = true;
	CharacterMovement->NavAgentProps.bCanCrouch = true;
	CharacterMovement->CrouchedHalfHeight = 65;
	CharacterMovement->MaxWalkSpeedCrouched = CrouchingSpeed;
	// -- Physical Interactions
	CharacterMovement->StandingDownwardForceScale = 0; // Cuz it's glitchy and unstable
	CharacterMovement->InitialPushForceFactor = 400;
	CharacterMovement->PushForceFactor = 75000;
	CharacterMovement->bTouchForceScaledToMass = false;

	// Prone OwnerMovement Setup	
	ProneMovement = Owner->GetProneMovement();
	ProneMovement->MaxSpeed = ProningSpeed;

	SetMovementState(EMovementState::Idle);

	OwnerAnimInst = Owner->GetMesh()->GetAnimInstance();
	// Montage Events	
	OwnerAnimInst->OnMontageBlendingOut.AddDynamic(this, &UHumanMovementComponent::OnMontageBlendingOut);
	OwnerAnimInst->OnMontageEnded.AddDynamic(this, &UHumanMovementComponent::OnMontageEnded);

	// Slope Effect Updator
	const float SlopeEffectRefreshRate = 0.5;
	World->GetTimerManager().SetTimer(SlopeUpdateTimer, this, &UHumanMovementComponent::ApplySlopeEffect, SlopeEffectRefreshRate, true);

	// Starts the main timer.
	World->GetTimerManager().SetTimer(MainTimer, this, &UHumanMovementComponent::Timer, MainTimerRefreshInterval, true);

	DefaultMeshZ = Owner->GetMesh()->GetRelativeLocation().Z;

	OwnerCamera = Owner->GetCameraManager();
}

// Called every frame
void UHumanMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// * EVENT
	// Executes once the character stopped moving.
	if (!IsCrouching() && !IsProning() && CurrentSpeed && !CharacterMovement->Velocity.Size())
		SetMovementState(EMovementState::Idle);

	CurrentSpeed = CharacterMovement->Velocity.Size();

	if (IsProning())
		// So it will be 0 or 1 and no other number.
		ProneBlendSpaceValue = (bool)ProneMovement->Velocity.Size();
	else
		WalkingBlendSpaceValue = CurrentSpeed / RunningSpeed;

	CalculateSurfaceAngle();

	UpdateIKBones();

	if (IsRagdoll())
		SyncActorLocationWithRagdoll();

	if (RotationMode == ERotationMode::AlwaysFaceControlRotation)
		FaceControlRotation(DeltaTime);

	// Forced OwnerMovement of MoveForDuration
	if (MovePlayer) Owner->AddMovementInput(ForcedMovementDirection);
}


// Syncs root location with mesh simulated body (ragdoll)
void UHumanMovementComponent::SyncActorLocationWithRagdoll()
{
	USkeletalMeshComponent* SKMesh = Owner->GetMesh();

	FVector HipsLocation = SKMesh->GetSocketLocation(TEXT("Hips"));
	FVector SpineLocation = SKMesh->GetSocketLocation(TEXT("Spine2"));
	FVector StartLocation = HipsLocation;
	FVector EndLocation = StartLocation + FVector().DownVector * 100;

	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, false, Owner);

	// Forward line trace
	World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Cyan, false, -1, '\000', 1);

	if (Hit.bBlockingHit)
	{
		FRotator HipsRotation = SKMesh->GetSocketRotation(TEXT("Hips"));
		bIsFacingUp = FVector::DotProduct(UKismetMathLibrary::GetUpVector(HipsRotation).GetSafeNormal(), FVector(0, 0, 1)) >= 0;
		FRotator RagdollRotation = UKismetMathLibrary::MakeRotFromZX(FVector(0, 0, 1), (bIsFacingUp ? (HipsLocation - SpineLocation) : (SpineLocation - HipsLocation)));

		Owner->SetActorLocation(FMath::VInterpTo(Owner->GetActorLocation(), FVector(HipsLocation.X, HipsLocation.Y, Hit.Location.Z + OwnerRoot->GetScaledCapsuleHalfHeight()), World->DeltaTimeSeconds, 5));
		Owner->SetActorRotation(RagdollRotation);
	}
	else
		Owner->SetActorLocation(FMath::VInterpTo(Owner->GetActorLocation(), HipsLocation, World->DeltaTimeSeconds, 10));
}

// Calculates SurfacePitch and GroundDirection aw
void UHumanMovementComponent::CalculateSurfaceAngle()
{
	const float LineTraceZHeight = 150;
	FHitResult FHit, BHit;
	FVector StartLocation, EndLocation;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, false, Owner);

	// Back Trace
	StartLocation = Owner->GetActorLocation() + (-Owner->GetActorForwardVector() * CapsuleSize.X);
	EndLocation = StartLocation + (Owner->GetActorUpVector() * -LineTraceZHeight);
	World->LineTraceSingleByChannel(BHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Cyan, false, -1, '\000', 1);

	// Front Trace
	StartLocation = Owner->GetActorLocation() + (Owner->GetActorForwardVector() * CapsuleSize.X);
	EndLocation = StartLocation + (Owner->GetActorUpVector() * -LineTraceZHeight);
	World->LineTraceSingleByChannel(FHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Cyan, false, -1, '\000', 1);

	if (FHit.bBlockingHit && BHit.bBlockingHit)
	{
		GroundDirection = FHit.Location - BHit.Location;
		SurfacePitch = Owner->CalculateDeltaAngle(
			GroundDirection
			, FVector(GroundDirection.X, GroundDirection.Y, 0)
			, true);
	}
}

void UHumanMovementComponent::ApplySlopeEffect()
{	
	const float MaxWalkableAngle = CharacterMovement->GetWalkableFloorAngle();

	SlopeEffect = SurfacePitch / MaxWalkableAngle;
	SlopeEffect *= IsRunning() ? RunningSlopeEffectCoefficient : SlopeEffectCoefficient;

	UpdateMaxSpeed();
}

void UHumanMovementComponent::GetMovementValues(float& MoveForward, float& MoveRight)
{
	MoveForward = MoveForwardValue;
	MoveRight = MoveRightValue;
}

void UHumanMovementComponent::MoveForward(float Value)
{
	if (!bCanControlPawn) return;

	MoveForwardValue = bAutoRun ? 1 : Value;

	// Turn off auto run if player pressed move keys
	if (bAutoRun && Value) ToggleAutoRun();

	if (!MoveForwardValue) return;

	if (!CurrentSpeed && IsIdle())
		SetMovementState(GetWalkMode());

	FVector MovementDirection;
	if (IsProning())
		MovementDirection = Owner->GetActorForwardVector().GetSafeNormal() * MoveForwardValue;
	else
	{
		// Character Forward Vector
		const FVector ActorForward = Owner->GetActorForwardVector().GetSafeNormal();
		// Desired direction that player wants to move towards it
		FVector DesiredDirection;
		constexpr int DeltaAngleCoefficient = 2;

		switch (RotationMode)
		{
		case ERotationMode::OrientToMovement:
			DesiredDirection = FRotator(0, Owner->GetControlRotation().Yaw, 0).Vector() * MoveForwardValue;
			// this will make player move towards it's forward direction and rotating to desired rotation [multiplication by DeltaAngleCoefficient is to make the rotation more realistic]
			MovementDirection = ActorForward * DeltaAngleCoefficient
				// Dot Product | If Desired Vector and the Actor forward were not quite opposite use desired direction else use right vector
				+ (((ActorForward | DesiredDirection) > -0.85) ? DesiredDirection : Owner->GetActorRightVector().GetSafeNormal());
			break;

		case ERotationMode::FreezeRotation:
			DesiredDirection = ActorForward * MoveForwardValue + Owner->GetActorRightVector().GetSafeNormal() * MoveRightValue;
			// this will make player move towards it's forward direction and rotating to desired rotation [multiplication by DeltaAngleCoefficient is to make the rotation more realistic]
			MovementDirection = ActorForward * DeltaAngleCoefficient
				// Dot Product | If Desired Vector and the Actor forward were not quite opposite use desired direction else use right vector
				+ (((ActorForward | DesiredDirection) > -0.85) ? DesiredDirection : Owner->GetActorRightVector().GetSafeNormal());
			break;

		default: //case ERotationMode::AlwaysFaceControlRotation: | OPTIMIZATION
			MovementDirection = ActorForward * Value;
		}
	}

	Owner->AddMovementInput(MovementDirection);
}

void UHumanMovementComponent::MoveRight(float Value)
{
	if (!bCanControlPawn) return;

	MoveRightValue = Value;

	if (!Value) return;
	// Turn off auto run if player pressed move keys
	else if (bAutoRun) ToggleAutoRun();

	if (!CurrentSpeed && IsIdle())
		SetMovementState(GetWalkMode());

	FVector MovementDirection;
	if (IsProning())
		MovementDirection = Owner->GetActorRightVector().GetSafeNormal() * Value;
	else
	{
		// Desired direction that player wants to move towards it
		FVector DesiredDirection;
		constexpr int DeltaAngleCoefficient = 2;

		switch (RotationMode)
		{
		case ERotationMode::OrientToMovement:
			DesiredDirection = FRotator(0, Owner->GetControlRotation().Yaw + 90, 0).Vector() * Value;
			// this will make player move towards it's forward direction and rotating to desired rotation [multiplication by DeltaAngleCoefficient is to make the rotation more realistic]
			MovementDirection = Owner->GetActorForwardVector().GetSafeNormal() * DeltaAngleCoefficient + DesiredDirection;
			break;

		case ERotationMode::FreezeRotation:
			DesiredDirection = Owner->GetActorForwardVector().GetSafeNormal() * MoveForwardValue + Owner->GetActorRightVector().GetSafeNormal() * MoveRightValue;
			// this will make player move towards it's forward direction and rotating to desired rotation [multiplication by DeltaAngleCoefficient is to make the rotation more realistic]
			MovementDirection = Owner->GetActorForwardVector().GetSafeNormal() * DeltaAngleCoefficient + DesiredDirection;
			break;

		default: //case ERotationMode::AlwaysFaceControlRotation: | OPTIMIZATION
			MovementDirection = Owner->GetActorRightVector().GetSafeNormal() * Value;
		}
	}

	Owner->AddMovementInput(MovementDirection);
}

void UHumanMovementComponent::Jump()
{
	if (IsFalling())
		return;

	if (!bCanControlPawn)
	{
		// Dodges on hardlanding if player press space...
		if (OwnerAnimInst->GetCurrentActiveMontage() == MovementActions.HardLandingMontage)
		{
			// Crouch			
			Owner->Crouch(false);
			SetMovementState(EMovementState::Crouching);
			// Dodge
			Dodge();
			// UnCrouch Afterwards
			PendingMovementState = EMovementState::Idle;
		}

		return;
	}

	// Dodge
	if (IsCrouching())
	{
		if (DodgeTimer > DodgeCooldown) // if it was not cooling down...
			Dodge();
	}
	// UnProne
	else if (IsProning())
		UnProne();
	// Jump
	else
	{
		// Return while cooling down...
		if (JumpTimer <= JumpCooldown) return;

		Owner->Jump();
		// This is for Falling animation
		JumpTimer = 0;

		SetMovementState(EMovementState::Jumping, false);

		CharacterMovement->bOrientRotationToMovement = false;

		FVector JumpVel = GroundDirection.GetSafeNormal() * JumpVelocity.X;
		JumpVel.Z = JumpVelocity.Y;
		CharacterMovement->Velocity = JumpVel;
	}

	// Reducing Stamina
	OwnerData->AddStamina(-5);
}

void UHumanMovementComponent::OnCapsuleHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Calculates Angle between actor and hit normal
	float HitNormalDotProduct = Hit.ImpactNormal | Owner->GetActorForwardVector().GetSafeNormal();
	bool RagdollOnJump = RagdollOnJumpingIntoStaticObject && IsFalling() && OtherComp->Mobility != EComponentMobility::Movable && HitNormalDotProduct < -0.8;
	bool RagdollOnHitSpeed = MaxStandingSpeed > 0 && OtherComp->Mobility == EComponentMobility::Movable && OtherActor->GetVelocity().Size() > MaxStandingSpeed;

	if (RagdollOnHitSpeed || RagdollOnJump)			
		ActivateRagdoll();	
}

void UHumanMovementComponent::Dodge()
{
	// Pausing MovementSpeed Updator
	World->GetTimerManager().PauseTimer(SlopeUpdateTimer);
	// Animation
	PlayMontage(MovementActions.DodgeMontage);
	// Physics
	const float ActualDodgingSpeed = DodgingSpeed * (1 - SlopeEffect);
	MoveForDuration(Owner->GetActorForwardVector(), ActualDodgingSpeed);
	CharacterMovement->Velocity = GroundDirection.GetSafeNormal() * ActualDodgingSpeed;

	// Camera Animation
	OwnerCamera->PlayCameraShake(ECameraShakeType::Dodging);
}

void UHumanMovementComponent::CrouchPressed()
{
	if (!bCanControlPawn || IsFalling())
	{
		if (bToggleCrouch && IsCrouching())
			PendingMovementState = EMovementState::Idle;
		else
			PendingMovementState = EMovementState::Crouching;

		return;
	}

	if (bToggleCrouch && IsCrouching())
		UnCrouch();
	else
		Crouch();
}

void UHumanMovementComponent::CrouchReleased()
{
	if (bToggleCrouch || !IsCrouching()) return;

	if (!bCanControlPawn)
	{
		PendingMovementState = EMovementState::Idle;
		return;
	}

	UnCrouch();
}

void UHumanMovementComponent::Crouch()
{
	// Prone To Crouch
	if (IsProning()) {

		if (!CanStandUp(CrouchCapsuleHalfHeight * 2 - CapsuleSize.X)) return;		

		if (CharacterMovement->IsCrouching())
		{
			OwnerRoot->SetCapsuleHalfHeight(CrouchCapsuleHalfHeight);
			Owner->GetMesh()->SetRelativeLocation(FVector(0, 0, DefaultMeshZ + CapsuleSize.Y - CrouchCapsuleHalfHeight));
		}

		// Transition Animation From Prone to Crouching
		OwnerAnimInst->Montage_Stop(0.1, MovementActions.TransitionsMontage);
		MoveForDuration(FVector(0), 0, 0, true);
		PlayMontage(MovementActions.TransitionsMontage, MovementActions.ProneToCrouchSectionName);

		ProneMovement->UnProne();
	}
	// Idle to Crouch
	else if (IsIdle())
		PlayMontage(MovementActions.TransitionsMontage, MovementActions.StandToCrouchSectionName);

	Owner->Crouch();
	SetMovementState(EMovementState::Crouching);
}

void UHumanMovementComponent::UnCrouch()
{
	if (!CanStandUp(CapsuleSize.Y * 2 - CrouchCapsuleHalfHeight)) return;

	Owner->UnCrouch();

	if (CurrentSpeed)
		SetMovementState(GetWalkMode());
	else
	{
		SetMovementState(EMovementState::Idle);
		PlayMontage(MovementActions.TransitionsMontage, MovementActions.CrouchToStandSectionName);
	}
}

void UHumanMovementComponent::PronePressed()
{
	if (!bCanControlPawn || IsFalling())
	{
		if (IsProning())
			PendingMovementState = EMovementState::Idle;
		else
			PendingMovementState = EMovementState::Proning;

		return;
	}

	if (IsProning())
		UnProne();
	else
		Prone();
}

void UHumanMovementComponent::Prone()
{
	if (!CanProne()) return;

	DeactivateFeetIK(false);
	MoveForDuration(FVector(0), 0, 0, true);

	if (IsCrouching())
	{
		PlayMontage(MovementActions.TransitionsMontage, MovementActions.CrouchToProneSectionName);
	}
	//if (IsIdle())
	else
	{
		PlayMontage(MovementActions.TransitionsMontage, MovementActions.StandToCrouchSectionName);
		PendingMontage = FMontageInfo(MovementActions.TransitionsMontage, MovementActions.CrouchToProneSectionName);
	}

	// Setting new Capsule half height
	float GapLength = OwnerRoot->GetCollisionShape().GetCapsuleHalfHeight() - CapsuleSize.X;
	OwnerRoot->SetCapsuleHalfHeight(CapsuleSize.X);

	ProneMovement->Prone();
	ProneMovement->SetRotationMode(RotationMode);
	SetMovementState(EMovementState::Proning, false);

	// Adjusts Capsule Location after half height change to stand still on the ground
	Owner->AddActorLocalOffset(FVector(0, 0, -GapLength));
	// Adjusts Mesh Location with the new capsule half height		
	Owner->GetMesh()->SetRelativeLocation(FVector(0, 0, DefaultMeshZ + CapsuleSize.Y - CapsuleSize.X));
}

void UHumanMovementComponent::UnProne()
{
	if (!CanStandUp(CapsuleSize.Y * 2 - CapsuleSize.X)) return;

	PlayMontage(MovementActions.TransitionsMontage, MovementActions.ProneToCrouchSectionName);
	PendingMontage = FMontageInfo(MovementActions.TransitionsMontage, MovementActions.CrouchToStandSectionName);

	if (CharacterMovement->IsCrouching())
		Owner->UnCrouch();

	ActivateFeetIK();

	//TODO Promote Cap half height change to function.
	// Setting new Capsule half height
	float GapLength = OwnerRoot->GetCollisionShape().GetCapsuleHalfHeight() - CapsuleSize.Y;
	OwnerRoot->SetCapsuleHalfHeight(CapsuleSize.Y);
	// Adjusts Capsule Location after half height change to stand still on the ground
	Owner->AddActorLocalOffset(FVector(0, 0, -GapLength));
	// Adjusts Mesh Location with the new capsule half height		
	Owner->GetMesh()->SetRelativeLocation(FVector(0, 0, DefaultMeshZ));

	ProneMovement->UnProne();
	MoveForDuration(FVector(0), 0, 0, true);
}

void UHumanMovementComponent::Sprint()
{
	// Toggles Running on AutoRun mode
	bIsRunKeyDown = bAutoRun ? !bIsRunKeyDown : true;

	if (!CurrentSpeed && !MoveForwardValue && !MoveRightValue)
		return;

	if (!bCanControlPawn)
	{
		PendingMovementState = EMovementState::Running;
		return;
	}

	if (IsCrouching()) UnCrouch();
	else if (IsProning()) UnProne();

	if (bAutoRun && IsRunning())
		SetMovementState(GetWalkMode());
	else
		SetMovementState(EMovementState::Running);

	DisableCombatMode();

	// Camera Animation
	OwnerCamera->PlayCameraShake(ECameraShakeType::Running);
}

// Deals with combat for sprinting like triggering aiming and combat mode...
void UHumanMovementComponent::DisableCombatMode()
{
	OwnerCombat->CancelCurrentAction();
	OwnerCombat->SetCombatMode(false);
}

void UHumanMovementComponent::UnSprint()
{
	if (bAutoRun) return;

	bIsRunKeyDown = false;

	// Camera Animation
	OwnerCamera->StopCameraShake(ECameraShakeType::Running);

	if (!bCanControlPawn)
	{
		PendingMovementState = EMovementState::Idle;
		return;
	}

	if (!IsRunning()) return;

	SetMovementState(GetWalkMode());

	// Combat | Trigger aiming back on if player was aiming before sprint
	OwnerCombat->CheckAiming();
}

void UHumanMovementComponent::PauseSprint()
{
	if (!IsRunning()) return;

	// Camera Animation
	OwnerCamera->StopCameraShake(ECameraShakeType::Running);

	SetMovementState(EMovementState::Jogging);
	// Resume Spring will handle this
	PendingMovementState = EMovementState::Running;

	UE_LOG(LogTemp, Error, TEXT("Sprint Paused"));
}

bool UHumanMovementComponent::ResumeSprint()
{
	if (!bIsRunKeyDown || PendingMovementState != EMovementState::Running)
	{
		PendingMovementState = EMovementState::None;
		return false;
	}

	HandlePendingMovementState();
	UE_LOG(LogTemp, Warning, TEXT("Sprint Resumed"));

	return true;
}

void UHumanMovementComponent::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	if (!Montage->GetGroupName().IsEqual("Movement")) return;

	// On Dodge End
	if (Montage == MovementActions.DodgeMontage)
	{
		// it will make the forced movement stop!
		ForcedMovementDuration = 0.1;
		// Initializes Dodge Cooling down
		DodgeTimer = 0;
		World->GetTimerManager().UnPauseTimer(SlopeUpdateTimer);
		if (!bInterrupted)
			UpdateMaxSpeed();
	}
	// On Hard Landing End
	else if (Montage == MovementActions.HardLandingMontage && !bInterrupted)
		SetMovementState(EMovementState::Idle);
	else if (Montage == MovementActions.GetupMontage)
		OnRagdollEnded();
	// it will turn movement back on for Dodge, Hardlanding and etc... except for movement transitions (e.g. prone to crouch)
	else if (Montage != MovementActions.TransitionsMontage)
		StopForcedMovement();

	HandlePendingMontage();
}

// Acts like all montages ended...
void UHumanMovementComponent::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (!Montage->GetGroupName().IsEqual("Movement")) return;	

	// If there was any movement montages playing then return
	for (FAnimMontageInstance* Instance : OwnerAnimInst->MontageInstances)
		if (Instance->Montage && Instance->Montage->GetGroupName().IsEqual("Movement")) return;

	HandlePendingMovementState();

	// Right now it uses for activating movement after proning.	
	StopForcedMovement();
}

EMovementState UHumanMovementComponent::GetWalkMode()
{
	if (bIsRunKeyDown)
		return EMovementState::Running;
	if (bWalkMode)
		return EMovementState::Walking;
	return EMovementState::Jogging;
}

void UHumanMovementComponent::ToggleWalkMode()
{
	bWalkMode = !bWalkMode;
	SetMovementState(GetWalkMode());
}

void UHumanMovementComponent::ToggleRagdoll()
{
	if (IsRagdoll())
		DeactivateRagdoll();
	else
		ActivateRagdoll();
}

void UHumanMovementComponent::ToggleAutoRun()
{
	bAutoRun = !bAutoRun;

	if (bAutoRun)
	{
		if (!IsProning() && !IsCrouching())
			Sprint();
	}
	else if (IsRunning())
		UnSprint();
}

void UHumanMovementComponent::ActivateRagdoll(bool bAutoDeactivate)
{	
	if (bAutoDeactivate)
	{
		RagdollTimeout = FMath::FRandRange(0.75, RagdolMaxlDelay);
		RagdollTimer = 0;
	}
	else
		RagdollTimer = RagdolMaxlDelay + 1;

	if (IsRagdoll()) return;

	DeactivateFeetIK();

	if (IsCrouching())
		UnCrouch();
	else if (IsProning())
		UnProne();

	OwnerAnimInst->StopAllMontages(0);
	OwnerCamera->StopCameraShake(ECameraShakeType::All);

	if (bAutoDeactivate)
	{
		RagdollTimeout = FMath::FRandRange(0.75, RagdolMaxlDelay);
		RagdollTimer = 0;
	}
	else
		RagdollTimer = RagdolMaxlDelay + 1;

	USkeletalMeshComponent* SKMesh = Owner->GetMesh();

	// Reseting Mesh Locaion [in some cases like proning it is essential]
	SKMesh->SetRelativeLocation(FVector(0, 0, DefaultMeshZ));
	// Disabling Physical Animation
	Owner->GetPhysicalAnimation()->ApplyPhysicalAnimationProfileBelow(TEXT("None"), TEXT("None"), true, true);
	SKMesh->SetConstraintProfileForAll(TEXT("None"));

	OwnerRoot->SetCollisionProfileName("Ragdoll");
	OwnerRoot->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CharacterMovement->SetMovementMode(EMovementMode::MOVE_Custom);

	SKMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SKMesh->SetSimulatePhysics(true);

	SetMovementState(EMovementState::Ragdoll);
	SetRotationMode(ERotationMode::OrientToMovement);

	bCanControlPawn = false;
}

void UHumanMovementComponent::DeactivateRagdoll()
{
	if (!IsRagdoll()) return;

	USkeletalMeshComponent* SKMesh = Owner->GetMesh();
	CharacterMovement->Velocity = FVector(0);

	// Saving Snapshot for Final Ragdoll Pose
	SKMesh->GetAnimInstance()->SavePoseSnapshot(TEXT("RagdollPose"));

	// Blueprint Event
	OnGetup.Broadcast();

	SKMesh->SetSimulatePhysics(false);
	SKMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	OwnerRoot->SetCollisionProfileName("Pawn");
	OwnerRoot->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CharacterMovement->SetMovementMode(EMovementMode::MOVE_Walking);

	// Plays Getup Animation Front/Back
	SKMesh->GetAnimInstance()->Montage_Play(MovementActions.GetupMontage);
	if (bIsFacingUp)
		SKMesh->GetAnimInstance()->Montage_JumpToSection(TEXT("Behind"), MovementActions.GetupMontage);
	else
		SKMesh->GetAnimInstance()->Montage_JumpToSection(TEXT("Front"), MovementActions.GetupMontage);

	// We do this to stop ragdoll tick and rotation updatings...
	SetMovementState(EMovementState::Idle);
	bCanControlPawn = false;
}

void UHumanMovementComponent::OnLanded(FHitResult Hit)
{
	SetMovementState(EMovementState::Landed);
	CharacterMovement->bOrientRotationToMovement = true;

	// Camera Animation
	OwnerCamera->StopCameraShake(ECameraShakeType::Falling);

	// This is for jumping cooldown
	JumpTimer = 0;

	if (Owner->GetVelocity().Z < -MinFallingSpeedToHardLand)
	{
		PlayMontage(MovementActions.HardLandingMontage);
		bCanControlPawn = false;

		// Camera Animation
		OwnerCamera->PlayCameraShake(ECameraShakeType::Hardlanding);
	}
	else
		if (PendingMovementState == EMovementState::None)
			SetMovementState(GetWalkMode());
		else
			HandlePendingMovementState();
}

void UHumanMovementComponent::OnFalling()
{
	// This is for if player started in air...
	if (!CharacterMovement) return;

	if (IsCrouching())
		UnCrouch();

	if (CurrentMovementState != EMovementState::Jumping)
	{
		SetMovementState(EMovementState::Falling);
		CharacterMovement->bOrientRotationToMovement = false;
	}

	// Camera Animation
	OwnerCamera->PlayCameraShake(ECameraShakeType::Falling);
}

// Called every MainTimerRefreshInterval second
// This timer has � MainTimerRefreshInterval error-tolerance
void UHumanMovementComponent::Timer()
{
	float TimerInterval = MainTimerRefreshInterval;

	// Jumping Timeout & Cooldown
	if (JumpTimer <= JumpCooldown)
	{
		const bool bIsJumpTimeoutGoing = JumpTimer < JumpTimeout;

		JumpTimer += TimerInterval;

		if (bIsJumpTimeoutGoing && JumpTimer >= JumpTimeout && CurrentMovementState == EMovementState::Jumping)
			SetMovementState(EMovementState::Falling);
	}

	// Dodge Cooldown Counter
	if (DodgeTimer <= DodgeCooldown)
		DodgeTimer += TimerInterval;

	// IK Bones
	if (IKTimer <= IKTimeout)
		IKTimer += TimerInterval;

	// Forced OwnerMovement
	if (ForcedMovementTimer < ForcedMovementDuration || !ForcedMovementDuration)
	{
		ForcedMovementTimer += TimerInterval;

		if (ForcedMovementTimer >= ForcedMovementDuration && ForcedMovementDuration)
			StopForcedMovement();
	}

	// Ragdoll Deactivator Timer
	if (RagdollTimer < RagdollTimeout)
	{
		RagdollTimer += TimerInterval;

		if (Owner->GetMesh()->GetPhysicsLinearVelocity("Hips").Size() > 5)
			RagdollTimer = 0;
		else if (RagdollTimer >= RagdollTimeout)
			DeactivateRagdoll();
	}
}

void UHumanMovementComponent::UpdateIKBones()
{
	if (bDisableIK)
	{
		if (!bIsIKReset)
		{
			// Disable Reseting... (cuz the half height already been changed)
			if (Owner->GetDefaultHalfHeight() == OwnerRoot->GetScaledCapsuleHalfHeight() || IsCrouching() || IsProning())
			{
				bIsIKReset = true;
				return;
			}
			// Updating Root Capsule Height
			OwnerRoot->SetCapsuleHalfHeight(FMath::FInterpTo(OwnerRoot->GetScaledCapsuleHalfHeight(), Owner->GetDefaultHalfHeight(), World->DeltaTimeSeconds, IKHipsInterpSpeed));
		}

		return;
	}

	if (CurrentSpeed > 25) IKTimer = 0;

	if (IKTimer > IKTimeout)
		return;

	float TraceDistanceAdjuster = 0;
	if (CurrentSpeed >= WalkingSpeed - 10)
		TraceDistanceAdjuster = 25;

	//Calculate Left Foot Offset
	float LeftFootOffset;
	FRotator LeftFootImpactNormal;
	//Tracing
	if (IKFootTrace(IKTraceDistance - TraceDistanceAdjuster, LeftFootBoneName, LeftFootOffset, LeftFootImpactNormal))
	{
		//Updating Rotation of Foot
		LeftFootRotation = FMath::RInterpTo(LeftFootRotation, LeftFootImpactNormal, World->DeltaTimeSeconds, IKFeetInterpSpeed);
		LeftFootRotation.Roll = FMath::Clamp<float>(LeftFootRotation.Roll, -30, 30);
	}
	else
		LeftFootRotation = FMath::RInterpTo(LeftFootRotation, FRotator().ZeroRotator, World->DeltaTimeSeconds, IKFeetInterpSpeed);

	// Calculate Right Foot Offset
	float RightFootOffset;
	FRotator RightFootImpactNormal;
	// Tracing
	if (IKFootTrace(IKTraceDistance - TraceDistanceAdjuster, RightFootBoneName, RightFootOffset, RightFootImpactNormal))
	{
		// Updating Rotation of Foot
		RightFootRotation = FMath::RInterpTo(RightFootRotation, RightFootImpactNormal, World->DeltaTimeSeconds, IKFeetInterpSpeed);
		RightFootRotation.Roll = FMath::Clamp<float>(RightFootRotation.Roll, -30, 30);
	}
	else
		RightFootRotation = FMath::RInterpTo(RightFootRotation, FRotator().ZeroRotator, World->DeltaTimeSeconds, IKFeetInterpSpeed);

	// These tracings are for the efector location if player was moving!
	if (CurrentSpeed <= WalkingSpeed + 10)
	{
		IKFootTrace(IKTraceDistance, LeftFootBoneName, LeftFootOffset, LeftFootImpactNormal);
		IKFootTrace(IKTraceDistance, RightFootBoneName, RightFootOffset, RightFootImpactNormal);
	}

	// Disables EffectorLocation and IK would work only on feet rotation
	if (FMath::Abs(SurfacePitch) > 15 && CurrentSpeed > 25 || IsJogging())
	{
		// Updating Effector Transforms
		IKHipsOffset = FMath::FInterpTo(IKHipsOffset, 0, World->DeltaTimeSeconds, IKHipsInterpSpeed);
		IKLeftEffectorLocation = FMath::FInterpTo(IKLeftEffectorLocation, 0, World->DeltaTimeSeconds, IKFeetInterpSpeed);
		IKRightEffectorLocation = FMath::FInterpTo(IKRightEffectorLocation, 0, World->DeltaTimeSeconds, IKFeetInterpSpeed);
		// Updating Root Capsule Height
		OwnerRoot->SetCapsuleHalfHeight(FMath::FInterpTo(OwnerRoot->GetScaledCapsuleHalfHeight(), Owner->GetDefaultHalfHeight(), World->DeltaTimeSeconds, IKHipsInterpSpeed));
		return;
	}

	//Update Hip Location
	float MinOffset = FMath::Min(LeftFootOffset, RightFootOffset);
	IKHipsOffset = FMath::FInterpTo(IKHipsOffset, MinOffset, World->DeltaTimeSeconds, IKHipsInterpSpeed);

	//Updating Root Capsule Height
	float Target = Owner->GetDefaultHalfHeight() - FMath::Abs(IKHipsOffset * 0.5);
	OwnerRoot->SetCapsuleHalfHeight(FMath::FInterpTo(OwnerRoot->GetScaledCapsuleHalfHeight(), Target, World->DeltaTimeSeconds, IKHipsInterpSpeed));

	//To adjust feet exactly on the surface
	float OffsetsDifference = FMath::Abs(RightFootOffset - LeftFootOffset);
	if (LeftFootOffset > RightFootOffset)
		LeftFootOffset += OffsetsDifference / 5;
	else
		RightFootOffset += OffsetsDifference / 5;

	//Update Feet Effector Locations
	IKLeftEffectorLocation = FMath::FInterpTo(IKLeftEffectorLocation, LeftFootOffset - IKHipsOffset, World->DeltaTimeSeconds, IKFeetInterpSpeed);
	IKRightEffectorLocation = FMath::FInterpTo(IKRightEffectorLocation, RightFootOffset - IKHipsOffset, World->DeltaTimeSeconds, IKFeetInterpSpeed);

	bIsIKReset = false;
}

bool UHumanMovementComponent::IKFootTrace(float Distance, FName SocketName, float& Offset, FRotator& ImpactNormalRotation)
{

	USkeletalMeshComponent* SKMesh = Owner->GetMesh();
	FVector StartLocation = SKMesh->GetSocketLocation(SocketName);
	StartLocation.Z = Owner->GetActorLocation().Z;

	FVector EndLocation = SKMesh->GetSocketLocation(SocketName);
	EndLocation.Z = Owner->GetActorLocation().Z - Distance - Owner->GetDefaultHalfHeight();

	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, false, Owner);
	World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);
	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Orange, false, -1, '\000', 1);

	if (Hit.bBlockingHit)
	{
		FVector Normal = Hit.ImpactNormal;
		ImpactNormalRotation = FRotator(-UKismetMathLibrary::DegAtan2(Normal.X, Normal.Z), 0, UKismetMathLibrary::DegAtan2(Normal.Y, Normal.Z));

		Offset = (Hit.Location - Hit.TraceEnd).Size() - Distance + IKAdjustOffset;

		return true;
	}

	Offset = 0;
	return false;
}

// Moves the player forcefuly for a certain duration
// @MovementDirection shouln't be normalized!
// @Duration 0 means lifetime.
void UHumanMovementComponent::MoveForDuration(FVector MovementDirection, float Speed, float Duration, bool bStopImmediately, bool bFreezeRotation)
{
	bCanControlPawn = false;

	ForcedMovementTimer = 0;
	ForcedMovementDuration = Duration;
	ForcedMovementDirection = MovementDirection;

	if (Speed) MovePlayer = true;

	CharacterMovement->MaxWalkSpeed = Speed;
	CharacterMovement->MaxWalkSpeedCrouched = Speed;

	SetRotationMode(bFreezeRotation ? ERotationMode::FreezeRotation : RotationMode);
	bIsForcedRotationMode = bFreezeRotation;

	if (bStopImmediately)
		OwnerRoot->SetAllPhysicsLinearVelocity(FVector(0));
}

// Deactivates move for duration effect.
void UHumanMovementComponent::StopForcedMovement()
{
	bCanControlPawn = true;
	MovePlayer = false;
	ForcedMovementTimer = 9999;

	if (bIsForcedRotationMode)
		RevertRotationMode(), bIsForcedRotationMode = false;
}

//TODO make this work actually or ditch it
void UHumanMovementComponent::UpdateCapsuleSize()
{
	CapsuleSize = FVector2D(
		OwnerRoot->GetCollisionShape().GetCapsuleRadius()
		, OwnerRoot->GetCollisionShape().GetCapsuleHalfHeight()
	);
}

bool UHumanMovementComponent::CanProne()
{
	FHitResult Hit;
	FVector StartLocation = Owner->GetActorLocation() + Owner->GetActorUpVector().GetSafeNormal() * (CapsuleSize.X - CapsuleSize.Y)
		, EndLocation = StartLocation + GroundDirection.GetSafeNormal() * CapsuleSize.Y;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("PronePossibilty", false, Owner);
	QueryParams.MobilityType = EQueryMobilityType::Static;

	const FText ProneDeniedMsg = FText::FromString(TEXT("You can not prone in here; not enough space."));

	// Forward line trace
	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
	{
		OwnerData->ShowMessage(ProneDeniedMsg);
		return false;
	}

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::White, false, 2, '\000', 2);

	EndLocation = StartLocation + GroundDirection.GetSafeNormal() * -CapsuleSize.Y;

	// Backward line trace
	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
	{
		OwnerData->ShowMessage(ProneDeniedMsg);
		return false;
	}

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::White, false, 2, '\000', 2);

	return true;
}

bool UHumanMovementComponent::CanStandUp(float Height)
{
	FHitResult Hit;
	FVector StartLocation = Owner->GetActorLocation()
		, EndLocation = StartLocation + Owner->GetActorUpVector().GetSafeNormal() * Height;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("StandUpPossibility", false, Owner);
	QueryParams.MobilityType = EQueryMobilityType::Static;

	const FText StandDeniedMsg = FText::FromString(TEXT("You can not do this; not enough space."));

	// Forward line trace
	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
	{
		OwnerData->ShowMessage(StandDeniedMsg);
		return false;
	}

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::White, false, 2, '\000', 2);

	return true;
}

// will be called after getup montage finished.
void UHumanMovementComponent::OnRagdollEnded()
{
	ActivateFeetIK();

	// Blueprint Event
	OnGotup.Broadcast();

	bCanControlPawn = true;
}

void UHumanMovementComponent::PrintMSLogs()
{
	UE_LOG(LogTemp, Error, TEXT(" --- LOG DEBUG PRINT --- "));

	for (int i = 0; i < MSLogsCount; i++)
		switch (MSLogs[i])
		{
		case EMovementState::Idle: UE_LOG(LogTemp, Warning, TEXT("Idle"));
			break;
		case EMovementState::Walking: UE_LOG(LogTemp, Warning, TEXT("Walking"));
			break;
		case EMovementState::Jogging: UE_LOG(LogTemp, Warning, TEXT("Jogging"));
			break;
		case EMovementState::Running: UE_LOG(LogTemp, Warning, TEXT("Running"));
			break;
		case EMovementState::Jumping: UE_LOG(LogTemp, Warning, TEXT("Jumping"));
			break;
		case EMovementState::InAir: UE_LOG(LogTemp, Warning, TEXT("InAir"));
			break;
		case EMovementState::Landed: UE_LOG(LogTemp, Warning, TEXT("Landed"));
			break;
		case EMovementState::Falling: UE_LOG(LogTemp, Warning, TEXT("Falling"));
			break;
		case EMovementState::Crouching: UE_LOG(LogTemp, Warning, TEXT("Crouching"));
			break;
		case EMovementState::Dodging: UE_LOG(LogTemp, Warning, TEXT("Doding"));
			break;
		case EMovementState::Proning: UE_LOG(LogTemp, Warning, TEXT("Proning"));
			break;
		case EMovementState::Ragdoll: UE_LOG(LogTemp, Warning, TEXT("Ragdoll"));
			break;
		default: UE_LOG(LogTemp, Warning, TEXT("Unknown State"))
			break;
		}

	switch (PendingMovementState)
	{
	case EMovementState::None: UE_LOG(LogTemp, Warning, TEXT("Pending : None"));
		break;
	case EMovementState::Crouching: UE_LOG(LogTemp, Warning, TEXT("Pending : Crouching"));
		break;
	case EMovementState::Idle: UE_LOG(LogTemp, Warning, TEXT("Pending : Idle"));
		break;
	case EMovementState::Walking: UE_LOG(LogTemp, Warning, TEXT("Pending : Walking"));
		break;
	case EMovementState::Jogging: UE_LOG(LogTemp, Warning, TEXT("Pending : Jogging"));
		break;
	case EMovementState::Running: UE_LOG(LogTemp, Warning, TEXT("Pending : Running"));
		break;
	case EMovementState::Proning: UE_LOG(LogTemp, Warning, TEXT("Pending : Proning"));
		break;
	default: UE_LOG(LogTemp, Warning, TEXT("Pending : Unknown State"))
		break;
	}

	UE_LOG(LogTemp, Error, TEXT(" --- LOG END --- "));
}

void UHumanMovementComponent::SetMovementState(EMovementState MState, bool bUpdateMaxSpeed)
{
	if (CurrentMovementState == MState
		// Player can only go to ragdoll and landing state while falling for now...
		|| (MState != EMovementState::Landed && MState != EMovementState::Ragdoll && CurrentMovementState == EMovementState::Falling))
		return;

	// If array was hitting the maximum count
	if (MSLogsCount == MAX_LOGS_COUNT)
		// Shifting the Array
		for (int i = 1; i < MAX_LOGS_COUNT; i++)
			MSLogs[i - 1] = MSLogs[i];
	else
		++MSLogsCount;

	MSLogs[MSLogsCount - 1] = MState;

	if (MState == EMovementState::Idle && IsRunning())
		UnSprint(), bIsRunKeyDown = true;

	CurrentMovementState = MState;

	// If player hold down sprint and then start to move this will help
	if (!CurrentSpeed && IsRunning())
		Sprint();

	if (!IsIdle() && !IsWalking() && !IsJogging())
		DeactivateFeetIK();
	else
		ActivateFeetIK();

	if (bUpdateMaxSpeed)
		UpdateMaxSpeed();

	// Updating movement montage of Combat component
	OwnerCombat->PlayMovementMontage(MState);

	// Cancels Item Intractions
	if (IsFalling() || IsRunning() || IsDodging() || (bCanelActionOnMovement && (IsWalking() || IsJogging())))
		OwnerData->Cancel(), bCanelActionOnMovement = false;

	if (bShowMovementStatesLog)
		PrintMSLogs();
}

void UHumanMovementComponent::UpdateMaxSpeed()
{
	switch (CurrentMovementState)
	{
	case EMovementState::Walking:
		CharacterMovement->MaxWalkSpeed = WalkingSpeed;
		break;
	case EMovementState::Jogging:
		CharacterMovement->MaxWalkSpeed = JogginSpeed;
		break;
	case EMovementState::Running:
		CharacterMovement->MaxWalkSpeed = RunningSpeed;
		break;
	case EMovementState::Crouching:
		CharacterMovement->MaxWalkSpeedCrouched = CrouchingSpeed;
		CharacterMovement->MaxWalkSpeedCrouched *= 1 - SlopeEffect;
		break;
		/* case EMovementState::Proning:
			break; */
	}

	// Applying Slope Effect
	CharacterMovement->MaxWalkSpeed *= 1 - SlopeEffect;
}

void UHumanMovementComponent::HandlePendingMovementState()
{
	if (PendingMovementState == EMovementState::None || PendingMovementState == CurrentMovementState) return;

	if (bShowMovementStatesLog)
		PrintMSLogs();

	if (IsRunning()) UnSprint();
	else if (IsCrouching()) UnCrouch();

	switch (PendingMovementState)
	{
	case EMovementState::Running:
		//if (IsProning()) UnProne();
		Sprint();
		break;

	case EMovementState::Crouching:
		Crouch();
		break;

	case EMovementState::Proning:
		Prone();
		break;

	default:
		if (IsProning()) UnProne();
	}

	PendingMovementState = EMovementState::None;
}

void UHumanMovementComponent::HandlePendingMontage()
{
	if (!PendingMontage.Montage) return;

	PlayMontage(PendingMontage.Montage, PendingMontage.SectionName);

	// Handling States | If it got massive use Swtich()
	if (IsProning() && PendingMontage.SectionName == MovementActions.CrouchToStandSectionName)
		SetMovementState(EMovementState::Idle, false);
	else if (PendingMontage.SectionName == MovementActions.CrouchToProneSectionName)
		SetMovementState(EMovementState::Proning, false);

	PendingMontage = FMontageInfo();
}

void UHumanMovementComponent::PlayMontage(UAnimMontage* Montage, FName SectionName)
{
	if (!Montage)
		UE_LOG(LogTemp, Error, TEXT("NULL MONTAGE TRYING TO BE PLAYED."));

	OwnerAnimInst->Montage_Play(Montage);
	if (!SectionName.IsNone())
		OwnerAnimInst->Montage_JumpToSection(SectionName, Montage);
}

bool UHumanMovementComponent::IsStrafeMovementEnabled()
{
	return (RotationMode == ERotationMode::AlwaysFaceControlRotation);
}

void UHumanMovementComponent::SetRotationMode(ERotationMode InRotationMode)
{
	CharacterMovement->bOrientRotationToMovement = (InRotationMode == ERotationMode::OrientToMovement);
	ExRotationMode = RotationMode;
	RotationMode = InRotationMode;

	// Prone Component
	if (ProneMovement)
		ProneMovement->SetRotationMode(InRotationMode);

	/*switch (RotationMode)
	{
	case ERotationMode::OrientToMovement:
		UE_LOG(LogTemp, Warning, TEXT("OrientToMovement"));
		break;

	case ERotationMode::FreezeRotation:
		UE_LOG(LogTemp, Warning, TEXT("FreezeRotation"));
		break;

	case ERotationMode::AlwaysFaceControlRotation:
		UE_LOG(LogTemp, Warning, TEXT("AlwaysFaceControlRotation"));
		break;
	}*/
}

void UHumanMovementComponent::FaceControlRotation(float DeltaTime)
{
	FRotator ControlRotation = Owner->GetControlRotation();
	FRotator OwnerRotation = Owner->GetActorRotation();

	if (!FMath::IsNearlyEqual(OwnerRotation.Yaw, ControlRotation.Yaw, 0.1f))
	{
		float DeltaAngle = Owner->CalculateDeltaAngle(OwnerRotation.Vector(), ControlRotation.Vector());
		OwnerRoot->AddWorldRotation(FRotator(0, DeltaTime * 20 * DeltaAngle, 0), true);
	}
}
