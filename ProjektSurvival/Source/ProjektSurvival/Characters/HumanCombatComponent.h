// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Structs.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HumanCombatComponent.generated.h"

class UAnimMontage;
class UAnimInstance;
class AHumanCharacter;
class UHumanDataComponent;
class UHumanMovementComponent;
class UHumanCameraManagerComponent;
class AGun;
class ABow;
class AThrowable;
class AMeleeWeapon;
class APController;
class UWidgetComponent;
class USkeletalMesh;
class UCapsuleComponent;
enum class EMovementState : uint8;

UENUM(BlueprintType)
enum class EAttackType : uint8 {
	NONE,
	RightPunch,
	LeftPunch,
	RightKick,
	LeftKick,
	Push,
	MeleeWeapon, //All sort of the melee weapons : Sword, Machetee Wrench, Hammer and etc...		
	Bow,
	Gun,
	Throwable
};

UENUM(BlueprintType)
enum class ECombatState : uint8 {
	NONE,
	Aiming,
	QuickAiming,
	Reloading,
	MeleeAttacking,
	Blocked,
	MovementState,
	ReactingToHit
};

// For selecting animation aim offset
UENUM(BlueprintType)
enum class EAimingType : uint8 {
	Bow,
	Pistol,
	QPistol,
	SMG,
	QSMG,
	Shotgun,
	QShotgun,
	AssualtRifle,
	QAssualtRifle,
	SniperRifle,
	QSniperRifle,
	RocketLauncher,
	QRocketLauncher
};

enum class EWeaponType {
	NONE,
	Gun,
	Melee,
	Bow,
	Throwable
};

USTRUCT(BlueprintType)
struct FHitReactionAnimStruct
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		FName SectionName;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY(EditDefaultsOnly)
		float MovingDuration;

	FHitReactionAnimStruct()
	{
		SectionName = TEXT("");
		MovementSpeed = 0;
		MovingDuration = 0;
	}
};

/**
 * An actor component that handles any sort of combat
 * Weapons and Throwables will be handled in here
 * Melee attacking and other combat stuff
 *
 * !CAUTION : Make sure you call SetupComponents() on intializing this class or it will break.
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), hideCategories = ("Variable", "Sockets", "Component Replication", "Activation", "Cooking", "Events", "Asset User Data", "Collision"))
class PROJEKTSURVIVAL_API UHumanCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHumanCombatComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Applies Physical Animation depend on the arguments
	void ApplyPhysicalAnimation(AActor* HumanActor, USkeletalMeshComponent* HitHumanSKMesh, FName HitBone);

	// Linear Damage
	void DealDamage(FDamage DamageData, FName HitBone);
	// Radial Damage
	void DealDamage(FVector DamageOrigin, float DamageRadius, float Damage);

	// UnArmed combat hit event (L/R Punch & Kick)
	UFUNCTION()
		void OnMeleeHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	// Acts like all montages ended...
	UFUNCTION()
		void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION()
		void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION()
		void OnMontageStarted(UAnimMontage* Montage);

	// Particle Systems
	// Blood
	UPROPERTY(EditDefaultsOnly, Category = "Particle Systems")
		UParticleSystem* BloodPS = nullptr;

	/*
	Finds out the direction of force after hit so that we can play the correct hit animation!
	@return Left or Right or Forward or Backward
	*/
	FName FindHitDirection(FVector HitActorForward, FVector HitImpactNormal);

	// detects what kind of attacking player is doing INPUT FUNCTION
	void Attack();
	void AttackKeyUp();
	void Reload();
	void PrimarySlot1();
	void PrimarySlot2();
	void SecondarySlot1();
	void SecondarySlot2();
	void ThrowableSlotsCycle();
	// Melee attack for guns...
	void MeleeAttack();

	// Aim key down (right mouse button down) event
	void AimKeyPressed();
	// Aim key up (right mouse button up) event
	void AimKeyReleased();	
	// character starts aiming.
	void Aim();
	// character stops aiming.
	void UnAim();
	// Starts aiming if player was suppose to be aiming
	void CheckAiming();

	void ChangeGunFireMode();
	void PlayMontage(ECombatState CombatState, UAnimMontage* Montage, FName SectionName = "Default", bool bIsUpperBodySlot = true);
	void StopMontage(float BlendTime = 0.3f);

	// Sets up the weapon different defualt values, Mesh, Animations and etc...
	AWeapon* SetupWeapon(TSubclassOf<AActor> WeaponBlueprint, EHotbarSlotLocation Slot, UItem_Weapon* Item);

	/* Plays a hit reaction montage
	@HitDirection it can be only (R,F,B,L) unless it wont work properly. */
	void PlayHitReactionMontage(FName HitDirection);
	// Cancels any sorta action like reloading, equiping, etc...
	void CancelCurrentAction();
	void GoRagdoll();
	bool IsRagdoll();
	// Checks the movment state and decides whether to play movement montage for gun or not
	void PlayMovementMontage(EMovementState State);
	// Swaps 2 weapon actors only
	void SwapWeapons(FHotbarSlot* Slot1, FHotbarSlot* Slot2);

	// Getters
	/* */
	// Aiming or Quick Aiming
	bool IsAiming() { return CombatState == ECombatState::Aiming || CombatState == ECombatState::QuickAiming; }
	bool IsAimingKeyDown() { return bIsAimKeyDown; }
	// whether player's carrying a gun or not
	bool IsUsingGun() { return AttackType == EAttackType::Gun; }
	bool IsReloading();
	bool GetCombatMode() { return bCombatMode; }
	bool IsGunBlocked() { return WeaponBlockageTimer < WeaponBlockageCheckDelay || bIsWeaponBlocked; }
	bool IsTriggerPulled() { return bIsAttackKeyDown && (FireTimer >= FirstFireDelay); }
	float GetMinHitSpeedToGoToRagdoll();
	float GetProjectileAligningRange() { return ProjectilesAligningRange; }
	FName GetPlayingMontageSection() { return PlayingMontageSection; }
	FHotbarSlot* GetActiveSlot() { return ActiveSlot; }

	UFUNCTION(BlueprintGetter, Category = "Combat")
		ECombatState GetCombatState() { return CombatState; }

	// Setters
	void SetCombatMode(bool bEnabled);
	void ResetCombatModeTimer();
	void PauseCombatModeTimer() { CombatModeTimer = FLT_MAX; }
	void SetCombatState(ECombatState State);
	// Goes into quick aiming mode if possible
	void StartQuickAiming();	
	void SetMovementAnimationData(FMovementAnimation InMovementAnim) { MovementAnimationData = InMovementAnim; }
	void ResumeCurrentMontage();
	void SetAimTypes(EAimingType InAimType, EAimingType InQuickAimType) { AimType = InAimType, QuickAimType = InQuickAimType; }
	void SetAttackType(EAttackType InAttackType) { AttackType = InAttackType; }
	void ClearActiveSlot() { ActiveSlot = nullptr; }
	void UnEquipActiveSlot();
	void SetUseAimPose(bool Value) { bUseAimPose = Value; }

	/* -- GUN PUBLIC*/
	// Setters
	void SetFireTimer(float Value) { FireTimer = Value; }
	void SetFireAfterDelay(bool Value) { FireAfterDelay = Value; }	
	// Getters
	float GetFireTimer() { return FireTimer; }
	float GetFirstFireDelay() { return FirstFireDelay; }
	float GetIsWeaponBlocked() { return bIsWeaponBlocked; }			

	// Setup Components
	void SetupComponents(USceneComponent* AimAdjusterRef, UWidgetComponent* BlockedWidgetRef
		, UCapsuleComponent* InRightHandCollision, UCapsuleComponent* InLeftHandCollision
		, UCapsuleComponent* InRightFootCollision, UCapsuleComponent* InLeftFootCollision);

	// Returns aim adjuster's pitch
	float GetAimAdjuster();
	void SetAimAdjuster(float Value);

	// Sets the widget that shows gun or any other weapons blockage by obstacle
	void SetBlockedWidget(bool Visibility, FVector Location = FVector());

	// Turns hit events & collision of the given attack type ON/OFF	
	void SetMeleeCollisions(bool bEnabled, EAttackType InAttackType);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Debuging")
		bool bDrawDebugHelpers = false;
	UPROPERTY(EditDefaultsOnly, Category = "Debuging")
		bool bPrintCombatStatesLog = false;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T1 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T2 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T3 = 1;

	// Combat Mode
	UPROPERTY(BlueprintReadWrite, Category = "Combat")
		bool bCombatMode = false;
	// The time that player will go to normal state if no combat actions was taken
	UPROPERTY(EditDefaultsOnly, Category = "Combat")
		float CombatModeTimeout = 5;	
	// damage deals from each punch or kick
	UPROPERTY(EditDefaultsOnly, Category = "Combat")
		float UnArmedCombatDamage = 7.5;

	// Animations!
	// Punching
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Punch")
		UAnimMontage* PunchMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Punch")
		FName RightPunchSectionName = "RightPunch";
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Punch")
		FName RightPunchBlockedSectionName = "RightPunchBlockedEnd";
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Punch")
		FName RightPunchEndSectionName = "RightPunchEnd";
	// Kicking
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Kick")
		UAnimMontage* KickMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Kick")
		FName RightKickSectionName = "RightKick";
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Kick")
		FName RightKickBlockedSectionName = "RightKickBlockedEnd";
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Kick")
		FName RightKickEndSectionName = "RightKickEnd";

	// HIT REACTION VARIABLES
	// Animation Montage
	UPROPERTY(EditDefaultsOnly, Category = "Hit Reaction")
		UAnimMontage* HitReactionMontage;
	// All of the FRONT HitReaction animation montage sections and their speed should be set with this array
	UPROPERTY(EditDefaultsOnly, Category = "Hit Reaction")
		TArray<FHitReactionAnimStruct> FrontHitReactionAnimations = TArray<FHitReactionAnimStruct>();
	// All of the BEHIND HitReaction animation montage sections and their speed should be set with this array
	UPROPERTY(EditDefaultsOnly, Category = "Hit Reaction")
		TArray<FHitReactionAnimStruct> BehindHitReactionAnimations = TArray<FHitReactionAnimStruct>();
	// All of the RIGHT HitReaction animation montage sections and their speed should be set with this array
	UPROPERTY(EditDefaultsOnly, Category = "Hit Reaction")
		TArray<FHitReactionAnimStruct> RightHitReactionAnimations = TArray<FHitReactionAnimStruct>();
	// All of the LEFT HitReaction animation montage sections and their speed should be set with this array
	UPROPERTY(EditDefaultsOnly, Category = "Hit Reaction")
		TArray<FHitReactionAnimStruct> LeftHitReactionAnimations = TArray<FHitReactionAnimStruct>();

	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
		float ProjectilesAligningRange = 200;

	UPROPERTY(EditDefaultsOnly, Category = "Weapons|Slots")
		TMap<EHotbarSlotLocation, FName> WeaponHolderSockets;

	// Minuimum Allowed Range for shooting with a gun
	UPROPERTY(EditDefaultsOnly, Category = "Weapons|Guns")
		float MinAllowedRangeForUsingGun = 30;
	// First fire delay when player was not aiming at all
	UPROPERTY(EditDefaultsOnly, Category = "Weapons|Guns")
		float FirstFireDelay = 0.1;

	// BLUEPRINT READ-ONLY 

	UPROPERTY(BlueprintReadOnly, Category = "Animations")
		bool bIsUpperBodySlot = false;
	// Activates Aim Offsets for player animations
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
		bool bUseAimPose = false;
	// For selecting the current aim offset animation
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
		EAimingType AimingType;

private:
	// Cooldowns and Delays will be calculated here
	void Timer(float DeltaTime);
	// All of the line traces of this class	
	void CheckHits();
	// checks if gun's blocked plays montage an stuff (Projectile Based Weapons)
	void CheckWeaponBlockage();
	//Interps Hit human physics blend weight to HitHumanPhysicsBlendWeightTarget all the time
	void UpdateHitHumanPhysicsWeight();
	// Decides which montage to play automaticly
	void PlayMontage();
	// Decides which montage end to play automaticly
	void PlayUnArmedEndMontage();
	// Decides which montage block to play automaticly
	void PlayUnArmedAttackBlockMontage();
	// Finds the index that HitActors,HitSKMeshes and etc... are not using.
	int FindFreePhysicalAnimArrayIndex();
	bool AreBoneParentsSimulatingPhysics(USkeletalMeshComponent* SkeletonMesh, FName BoneName);
	bool IsBoneChildOf(FName BoneName, FName TargetParent);		
	// Equips Slot
	void EquipSlot(EHotbarSlotLocation SlotLocation);		

	// Finds the best attack type depend on the situation of player and the world using traces!
	EAttackType FindBestUnArmedAttackType();

	UWorld* World = nullptr;
	// Skeletal Mesh
	USkeletalMeshComponent* SKMesh = nullptr;
	UAnimInstance* OwnerAnimInst = nullptr;
	USkeletalMeshComponent* HitSKMeshes[100];
	AHumanCharacter* HitHumen[100];
	UAnimMontage* PlayingMontage = nullptr;
	FName PlayingMontageSection;
	USceneComponent* RootComp = nullptr;
	AHumanCharacter* Owner = nullptr;
	UHumanDataComponent* OwnerData = nullptr;
	UHumanMovementComponent* OwnerMovement = nullptr;
	UHumanCameraManagerComponent* OwnerCamera = nullptr;
	USkeletalMeshComponent* WeaponSKMesh = nullptr;
	APController* Controller = nullptr;
	AThrowable* ThrowableActor = nullptr;

	/* Human Character Components */
	// adjusts aiming angle
	USceneComponent* AimAdjuster = nullptr;
	// shows weapon's shot blockage
	UWidgetComponent* BlockedWidget = nullptr;
	// Melee Combat Collisions
	UCapsuleComponent* RightHandCollision = nullptr
		, * LeftHandCollision = nullptr
		, * RightFootCollision = nullptr
		, * LeftFootCollision = nullptr;

	// Combat States
	ECombatState CombatState;
	static const int CS_MAX_LOGS_COUNT = 5;
	int CSLogsCount = 0;
	ECombatState CombatStatesLog[CS_MAX_LOGS_COUNT];
	void PrintCSLog();
	float CombatStateResetTimer = 50;
	float CombatStateResetTime = 0.3;

	FMovementAnimation MovementAnimationData;
	// Active Attack type
	EAttackType AttackType;

	FName HitBones[100];

	// the free index for Physical Animation arrays
	int PhysicalAnimationFreeIndex = -1;

	// Holds Active indexes of PhysicalAnim
	TArray<int> PhysicalAnimActiveIndexes;

	// Animation
	// For selecting the current aim offset animation	
	EAimingType AimType;
	EAimingType QuickAimType;

	//Hit human physics blend weight interps to this value
	float HitHumanPhysicsBlendWeightTargets[100] = { 0 };
	float CurrentHitHumanPhysicsBlendWeights[100] = { 0 };
	float CombatModeTimer = 50;

	// for FirstFireDelay...
	float FireTimer = 50;
	bool FireAfterDelay = false;

	bool bDidCollisionHit = false;
	bool bDidTraceHit = false;
	bool bDidAttackEnd = true;		
	bool bRevertRotationMode = false;
	// true when the attack action is Key_Down, used for automatic guns
	bool bIsAttackKeyDown = false;
	bool bIsAimKeyDown = false;

	// Throwables VARIABLES
	float ThrowableEquipTimer = 50;
	float ThrowableDelayTimer = 50;
	bool bHasThrowable = false;		

	// GUN VARIABLES	
	bool bIsWeaponBlocked = false;
	float WeaponBlockageTimer = 50;
	float WeaponBlockageCheckDelay = 0.5;
	bool bGoToQuickAiming = false;
	EMovementState CombatMovementState;	

	// Hotbar
	FHotbarSlot* ActiveSlot = nullptr;
	// Use this after player had thrown a throwable
	FHotbarSlot* ThrowableSlotToRemove = nullptr;
	EWeaponType ActiveWeaponType = EWeaponType::NONE;
	int ActiveThrowableSlot = 1;
};
