// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "ProneMovementComponent.generated.h"

class UCharacterMovementComponent;
class AHumanCharacter;
class UCapsuleComponent;
enum class ERotationMode : uint8;

/**
 * It's an improved and customized version of FloatingPawnMovement
 * And will implement prone movement to character.
 * Compatibale with capsule movement
 * You have to call setup on initialzing to set the prone collision.
 * ERotationMode is from HumanMovementComponent.
 *
 * Capsule half height won't be handled by this component.
 */
UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent))
class PROJEKTSURVIVAL_API UProneMovementComponent : public UFloatingPawnMovement
{
	GENERATED_BODY()

		void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	// Constructor
	UProneMovementComponent();

	void BeginPlay() override;

	void Setup(UCapsuleComponent* InProneCollision);

	//TODO add commnet
	void Prone();
	//TODO add commnet
	void UnProne();

	void SetRotationMode(ERotationMode InRotationMode) { RotationMode = InRotationMode; }

protected:

	UPROPERTY(EditAnywhere, Category = "Settings")
		ERotationMode RotationMode;
	// Adds force to the obstacles on hit, you can set it to 0 to disable it.
	UPROPERTY(EditAnywhere, Category = "Settings")
		bool EnablePhysicalInteraction = true;

	UPROPERTY(EditAnywhere, Category = "Debuging")
		bool bDrawDebugHelpers = false;


private:	

	class AHumanCharacter* Owner;
	UWorld* World;
	UPrimitiveComponent* OwnerRoot = nullptr;
	UCapsuleComponent* ProneCollision = nullptr;
	UCharacterMovementComponent* CharacterMovement = nullptr;

	// Checks whether player is falling or not by line tracing and goes to ragdoll if player was falling
	void CheckFalling();
	bool bIsFalling = false;

	// It will manage all of blocking obstacles by stopping the movement or adding force to the obstacle
	void CheckObstaclesBlocking();

	float CurrentSpeed = 0;
	float DefaultMaxSpeed = 0;

	// Radius and HalfHeight will be set in begin play.
	float CapsuleRadius;
	float CapsuleHalfHeight;
	float Alpha = 0;

	void OrientToMovement();
	void FaceControlRotation();

	// resets the actor rotation and deactivates prone afterwards
	bool bDeactivate = false;
	// it'll be false once after synced with floor after proning
	bool bFirstSync = true;
	void SynchronizeRotationWithSurface();
	// Holds Current Last Calculated Surface Roll
	float SurfaceRoll = 0;
	float CalculateSurfaceRoll();
	// Holds Current Last Calculated Surface Pitch
	float SurfacePitch = 0;
	float CalculateSurfacePitch();

};