// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "../Structs.h"
#include "Components/ActorComponent.h"
#include "HumanDataComponent.generated.h"

class APController;
class UItem;
class UItem_Wearable;

enum class ESlotType {
	Inventory,
	Proximity,
	Wearables,
	Hotbar
};

UENUM(BlueprintType)
enum class EWearableSlot : uint8 {
	Head,
	Backpack,
	Shirt,
	Pants,
	RHand,
	LHand,
	LFoot,
	RFoot
};

UENUM(BlueprintType)
enum class EWeaponSlotType : uint8 {
	HeavyWeapon,
	LightWeapon,
	Throwable
};

USTRUCT(BlueprintType)
struct FWearableSlot
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItem_Wearable* Item;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* WearableActor;

	FWearableSlot()
	{
		ID = -1;
		Item = nullptr;
		WearableActor = nullptr;
	}

	FWearableSlot(int IDToSet, UItem_Wearable* ItemToSet, AActor* WeaponActorToSet)
	{
		ID = IDToSet;
		Item = ItemToSet;
		WearableActor = WeaponActorToSet;
	}
};

/*
All of player data will be held in here
Inventory, Living Stats (Stamina, Hunger, health and etc...), Hotbar slots and etc...
 | NO TICKING
*/
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJEKTSURVIVAL_API UHumanDataComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHumanDataComponent();

	// Blueprint getters
	UFUNCTION(BlueprintPure, Category = "Life")
		float GetHealth() { return Health / 100; }
	UFUNCTION(BlueprintPure, Category = "Life")
		float GetMaxHealth() { return MaxHealth / 100; }
	UFUNCTION(BlueprintPure, Category = "Life")
		float GetStamina() { return Stamina / 100; }
	UFUNCTION(BlueprintPure, Category = "Life")
		float GetHunger() { return Hunger / 100; }
	UFUNCTION(BlueprintPure, Category = "Life")
		float GetThirst() { return Thirst / 100; }

	// Interaction Input Handling
	void OnInteractPressed();
	void OnInteractReleased();
	void OnInteractHeld();

	// Inventory
	int GetInventoryMaxCapacity() { return InventoryMaxCapacity; }
	int GetInventoryUsedCapacity() { return InventoryUsedCapacity; }
	void AddInventoryMaxCapacity(int Amount) { InventoryMaxCapacity += Amount; }
	// AmountToAdd : 0 means add max possible amount to inventory
	bool AddItemToInventoryFromProximity(int ID, int AmountToAdd = 0);
	// AmountToAdd : 0 means add max possible amount to inventory
	bool AddItemToInventoryFromProximity(APhysicalItem* PhItem, int AmountToAdd = 0);
	bool AddItemToInventoryFromHotbar(int ID);
	bool AddItemToInventoryFromWeared(int ID);
	TArray<FInventorySlot> GetInventorySlots() { return InventorySlots; }
	bool IsInventoryOpen() { return bIsInventoryOpen; }

	void ToggleShowInventory();

	void UpdateInventorySlot(int ID, int AmountToAdd);
	FInventorySlot GetInventorySlotByID(int ID);
	// @return index of the found node, -1 if nothing was found!	
	int FindInventorySlotIndexByItem(UItem* Item);

	FTransform FindBestDropLocation();

	// Proximity
	TArray<FProximitySlot> GetProximitySlots() { return ProximitySlots; }
	FProximitySlot GetProximitySlotByID(int ID);
	FProximitySlot GetProximitySlotByID(int ID, int& Index);
	void UpdateProximitySlot(int ID, int AmountToAdd);
	void UpdateProximitySlots();
	int FindPhysicalItemId(APhysicalItem* PhItem);
	void DetectProximitySlots();
	// Adds proximity item to Array and UI
	void AddProximitySlot(FProximitySlot PSlot);
	void RemoveProximitySlot(int ID);
	int FindEmptyProximityID();

	void AddHealth(float Amount);
	void AddHunger(float Amount);
	void AddThirst(float Amount);
	void AddStamina(float Amount);
	void Cancel();

	// For now uses for canceling consuming...
	FTimerHandle InUseItemTimer;

	void SetInUseItem(int ID = -1, ESlotType SlotType = ESlotType::Inventory) { InUseItemID = ID, InUseItemSlotType = SlotType, InUsePhItem = nullptr; }
	void SetInUseItem(APhysicalItem* PhItem) { InUsePhItem = PhItem, InUseItemID = -1, InUseItemSlotType = ESlotType::Proximity; }
	bool IsUsingAnyItem() { return (InUseItemID != -1); }

	// UI
	void ShowMessage(FText Text, float ShowTime = 2);
	void SetHint(FText Text);
	void ShowStackFrame();

	// Items Action
	/*Using item from inventory*/
	void ConsumeInUseItem();
	// Using item directly from physical item
	void ConsumeInUsePhItem();

	// Wearables		
	TArray<FWearableSlot> GetWearedSlots() { return WearedSlots; }
	bool EquipWearableItem(FWearableSlot WSlot);
	// @return index of WearableSlots if item was weared else -1 will be returned.
	int GetWearableSlotIndex(EWearableSlot SlotType);
	int GetWearableSlotIndex(int ID);
	int FindEmptyWearingID();
	void RemoveWearableSlot(EWearableSlot SlotType);
	FWearableSlot GetWearableSlotByID(int ID);

	// Hotbar Weapons		
	FHotbarSlot* GetHotbarSlots() { return HotbarSlots; }
	bool EquipHotbarSlot(FHotbarSlot HSlot);
	bool RemoveHotbarSlot(int ID, bool ClearOnly = false);
	FHotbarSlot* GetHotbarSlot(EHotbarSlotLocation SlotLocation);
	bool SwapHotbarSlots(EHotbarSlotLocation SlotLocation, int ID);
	int FindEmptyHotbarSlot(EWeaponSlotType SlotType);
	bool HasThrowables();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		bool bDrawDebugHelpers = false;
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		int T1 = 25;

	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float MaxHealth = 100.f;

	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float StaminaDropRate = 0.25f;
	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float StaminaIncreaseRate = 0.5f;
	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float StaminaRecoverDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float HungerDropRate = 0.05f;

	UPROPERTY(EditDefaultsOnly, Category = "Life")
		float ThirstDropRate = 0.1f;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		float ItemsDetectorTraceLength = 350;
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		TArray<UItem*> DefaultInventorySlots;
	// Update proximity items with this interval
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		float InventoryProximityRefreshInterval = 1;
	// Sphere trace to detect proximity items in range
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		float ProximityDetectionRadius = 200;

private:
	// Called when the game starts
	virtual void BeginPlay() override;

	void CustomTick();
	void StartStaminaRecovering() { bRecoverStamina = true; }

	UWorld* World = nullptr;
	class AHumanCharacter* Owner = nullptr;
	APController* Controller = nullptr;
	class UHumanMovementComponent* OwnerMovement = nullptr;
	class UHumanCameraManagerComponent* OwnerCamera = nullptr;
	class UHumanCombatComponent* OwnerCombat = nullptr;

	// Life Parameters
	float Health = 100
		, Stamina = 100
		, Hunger = 100
		, Thirst = 100;

	bool bRecoverStamina = false;
	FTimerHandle StaminaRecoveryTimer;

	static const int HotbarSlotsCount = 8;
	// Hotbar Slots
	FHotbarSlot HotbarSlots[HotbarSlotsCount] = {
		FHotbarSlot(0,EHotbarSlotLocation::HeavyWeapon1)
		,FHotbarSlot(1,EHotbarSlotLocation::HeavyWeapon2)
		,FHotbarSlot(2,EHotbarSlotLocation::LightWeapon1)
		,FHotbarSlot(3,EHotbarSlotLocation::LightWeapon2)
		,FHotbarSlot(4,EHotbarSlotLocation::Throwable1)
		,FHotbarSlot(5,EHotbarSlotLocation::Throwable2)
		,FHotbarSlot(6,EHotbarSlotLocation::Throwable3)
		,FHotbarSlot(7,EHotbarSlotLocation::Throwable4)
	};
	void ReplaceHotbarSlot(EHotbarSlotLocation SlotLocation, FHotbarSlot HSlot);

	// Inventory
	void SetIsInventoryOpen(bool bValue);
	int InventoryMaxCapacity = 500;
	int InventoryUsedCapacity = 0;
	TArray<FInventorySlot> InventorySlots;
	int FindEmptyInventoryID();

	int CheckInventorySpaceForItem(UItem* Item);
	// DO NOT CALL IT MANUALLY, USE AddItemToInventoryFrom... functions
	// Adds Item to slots, UI, updates InventoryCapacity
	void AddItemToInventory(UItem* Item, int Amount);

	TArray<FProximitySlot> ProximitySlots;
	FTimerHandle ProximityDetectorTimer;
	bool bIsInventoryOpen = false;

	// Proximity Loading
	const int MaxItemsLoadPerFrame = 10;
	TArray<AActor*> ProximityActorsToIgnore;

	// Items Interaction
	int InUseItemID = -1;
	ESlotType InUseItemSlotType;
	APhysicalItem* InUsePhItem;
	APhysicalItem* InteractedItem = nullptr;
	bool bIsInteractedKeyDown = false;
	const float InteractionHoldTime = 0.15;
	FTimerHandle InteractionTimer;
	bool bCancelActionOnMovement = false;

	// UI
	FTimerHandle MessageTimer;
	void ClearMessage();

	// Wearables
	TArray<FWearableSlot> WearedSlots;
	void ReplaceWearableSlot(FWearableSlot SlotToReplace, FWearableSlot Replacement);

};