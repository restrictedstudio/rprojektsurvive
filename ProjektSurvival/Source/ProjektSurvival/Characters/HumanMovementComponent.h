// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HumanMovementComponent.generated.h"

class AHumanCharacter;
class UCharacterMovementComponent;
class UCapsuleComponent;
class UProneMovementComponent;
class APhysicalItem;
enum class ERotationMode : uint8;

// Blueprint event dispatchers
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGetupDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGotupDelegate);

UENUM(BlueprintType)
enum class EMovementState : uint8 {
	Idle,
	Walking,
	Jogging,
	Running,
	Crouching,
	Proning,
	Turning,
	Climbing,
	Swimming,
	InAir, // Jumping
	Falling,
	Landed,
	Dodging,
	Jumping,
	Ragdoll,
	None
};

USTRUCT(BlueprintType)
struct FMovementAction
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = "HardLanding")
		UAnimMontage* HardLandingMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Dodge")
		UAnimMontage* DodgeMontage;

	// Getup montage for ragdoll to normal state... | Should have 2 montage sections 1.Behind 2.Front
	UPROPERTY(EditDefaultsOnly, Category = "Ragdoll")
		UAnimMontage* GetupMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Transitions")
		UAnimMontage* TransitionsMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Transitions")
		FName CrouchToStandSectionName;
	UPROPERTY(EditDefaultsOnly, Category = "Transitions")
		FName StandToCrouchSectionName;
	UPROPERTY(EditDefaultsOnly, Category = "Transitions")
		FName CrouchToProneSectionName;
	UPROPERTY(EditDefaultsOnly, Category = "Transitions")
		FName ProneToCrouchSectionName;

	FMovementAction()
	{
		CrouchToStandSectionName = TEXT("CrouchToStand");
		StandToCrouchSectionName = TEXT("StandToCrouch");
		CrouchToProneSectionName = TEXT("CrouchToProne");
		ProneToCrouchSectionName = TEXT("ProneToCrouch");
	}
};

USTRUCT()
struct FMontageInfo
{
	GENERATED_BODY()

public:
	UAnimMontage* Montage;
	FName SectionName;

	FMontageInfo()
	{
		Montage = nullptr;
		SectionName = NAME_None;
	}
	FMontageInfo(UAnimMontage* InMontage, FName InSectionName)
	{
		Montage = InMontage;
		SectionName = InSectionName;
	}
};

/**
 * An actor component that handles any sort of movement
 * Compatibale with UE4 Character pawn
 * IK Bones are also implemnted in here. (Feet IK for now...)
 * Handles Cruoch, Prone (Using UProneMovementComponent), Jump and etc...
 * Optimized and mostly Event-Based.
 * Root (Capsule Component) half height also handles in here
 *
 * !CAUTION : DO NOT CHANGE CHARACTER MOVEMENT COMPONENT THAT'LL HAVE NO EFFECT.
 * DEFAULT VALUES OF CHARACTER MOVEMENT COMPONENT WILL BE SET IN BEGINPLAY() OF THIS CLASS.
 */
UCLASS(BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), hideCategories = ("Variable", "Sockets", "Component Replication", "Activation", "Cooking", "Events", "Asset User Data", "Collision"))
class PROJEKTSURVIVAL_API UHumanMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHumanMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Debuging")
		bool bShowMovementStatesLog = false;
	UPROPERTY(EditDefaultsOnly, Category = "Debuging")
		bool bDrawDebugHelpers = false;

	UPROPERTY(EditAnywhere, Category = "Settings")
		bool bToggleCrouch = true;

	// If the physical object had more speed on hit, then ragdoll will be activated.
	// less than 0 means no interaction can make character to go ragdoll.
	UPROPERTY(EditDefaultsOnly, Category = "Physics Interaction")
		float MaxStandingSpeed = 500;
	// character will go ragdoll if smash to sth static can be tree or rock...
	UPROPERTY(EditDefaultsOnly, Category = "Physics Interaction")
		bool RagdollOnJumpingIntoStaticObject = true;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float WalkingSpeed = 160;
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float JogginSpeed = 275;
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float RunningSpeed = 575;

	// The rate of player turning while moving 
	// * It will calculate walking and other types of movement 
	// * turning rate automatically based on this value
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float TurningRate = 300;

	// Different OwnerMovement Animations | Montages
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		FMovementAction MovementActions;

	// * 1 Applies Maximum slope effect depend on max walkable slope and -1 will do the opposite effect!
	// * in MaxWalkable slope angle character will stop moving if slope effect be 1...
	// * this doesn't apply to Running
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Slope", meta = (ClampMin = "-1",ClampMax="1"))
		float SlopeEffectCoefficient = 0.25;
	// * Running Slope effect
	// @see SlopeEffectCoefficient
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Slope", meta = (ClampMin = "-1", ClampMax = "1"))
		float RunningSlopeEffectCoefficient = 0.4;

	// Y is Up Velocity and X is Forward Velocity
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Jumping")
		FVector2D JumpVelocity = FVector2D(575, 350);
	//The Velocity that player will hard land on the ground
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Jumping")
		float MinFallingSpeedToHardLand = 700;

	UPROPERTY(EditDefaultsOnly, Category = "Movement|Crouching")
		float CrouchingSpeed = 200;
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Crouching")
		float CrouchCapsuleHalfHeight = 65;
	UPROPERTY(EditDefaultsOnly, Category = "Movement|Crouching")
		float DodgingSpeed = 500;

	UPROPERTY(EditDefaultsOnly, Category = "Movement|Proning")
		float ProningSpeed = 25;

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		EMovementState CurrentMovementState = EMovementState::Idle;

	// ** IK BONES **
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		FName LeftFootBoneName = "LeftFoot";
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		FName RightFootBoneName = "RightFoot";
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		float IKAdjustOffset = 1;
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		float IKTraceDistance = 30;
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		float IKHipsInterpSpeed = 5;
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		float IKFeetInterpSpeed = 12;
	UPROPERTY(EditDefaultsOnly, Category = "Bones|Feet IK")
		float IKTimeout = 0.5;

	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		FRotator LeftFootRotation = FRotator().ZeroRotator;
	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		FRotator RightFootRotation = FRotator().ZeroRotator;
	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		float IKHipsOffset = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		float IKLeftEffectorLocation = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		float IKRightEffectorLocation = 0;
	UPROPERTY(BlueprintReadOnly, Category = "Bones|Feet IK")
		bool bDisableIK = false;

	// Blend Space Value (Walk-Jog-Run)	
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
		float WalkingBlendSpaceValue = 0;
	// Blend Space Value (Prone) | I'll be only 0 or 1 and thats how we handle it in anim blueprint
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
		float ProneBlendSpaceValue = 0;

	// Evenets
	UFUNCTION()
		void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);
	// Acts like all montages ended...
	UFUNCTION()
		void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SyncActorLocationWithRagdoll();

	/* EVENTS */
	// Gets up from ragdoll state; should be implemented in anim blueprint class.
	UPROPERTY(BlueprintAssignable, Category = "Ragdoll")
		FGetupDelegate OnGetup;
	// calls when getting up is finished (when montage ended...)
	UPROPERTY(BlueprintAssignable, Category = "Ragdoll")
		FGotupDelegate OnGotup;

	UFUNCTION(BlueprintPure)
		void GetMovementValues(float& MoveForward, float& MoveRight);

	UFUNCTION(BlueprintPure, meta = (BlueprintThreadSafe))
		EMovementState GetMovementState() { return CurrentMovementState; }

	//Axis
	void MoveForward(float Value);
	void MoveRight(float Value);
	//Actions
	void Jump();
	void CrouchPressed();
	void CrouchReleased();
	void PronePressed();

	void Sprint();
	void UnSprint();
	void PauseSprint();
	bool ResumeSprint();

	// Switches between Walk/Jog
	void ToggleWalkMode();
	void ToggleRagdoll();
	void ToggleAutoRun();

	void Crouch();
	void UnCrouch();

	void Prone();
	void UnProne();

	void OnLanded(FHitResult Hit);
	void OnFalling();

	float GetSpeed() { return CurrentSpeed; }
	float GetSurfacePitch() { return SurfacePitch; }

	// Moves the player forcefuly for a certain duration
	// @MovementDirection shouln't be normalized!
	// @Duration 0 means lifetime.
	void MoveForDuration(FVector MovementDirection, float Speed, float Duration = 0, bool bStopImmediately = false, bool bFreezeRotation = false);
	// Deactivates move for duration effect.
	void StopForcedMovement();
	void StopMovementForcefully() { MoveForDuration(FVector(), 0); }

	// @bAutoDeactivate if true will deactivate ragdoll after it stopped moving with a small delay...
	void ActivateRagdoll(bool bAutoDeactivate = true);

	// X is Radius and Y is the HalfHeight of capsule
	FVector2D GetDefaultCapsuleSize() { return CapsuleSize; }

	// Returns move right input key value | Ex GetAnySideMovement()
	float GetMoveRightValue() { return MoveRightValue; }

	/* MOVEMENT STATES CHECKERS */
	//
	inline bool IsIdle() { return CurrentMovementState == EMovementState::Idle; }
	inline bool IsCrouching() { return CurrentMovementState == EMovementState::Crouching; }
	inline bool IsProning() { return CurrentMovementState == EMovementState::Proning; }
	inline bool IsWalking() { return CurrentMovementState == EMovementState::Walking; }
	inline bool IsJogging() { return CurrentMovementState == EMovementState::Jogging; }
	inline bool IsRunning() { return CurrentMovementState == EMovementState::Running; }
	inline bool IsRagdoll() { return CurrentMovementState == EMovementState::Ragdoll; }
	inline bool IsDodging() { return CurrentMovementState == EMovementState::Dodging; }
	// EMovementState::Falling || EMovementState::Jumping States returns true!!
	inline bool IsFalling() { return CurrentMovementState == EMovementState::Falling || CurrentMovementState == EMovementState::Jumping; }
	//bool IsLanding() { return CurrentMovementState == EMovementState::Landing; }

	// calls HumanDataComponent::Cancel on movement.
	void CancelActionOnMovement(bool bEnabled) { bCanelActionOnMovement = bEnabled; }

	bool GetCarryMode() { return bCarryMode; }
	void SetCarryMode(bool bValue, APhysicalItem* InCarryingPhItem = nullptr) { bCarryMode = bValue, CarryingPhItem = InCarryingPhItem; }
	APhysicalItem* GetCarryingPhItem() { return CarryingPhItem; }

	UFUNCTION(BlueprintPure, meta = (BlueprintThreadSafe))
		bool IsStrafeMovementEnabled();
	void SetRotationMode(ERotationMode InRotationMode);
	void RevertRotationMode() { SetRotationMode(ExRotationMode); }
	ERotationMode GetRotationMode() { return RotationMode; }

private:

	UFUNCTION()
		void OnCapsuleHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void Dodge();
	float DodgeCooldown = 0.5, DodgeTimer = 10;

	EMovementState GetWalkMode();

	const float MainTimerRefreshInterval = 0.05;
	FTimerHandle MainTimer;
	void Timer();

	ERotationMode RotationMode, ExRotationMode;
	void FaceControlRotation(float DeltaTime);

	/** IK BONES **/
	// All IK Bones actions
	void UpdateIKBones();
	// Line traces of feet for IKBones
	bool IKFootTrace(float Distance, FName SocketName, float& Offset, FRotator& ImpactNormalRotation);
	// Indicates that where is capsule half height reset to it's default due to IK code
	bool bIsIKReset = false;
	float IKTimer = 0;
	void ActivateFeetIK() { bDisableIK = false, IKTimer = 0; }
	void DeactivateFeetIK(bool ResetCapHeight = true) { bDisableIK = true, bIsIKReset = !ResetCapHeight; }

	UWorld* World = nullptr;
	UCharacterMovementComponent* CharacterMovement = nullptr;
	UProneMovementComponent* ProneMovement = nullptr;
	class UHumanCameraManagerComponent* OwnerCamera = nullptr;
	class UHumanCombatComponent* OwnerCombat = nullptr;
	class UHumanDataComponent* OwnerData = nullptr;

	AHumanCharacter* Owner = nullptr;
	UCapsuleComponent* OwnerRoot = nullptr;

	class UAnimInstance* OwnerAnimInst;
	/** Animation - Montage **/
	void PlayMontage(UAnimMontage* Montage, FName SectionName = NAME_None);

	void UpdateCapsuleSize();
	// Default Capsule Size | X is Radius and Y is the HalfHeight of capsule
	FVector2D CapsuleSize = FVector2D(0);

	float DefaultMeshZ = 0;

	// Checks if has have enough space to prone!
	bool CanProne();
	// Checks if character has enough space to stand up from crouch or prone
	bool CanStandUp(float Height);

	// *** RAGDOLL ***
	bool bIsFacingUp = false;
	// will be called after getup montage finished.
	void OnRagdollEnded();
	void DeactivateRagdoll();
	float RagdollTimer = 20;
	float RagdollTimeout = 0;
	const float RagdolMaxlDelay = 2;

	float CurrentSpeed = 0;

	// Related to MoveForDuration()
	// Elapsed time since movement start
	float ForcedMovementTimer = 100;
	float ForcedMovementDuration = 0;
	bool bIsForcedRotationMode = false;
	FVector ForcedMovementDirection;
	bool MovePlayer = false;

	// Calculates SurfacePitch and GroundDirection aw
	void CalculateSurfaceAngle();
	float SurfacePitch = 0;
	FVector GroundDirection;
	void ApplySlopeEffect();
	float SlopeEffect = 1;
	FTimerHandle SlopeUpdateTimer;

	float MoveRightValue = 0, MoveForwardValue = 0;

	// the time to animation blueprint recognize jump state...
	float JumpTimeout = 0.1;
	// player can't jump within this time range
	float JumpCooldown = 0.3, JumpTimer = 10;

	// Deals with combat for sprinting like triggering aiming and combat mode...
	void DisableCombatMode();

	// @false : means character will jog on default movement
	// @true : means character will walk on default movement
	bool bWalkMode = false;

	bool bIsRunKeyDown = false;

	bool bAutoRun = false;

	bool bCanControlPawn = true;

	bool bCanelActionOnMovement = false;

	// Carriage
	bool bCarryMode = false;
	APhysicalItem* CarryingPhItem = nullptr;

	EMovementState PendingMovementState = EMovementState::None;
	void HandlePendingMovementState();

	FMontageInfo PendingMontage = FMontageInfo();
	void HandlePendingMontage();

	// MovementStateLogs Holds last MaxLogsCount records of movement state changes
	static const int MAX_LOGS_COUNT = 10;
	int MSLogsCount = 0;
	EMovementState MSLogs[MAX_LOGS_COUNT];
	void PrintMSLogs();

	// Add OwnerMovement State Log
	void SetMovementState(EMovementState MState, bool bUpdateMaxSpeed = true);
	// Updates MaxSpeed of Player depend on the current movement state
	void UpdateMaxSpeed();

};