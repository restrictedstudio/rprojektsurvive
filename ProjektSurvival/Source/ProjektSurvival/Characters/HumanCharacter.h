// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HumanCharacter.generated.h"

class UProneMovementComponent;
class UHumanCameraManagerComponent;
class UHumanDataComponent;
class UHumanCombatComponent;

UENUM(BlueprintType)
enum class ERotationMode : uint8 {
	NONE,
	OrientToMovement,
	AlwaysFaceControlRotation,
	FreezeRotation
};

UCLASS()
class PROJEKTSURVIVAL_API AHumanCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AHumanCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// ********* SETTINGS ************
	UPROPERTY(EditAnywhere, Category = "Settings")
		float MouseSensetivity = 100;

	/* ########### Components ########### */

	// ********* MOVEMENT ************
	UPROPERTY(VisibleAnywhere, Category = "Component", meta = (BlueprintThreadSafe))
		class UHumanMovementComponent* MovementComp = nullptr;
	// ********* PRONE ************
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UProneMovementComponent* ProneMovementCmp = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* ProneCollision = nullptr;

	// ********* DATA ************
	UPROPERTY(VisibleAnywhere, Category = "Component", meta = (BlueprintThreadSafe))
		UHumanDataComponent* DataComp = nullptr;

	// ********* COMBAT ************
	UPROPERTY(VisibleAnywhere, Category = "Component", meta = (BlueprintThreadSafe))
		UHumanCombatComponent* CombatComp = nullptr;
	// Helps to align weapon in crosshair's direction
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* AimAdjuster = nullptr;
	// Blocked Widget (for weapons ...)
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
		class UWidgetComponent* BlockedWidget;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* RightHandCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* LeftHandCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* RightFootCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* LeftFootCollision = nullptr;

	// ********* PHYSICS ************
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UPhysicalAnimationComponent* PhysicalAnimation = nullptr;

	// ********* CAMERA ************
	// This component handles any camera related stuff
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UHumanCameraManagerComponent* CameraManagerComp = nullptr;
	// Third person camera boom
	UPROPERTY(VisibleAnywhere, Category = "Component")
		class USpringArmComponent* TPSpringArm = nullptr;
	// Third person camera
	UPROPERTY(VisibleAnywhere, Category = "Component")
		class UCameraComponent* TPCamera = nullptr;
	// First person camera
	UPROPERTY(VisibleAnywhere, Category = "Component")
		class UCameraComponent* FPCamera = nullptr;

	// ********* ANIMATION ************		
	UPROPERTY(BlueprintReadOnly, Category = "Animation")
		FRotator NeckRotation = FRotator();
	UPROPERTY(BlueprintReadOnly, Category = "Animation")
		FRotator WaistRotation = FRotator();

	// in seconds | blinks with a random time with max of this var
	UPROPERTY(EditDefaultsOnly, Category = "Animation|MorphTargets")
		float MaxBlinkingInterval = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Animation|Bones")
		float NeckRotationInterpolatinSpeed = 7;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Bones")
		int MaxNeckRotaionPitchAngle = 30;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Bones")
		int MaxNeckRotaionYawAngle = 55;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Bones")
		float MaxLeaningAngle = 4;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Bones")
		int MaxWaistRotationAngle = 25;

	void Falling() override;

protected:

	void Landed(const FHitResult& Hit) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float CalculateDeltaAngle(FVector Vec1, FVector Vec2, bool ContainZAxis = false);

	// Disables All Components	
	void Die();

	// Human OwnerMovement Component
	UFUNCTION(BlueprintPure, Category = "Components", meta = (BlueprintThreadSafe))
		UHumanMovementComponent* GetMovement() { return MovementComp; }
	// Human Prone OwnerMovement Component
	UFUNCTION(BlueprintPure, Category = "Components", meta = (BlueprintThreadSafe))
		UProneMovementComponent* GetProneMovement() { return ProneMovementCmp; }
	// Human Camera Manager Component
	UFUNCTION(BlueprintPure, Category = "Components", meta = (BlueprintThreadSafe))
		UHumanCameraManagerComponent* GetCameraManager() { return CameraManagerComp; }
	// Human Data Component
	UFUNCTION(BlueprintPure, Category = "Components", meta = (BlueprintThreadSafe))
		UHumanDataComponent* GetData() { return DataComp; }
	// Human Combat Component
	UFUNCTION(BlueprintPure, Category = "Components", meta = (BlueprintThreadSafe))
		UHumanCombatComponent* GetCombat() { return CombatComp; }
	// Physical Animation Component for Generating hit effect on characters body
	UPhysicalAnimationComponent* GetPhysicalAnimation() { return PhysicalAnimation; }

	// Calls a HumanMovementCompoent Function.
	void ActivateRagdoll(bool bAutoDeactivate = true);

	/* Clicking Inputs thesea are technically Left/Right Mouse Clicks */
	// *********** Combat ************
	void AttackKeyPressed();
	void AttackKeyReleased();
	void AimKeyPressed();
	void AimKeyReleased();

	// *********** Bones ************
	void DisableNeckRotation(bool bDisabled) { bDisableNeckRotation = bDisabled; }

private:

	UWorld* World = nullptr;	

	void InitializeProneMovement();

	// *********** Bones ************
	void SetNeckRotation();
	void SetWaistRotation();
	bool bDisableNeckRotation = false;
	bool bDisableWaistRotation = false;

	// *********** Morph Targets ************
	float ElapsedTimeSinceLastBlink = 50;
	float BlinkValue = 0;
	// the time between 2 blinks
	int BlinkingInterval = 2;
	// Sets the morph targets of eyes and eyelashes
	void Blink();
	bool bAreEyesOpen = true;

	// ******** COMBAT **********
	/* Add combat needed components... */
	void SetupCombat();

	// ******** CAMERA **********
	/* Adds SpringArm,Camera and etc... */
	void SetupCamera();
};