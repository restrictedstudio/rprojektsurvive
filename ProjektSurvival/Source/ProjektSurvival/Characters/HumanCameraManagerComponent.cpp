// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "HumanCameraManagerComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/PlayerCameraManager.h"
#include "HumanMovementComponent.h"
#include "HumanCombatComponent.h"
#include "HumanCharacter.h"
#include "Camera/CameraAnimInst.h"
#include "../Controllers/PController.h"

// Sets default values for this component's properties
UHumanCameraManagerComponent::UHumanCameraManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetComponentTickEnabled(false);

	Owner = Cast<AHumanCharacter>(GetOwner());
}

// Called when the game starts
void UHumanCameraManagerComponent::BeginPlay()
{
	Super::BeginPlay();	

	World = GetWorld();
	OwnerMovement = Owner->GetMovement();
	OwnerCombat = Owner->GetCombat();
	Controller = Owner->GetController<APController>();

	// Camera Bounds
	CameraManager = UGameplayStatics::GetPlayerCameraManager(World, 0);
	CameraManager->ViewPitchMax = Max3rdPCameraPitch;
	CameraManager->ViewPitchMin = Min3rdPCameraPitch;

	// Third Person SpringArm Setups
	TPSpringArm->SetAbsolute(true, false, false);
	TPSpringArm->SetWorldLocation(Owner->GetActorLocation());
	TPSpringArmDefaultArmLength = TPSpringArm->TargetArmLength;

	// Applying the default viewport
	TPSpringArm->TargetArmLength = TPSpringArmDefaultArmLength + Viewport * ViewportsDelta;

	ActiveCamera = TPSpringArm;

	FPCameraDefaultLocation = FPCamera->GetRelativeLocation();

	// Starts the main timer.
	World->GetTimerManager().SetTimer(MainTimer, this, &UHumanCameraManagerComponent::Timer, MainTimerRefreshInterval, true);

	SetCameraFollowSpeed(0);
}

// Called every MainTimerRefreshInterval second
void UHumanCameraManagerComponent::Timer()
{
	// Delta Time for each call
	float DT = MainTimerRefreshInterval;

	if (bIsAutoPilotEnabled && CameraAutoPilotTimer < AutoPilotTriggerTime)
		CameraAutoPilotTimer += DT;
}

// Called every frame
void UHumanCameraManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	// Updates Camera Delta Angle with Character every frame
	CameraDeltaAngle = Owner->CalculateDeltaAngle(Owner->GetActorForwardVector(), Owner->GetControlRotation().Vector());

	SyncTPCameraLocationWithCharacter(DeltaTime);

	RotateToDesiredSynchronizer(DeltaTime);

	AutoPilotManager();

	UpdateAimingPitch(DeltaTime);

	UpdateFPCameraDriver();
}

void UHumanCameraManagerComponent::UpdateFPCameraDriver()
{
	// First Person Driver Mode (for Aiming)
	if (bIsFPCameraDriverActive || FPCameraDriverAlpha > 0)
	{
		if (!FPCameraDriver) return;

		FPCameraDriverAlpha = FMath::Clamp<float>(FMath::FInterpTo(FPCameraDriverAlpha, FPCameraDriverAlpha + (bIsFPCameraDriverActive ? 1 : -1), World->DeltaTimeSeconds, 5), 0, 1);
		FPCamera->SetRelativeLocation(
			FMath::Lerp<FVector>(FPCameraDefaultLocation,
				UKismetMathLibrary::MakeRelativeTransform(FPCameraDriver->GetComponentTransform(), Owner->GetMesh()->GetSocketTransform(TEXT("Camera"))).GetLocation()
				, FPCameraDriverAlpha));
	}
}

// Syncs camera with desired rotation from RotateTo()
void UHumanCameraManagerComponent::RotateToDesiredSynchronizer(float DeltaTime)
{
	if (!AutoRotation.bIsRotatingToDesiredRotation) return;

	// Current rotation
	FRotator ControlRotation = Owner->GetControlRotation();

	// Delta Rotation for each Frame
	FVector2D DeltaRotation = AutoRotation.RotationRate * 100 * DeltaTime;

	// Finding new Pitch & Yaw
	float Yaw = FMath::FixedTurn(ControlRotation.Yaw, AutoRotation.DesiredRotation.Yaw, DeltaRotation.X);
	float Pitch = FMath::FixedTurn(ControlRotation.Pitch, AutoRotation.DesiredRotation.Pitch, DeltaRotation.Y);

	// Applying Pitch & Yaw
	Controller->SetControlRotation(FRotator(Pitch, Yaw, 0));

	// Checking whether rotation's over or not
	if (Owner->GetControlRotation().Equals(AutoRotation.DesiredRotation, 1))
		StopAutoRotation();
}

// Syncs TP SpringArm Location with Character Location via Vector Interpolation
void UHumanCameraManagerComponent::SyncTPCameraLocationWithCharacter(float DeltaTime)
{
	FVector TPCamTargetLocation = Owner->GetActorLocation();
	TPCamTargetLocation.Z += CameraDefaultHeight;

	FVector TPCamCurrentLocation = TPSpringArm->GetComponentLocation();

	// Causes Camera random annoying shakes.
	//if (TPCamCurrentLocation.Equals(TPCamTargetLocation, 1)) return;

	// Experiential Value
	constexpr float SpeedDivisionCoefficient = 50;
	TPSpringArm->SetWorldLocation(FMath::VInterpTo(TPCamCurrentLocation, TPCamTargetLocation, DeltaTime, CameraFollowSpeed + OwnerMovement->GetSpeed() / SpeedDivisionCoefficient));
}

void UHumanCameraManagerComponent::ChangeViewport()
{
	// Modulus Algorithm
	Viewport = ++Viewport % ViewportsNum;

	float ExArmLength = TPSpringArm->TargetArmLength;
	TPSpringArm->TargetArmLength = TPSpringArmDefaultArmLength + Viewport * ViewportsDelta;
	// This will make a smooth transition between viewports by taking advantage of the absolute location of TP SpringArm
	TPSpringArm->AddWorldOffset(Owner->GetControlRotation().Vector() * (TPSpringArm->TargetArmLength - ExArmLength));

	//TODO Increase interp speed on aiming cuz its so important to be accurate.
	//TODO Implement FP Camera when you were ready
}

void UHumanCameraManagerComponent::ToggleLookBehind()
{
	if (OwnerCombat->IsAiming()) return;

	bIsLookingBehind = !bIsLookingBehind;

	// Revereses The ArmLength and Camera Rotation
	TPSpringArm->TargetArmLength *= -1;
	TPCamera->AddLocalRotation(FRotator(0, 180, 0));
	// It will maintain camera rotation somehow
	Controller->SetControlRotation(Owner->GetActorRotation());
}

void UHumanCameraManagerComponent::ActivateFreelook()
{
	if (bIsFreelooking || OwnerCombat->IsAiming()) return;

	bIsFreelooking = true;
	FreelookStartYaw = Owner->GetControlRotation().Yaw;

	// Disabling auto pilot mode on freelooking
	SetAutoPilotModeEnabled(false);
	
	OwnerMovement->SetRotationMode(ERotationMode::FreezeRotation);
}

void UHumanCameraManagerComponent::DeactivateFreelook()
{
	if (!bIsFreelooking) return;

	FRotator FreelookStart = Owner->GetControlRotation();
	FreelookStart.Yaw = FreelookStartYaw;
	RotateTo(FAutoRotation(FreelookStart, FVector2D(3.5), &bIsFreelooking));

	// Enabling auto pilot mode back
	SetAutoPilotModeEnabled(true);
}

void UHumanCameraManagerComponent::TwistCamera(float Value)
{
	if (!Value || bLockCamera) return;

	Owner->AddControllerYawInput(Value);

	// Each Mouse OwnerMovement Resets Camera Auto Pilot Timer
	CameraAutoPilotTimer = 0;

	if (AutoRotation.bIsRotatingToDesiredRotation)
		StopAutoRotation();	
}

void UHumanCameraManagerComponent::ElevateCamera(float Value)
{
	MouseYValue = bLockCamera ? 0 : Value;

	if (!Value || bLockCamera) return;

	Owner->AddControllerPitchInput(bIsLookingBehind ? Value : -Value);
	CameraPitch = ActiveCamera->GetComponentRotation().Pitch;

	// Each Mouse OwnerMovement Resets Camera Auto Pilot Timer
	CameraAutoPilotTimer = 0;

	if (AutoRotation.bIsRotatingToDesiredRotation)
		StopAutoRotation();
}

// Adds Custom Pitch to camera
void UHumanCameraManagerComponent::AddCamOffset(float Value)
{
	if (!Value || bLockCamera) return;

	float ExCameraPitch = CameraPitch;

	Owner->AddControllerPitchInput(Value);
	CameraPitch = ActiveCamera->GetComponentRotation().Pitch;

	// Syncing the aiming direction with crosshair
	if (!bUpdateAimPitch)
		AimingPitch += (CameraPitch - ExCameraPitch);

	// Resets Camera Auto Pilot Timer
	CameraAutoPilotTimer = 0;

	if (AutoRotation.bIsRotatingToDesiredRotation)
		StopAutoRotation();
}

void UHumanCameraManagerComponent::StopAutoRotation()
{
	AutoRotation.bIsRotatingToDesiredRotation = false;

	if (AutoRotation.bEndIndicator != nullptr)
	{
		OnAutoRotationEnded();

		*AutoRotation.bEndIndicator = false;
		AutoRotation.bEndIndicator = nullptr;
	}
}

void UHumanCameraManagerComponent::OnAutoRotationEnded()
{
	// Freelook ended.
	if (AutoRotation.bEndIndicator == &bIsFreelooking)
		OwnerMovement->RevertRotationMode();
}

// Manages Auto pilot related stuff
void UHumanCameraManagerComponent::AutoPilotManager()
{
	if (!OwnerMovement->GetSpeed() || CameraAutoPilotTimer < AutoPilotTriggerTime) return;

	FRotator AutoPilotRotation = Owner->GetActorRotation();
	const float RoundedSurfacePitch = FMath::RoundToInt(OwnerMovement->GetSurfacePitch() / 10) * 10;
	AutoPilotRotation.Pitch = RoundedSurfacePitch - AutoPilotDefaultPitch;

	RotateTo(FAutoRotation(AutoPilotRotation, AutoPilotRotationRate));
}

void UHumanCameraManagerComponent::SetCameras(USpringArmComponent* InTPSpringArm, UCameraComponent* InTPCamera, UCameraComponent* InFPCamera)
{
	TPSpringArm = InTPSpringArm;
	TPCamera = InTPCamera;
	FPCamera = InFPCamera;
}

void UHumanCameraManagerComponent::UpdateAimingPitch(float DeltaTime)
{
	if (!bUpdateAimPitch) return;

	// Uses Aim Adjuster Socket on skeletal mesh to find the aiming rotation		
	float AimingDeltaAngle = 0;

	// Setting Cam pitch in -90 to +90 range
	float CamPitch = Owner->GetControlRotation().Pitch;
	if (CamPitch >= 180) CamPitch -= 360;

	if (OwnerCombat->IsAiming())
		AimingDeltaAngle = CamPitch - OwnerCombat->GetAimAdjuster();
	AimingPitch = FMath::FInterpTo(AimingPitch, CamPitch + AimingDeltaAngle, DeltaTime, 6);

	if (!OwnerCombat->IsAiming())
		AimingPitch = 0;//, bUpdateAimPitch = false;
	//AimingPitch = FMath::FInterpTo(AimingPitch, 0, World->DeltaTimeSeconds, 1);
}

void UHumanCameraManagerComponent::RotateTo(FAutoRotation DesiredRotation)
{
	AutoRotation = DesiredRotation;
}

void UHumanCameraManagerComponent::PlayCameraShake(ECameraShakeType ShakeType)
{
	switch (ShakeType)
	{
	case ECameraShakeType::Running:

		if (!RunningCameraShake)
			RunningCameraShake = CameraManager->PlayCameraShake(CameraShakeOfRunning);
		break;

	case ECameraShakeType::Falling:

		if (!FallingCameraShake)
			FallingCameraShake = CameraManager->PlayCameraShake(CameraShakeOfFalling);
		break;

	case ECameraShakeType::Dodging:
		CameraManager->PlayCameraShake(CameraShakeOfDodging);
		break;

	case ECameraShakeType::Hardlanding:
		CameraManager->PlayCameraShake(CameraShakeOfHardLanding);
		break;
	}
}

void UHumanCameraManagerComponent::StopCameraShake(ECameraShakeType ShakeType)
{	
	// These are infinitie camera shakes that needs to be stopped.
	switch (ShakeType)
	{
	case ECameraShakeType::Running:

		if (RunningCameraShake)
			CameraManager->StopCameraShake(RunningCameraShake, false);
		RunningCameraShake = nullptr;
		break;

	case ECameraShakeType::Falling:

		if (FallingCameraShake)
			CameraManager->StopCameraShake(FallingCameraShake, false);
		FallingCameraShake = nullptr;
		break;

	case ECameraShakeType::All:
		CameraManager->StopAllCameraShakes(false);
		break;
	}
}

// @return Active camera location
FVector UHumanCameraManagerComponent::GetViewportLocation()
{
	return (IsFirstPerson() ? FPCamera : TPCamera)->GetComponentLocation();
}