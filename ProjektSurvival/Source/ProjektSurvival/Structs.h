// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Structs.generated.h"

class UAnimMontage;
class UItem;
class AWeapon;
class UItem_Weapon;
class APhysicalItem;

USTRUCT(BlueprintType)
struct FMovementAnimation
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideIdle = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideIdle"))
		FName IdleSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideWalking = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideWalking"))
		FName WalkingSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideJoging = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideJoging"))
		FName JoggingSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideStrafeRight = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideStrafeRight"))
		FName StrafeRightSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideStrafeLeft = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideStrafeLeft"))
		FName StrafeLeftSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideRunning = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideRunning"))
		FName RunningSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideJumping = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideJumping"))
		FName JumpingSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideFalling = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideFalling"))
		FName FallingSectionName;

	// Crouching

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideCrouchIdle = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideCrouchIdle"))
		FName CrouchIdleSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideCrouchWalking = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideCrouchWalking"))
		FName CrouchWalkingSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideCrouchStrafeRight = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideCrouchStrafeRight"))
		FName CrouchStrafeRightSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideCrouchStrafeLeft = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideCrouchStrafeLeft"))
		FName CrouchStrafeLeftSectionName;

	// Proning

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideProneIdle = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideProneIdle"))
		FName ProneIdleSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideProneMove = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideProneMove"))
		FName ProneMoveSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideProneStrafeRight = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideProneStrafeRight"))
		FName ProneStrafeRightSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bOverrideProneStrafeLeft = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bOverrideProneStrafeLeft"))
		FName ProneStrafeLeftSectionName;

	FMovementAnimation()
	{
		AnimMontage = nullptr;
		IdleSectionName = TEXT("Idle");
		WalkingSectionName = TEXT("Walk");
		JoggingSectionName = TEXT("Jog");
		StrafeRightSectionName = TEXT("StrafeRight");
		StrafeLeftSectionName = TEXT("StrafeLeft");
		RunningSectionName = TEXT("Run");
		JumpingSectionName = TEXT("Jump");
		FallingSectionName = TEXT("Fall");

		CrouchIdleSectionName = TEXT("CrouchIdle");
		CrouchWalkingSectionName = TEXT("CrouchWalk");
		CrouchStrafeRightSectionName = TEXT("CrouchStrafeRight");
		CrouchStrafeLeftSectionName = TEXT("CrouchStrafeLeft");

		ProneIdleSectionName = TEXT("ProneIdle");
		ProneMoveSectionName = TEXT("ProneMove");
		ProneStrafeRightSectionName = TEXT("ProneStrafeRight");
		ProneStrafeLeftSectionName = TEXT("ProneStrafeLeft");
	}
};

USTRUCT(BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItem* Item;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Amount;

	FInventorySlot()
	{
		ID = -1;
		Item = nullptr;
		Amount = 0;
	}

	FInventorySlot(int IDToSet, UItem* ItemToSet, int AmountToSet)
	{
		ID = IDToSet;
		Item = ItemToSet;
		Amount = AmountToSet;
	}
};

USTRUCT(BlueprintType)
struct FDamage
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Head;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Body;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Arms;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Legs;

	FDamage()
	{
		Head = 100;
		Body = 30;
		Arms = 15;
		Legs = 15;
	}

	FDamage(float Value)
	{
		Head = Value;
		Body = Value;
		Arms = Value;
		Legs = Value;
	}
};

USTRUCT(BlueprintType)
struct FProximitySlot
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItem* Item;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APhysicalItem* PhItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Amount;

	FProximitySlot()
	{
		ID = -1;
		Item = nullptr;
		PhItem = nullptr;
		Amount = 0;
	}

	FProximitySlot(int IDToSet, UItem* ItemToSet, APhysicalItem* PhItemToSet, int AmountToSet)
	{
		ID = IDToSet;
		Item = ItemToSet;
		PhItem = PhItemToSet;
		Amount = AmountToSet;
	}
};

UENUM(BlueprintType)
enum class EHotbarSlotLocation : uint8 {
	NONE, // this one is for search fucntions
	HeavyWeapon1,
	HeavyWeapon2,
	LightWeapon1,
	LightWeapon2,
	Throwable1,
	Throwable2,
	Throwable3,
	Throwable4
};

USTRUCT(BlueprintType)
struct FHotbarSlot
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItem_Weapon* Item;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AWeapon* WeaponActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EHotbarSlotLocation SlotLocation;

	FHotbarSlot()
	{
		ID = -1;
		Item = nullptr;
		WeaponActor = nullptr;
	}

	FHotbarSlot(int IDToSet, EHotbarSlotLocation SlotLocationToSet)
	{
		ID = IDToSet;
		Item = nullptr;
		WeaponActor = nullptr;
		SlotLocation = SlotLocationToSet;
	}

	FHotbarSlot(int IDToSet, UItem_Weapon* ItemToSet, AWeapon* WeaponActorToSet, EHotbarSlotLocation SlotLocationToSet)
	{
		ID = IDToSet;
		Item = ItemToSet;
		WeaponActor = WeaponActorToSet;
		SlotLocation = SlotLocationToSet;
	}
};


/**
 * Global structs are held in this class
 * EHotbarSlotLocation is also here -_- i had no choice...
 */
class PROJEKTSURVIVAL_API Structs
{
	// Empty
};
