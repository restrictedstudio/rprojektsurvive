// Fill out your copyright notice in the Description page of Project Settings.


#include "PController.h"
//#include "DrawDebugHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "../UI/InventoryWidget.h"
#include "../UI/PlayerHUDWidget.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"

void APController::BeginPlay()
{

}

void APController::CreateInventoryUIWidget()
{
	if (!ensure(InventoryUIWidgetBP)) return;

	InventoryUI = CreateWidget<UInventoryWidget>(this, InventoryUIWidgetBP);
	InventoryUI->AddToViewport();
}

void APController::CreateHUDWidget()
{
	if (!ensure(PlayerHUDBP) || !GetPawn()->GetClass()->IsChildOf<AHumanCharacter>()) return;

	HUD = CreateWidget<UPlayerHUDWidget>(this, PlayerHUDBP);
	HUD->Setup(GetPawn<AHumanCharacter>()->GetData());
	HUD->AddToViewport();
}


void APController::Tick(float DeltaTime)
{
	GetSightRayHitLocation();
}

void APController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	// CLEARING ALL WIDGETS
	UWidgetLayoutLibrary::RemoveAllWidgets(GetWorld());

	if (aPawn->GetClass()->IsChildOf<AHumanCharacter>())
	{
		// HUD
		CreateHUDWidget();
		// INVENTORY
		CreateInventoryUIWidget();
	}
}

FVector APController::GetSightRayHitLocation(float MaxLength) const
{
	// Get world location of linetrace through crosshair, true if hits landscape	
		// Find the crosshair position in pixel coordinates
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D ScreenLocation = FVector2D(ViewportSizeX * CrosshairXLocation, ViewportSizeY * CrosshairYLocation);

	// "De-project" the screen position of the crosshair to a world direction
	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		// Line-trace along that LookDirection, and see what we hit (up to max range)
		return GetLookVectorHitLocation(LookDirection, MaxLength);
	}

	return FVector(0);
}

void APController::ToggleCrosshairVisibility(bool bShow)
{
	if (!ensure(HUD) || !IsLocalPlayerController()) return;

	HUD->SetVisibility(bShow ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

// Gets the line of the trace in 200 radius of the camera location.
void APController::GetTraceLine(FVector& LineStart, FVector& LineEnd)
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D ScreenLocation = FVector2D(ViewportSizeX * CrosshairXLocation, ViewportSizeY * CrosshairYLocation);

	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		LineStart = PlayerCameraManager->GetCameraLocation() + (LookDirection * 200);
		LineEnd = PlayerCameraManager->GetCameraLocation() + (LookDirection * -200);
	}
	else
	{
		LineStart = FVector(0);
		LineEnd = FVector(0);
	}
}

void APController::GetItemTraceLine(FVector& LineStart, FVector& LookDirection)
{
	if (!ensure(GetWorld() && PlayerCameraManager)) return;

	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D ScreenLocation = FVector2D(ViewportSizeX * CrosshairXLocation, ViewportSizeY * CrosshairYLocation);

	GetLookDirection(ScreenLocation, LookDirection);
	LineStart = PlayerCameraManager->GetCameraLocation() + (LookDirection * 50);
}

FVector APController::GetLookVectorHitLocation(FVector LookDirection, float MaxLength) const
{
	if (!ensure(GetWorld() && PlayerCameraManager)) return FVector(0);

	FHitResult HitResult;
	FVector FinalTargetLocation;
	FVector  StartLocation = PlayerCameraManager->GetCameraLocation() + (LookDirection * 50);
	FVector  EndLocation = StartLocation + (LookDirection * (MaxLength ? MaxLength : LineTraceRange));
	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel4))
		FinalTargetLocation = HitResult.Location;
	else
	{
		// IF NOTHING WAS HIT
		EndLocation = StartLocation + (LookDirection * (MaxLength ? MaxLength : 2000));
		GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel4);
		FinalTargetLocation = HitResult.TraceEnd;
	}

	return FinalTargetLocation;
}

bool APController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldLocation; // To be discarded
	return  DeprojectScreenPositionToWorld(
		ScreenLocation.X,
		ScreenLocation.Y,
		CameraWorldLocation,
		LookDirection
	);
}
