// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PController.generated.h"

class UUserWidget;
class UInventoryWidget;
class UInventorySlotWidget;
class UInventoryContextMenuWidget;
class UPlayerHUDWidget;

/**
 *
 */
UCLASS()
class PROJEKTSURVIVAL_API APController : public APlayerController
{
	GENERATED_BODY()

public:

	// Returns crosshair hit location
	// @MaxLength = 0 uses the deafult length
	FVector GetSightRayHitLocation(float MaxLength = 0) const;
	void ToggleCrosshairVisibility(bool bShow);
	void GetTraceLine(FVector& LineStart, FVector& LineEnd);
	void GetItemTraceLine(FVector& LineStart, FVector& LookDirection);

	UInventoryWidget* GetInventoryUI() { return InventoryUI; }
	UPlayerHUDWidget* GetHUD() { return HUD; }

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UInventorySlotWidget> InventorySlotWidgetBP;
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UInventoryContextMenuWidget> InventoryContextMenuBP;

protected:
	void OnPossess(APawn* aPawn) override;	

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UInventoryWidget> InventoryUIWidgetBP;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UPlayerHUDWidget> PlayerHUDBP;

	UPROPERTY(EditDefaultsOnly)
		float CrosshairXLocation = 0.5;

	UPROPERTY(EditDefaultsOnly)
		float CrosshairYLocation = 0.5;

	UPROPERTY(EditDefaultsOnly)
		float LineTraceRange = 10000;

private:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void CreateInventoryUIWidget();	
	void CreateHUDWidget();	

	UPlayerHUDWidget* HUD = nullptr;
	UInventoryWidget* InventoryUI = nullptr;

	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;
	// @MaxLength = 0 uses the deafult length
	FVector GetLookVectorHitLocation(FVector LookDirection, float MaxLength = 0) const;
};