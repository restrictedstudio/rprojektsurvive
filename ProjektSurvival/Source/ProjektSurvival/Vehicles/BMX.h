
// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BMX.generated.h"

class UInputComponent;
class USpringArmComponent;
class UCameraComponent;
class UBoxComponent;
class UArrowComponent;
class USphereComponent;
class USkeletalMeshComponent;
class UFloatingPawnMovement;

/*
2-Wheel Vehicle Class
Note : Use "Front" prefix for Front Wheels!
*/

UCLASS()
class PROJEKTSURVIVAL_API ABMX : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABMX();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		float GetLeanAngle();

	UFUNCTION(BlueprintCallable)
		float GetGearsAngle();

	void UnPossessed() override;


protected:

	UFUNCTION(BlueprintCallable, Category = "Components Setup")
		void Initialise(TArray<UStaticMeshComponent*> WheelsMeshesToSet, USceneComponent* BlockageTracker);

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float WheelsTraceHeight = 50;

	UPROPERTY(EditDefaultsOnly, Category = "Actions")
		float MaxWheelieAngle = 120;

	UPROPERTY(EditDefaultsOnly, Category = "Steering")
		float MaxSteeringAngle = 40;

	UPROPERTY(EditDefaultsOnly, Category = "Steering")
		float MaxLeanAngle = 25;

	UPROPERTY(EditDefaultsOnly, Category = "Steering")
		float SteeringInterpSpeed = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Steering")
		float LeaningInterpSpeed = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Air")
		float AirControl = 50;

	UPROPERTY(EditDefaultsOnly, Category = "Air")
		float ElavationInterpSpeed = 1.0f;

	// in kilometer per hour (km/h)
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float TopSpeed = 100;

	// in kilometer per hour (km/h)
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float ReverseTopSpeed = 25;

	// in kilometer per hour (km/h)
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float GearsCount = 1;

	// Between 0.0 to 1.0
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float RelativeAccelaration = 0.2;

	// Between 0.0 to 1.0
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float RelativeRevereseAccelaration = 0.2;

	// Between 0.0 to 1.0
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float RelativeBrakePower = 0.5;

	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float RelativeNeutralPower = 2;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float BottomTraceLength = 75;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float FrontAndRearTracesLength = 200;

	UPROPERTY(EditDefaultsOnly, Category = "Physics")
		float MaxFallingSpeed = 2500;
	UPROPERTY(EditDefaultsOnly, Category = "Physics")
		float MaxAngularSpeed = 750;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UArrowComponent* DebugArrow = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* FrontTraceComponent = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* BackTraceComponent = nullptr;

	// Place it in front of the vehicle, Tracks if movement is blocked!
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* BlockageTrackerComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		USpringArmComponent* SpringArm = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCameraComponent* ThirdPersonCamera = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* FrontWheelBoxCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RearWheelBoxCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USphereComponent* FrontWheelSphereCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USphereComponent* RearWheelSphereCollision = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UFloatingPawnMovement* FloatingPawnComponent = nullptr;

	// Camera Variables	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
		float MaxCameraPitch = -75;
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
		float MinCameraPitch = 20;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float FallingArmLength = 200;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float FallingMaxCameraHeight = 130;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float FallingMinCameraHeight = -40;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float BrakingArmLength = -100;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float BoostingArmLength = 250;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float MovementArmLength = 150;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float CameraDefaultPitch = 20;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float TurningAngle = 10;
	UPROPERTY(EditAnywhere, Category = "Camera")
		float AutoCamSwitchTime = 3;	
	UPROPERTY(EditAnywhere, Category = "Camera")
		float ViewportsDelta = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Actions")
		float WheelieForcePower = 2000;

	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T1 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T2 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T3 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		bool bDrawDebugTraces = false;

	UFUNCTION(BlueprintCallable)
		float GetSteeringAngle();

	TArray<UStaticMeshComponent*> WheelsMeshes;
	UBoxComponent* BoxRoot = nullptr;
	//Velocity in KM/H
	float CurrentSpeed = 0;

	// Holds the moving direction (0 is Idle, 1 is going Forward, -1 is going Backward)
	float Orientation = 0;

	//Gets the projected ForwardVector on GroundSurface and updates the MovementDirection
	virtual void UpdateMovementDirection();
	// Cooldowns and Delays are calculated here
	virtual void Timer();
	const float MainTimerRefreshInterval = 0.085;
	FTimerHandle MainTimer;
	// Plays Wheels and Gears rotation animations
	virtual void RotateWheelsAndGears(float DeltaTime);
	// Calculates the orientation of the vehicle movement
	virtual void SetOrientation();
	// Absoulute Forward of the vehicle (No Z Offset)
	virtual void SetAbsouluteForward();
	virtual void UpdatePhysicVelocities();
	// Calculates SlopeEffect & ReverseSlopeEffect using SurfacePitch
	virtual void CalculateSlopeEffects();
	// Possession and UnPossession Codes
	virtual void PossessionManagerTick();
	// All the Camera Animations
	virtual void CameraAnimationsTick();
	// Camera shaking to feel the speed
	virtual void ShakeCamera();
	// Line Traces for SurfacePitch,OwnerMovement Blockage,Crashing Recognization and etc...	
	virtual void LineTracing();
	// Adds SpringArm,Camera and etc...
	virtual void SetupCamera();
	// OwnerMovement of Vehicle while it's UnPossessed
	virtual void UnPossesedMovement();
	// Find the angle between two vectors
	virtual int FindDeltaAngle(FVector Vec1, FVector Vec2, bool ContainZAxis = false);

	////OwnerMovement Functions
	//Axis
	virtual void MoveForward(float Value);
	virtual void MoveRight(float Value);
	virtual void Elevate(float Value);
	virtual void LookBehind();
	virtual void LookFront();
	virtual void ChangeViewport();

	//Camera Navigation Functions
	void CamTwisting(float Value);
	void ElevateCamera(float Value);

	UInputComponent* PlayerInputs = nullptr;
	UWorld* World = nullptr;

	bool bIsFalling = false;
	bool bValidGroundDirection = false;
	bool bIsBlocked = false;
	bool bIsStuck = false;
	bool bIsNeutralElevating = false;
	bool bShakeCamera = false;
	bool bRollClockwise = false;
	bool bResetFallingCamera = false;
	bool bResetJumpCamera = false;	
	bool bLookBehind = false;
	bool bResetCameraRotation = false;

	float AirTime = 50;
	float ElapsedTimeSinceStopped = 50;
	float ElapsedTimeSinceCameraManualControl = 50;
	float SteeringAngle = 0;
	float LastSpeed = 0;
	float Acceleration = 0;
	float DefaultTurningBoost = 0;
	float CrashAlpha = 0;
	float TotalMass = 1;
	float DefaultArmLength;
	float MouseSensetivity = 100;
	float Viewport = 0;

	int SurfacePitch = 0;

	FVector GroundDirection = FVector::ZeroVector;
	FVector MovementDirection = FVector::ZeroVector;
	FVector AbsouluteForward = FVector::ZeroVector;
	FVector CrashImpulse = FVector::ZeroVector;

	float const MAX_ACCELARATION = 5000;
	float const FALLING_TIMEOUT = 2;
	float const BLOCKAGE_TRACE_LENGTH = 8;
	float const BLOCKAGE_IMPULSE = 2500;
	float const REVERSE_MOVE_DELAY = 0.1;	
private:

	// Between 0 to 100
	UPROPERTY(EditDefaultsOnly, Category = "Engine")
		float JumpPower = 10;

	void Jump();
	void JumpTick();

	FVector JumpStartVec;
	
	USkeletalMeshComponent* SKMesh = nullptr;

	bool bIsPossessed = true;
	bool bIsBraking = false;
	bool bIsAbleToMove = true;
	bool bIsWheelieing = false;
	bool bIsBackWheelieing = false;
	bool bWheelieForJump = false;
	bool bAdjustRotationForJump = false;
	bool bIsJumping = false;
	bool bIsBoosting = false;

	int BoostCounter = 0;
	int COMRandom = 0;

	float COMAlpha = 0;
	float LastMoveForwardValue = 0;
	float MoveForwardValue = 0, MoveRightValue = 0;
	float NeutralAlpha = 0;	
	float LeanAngle = 0, GearsAngle = 0;
	float ElapsedTimeSinceJump = 50;
	float ElapsedTimeSinceBoost = 50;
	// Calculates effect of slope on speed and other stuff
	float SlopeEffect = 1;
	// Calculates revers effect of slope on speed and other stuff
	float ReverseSlopeEffect = 1;
	// AirControl of the vehicle
	float AirRotationValue = 0;
	float JumpRotationAlpha = 0;
	float LastJumpAngle = 0;

	// in Seconds
	//
	float const JUMP_COOLDOWN = 0.25;
	float const BOOST_TIME_INTERVAL = 0.15;

};
