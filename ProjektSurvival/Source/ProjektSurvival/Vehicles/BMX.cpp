// Fill out your copyright notice in the Description page of Project Settings.

#include "BMX.h"
#include "PhysicsEngine/ConstraintInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ArrowComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Controller.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ABMX::ABMX()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComp"));

	RootComp->SetSimulatePhysics(true);
	RootComp->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	SetRootComponent(RootComp);

	FloatingPawnComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingPawnComponent"));
	FloatingPawnComponent->Deceleration = RelativeNeutralPower * MAX_ACCELARATION;

	// Front And Back traces for calculating bIsFalling,SurfacePitch and etc...
	FrontTraceComponent = CreateDefaultSubobject<USceneComponent>(TEXT("FrontTraceComponent"));
	FrontTraceComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	BackTraceComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BackTraceComponent"));
	BackTraceComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	DebugArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DebugArrow"));
	DebugArrow->bHiddenInGame = false;
	DebugArrow->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	//Setting up box collisions of the wheels
	FrontWheelBoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("FrontWheelBoxCollision"));
	FrontWheelBoxCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	FrontWheelBoxCollision->SetCollisionProfileName(TEXT("Pawn"));
	FrontWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RearWheelBoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RearWheelBoxCollision"));
	RearWheelBoxCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	RearWheelBoxCollision->SetCollisionProfileName(TEXT("Pawn"));
	RearWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//Setting up sphere collisions of the wheels
	FrontWheelSphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("FrontWheelSphereCollision"));
	FrontWheelSphereCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	FrontWheelSphereCollision->SetCollisionProfileName(TEXT("Pawn"));
	FrontWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	FrontWheelSphereCollision->SetNotifyRigidBodyCollision(true);
	RearWheelSphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("RearWheelSphereCollision"));
	RearWheelSphereCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	RearWheelSphereCollision->SetCollisionProfileName(TEXT("Pawn"));
	RearWheelSphereCollision->SetNotifyRigidBodyCollision(true);
	RearWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	SetupCamera();
}

// Called when the game starts or when spawned
void ABMX::BeginPlay()
{
	Super::BeginPlay();
	if (!ensure(GetWorld() && FindComponentByClass<USkeletalMeshComponent>() &&
		RearWheelSphereCollision&& FrontWheelSphereCollision)) return;

	// Inialisations		
	BoxRoot = Cast<UBoxComponent>(GetRootComponent());
	World = GetWorld();
	DefaultTurningBoost = FloatingPawnComponent->TurningBoost;
	DefaultArmLength = SpringArm->TargetArmLength;
	SKMesh = FindComponentByClass<USkeletalMeshComponent>();
	RootComp->SetPhysicsMaxAngularVelocityInDegrees(MaxAngularSpeed);
	TotalMass = RootComp->GetMass() + RearWheelSphereCollision->GetMass() + FrontWheelSphereCollision->GetMass();

	// Starts the main timer.
	World->GetTimerManager().SetTimer(MainTimer, this, &ABMX::Timer, MainTimerRefreshInterval, true);
}

// Called every frame
void ABMX::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!ensure(World))	return;

	CameraAnimationsTick();

	PossessionManagerTick();

	CurrentSpeed = FloatingPawnComponent->Velocity.Size() / 27.8;

	if (((bIsWheelieing || bIsBackWheelieing) && (FMath::Abs(RootComp->GetComponentRotation().Pitch) - FMath::Abs(SurfacePitch) < 50) && bValidGroundDirection) && FMath::Abs(RootComp->GetComponentRotation().Pitch) < 85)
		RootComp->AddLocalRotation(FRotator(0, 0, -FMath::Clamp<float>(RootComp->GetComponentRotation().Roll, -1, 1) * World->DeltaTimeSeconds * 10));

	UpdatePhysicVelocities();

	CalculateSlopeEffects();

	SetAbsouluteForward();

	SetOrientation();

	if (!bIsFalling)
		AirTime = 0;	

	RotateWheelsAndGears(DeltaTime);

	JumpTick();

	LineTracing();

	UpdateMovementDirection();
}

void ABMX::UpdatePhysicVelocities()
{
	if (!bIsFalling)
		RootComp->SetAllPhysicsAngularVelocityInDegrees(RootComp->GetPhysicsAngularVelocityInDegrees() * 0.8);
	FVector RootLinearVelocity = RootComp->GetPhysicsLinearVelocity();
	RootLinearVelocity.Z = FMath::Clamp<float>(RootLinearVelocity.Z, -MaxFallingSpeed, MaxFallingSpeed);
	RootComp->SetAllPhysicsLinearVelocity(RootLinearVelocity);
}

// Calculates SlopeEffect & ReverseSlopeEffect using SurfacePitch
void ABMX::CalculateSlopeEffects()
{
	if (SurfacePitch > 0) {
		SlopeEffect = 1 + (SurfacePitch / 70);
		ReverseSlopeEffect = 1 - FMath::Abs(SurfacePitch / 80);
	}
	else {
		SlopeEffect = 1 - FMath::Abs(SurfacePitch / 80);
		ReverseSlopeEffect = 1 + FMath::Abs(SurfacePitch / 80);
	}
}

// Possession and UnPossession Codes
void ABMX::PossessionManagerTick()
{
	if (!ensure(World && FrontWheelBoxCollision && RearWheelBoxCollision
		&& FrontWheelSphereCollision && RearWheelSphereCollision)) return;

	if (!IsPlayerControlled()) {
		if (bIsPossessed) {

			COMRandom = FMath::FRandRange(1, 2);
			if (FMath::FRandRange(0, 1) > 0.5)
				COMRandom *= -1;

			LastSpeed /= 2;
			NeutralAlpha = 100;
			//Setting box collisions of the wheels					
			FrontWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			RearWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			//Setting sphere collisions of the wheels		
			FrontWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			RearWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			RootComp->SetCenterOfMass(FVector(0, 0, 0));
			bIsPossessed = false;
		}

		UnPossesedMovement();
	}
	else {
		if (!bIsPossessed) {
			//Setting box collisions of the wheels		
			FrontWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			RearWheelBoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			//Setting sphere collisions of the wheels					
			FrontWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			RearWheelSphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			RootComp->SetCenterOfMass(FVector(0, 0, -50));
			bIsPossessed = true;
		}
	}
}

int ABMX::FindDeltaAngle(FVector Vec1, FVector Vec2, bool ContainZAxis)
{
	// No Z Offset
	if (!ContainZAxis)
	{
		Vec1.Z = 0;
		Vec2.Z = 0;
	}
	// Normalizing
	Vec1.Normalize();
	Vec2.Normalize();

	int DeltaAngle = FMath::TruncToInt( // 1.Rounds down DeltaAngle	
		/* 0.Gets the angle between Vec1 & Vec2 */
		UKismetMathLibrary::DegAcos(FVector::DotProduct(
			Vec1,
			Vec2
		)));

	// 2.Figuring out the sign of DeltaAngle (+/-)
	if (!ContainZAxis) {
		if (FVector::CrossProduct(Vec1, Vec2).Z < 0) DeltaAngle = -DeltaAngle;
	}
	else
		if (Vec1.Z < Vec2.Z) DeltaAngle = -DeltaAngle;

	return DeltaAngle;
}

// All the Camera Animations
void ABMX::CameraAnimationsTick()
{
	// Shakes only if bShakeCamera be True
	ShakeCamera();
	// if player is on manual camera controlling mode
	bool bIsManual = (ElapsedTimeSinceCameraManualControl < AutoCamSwitchTime);

	if (bIsBoosting || bIsFalling)
		bShakeCamera = true;
	else
		bShakeCamera = false;

	// Gets the angle between SpringArm and Vehicle forward
	int DeltaAngle = FindDeltaAngle(
		RootComp->GetForwardVector()
		, SpringArm->GetForwardVector() //TODO FirstPerson?
	);

	if (bResetCameraRotation)
	{
		//TODO T1 :/
		if (bIsManual || FMath::Abs(DeltaAngle) < T1)
			bResetCameraRotation = false;
		SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, SurfacePitch - CameraDefaultPitch, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle, World->DeltaTimeSeconds, 4.5), 0));
	}

	// Falling Camera Animation
	if (bIsFalling && !bIsJumping && !bValidGroundDirection && !bResetJumpCamera)
	{
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, DefaultArmLength + FallingArmLength + (Viewport * ViewportsDelta), World->DeltaTimeSeconds, 2);
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, FallingMaxCameraHeight, World->DeltaTimeSeconds, 4)));
		if (bIsManual || bLookBehind)
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, -15, World->DeltaTimeSeconds, 4.5), SpringArm->GetComponentRotation().Yaw, 0));
		else
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, -15, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle, World->DeltaTimeSeconds, 4.5), 0));
		bResetFallingCamera = true;
		return;
	}
	else if (!bIsFalling)
		bResetJumpCamera = false;

	// Keeps the camera almost steady while jumping.
	if (bIsJumping)
	{		
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, -100, World->DeltaTimeSeconds, 5)));
		bResetJumpCamera = true;
	}
	// Wheelie & Slope Animations
	else if (bIsWheelieing || bIsBackWheelieing || FMath::Abs(SurfacePitch) > 20)
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, 50, World->DeltaTimeSeconds, 5)));
	// Resets Camera after jump
	else if (SpringArm->GetRelativeLocation().Z < 0 && !bResetFallingCamera)
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, 0, World->DeltaTimeSeconds, 4)));
	// Resets Camera on Landing
	else if (bResetFallingCamera && !bResetJumpCamera) {
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, FallingMinCameraHeight - 10, World->DeltaTimeSeconds, 10)));
		if (bIsManual || bLookBehind)
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, 0, World->DeltaTimeSeconds, 4.5), SpringArm->GetComponentRotation().Yaw, 0));
		else
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, 0, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle, World->DeltaTimeSeconds, 15), 0));
		if (SpringArm->GetRelativeLocation().Z <= FallingMinCameraHeight)
			bResetFallingCamera = false;
	}
	// Resetes Camera after wheelieing
	else if (!bIsWheelieing && !bIsBackWheelieing)
		SpringArm->SetRelativeLocation(FVector(0, 0, FMath::FInterpTo(SpringArm->GetRelativeLocation().Z, 0, World->DeltaTimeSeconds, 3)));

	// Camera Animations for Forward/Backward OwnerMovement
	if (bIsBraking || CrashAlpha > 1)
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, DefaultArmLength + BrakingArmLength + (Viewport * ViewportsDelta), World->DeltaTimeSeconds, 10);
	else if (bIsBoosting && !bLookBehind)
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, DefaultArmLength + BoostingArmLength + (Viewport * ViewportsDelta), World->DeltaTimeSeconds, 2);
	else if (MoveForwardValue && CurrentSpeed > 5 && !bIsJumping)
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, DefaultArmLength + MovementArmLength + (Viewport * ViewportsDelta), World->DeltaTimeSeconds, 1);
	else if (!MoveForwardValue && CurrentSpeed < TopSpeed / 3)
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, DefaultArmLength + (Viewport * ViewportsDelta), World->DeltaTimeSeconds, 1);

	// Camera Animations for Turnings
	if (!bIsJumping && bValidGroundDirection && !bIsWheelieing && !bIsBackWheelieing && !bIsFalling && !bIsManual) {
		if (MoveRightValue && MoveForwardValue > 0 && CurrentSpeed > 1 && !bIsBraking && !bLookBehind)
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, SurfacePitch - CameraDefaultPitch, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle, World->DeltaTimeSeconds, 4.5), 0));
		else if (MoveRightValue && CurrentSpeed > 1 && bIsBraking && !bLookBehind)
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, SurfacePitch - CameraDefaultPitch, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle + (TurningAngle * (DeltaAngle >= 0 ? 1 : -1)), World->DeltaTimeSeconds, 20), 0));
		else if ((MoveForwardValue > 0 && !bLookBehind) || (CurrentSpeed > 1 && Orientation > 0 && !MoveForwardValue && !bLookBehind))
			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, SurfacePitch - CameraDefaultPitch, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - DeltaAngle, World->DeltaTimeSeconds, 4.5), 0));
		else if ((MoveForwardValue < 0 && !bIsBraking) || bLookBehind) {
			// Gets the angle between SpringArm and Vehicle backward
			int RDeltaAngle = FindDeltaAngle(
				RootComp->GetForwardVector() * -1
				, SpringArm->GetForwardVector() //TODO FirstPerson?
			);

			float InterpSpeed = 4;
			if (bLookBehind)
				InterpSpeed = 5.5;
			else if (!bLookBehind && MoveRightValue)
				InterpSpeed = 5;

			SpringArm->SetRelativeRotation(FRotator(FMath::FInterpTo(SpringArm->GetComponentRotation().Pitch, -SurfacePitch - CameraDefaultPitch, World->DeltaTimeSeconds, 4.5), FMath::FInterpTo(SpringArm->GetComponentRotation().Yaw, SpringArm->GetComponentRotation().Yaw - RDeltaAngle, World->DeltaTimeSeconds, InterpSpeed), 0));
		}
	}
}

// Camera shaking to feel the speed
void ABMX::ShakeCamera()
{
	if (bShakeCamera)
	{
		if ((ThirdPersonCamera->GetComponentRotation().Roll >= 1 && bRollClockwise) || (ThirdPersonCamera->GetComponentRotation().Roll <= -1 && !bRollClockwise))
			bRollClockwise = !bRollClockwise;

		if (bRollClockwise)
			ThirdPersonCamera->SetRelativeRotation(FRotator(0, 0, FMath::FInterpTo(ThirdPersonCamera->GetComponentRotation().Roll, 40, World->DeltaTimeSeconds, 0.03)));
		else
			ThirdPersonCamera->SetRelativeRotation(FRotator(0, 0, FMath::FInterpTo(ThirdPersonCamera->GetComponentRotation().Roll, -50, World->DeltaTimeSeconds, 0.03)));
	}
	else
		ThirdPersonCamera->SetRelativeRotation(FRotator(0, 0, FMath::FInterpTo(ThirdPersonCamera->GetComponentRotation().Roll, 0, World->DeltaTimeSeconds, 0.03)));
}

// Jump has 2 parts [1.Wheelieing 2.Back Wheelieing] 
void ABMX::JumpTick()
{
	if (!bIsJumping || !bIsPossessed) return;

	// the Angle for wheelieng is Random
	int GeneratedAngle = FMath::FRandRange(30, 40);
	// Angle between Vehicle Forward & Surface Angle of Where vehicle started jumping	
	int DeltaAngle = FindDeltaAngle(
		RootComp->GetForwardVector(),
		JumpStartVec
		, true
	);

	// Part 1 [Wheelieing]
	if (bWheelieForJump && DeltaAngle < GeneratedAngle) {
		RootComp->AddImpulse(RootComp->GetUpVector() * JumpPower * TotalMass);
		JumpRotationAlpha = FMath::FInterpTo(JumpRotationAlpha, 10, World->DeltaTimeSeconds, 2);
		RootComp->AddLocalRotation(FRotator(6 * JumpRotationAlpha / 100 * World->DeltaTimeSeconds * 42, 0, 0), true);
	}
	// Part 1.5
	else if (bWheelieForJump && DeltaAngle >= GeneratedAngle) {
		bWheelieForJump = false;
		bAdjustRotationForJump = true;
		JumpRotationAlpha = 0;
	}
	// Part 2 [Back Wheelieing]
	else if (bAdjustRotationForJump)
	{
		JumpRotationAlpha = FMath::FInterpTo(JumpRotationAlpha, 100, World->DeltaTimeSeconds * 42, 2.5);
		if (DeltaAngle < FMath::FRandRange(-2, 2))
			bAdjustRotationForJump = false, JumpRotationAlpha = 50, bIsJumping = false, ElapsedTimeSinceJump = 0;

		if (MoveRightValue)
			RootComp->AddAngularImpulseInDegrees(RootComp->GetUpVector().GetSafeNormal() * MoveRightValue * TotalMass * World->DeltaTimeSeconds * 399999);

		RootComp->AddLocalRotation(FRotator(-5 * JumpRotationAlpha / 100 * World->DeltaTimeSeconds * 42, 0, 0), true);
	}
	// Part 2.5
	else if (JumpRotationAlpha > 5 && bIsFalling) {
		JumpRotationAlpha = FMath::FInterpTo(JumpRotationAlpha, 0, World->DeltaTimeSeconds * 42, 0.5);
		RootComp->AddLocalRotation(FRotator(-0.25 * World->DeltaTimeSeconds * 10, 0, 0), true);
	}
}

// OwnerMovement of Vehicle while it's UnPossessed
void ABMX::UnPossesedMovement()
{
	if (CurrentSpeed < 0.2 && SteeringAngle == 0 && LeanAngle == 0)
	{
		LastSpeed = 0;
		return;
	}

	if (FMath::Abs(RootComp->GetComponentRotation().Roll) > 60) {
		FloatingPawnComponent->Deceleration = 999;
		SteeringAngle = FMath::FInterpTo(SteeringAngle, 0, World->DeltaTimeSeconds, 15);
		LeanAngle = FMath::FInterpTo(LeanAngle, 0, World->DeltaTimeSeconds, 15);
		return;
	}

	if (bIsBlocked)
		LastSpeed = 0, NeutralAlpha = 0;

	float AngleMultiplier = 1;
	if (SurfacePitch > 20)
		AngleMultiplier = 10;

	float TurnMultiplier = 1;
	if (FMath::Abs(SteeringAngle) > 5)
		TurnMultiplier = 10;

	// Center of Mass Alpha changes over time
	COMAlpha = FMath::FInterpTo(COMAlpha, COMAlpha - 100, World->DeltaTimeSeconds, 0.5 * TurnMultiplier);
	RootComp->SetCenterOfMass(FVector(0, COMRandom * COMAlpha / 100, 0));

	// Neutral OwnerMovement of the Vehicle
	if (FMath::Abs(CurrentSpeed) >= 0.2 && !bIsFalling) {
		NeutralAlpha = FMath::Clamp<float>(FMath::FInterpTo(NeutralAlpha, NeutralAlpha - 100, World->DeltaTimeSeconds, RelativeNeutralPower * SlopeEffect * AngleMultiplier * (1 - (LastSpeed / (TopSpeed + 10)))), 0, 100);
		FloatingPawnComponent->TurningBoost = DefaultTurningBoost * 2;
		FloatingPawnComponent->Deceleration = MAX_ACCELARATION;
		FloatingPawnComponent->MaxSpeed = LastSpeed * 27.8 * NeutralAlpha / 100 * ReverseSlopeEffect;
		AddMovementInput(MovementDirection * Orientation, 1);
	}
	else if (bIsFalling)
		FloatingPawnComponent->Deceleration = 250;
	else if (!bIsFalling)
		FloatingPawnComponent->Deceleration = MAX_ACCELARATION;

	// Adds rotation depend on SteeringAngle & LeanAngle
	if (CurrentSpeed >= 0.5 && !bIsFalling)
		RootComp->AddLocalRotation(FRotator(0, (FMath::Abs(SteeringAngle / MaxSteeringAngle) + (FMath::Abs(LeanAngle * 1.1 / MaxLeanAngle))) * World->DeltaTimeSeconds * 100 * Orientation, 0));
}

// Cooldowns and Delays are calculated here
void ABMX::Timer()
{	
	if (ElapsedTimeSinceJump < JUMP_COOLDOWN)
		ElapsedTimeSinceJump += MainTimerRefreshInterval;

	if (AirTime < FALLING_TIMEOUT)
		AirTime += MainTimerRefreshInterval;

	if (ElapsedTimeSinceStopped < REVERSE_MOVE_DELAY)
		ElapsedTimeSinceStopped += MainTimerRefreshInterval;

	if (ElapsedTimeSinceCameraManualControl < AutoCamSwitchTime)
		ElapsedTimeSinceCameraManualControl += MainTimerRefreshInterval;
}

// Plays Wheels and Gears rotation animations
void ABMX::RotateWheelsAndGears(float DeltaTime)
{
	float PhysicsVelocity = 0;
	if (!bIsFalling) {
		PhysicsVelocity = FVector(RootComp->GetComponentVelocity().X, RootComp->GetComponentVelocity().Y, 0).Size() / 27.8;
	}
	//Wheels FW/BW Rotation [ -50 is Experimental!! ]
	for (int i = 0; i < WheelsMeshes.Num(); i++) {
		if ((WheelsMeshes[i]->GetName().Contains(TEXT("Rear")) && !bIsBraking) || WheelsMeshes[i]->GetName().Contains(TEXT("Front"))) {
			WheelsMeshes[i]->AddLocalRotation(FRotator((CurrentSpeed + PhysicsVelocity) * DeltaTime * Orientation * -50, 0, 0));
			if ((!bIsBraking && MoveForwardValue > 0 && Orientation > 0) || bIsBoosting) {
				GearsAngle += CurrentSpeed * DeltaTime * 7;
				//Resets GearsAngle to prevent very large numbers!
				if (GearsAngle > 360) GearsAngle = 0;
			}
		}
	}
}

// Calculates the orientation of the vehicle movement
void ABMX::SetOrientation()
{
	if (bIsBraking)
		return;

	Orientation = FVector::DotProduct(FloatingPawnComponent->Velocity.GetSafeNormal(), AbsouluteForward);
	if (Orientation > 0)
		Orientation = 1;
	else if (Orientation < 0)
		Orientation = -1;
}

// Absoulute Forward of the vehicle (No Z Offset)
void ABMX::SetAbsouluteForward()
{
	AbsouluteForward = RootComp->GetForwardVector();
	AbsouluteForward.Z = 0;
	AbsouluteForward = AbsouluteForward.GetSafeNormal();
}

// Adds SpringArm,Camera and etc...
void ABMX::SetupCamera() {
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetAbsolute(false, true, false);
	SpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SpringArm->SetRelativeLocation(FVector(0, 0, 0));
	SpringArm->bInheritRoll = false, SpringArm->bInheritPitch = false, SpringArm->bInheritYaw = false;
	SpringArm->SetRelativeRotation(FRotator(-30, 0, 0));

	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("3rdPersonCamera"));
	ThirdPersonCamera->AttachToComponent(SpringArm, FAttachmentTransformRules::KeepRelativeTransform);
}

void ABMX::UpdateMovementDirection()
{
	// Projected forward vector on the ground	
	FVector ProjectedForward = RootComp->GetForwardVector().ProjectOnTo(GroundDirection);

	if (ProjectedForward.IsZero())
		return;

	if (!bIsFalling && bValidGroundDirection && !bIsWheelieing)
		MovementDirection = FVector(ProjectedForward.X, ProjectedForward.Y, FMath::FInterpTo(MovementDirection.Z, ProjectedForward.Z, World->DeltaTimeSeconds, 5));
	else
		MovementDirection = FMath::VInterpTo(MovementDirection, AbsouluteForward, World->DeltaTimeSeconds, 10);

	/*if (ensure(DebugArrow))
		DebugArrow->SetWorldRotation(MovementDirection.Rotation());*/
}

// Line Traces for SurfacePitch,OwnerMovement Blockage,Crashing Recognization and etc...
void ABMX::LineTracing()
{
	if (!ensure(FrontTraceComponent && BackTraceComponent && BlockageTrackerComponent)) return;

	FHitResult BlockageTrace, BackTrace, FrontTrace, BottomTrace, FrontGroundTrace, RearGroundTrace;
	FVector StartVec, EndVec;

	FVector AngleAdjust = FVector::ZeroVector;
	if (FMath::Abs(SurfacePitch) < 50)
		AngleAdjust = (RootComp->GetComponentRotation().Vector() * SurfacePitch);

	// Front tip
	StartVec = FrontTraceComponent->GetComponentLocation();
	EndVec = FrontTraceComponent->GetComponentLocation() - (FVector::UpVector * WheelsTraceHeight * (1 + (FMath::Abs(SurfacePitch) / 75))) + AngleAdjust;
	World->LineTraceSingleByChannel(FrontTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
	if (bDrawDebugTraces)
		DrawDebugLine(World, StartVec, EndVec, FColor::Red, false, -1, 0, 1);
	// Back end
	StartVec = BackTraceComponent->GetComponentLocation();
	EndVec = BackTraceComponent->GetComponentLocation() - (FVector::UpVector * WheelsTraceHeight * (1 + (FMath::Abs(SurfacePitch) / 75))) + AngleAdjust;
	World->LineTraceSingleByChannel(BackTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
	if (bDrawDebugTraces)
		DrawDebugLine(World, StartVec, EndVec, FColor::Red, false, -1, 0, 1);

	// (ABS) Bottom trace
	StartVec = RootComp->GetComponentLocation() + (FVector::DownVector * (BoxRoot->GetScaledBoxExtent().Z + 1));
	EndVec = (RootComp->GetComponentLocation() + (FVector::DownVector * (BoxRoot->GetScaledBoxExtent().Z + 1))) + (FVector::DownVector * BottomTraceLength);
	World->LineTraceSingleByChannel(BottomTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
	if (bDrawDebugTraces)
		DrawDebugLine(World, StartVec, EndVec, FColor::Green, false, -1, 0, 1);

	if (BottomTrace.bBlockingHit && CurrentSpeed == 0 && FMath::Abs(RootComp->GetComponentRotation().Pitch) > 70)
		bIsStuck = true;
	else
		if (bValidGroundDirection)
			bIsStuck = false;


	// Blockage trace
	if (ensure(BlockageTrackerComponent)) {

		if (bIsWheelieing) {
			StartVec = BlockageTrackerComponent->GetComponentLocation();
			EndVec = BlockageTrackerComponent->GetComponentLocation() + AbsouluteForward * BLOCKAGE_TRACE_LENGTH;
			World->LineTraceSingleByChannel(BlockageTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
			if (bDrawDebugTraces)
				DrawDebugLine(World, StartVec, EndVec, FColor::Purple, false, -1, 0, 1);
		}
		else if (bIsBackWheelieing)
		{
			StartVec = BlockageTrackerComponent->GetComponentLocation() + (FVector::UpVector * WheelsTraceHeight);
			EndVec = BlockageTrackerComponent->GetComponentLocation() + (FVector::UpVector * WheelsTraceHeight) + AbsouluteForward * BLOCKAGE_TRACE_LENGTH * 2;
			World->LineTraceSingleByChannel(BlockageTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
			if (bDrawDebugTraces)
				DrawDebugLine(World, StartVec, EndVec, FColor::Red, false, -1, 0, 1);
		}
		else
		{
			StartVec = BlockageTrackerComponent->GetComponentLocation();
			EndVec = BlockageTrackerComponent->GetComponentLocation() + (AbsouluteForward * BLOCKAGE_TRACE_LENGTH) + (FVector::UpVector * 6);
			World->LineTraceSingleByChannel(BlockageTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
			if (bDrawDebugTraces)
				DrawDebugLine(World, StartVec, EndVec, FColor::Blue, false, -1, 0, 1);
		}


		// Crashing Physics!
		if (BlockageTrace.bBlockingHit)
		{
			if (!bIsBlocked && Orientation > 0) {
				//UE_LOG(LogTemp, Warning, TEXT("Crashing"));
				if (bIsWheelieing)
					CrashImpulse = (FVector::UpVector + (-AbsouluteForward)) * 400 * CurrentSpeed;
				else if (bIsBackWheelieing)
					CrashImpulse = (RootComp->GetComponentRotation().Vector() + RootComp->GetUpVector().GetSafeNormal()) * CurrentSpeed * 600;
				else
					CrashImpulse = ((-1 * RootComp->GetComponentRotation().Vector()) + RootComp->GetUpVector().GetSafeNormal()) * CurrentSpeed * 200;

				CrashAlpha = 100;
			}
			bIsBlocked = true;
		}
		else
			bIsBlocked = false;

		if (CrashAlpha >= 0 && FMath::Abs(RootComp->GetComponentRotation().Roll) < 60) {
			float InterpSpeed = 6 - (2 * LastSpeed / 100);
			if (InterpSpeed < 1)
				InterpSpeed = 1;

			CrashAlpha = FMath::FInterpTo(CrashAlpha, CrashAlpha - 100, World->DeltaTimeSeconds, InterpSpeed);
			if (!bIsWheelieing)
				RootComp->AddImpulseAtLocation(CrashImpulse * CrashAlpha / 100 * TotalMass, BackTraceComponent->GetComponentLocation());
			else
				RootComp->AddImpulseAtLocation(CrashImpulse * CrashAlpha / 100 * TotalMass, FrontTraceComponent->GetComponentLocation());
		}
	}

	// (Abs Down) Front and Rear traces for GroundDirection!
	StartVec = BackTraceComponent->GetComponentLocation();
	EndVec = BackTraceComponent->GetComponentLocation() + (FVector::DownVector * FrontAndRearTracesLength);
	World->LineTraceSingleByChannel(RearGroundTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
	if (bDrawDebugTraces)
		DrawDebugLine(World, StartVec, EndVec, FColor::Yellow, false, -1, 0, 1);

	StartVec = FrontTraceComponent->GetComponentLocation();
	EndVec = FrontTraceComponent->GetComponentLocation() + (FVector::DownVector * FrontAndRearTracesLength);
	World->LineTraceSingleByChannel(FrontGroundTrace, StartVec, EndVec, ECollisionChannel::ECC_Visibility);
	if (bDrawDebugTraces)
		DrawDebugLine(World, StartVec, EndVec, FColor::Yellow, false, -1, 0, 1);

	if (RearGroundTrace.bBlockingHit && FrontGroundTrace.bBlockingHit)
	{
		bValidGroundDirection = true;
		GroundDirection = FrontGroundTrace.Location - RearGroundTrace.Location;
	}
	else
		bValidGroundDirection = false;

	int ExSurfaceAngle = SurfacePitch;

	FRotator RootWorldRotation = RootComp->GetComponentRotation();
	RootWorldRotation.Pitch = 0;
	SurfacePitch = FindDeltaAngle(
		GroundDirection
		, RootComp->GetForwardVector()
		, true
	);

	if (!bIsFalling && bValidGroundDirection && !bIsWheelieing && bIsBackWheelieing && !bIsJumping && bIsPossessed)
	{
		// Rotates the bike on changing angles (slopes)
		float DeltaAngle = SurfacePitch - ExSurfaceAngle;
		if (DeltaAngle > 1 || DeltaAngle < -1) {
			float Clockwise = 0;
			if (DeltaAngle > 0)
				Clockwise = -1;
			else
				Clockwise = 1;

			RootComp->AddAngularImpulseInDegrees(RootComp->GetRightVector().GetSafeNormal() * Clockwise * TotalMass * World->DeltaTimeSeconds * 999000);
		}
	}

	bool Temp = false;
	if (bIsFalling)
		Temp = true;

	// Sets bIsFalling and bIsAbleToMove
	if (BackTrace.bBlockingHit)
	{
		bIsAbleToMove = true;
		bIsFalling = false;
		bIsBackWheelieing = false;

		if (!FrontTrace.bBlockingHit)
			bIsWheelieing = true;
		else
			bIsWheelieing = false;
	}
	else {
		if (!FrontTrace.bBlockingHit) {
			bIsAbleToMove = true, bIsFalling = true;
			bIsWheelieing = false, bIsBackWheelieing = false;
		}
		else {
			bIsBackWheelieing = true, bIsFalling = false, bIsAbleToMove = false;
		}
	}

	if (bIsFalling)
		if (BottomTrace.bBlockingHit && AirTime >= FALLING_TIMEOUT)
			bIsFalling = false;

	// Sets the linear velocity after landing
	FVector LinearVel = RootComp->GetPhysicsLinearVelocity();
	LinearVel.X = 0;
	LinearVel.Y = 0;
	LinearVel.Z /= 10;
	if (bIsFalling == false && Temp == true)
		RootComp->SetAllPhysicsLinearVelocity(LinearVel);
}

float ABMX::GetLeanAngle()
{
	return LeanAngle;
}

float ABMX::GetGearsAngle()
{
	return GearsAngle;
}

void ABMX::UnPossessed()
{
	UE_LOG(LogTemp, Warning, TEXT("Un Fucking Possessed."));
}

float ABMX::GetSteeringAngle()
{
	return SteeringAngle;
}

// Called to bind functionality to input
void ABMX::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputs = PlayerInputComponent;

	//OwnerMovement
	PlayerInputs->BindAxis(TEXT("MoveForward"), this, &ABMX::MoveForward);
	PlayerInputs->BindAxis(TEXT("MoveRight"), this, &ABMX::MoveRight);
	PlayerInputs->BindAxis(TEXT("Elevate"), this, &ABMX::Elevate);
	//Camera
	PlayerInputs->BindAxis(TEXT("ElevateCamera"), this, &ABMX::ElevateCamera);
	PlayerInputs->BindAxis(TEXT("TwistCamera"), this, &ABMX::CamTwisting);
	PlayerInputs->BindAction(TEXT("LookBehind"), EInputEvent::IE_Pressed, this, &ABMX::LookBehind);
	PlayerInputs->BindAction(TEXT("LookBehind"), EInputEvent::IE_Released, this, &ABMX::LookFront);
	PlayerInputs->BindAction(TEXT("ChangeViewport"), EInputEvent::IE_Pressed, this, &ABMX::ChangeViewport);
	//Actions
	PlayerInputs->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ABMX::Jump);
}

void ABMX::Initialise(TArray<UStaticMeshComponent*> WheelsMeshesToSet, USceneComponent* BlockageTracker) {
	WheelsMeshes = WheelsMeshesToSet;
	BlockageTrackerComponent = BlockageTracker;
}

void ABMX::MoveForward(float Value)
{
	if (!ensure(World)) return;

	MoveForwardValue = Value;

	// Checks on each key switch (up and down)
	if (Value != LastMoveForwardValue && Value != -1 && LastMoveForwardValue != -1) {
		BoostCounter++;
		if (ElapsedTimeSinceBoost < BOOST_TIME_INTERVAL && BoostCounter > 5)
			bIsBoosting = true;
		else
			bIsBoosting = false;
		ElapsedTimeSinceBoost = 0;
	}
	LastMoveForwardValue = Value;
	if (ElapsedTimeSinceBoost > BOOST_TIME_INTERVAL || Value == -1)
		bIsBoosting = false, BoostCounter = 0;

	if (Value != -1)
		ElapsedTimeSinceBoost += World->DeltaTimeSeconds;

	//Setting the LastSpeed for neutral movement!
	if (Value && !bIsBraking && !bIsFalling && !bIsWheelieing)
		LastSpeed = FMath::Clamp<float>(CurrentSpeed, -TopSpeed, TopSpeed);

	if (bIsBlocked)
		LastSpeed = 0, NeutralAlpha = 0;

	if (Value && !bIsBraking)
		NeutralAlpha = FMath::FInterpTo(NeutralAlpha, 100, World->DeltaTimeSeconds, 5);

	float AngleMultiplier = 1;
	if (SurfacePitch > 20)
		AngleMultiplier = 10;

	// Neutral OwnerMovement of the Vehicle
	if ((!bIsStuck && !Value && !bIsFalling && !bIsBoosting) || (bIsWheelieing && FMath::Abs(CurrentSpeed) >= 0.2)) {
		NeutralAlpha = FMath::Clamp<float>(FMath::FInterpTo(NeutralAlpha, NeutralAlpha - 100, World->DeltaTimeSeconds, RelativeNeutralPower * SlopeEffect * AngleMultiplier * (1 - (LastSpeed / (TopSpeed + 10)))), 0, 100);
		FloatingPawnComponent->TurningBoost = DefaultTurningBoost * 2;

		if (Orientation > 0) {
			FloatingPawnComponent->Deceleration = MAX_ACCELARATION;
			FloatingPawnComponent->MaxSpeed = LastSpeed * 27.8 * NeutralAlpha / 100 * ReverseSlopeEffect;
			AddMovementInput(MovementDirection * Orientation, 1);
			UE_LOG(LogTemp, Warning, TEXT("EXECUTED 0 %f"), World->DeltaTimeSeconds);
		}
		else
		{
			FloatingPawnComponent->Deceleration = 9999;
		}

		if (bIsBraking)
			LastSpeed = CurrentSpeed;

		bIsBraking = false;
		return;
	}


	// If falling then just keep the movement going
	if (bIsFalling && !bIsWheelieing && !bIsStuck) {
		FloatingPawnComponent->Deceleration = 250;
		return;
	}

	// Setting Braking Status
	if (Orientation > 0 && CurrentSpeed > 0.5 && Value < 0)
		bIsBraking = true;
	else
		bIsBraking = false, FloatingPawnComponent->TurningBoost = DefaultTurningBoost;

	if (bIsBoosting) {
		float const BOOST_VALUE = 1.5;
		FloatingPawnComponent->Acceleration = RelativeAccelaration * MAX_ACCELARATION * ReverseSlopeEffect * BOOST_VALUE;
		FloatingPawnComponent->MaxSpeed = TopSpeed * 27.8 * ReverseSlopeEffect * BOOST_VALUE;
		AddMovementInput(MovementDirection, 1);
		UE_LOG(LogTemp, Warning, TEXT("EXECUTED 1 %f"), World->DeltaTimeSeconds);
	}
	// Normal OwnerMovement
	else if (Value > 0) {
		FloatingPawnComponent->Acceleration = RelativeAccelaration * MAX_ACCELARATION * ReverseSlopeEffect;
		FloatingPawnComponent->MaxSpeed = TopSpeed * 27.8 * ReverseSlopeEffect;
		AddMovementInput(MovementDirection, Value);
		UE_LOG(LogTemp, Warning, TEXT("EXECUTED 2 %f"), Value);
	}
	// Normal Reverse OwnerMovement
	else if (Value < 0) {
		if (bIsBraking) {
			// Boosts Brakes if not dirfting!
			float BrakeBooster = 1;
			if (!MoveRightValue || CurrentSpeed < TopSpeed / 2) {
				BrakeBooster = 3;
			}

			FloatingPawnComponent->Deceleration = MAX_ACCELARATION;
			NeutralAlpha = FMath::Clamp<float>(FMath::FInterpTo(NeutralAlpha, NeutralAlpha - 100, World->DeltaTimeSeconds, RelativeBrakePower * BrakeBooster * ReverseSlopeEffect), 0, 100);
			FloatingPawnComponent->TurningBoost = DefaultTurningBoost;
			FloatingPawnComponent->MaxSpeed = LastSpeed * 27.8 * NeutralAlpha / 100;
			AddMovementInput(MovementDirection * Orientation, NeutralAlpha / 100);
			UE_LOG(LogTemp, Warning, TEXT("EXECUTED 2 %f"), NeutralAlpha / 100);

			if (CurrentSpeed < 1)
				ElapsedTimeSinceStopped = 0;
		}
		else if (ElapsedTimeSinceStopped >= REVERSE_MOVE_DELAY) {
			FloatingPawnComponent->Acceleration = RelativeRevereseAccelaration * MAX_ACCELARATION * SlopeEffect;
			FloatingPawnComponent->MaxSpeed = ReverseTopSpeed * 27.8 * SlopeEffect;
			AddMovementInput(MovementDirection, Value);
			UE_LOG(LogTemp, Warning, TEXT("EXECUTED 2 %f"), Value);
		}
	}
}

void ABMX::MoveRight(float Value)
{
	if (!ensure(World)) return;

	MoveRightValue = Value;

	float ExSteeringAngle = SteeringAngle;

	if (bIsFalling || bIsWheelieing)
		SteeringAngle = FMath::FInterpTo(SteeringAngle, MaxSteeringAngle * Value, World->DeltaTimeSeconds, 10);
	else
		SteeringAngle = FMath::FInterpTo(SteeringAngle, MaxSteeringAngle * Value * (1 - (CurrentSpeed / TopSpeed)), World->DeltaTimeSeconds, SteeringInterpSpeed);

	if (FMath::Abs(SteeringAngle) < 10 && CurrentSpeed > TopSpeed / 2)
		SteeringAngle = FMath::FInterpTo(SteeringAngle, 10 * Value, World->DeltaTimeSeconds, SteeringInterpSpeed);

	//Wheels L/R Rotation [ 4.5 is Experimental ]
	for (int i = 0; i < WheelsMeshes.Num(); i++) {
		if (WheelsMeshes[i]->GetName().Contains(TEXT("Front"))) {
			WheelsMeshes[i]->AddRelativeRotation(FRotator(0, (SteeringAngle - ExSteeringAngle) / 4.5, 0));
		}
	}

	float RotationDamper = 1;
	if (CurrentSpeed < 10)
		RotationDamper = CurrentSpeed / 10;

	float BrakeBooster = 1;
	if (bIsBraking && CurrentSpeed > TopSpeed / 2)
		BrakeBooster = 1.3;

	float WheelieRotationDamper = 1;
	if (bIsWheelieing || bIsBackWheelieing)
		WheelieRotationDamper = 0.1;

	// Leaning in order to turn	
	if (bIsFalling || bIsWheelieing || bIsBackWheelieing)
		LeanAngle = FMath::FInterpTo(LeanAngle, 0, World->DeltaTimeSeconds, 10);
	else {
		float Target = MaxLeanAngle * -Value * (CurrentSpeed / TopSpeed);
		if (BrakeBooster > 1)
			Target = Value * -55;

		LeanAngle = FMath::FInterpTo(LeanAngle, Target, World->DeltaTimeSeconds, LeaningInterpSpeed);
	}

	//Applying the rotation
	if (CurrentSpeed >= 0.5 && !bIsFalling)
		RootComp->AddLocalRotation(FRotator(0, (FMath::Abs(SteeringAngle / MaxSteeringAngle) + (FMath::Abs(LeanAngle * 1.1 / MaxLeanAngle))) * World->DeltaTimeSeconds * 100 * Value * RotationDamper * Orientation * BrakeBooster * WheelieRotationDamper, 0));

	if (bIsFalling)
		RootComp->AddLocalRotation(FRotator(0, AirControl * World->DeltaTimeSeconds * 50 * Value, 0));
}

// Elevation in air and on land (Wheelieing and Rotating)
void ABMX::Elevate(float Value)
{
	if (!Value)
		RootComp->SetCenterOfMass(FVector(0, 0, -50));

	if (!ensure(World)) return;

	if (bIsJumping) {
		return;
		AirRotationValue = 0;
	}

	if (FMath::Abs(AirRotationValue) < 5 && Value)
		AirRotationValue = FMath::FInterpTo(AirRotationValue, Value * AirControl + (FMath::Abs(AirRotationValue) * Value), World->DeltaTimeSeconds, 2);
	else
		AirRotationValue = FMath::FInterpTo(AirRotationValue, 0, World->DeltaTimeSeconds, 0.5);

	if (bIsFalling)
		RootComp->AddLocalRotation(FRotator(AirRotationValue * World->DeltaTimeSeconds * 100, 0, 0), true);
	else
		AirRotationValue = FMath::FInterpTo(AirRotationValue, 0, World->DeltaTimeSeconds, 10);

	if (!Value) {
		bIsNeutralElevating = true;
		return;
	}

	if (Orientation < 0 || (Value < 0 && FMath::Abs(SurfacePitch) > 10))
		return;

	float WheelieAlpha = 1 - FMath::Abs((RootComp->GetComponentRotation().Pitch) / (MaxWheelieAngle * 2));
	//TODO IF More than 75 degs then UnPossess!	
	if (!bIsFalling && CurrentSpeed > 5 || bIsStuck) {

		if (bIsNeutralElevating)
			bIsNeutralElevating = false;

		RootComp->SetCenterOfMass((Value > 0 ? RearWheelSphereCollision : FrontWheelSphereCollision)->GetRelativeLocation());
		RootComp->AddImpulseAtLocation((Value > 0 ? FrontWheelSphereCollision : RearWheelSphereCollision)->GetUpVector().GetSafeNormal()* WheelieForcePower* WheelieAlpha* SlopeEffect* FMath::Abs(Value), (Value > 0 ? FrontWheelSphereCollision : RearWheelSphereCollision)->GetComponentLocation());
		return;

		FVector WheelieImpulse;
		float ExPitch = RootComp->GetComponentRotation().Pitch;

		if (RootComp->GetComponentRotation().Pitch > 30 || RootComp->GetComponentRotation().Pitch < -30) {
			WheelieImpulse = -RootComp->GetRightVector().GetSafeNormal() * WheelieForcePower * WheelieAlpha * SlopeEffect * Value;
		}
		else
			WheelieImpulse = -RootComp->GetRightVector().GetSafeNormal() * WheelieForcePower * WheelieAlpha * SlopeEffect * Value;

		WheelieImpulse.Z = 0;
		//Applying Impulse in order to wheelie
		RootComp->AddAngularImpulseInDegrees(WheelieImpulse * TotalMass * World->DeltaTimeSeconds * 1000);

		//Applying harsh impulse when wheelie angle gets big!
		if (FMath::Abs(RootComp->GetComponentRotation().Pitch) > 65)
			RootComp->AddAngularImpulseInDegrees(WheelieImpulse * TotalMass * World->DeltaTimeSeconds * 2000);
	}
}

void ABMX::LookBehind()
{
	bResetCameraRotation = false;
	bLookBehind = true;
	ElapsedTimeSinceCameraManualControl = 50;
}

void ABMX::LookFront()
{
	bLookBehind = false;
	bResetCameraRotation = true;
	ElapsedTimeSinceCameraManualControl = 50;
}

void ABMX::ChangeViewport()
{
	if (Viewport == 2)
		Viewport = 0;
	else
		Viewport++;
}

//Camera rotation over X axis
void ABMX::CamTwisting(float Value)
{
	if (!ensure(World) || !Value) return;

	if (Value)
		ElapsedTimeSinceCameraManualControl = 0;

	SpringArm->AddWorldRotation(FRotator(0, Value * World->GetDeltaSeconds() * MouseSensetivity, 0));
}

// Camera rotation over Y axis
void ABMX::ElevateCamera(float Value)
{
	if (!ensure(World)) return;

	if (Value)
		ElapsedTimeSinceCameraManualControl = 0;

	float CamPitch = SpringArm->GetComponentRotation().Pitch;

	if (!Value || (CamPitch < MaxCameraPitch && Value < 0) || (CamPitch > MinCameraPitch&& Value > 0)) { return; }

	SpringArm->AddLocalRotation(FRotator(Value * World->GetDeltaSeconds() * MouseSensetivity, 0, 0));
}

//Getting variables ready to start jumping
void ABMX::Jump() {
	if (!ensure(World)) return;

	if (ElapsedTimeSinceJump <= JUMP_COOLDOWN || bIsWheelieing || bIsFalling || bIsJumping || bIsBackWheelieing || RootComp->GetComponentRotation().Pitch > 70) return;

	JumpStartVec = RootComp->GetComponentRotation().Vector();
	LastJumpAngle = RootComp->GetComponentRotation().Pitch;
	JumpRotationAlpha = 100;
	bWheelieForJump = true;
	bIsJumping = true;
}

