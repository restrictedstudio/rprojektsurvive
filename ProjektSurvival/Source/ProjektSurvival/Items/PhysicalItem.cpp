// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "PhysicalItem.h"
#include "Item.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanMovementComponent.h"
#include "../Controllers/PController.h"
#include "TimerManager.h"
#include "Components/BoxComponent.h"
#include "Animation/AnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

// Sets default values
APhysicalItem::APhysicalItem()
{
	PrimaryActorTick.bCanEverTick = false;

	// Setting up the Root Component
	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComp->SetCollisionProfileName(TEXT("InteractableItem"));
	RootComp->SetNotifyRigidBodyCollision(false);
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	RootComp->SetBoxExtent(FVector(20, 20, 20));
	RootComp->SetMassOverrideInKg();
	SetRootComponent(RootComp);

	// Skeletal Mesh
	SkeletonMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletonMesh"));
	SkeletonMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	// Static Mesh
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	StaticMesh->SetCollisionProfileName("NoCollision");
	// Widget
	Widget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	Widget->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	Widget->SetWidgetSpace(EWidgetSpace::Screen);
	Widget->SetVisibility(false);
}

void APhysicalItem::Setup(AHumanCharacter* HumanToSet, APController* ControllerToSet)
{
	Character = HumanToSet, Controller = ControllerToSet;
	OwnerMovement = Character->GetMovement();
	OwnerData = Character->GetData();
}

UBoxComponent* APhysicalItem::ActivatePhysics()
{
	RootComp->SetCollisionProfileName(TEXT("InteractableItem"));
	RootComp->SetSimulatePhysics(true);
	World->GetTimerManager().SetTimer(PhysicsTimer, this, &APhysicalItem::CheckPhysics, 5, false);
	return RootComp;
}

float APhysicalItem::GetActorHalfHeight()
{
	return RootComp->GetScaledBoxExtent().Z;
}

UStaticMesh* APhysicalItem::GetStaticMesh()
{
	return StaticMesh->GetStaticMesh();
}

USkeletalMesh* APhysicalItem::GetSkeletalMesh()
{
	return SkeletonMesh->SkeletalMesh;
}

void APhysicalItem::ExecuteAction(FName ActionName)
{	
	if (ActionName == TEXT("Equip") || ActionName == TEXT("Consume"))
		Item->Use(Controller, this), OwnerMovement->CancelActionOnMovement(true);
	else if (ActionName == TEXT("Grab"))
		Take();
	else if (ActionName == TEXT("Carry"))
		Carry();
	else
		bIsTaken = false;
}

void APhysicalItem::Take(bool bOnGround)
{	
	OwnerData->AddItemToInventoryFromProximity(this);
}

void APhysicalItem::Throw()
{
	if (!RootComp) return;

	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	RootComp->SetCollisionProfileName(TEXT("InteractableItem"));
	RootComp->SetSimulatePhysics(true);
	World->GetTimerManager().SetTimer(PhysicsTimer, this, &APhysicalItem::CheckPhysics, 5, false);

	FVector ItemTraceStart, LookDirection;
	Controller->GetItemTraceLine(ItemTraceStart, LookDirection);
	LookDirection += FVector::UpVector;

	FVector Force = LookDirection * AnimData.ThrowStrength;

	// Applying Angular and Linear force
	RootComp->AddAngularImpulseInDegrees(FVector(FMath::FRandRange(-1, 1)) * AnimData.ThrowAngularForce);
	RootComp->AddImpulse(Force);

	bIsTaken = false;
	OwnerMovement->SetCarryMode(false);
}

void APhysicalItem::Drop()
{
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	RootComp->SetCollisionProfileName(TEXT("InteractableItem"));
	RootComp->SetSimulatePhysics(true);
	World->GetTimerManager().SetTimer(PhysicsTimer, this, &APhysicalItem::CheckPhysics, 5, false);

	bIsTaken = false;
	OwnerMovement->SetCarryMode(false);
}

void APhysicalItem::Carry()
{
	if (OwnerMovement->GetCarryMode())
	{
		OwnerData->ShowMessage(FText::FromString(TEXT("You're already carrying something!")), 1.f);
		return;
	}

	RootComp->SetSimulatePhysics(false);
	RootComp->SetCollisionProfileName("NoCollision");

	// Attaching to carry bone
	AttachToComponent(Character->GetMesh(), FAttachmentTransformRules(EAttachmentRule::KeepWorld, false), AnimData.CarryAttachBone);
	SetActorRelativeTransform(AnimData.CarryAttachmentRelativeTransform);

	OwnerMovement->SetCarryMode(true, this);
}

// Called when the game starts or when spawned
void APhysicalItem::BeginPlay()
{
	Super::BeginPlay();

	Item = ItemBP->GetDefaultObject<UItem>();
	World = GetWorld();
}

void APhysicalItem::CheckPhysics()
{
	if (!RootComp->IsSimulatingPhysics())
	{
		World->GetTimerManager().ClearTimer(PhysicsTimer);
		return;
	}

	if (GetVelocity().Size() < 2)
	{
		RootComp->SetSimulatePhysics(false);
		World->GetTimerManager().ClearTimer(PhysicsTimer);
	}
}

APController* APhysicalItem::GetPlayerController()
{
	return Controller;
}

// Set the mouse in the middle of the widget to make a habbit
void APhysicalItem::SetMousePositionOnWidget()
{
	if (!Controller) return;

	FVector2D ScreenLocation;
	Controller->ProjectWorldLocationToScreen(Widget->GetComponentLocation(), ScreenLocation);
	Controller->SetMouseLocation(ScreenLocation.X, ScreenLocation.Y);
}
