// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "Item_Wearable.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"
#include "../Controllers/PController.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/StaticMesh.h"
#include "Engine/SkeletalMesh.h"
#include "Animation/SkeletalMeshActor.h"

bool UItem_Wearable::Use(APController* PlayerController, int ID, ESlotType SlotType)
{
	AHumanCharacter* HumanActor = PlayerController->GetPawn<AHumanCharacter>();
	UHumanDataComponent* Data = HumanActor->GetData();

	APhysicalItem* PhItem = nullptr;

	switch (SlotType)
	{
	case ESlotType::Inventory:
		PhItem = Data->GetInventorySlotByID(ID).Item->GetPhysicalItem();
		break;

	case ESlotType::Proximity:
		PhItem = Data->GetProximitySlotByID(ID).PhItem;
		break;
	}

	FWearableSlot WSlot =
		FWearableSlot(Data->FindEmptyWearingID(), this
			, SpawnAndAttachWearable(PlayerController, PhItem));

	if (!Data->EquipWearableItem(WSlot)) return false;

	switch (SlotType)
	{
	case ESlotType::Inventory:
		Data->UpdateInventorySlot(ID, -1);
		break;

	case ESlotType::Proximity:
		Data->UpdateProximitySlot(ID, -1);
		break;
	}

	return true;
}

bool UItem_Wearable::Use(APController* PlayerController, APhysicalItem* PhItem)
{
	UHumanDataComponent* Data = PlayerController->GetPawn()->FindComponentByClass<UHumanDataComponent>();

	FWearableSlot WSlot =
		FWearableSlot(Data->FindEmptyWearingID(), this
			, SpawnAndAttachWearable(PlayerController, PhItem));

	Data->EquipWearableItem(WSlot);

	// Updating Amount
	PhItem->SetAmount(PhItem->GetAmount() - 1);
	if (!PhItem->GetAmount())
		PhItem->Destroy();

	return true;
}

AActor* UItem_Wearable::SpawnAndAttachWearable(APController* PlayerController, APhysicalItem* PhItem)
{
	// Getting Human SK Mesh		
	USkeletalMeshComponent* HumanMesh = PlayerController->GetPawn<AHumanCharacter>()->GetMesh();

	// SKELETON MESH
	if (AttachType == EWearableAttachType::SkeletonMesh)
	{		
		// Spawning Skeletal Mesh Actor of Item
		ASkeletalMeshActor* SKMeshActor =
			PlayerController->GetWorld()->SpawnActor<ASkeletalMeshActor>();

		USkeletalMeshComponent* SKMesh = SKMeshActor->GetSkeletalMeshComponent();
		// Setting up Static Mesh Paramters
		SKMesh->SetSkeletalMesh(PhItem->GetSkeletalMesh());
		SKMesh->SetMobility(EComponentMobility::Movable);
		SKMesh->SetRelativeLocation(FVector(0));
		// Since all the items matrials should have only one slot so we won't go any further than this
		SKMesh->SetMaterial(0, PhItem->GetSkeletalMeshComponent()->GetMaterial(0));
		SKMesh->AttachToComponent(HumanMesh, FAttachmentTransformRules::KeepRelativeTransform);
		SKMesh->SetMasterPoseComponent(HumanMesh);

		return SKMeshActor;
	}
	// SOCKET
	else
	{
		// Spawning Static Mesh Actor of Item		
		AStaticMeshActor* StaticMeshActor =
			PlayerController->GetWorld()->SpawnActor<AStaticMeshActor>();

		UStaticMeshComponent* StaticMesh = StaticMeshActor->GetStaticMeshComponent();
		// Setting up Static Mesh Paramters
		StaticMesh->SetMobility(EComponentMobility::Movable);
		StaticMesh->SetStaticMesh(PhItem->GetStaticMesh());
		// Since all the items matrials should have only one slot so we won't go any further than this
		StaticMesh->SetMaterial(0, PhItem->GetStaticMeshComponent()->GetMaterial(0));
		StaticMesh->SetRelativeTransform(SocketAttachmentTransform);
		StaticMesh->AttachToComponent(HumanMesh, FAttachmentTransformRules::KeepRelativeTransform, SocketName);

		return StaticMeshActor;
	}
}
