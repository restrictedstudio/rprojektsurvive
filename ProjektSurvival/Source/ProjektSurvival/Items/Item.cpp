// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "Item.h"
#include "Engine/World.h"
#include "../Structs.h"
#include "../Controllers/PController.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanMovementComponent.h"
#include "Item_Wearable.h"
#include "Item_Weapon.h"

bool UItem::Take(APController* PlayerController, int ID, ESlotType SlotType, int AmountToTake)
{
	if (!PlayerController->GetPawn()->GetClass()->IsChildOf<AHumanCharacter>())
		return false;

	UHumanDataComponent* DataComp = PlayerController->GetPawn()->FindComponentByClass<UHumanDataComponent>();

	switch (SlotType)
	{
	case ESlotType::Proximity:
		return DataComp->AddItemToInventoryFromProximity(
			ID,
			AmountToTake);

	case ESlotType::Hotbar:
		return DataComp->AddItemToInventoryFromHotbar(ID);

	case ESlotType::Wearables:
		return DataComp->AddItemToInventoryFromWeared(ID);
	}

	return false;
}

void UItem::Drop(APController* PlayerController, int ID, ESlotType SlotType, int AmountToDrop)
{
	if (!PlayerController->GetPawn()->GetClass()->IsChildOf<AHumanCharacter>())
		return;

	//TODO Add to proximity slots

	UHumanDataComponent* DataComp = PlayerController->GetPawn()->FindComponentByClass<UHumanDataComponent>();

	APhysicalItem* PhItem =
		PlayerController->GetWorld()->SpawnActor<APhysicalItem>(PhysicalItemBP);

	PhItem->SetActorLabel(Name.ToString());

	// Finding Drop Transform
	FTransform DropTransform = DataComp->FindBestDropLocation();

	if (DropTransform.Equals(FTransform())) // Sometimes happend!
		return;

	PhItem->SetActorTransform(DropTransform);
	PhItem->AddActorLocalOffset(PhItem->GetActorUpVector() * PhItem->GetActorHalfHeight());

	int Amount = 1;
	switch (SlotType)
	{
	case ESlotType::Inventory:
		Amount = AmountToDrop ? AmountToDrop : DataComp->GetInventorySlotByID(ID).Amount;
		DataComp->UpdateInventorySlot(ID, -Amount);
		DataComp->AddProximitySlot(FProximitySlot(DataComp->FindEmptyProximityID(), this, PhItem, Amount));
		break;

	case ESlotType::Hotbar:
		Amount = 1;
		DataComp->RemoveHotbarSlot(ID);
		break;

	case ESlotType::Wearables:
		Amount = 1;
		DataComp->RemoveWearableSlot(DataComp->GetWearableSlotByID(ID).Item->GetCompatibleSlot());
		break;
	}

	PhItem->SetAmount(Amount);
}

APhysicalItem* UItem::Carry(APController* PlayerController, int ID, ESlotType SlotType)
{
	AHumanCharacter* Character = PlayerController->GetPawn<AHumanCharacter>();
	if (!Character)
		return nullptr;

	UHumanMovementComponent* Movement = Character->GetMovement();
	UHumanDataComponent* DataComp = Character->GetData();

	if (Movement->GetCarryMode())
		return nullptr;

	// Getting Physical Item
	APhysicalItem* PhItem;
	if (SlotType == ESlotType::Inventory)
		PhItem = PlayerController->GetWorld()->SpawnActor<APhysicalItem>(PhysicalItemBP);
	else if (SlotType == ESlotType::Proximity)
		PhItem = DataComp->GetProximitySlotByID(ID).PhItem;
	else
		return nullptr;

	PhItem->SetActorLabel(Name.ToString());
	PhItem->Setup(Character, PlayerController);
	PhItem->Carry();

	// Updating the amount of item in inventory and PhItem	
	if (SlotType == ESlotType::Inventory)
	{
		int Amount = DataComp->GetInventorySlotByID(ID).Amount;
		PhItem->SetAmount(Amount);
		DataComp->UpdateInventorySlot(ID, -Amount);
	}
	else if (SlotType == ESlotType::Proximity)
		DataComp->RemoveProximitySlot(ID);

	return PhItem;
}

void UItem::Throw(APController* PlayerController, int ID, ESlotType SlotType)
{
	APhysicalItem* PhItem = Carry(PlayerController, ID, SlotType);

	if (PhItem)
		PhItem->Throw();
}
