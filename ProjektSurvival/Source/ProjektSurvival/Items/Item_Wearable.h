// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Item_Wearable.generated.h"

class USkeletonMesh;
enum class EWearableSlot : uint8;
enum class ESlotType;

UENUM(BlueprintType)
enum class EWearableAttachType : uint8 {
	Socket,
	SkeletonMesh
};

/**
 * All of the wearable in game should use this item class.
 */
UCLASS()
class PROJEKTSURVIVAL_API UItem_Wearable : public UItem
{
	GENERATED_BODY()

public:

	bool Use(APController* PlayerController, int ID, ESlotType SlotType) override;
	bool Use(APController* PlayerController, APhysicalItem* PhItem) override;

	//TODO Clean up
	//UFUNCTION(BlueprintPure, Category = "Wearables")
	EWearableSlot GetCompatibleSlot() { return WearableSlot; }

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		EWearableSlot WearableSlot;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		EWearableAttachType AttachType;
	// Socket name that wearable would attach to
	UPROPERTY(EditDefaultsOnly, Category = "Usability|Socket", meta = (EditCondition = "AttachType == EWearableAttachType::Socket"))
		FName SocketName;
	UPROPERTY(EditDefaultsOnly, Category = "Usability|Socket", meta = (EditCondition = "AttachType == EWearableAttachType::Socket"))
		FTransform SocketAttachmentTransform;

private:
	AActor* SpawnAndAttachWearable(APController* PlayerController, APhysicalItem* PhItem);
};
