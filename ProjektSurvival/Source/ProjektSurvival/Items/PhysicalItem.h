// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicalItem.generated.h"

class UItem;
class UAnimMontage;
class UBoxComponent;
class UWidgetComponent;
class AHumanCharacter;
class APController;
class UStaticMesh;
class USkeletalMesh;

USTRUCT(BlueprintType)
struct FPhysicalItemAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName TakeItemSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName TakeItemOnGroundSectionName;

	UPROPERTY(EditDefaultsOnly)
		bool bIsCarriable;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		FName CarrySectionName;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		FName DropSectionName;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		FName CarryLoopSectionName;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		FName CarryAttachBone;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		float CarryAttachNotifier;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsCarriable"))
		FTransform CarryAttachmentRelativeTransform;

	UPROPERTY(EditDefaultsOnly)
		bool bIsThrowable;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		FName ThrowSectionName;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		FName ThrowAttachBone;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		float ThrowAttachNotifier;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		FTransform ThrowAttachmentRelativeTransform;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		float ThrowStrength;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bIsThrowable"))
		float ThrowAngularForce;


	FPhysicalItemAnimation()
	{
		AnimMontage = nullptr;
		TakeItemSectionName = TEXT("Take");
		TakeItemOnGroundSectionName = TEXT("TakeOnGround");

		bIsCarriable = false;
		CarrySectionName = TEXT("Carry");
		DropSectionName = TEXT("Drop");
		CarryLoopSectionName = TEXT("CarryLoop");
		CarryAttachBone = TEXT("RightHand");
		CarryAttachNotifier = 0.5f;
		CarryAttachmentRelativeTransform = FTransform();

		bIsThrowable = false;
		ThrowSectionName = TEXT("Throw");
		ThrowAttachBone = TEXT("RightHand");
		ThrowAttachNotifier = 0.25f;
		ThrowAttachmentRelativeTransform = FTransform();
		ThrowStrength = 750;
		ThrowAngularForce = 250;
	}
};


UCLASS(HideCategories = (Rendering, Replication, Input, Actor, Collision))
class PROJEKTSURVIVAL_API APhysicalItem : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APhysicalItem();

	void Setup(AHumanCharacter* HumanToSet, APController* ControllerToSet);
	
	void SetIsTaken(bool Value) { bIsTaken = Value; }
	bool IsTaken() { return bIsTaken; }
	void SetAmount(int NewAmount) { Amount = NewAmount; }
	int GetAmount() { return Amount; }
	float GetActorHalfHeight();
	UItem* GetItem() { return Item; }
	FPhysicalItemAnimation GetAnimData() { return AnimData; }
	UStaticMesh* GetStaticMesh();
	UStaticMeshComponent* GetStaticMeshComponent() { return StaticMesh; }
	USkeletalMesh* GetSkeletalMesh();
	USkeletalMeshComponent* GetSkeletalMeshComponent() { return SkeletonMesh; }
	UBoxComponent* ActivatePhysics();

	UFUNCTION(BlueprintImplementableEvent, Category = "Item Widget")
		void ShowWidget();
	UFUNCTION(BlueprintImplementableEvent, Category = "Item Widget")
		void HideWidget();

	UFUNCTION(BlueprintCallable, Category = "Item Actions")
		virtual void ExecuteAction(FName ActionName);

	UFUNCTION(BlueprintCallable, Category = "Item Actions")
		void Take(bool bOnGround = false);
	UFUNCTION(BlueprintCallable, Category = "Item Actions")
		void Throw();
	UFUNCTION(BlueprintCallable, Category = "Item Actions")
		void Drop();
	UFUNCTION(BlueprintCallable, Category = "Item Actions")
		void Carry();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings|Widget")
		bool bIsEquipable = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings|Widget")
		bool bIsCarriable = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings|Widget")
		bool bIsConsumable = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// turns off physics if not using.
	virtual void CheckPhysics();

	UFUNCTION(BlueprintCallable, Category = "Controller")
		APController* GetPlayerController();

	// Set the mouse in the middle of the widget to make a habbit
	UFUNCTION(BlueprintCallable, Category = "Mouse")
		void SetMousePositionOnWidget();

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USkeletalMeshComponent* SkeletonMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
		UWidgetComponent* Widget;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		FPhysicalItemAnimation AnimData = FPhysicalItemAnimation();
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		TSubclassOf<UItem> ItemBP;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
		int Amount = 1;

	//public:	
	//	// Called every frame
	//	virtual void Tick(float DeltaTime) override;
	FTimerHandle PhysicsTimer;
	UItem* Item;
	APController* Controller;
	AHumanCharacter* Character;
	class UHumanMovementComponent* OwnerMovement;
	class UHumanDataComponent* OwnerData;
	UWorld* World;

	bool bIsTaken = false;
};
