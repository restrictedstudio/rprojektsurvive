// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "Item_Consumable.h"
#include "PhysicalItem.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanCharacter.h"
#include "../UI/PlayerHUDWidget.h"
#include "../Controllers/PController.h"

bool UItem_Consumable::Use(APController* PlayerController, int ID, ESlotType SlotType)
{
	UHumanDataComponent* Data = PlayerController->GetPawn<AHumanCharacter>()->GetData();
	if (Data->IsUsingAnyItem())
	{
		Data->ShowMessage(FText::FromString("Can't use more than one Item at the same time!"));
		return false;
	}

	// if true then consuming would have no effect on player then we just won't allow this
	if ((!HungerRecoverAmount || Data->GetHunger() >= 1)
		&& (!ThirstRecoverAmount || Data->GetThirst() >= 1)
		&& (!HealthRecoverAmount || Data->GetHealth() >= Data->GetMaxHealth())
		&& (!StaminaRecoverAmount || Data->GetStamina() >= 1))
	{
		Data->ShowMessage(FText::FromString("You're full already, can't constume it right now!"));
		return false;
	}

	Data->SetInUseItem(ID, SlotType);
	Data->SetHint(FText::FromString("PRESS F TO CANCEL"));
	PlayerController->GetHUD()->SetupWaitBar(ConsumingTime);

	PlayerController->GetWorld()->GetTimerManager().SetTimer(Data->InUseItemTimer, Data, &UHumanDataComponent::ConsumeInUseItem, ConsumingTime, false);
	return true;
}

bool UItem_Consumable::Use(APController* PlayerController, APhysicalItem* PhItem)
{
	UHumanDataComponent* Data = PlayerController->GetPawn<AHumanCharacter>()->GetData();
	if (Data->IsUsingAnyItem())
	{
		Data->ShowMessage(FText::FromString("Can't use more than one Item at the same time!"));
		return false;
	}

	// if true then consuming would have no effect on player then we just won't allow this
	if ((!HungerRecoverAmount || Data->GetHunger() >= 1)
		&& (!ThirstRecoverAmount || Data->GetThirst() >= 1)
		&& (!HealthRecoverAmount || Data->GetHealth() >= Data->GetMaxHealth())
		&& (!StaminaRecoverAmount || Data->GetStamina() >= 1))
	{
		Data->ShowMessage(FText::FromString("You're full already, can't constume it right now!"));
		return false;
	}

	Data->SetInUseItem(PhItem);
	Data->SetHint(FText::FromString("PRESS F TO CANCEL"));	
	PlayerController->GetHUD()->SetupWaitBar(ConsumingTime);



	PlayerController->GetWorld()->GetTimerManager().SetTimer(Data->InUseItemTimer, Data, &UHumanDataComponent::ConsumeInUsePhItem, ConsumingTime, false);
	return true;
}
