// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "../Structs.h"
#include "Item_Weapon.generated.h"

class AWeapon;
enum class EWeaponSlotType : uint8;
enum class ESlotType;

/**
 * Weapons UObject stores crucial weapon data.
 */
UCLASS()
class PROJEKTSURVIVAL_API UItem_Weapon : public UItem
{
	GENERATED_BODY()

public:
	bool Use(APController* PlayerController, int ID, ESlotType SlotType) override;
	bool Use(APController* PlayerController, APhysicalItem* PhItem) override;
	bool EquipAt(APController* PlayerController, int ID, ESlotType SlotType, EHotbarSlotLocation SlotLocation);

	EWeaponSlotType GetCompatibleSlot() { return Slot; }
	FTransform GetSocketTransform() { return DockSocketAttachmentTransform; }

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		EWeaponSlotType Slot;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		TSubclassOf<AWeapon> WeaponBlueprint;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		FTransform DockSocketAttachmentTransform;
};
