// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "PhysicalItem.h"
#include "UObject/NoExportTypes.h"
#include "Item.generated.h"

class UTexture2D;
enum class ESlotType;

UENUM(BlueprintType)
enum class EItemType : uint8 {
	Misc,
	Weapon,
	Attachment,
	Equipment,
	Consumable
};

/**
 * Base class for all of the items (UObject), mostly an interface
 * but it can be a proper class as well...
 */
UCLASS(Blueprintable, BlueprintType)
class PROJEKTSURVIVAL_API UItem : public UObject
{
	GENERATED_BODY()

public:

	/* Getters */
	// 
	UFUNCTION(BlueprintPure, Category = "Getter")
		FName GetName() { return Name; }
	UFUNCTION(BlueprintPure, Category = "Getter")
		FText GetDescryption() { return Descryption; }
	UFUNCTION(BlueprintPure, Category = "Getter")
		int GetCapacity() { return Capacity; }
	UFUNCTION(BlueprintPure, Category = "Getter")
		EItemType GetItemType() { return ItemType; }
	UFUNCTION(BlueprintPure, Category = "Getter")
		UTexture2D* GetThumbnail() { return Thumbnail; }
	UFUNCTION(BlueprintPure, Category = "Getter")
		APhysicalItem* GetPhysicalItem() { return PhysicalItemBP.GetDefaultObject(); }
	
	virtual bool Use(APController* PlayerController, int ID, ESlotType SlotType) { return false; }

	// this is for physical item direct use (without opening inventory)
	virtual bool Use(APController* PlayerController, APhysicalItem* PhItem) { return false; }

	// AmountToDrop : 0 means drop all
	void Drop(APController* PlayerController, int ID, ESlotType SlotType, int AmountToDrop = 0);

	APhysicalItem* Carry(APController* PlayerController, int ID, ESlotType SlotType);

	void Throw(APController* PlayerController, int ID, ESlotType SlotType);

	// AmountToTake : 0 means take al
	bool Take(APController* PlayerController, int ID, ESlotType SlotType, int AmountToTake = 0);

protected:

	// name of the item.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		FName Name;

	// brief descryption of Item for the player.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		FText Descryption;

	// The capacity of the object that takes in inventory
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		int Capacity = 10;

	// The capacity of the object that takes in inventory
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		EItemType ItemType;

	// Thumbnail to show in inventory or other places.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		UTexture2D* Thumbnail;

	// will be used on drop [ Drop() ]
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		TSubclassOf<APhysicalItem> PhysicalItemBP;
};
