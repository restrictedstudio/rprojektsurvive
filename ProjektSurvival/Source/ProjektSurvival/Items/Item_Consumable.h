// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Item_Consumable.generated.h"

class APhysicalItem;
enum class ESlotType;

/**
 *
 */
UCLASS()
class PROJEKTSURVIVAL_API UItem_Consumable : public UItem
{
	GENERATED_BODY()

public:
	bool Use(APController* PlayerController, int ID, ESlotType SlotType) override;
	bool Use(APController* PlayerController, APhysicalItem* PhItem) override;

	float GetHungerRecoverAmount() { return HungerRecoverAmount; }
	float GetHealthRecoverAmount() { return HealthRecoverAmount; }
	float GetThirstRecoverAmount() { return ThirstRecoverAmount; }
	float GetStaminaRecoverAmount() { return StaminaRecoverAmount; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		float HungerRecoverAmount = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		float ThirstRecoverAmount = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		float HealthRecoverAmount = 0;
	UPROPERTY(EditDefaultsOnly, Category = "Usability")
		float StaminaRecoverAmount = 0;
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float ConsumingTime = 1;
};
