// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "Item_Weapon.h"
#include "../Weapons/Weapon.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanCombatComponent.h"
#include "../Controllers/PController.h"
#include "Engine/World.h"

bool UItem_Weapon::Use(APController* PlayerController, int ID, ESlotType SlotType)
{	
	AHumanCharacter* HumanActor = PlayerController->GetPawn<AHumanCharacter>();
	UHumanCombatComponent* CombatComp = HumanActor->FindComponentByClass<UHumanCombatComponent>();

	UHumanDataComponent* Data = HumanActor->GetData();
	APhysicalItem* PhItem = nullptr;
	switch (SlotType)
	{
	case ESlotType::Inventory:
		PhItem = Data->GetInventorySlotByID(ID).Item->GetPhysicalItem();
		break;

	case ESlotType::Proximity:
		PhItem = Data->GetProximitySlotByID(ID).PhItem;
		break;
	}

	int Index = Data->FindEmptyHotbarSlot(Slot);
	if (Index == -1) return false;
	EHotbarSlotLocation SlotLocation = Data->GetHotbarSlots()[Index].SlotLocation;

	AWeapon* WeaponActor = CombatComp->SetupWeapon(WeaponBlueprint, SlotLocation, this);
	if (!WeaponActor) return false;

	FHotbarSlot HSlot =
		FHotbarSlot(-1, this, WeaponActor, SlotLocation);

	if (!Data->EquipHotbarSlot(HSlot)) return false;

	switch (SlotType)
	{
	case ESlotType::Inventory:
		Data->UpdateInventorySlot(ID, -1);
		break;

	case ESlotType::Proximity:
		Data->UpdateProximitySlot(ID, -1);
		break;
	}

	return true;
}

bool UItem_Weapon::Use(APController* PlayerController, APhysicalItem* PhItem)
{
	AHumanCharacter* HumanActor = PlayerController->GetPawn<AHumanCharacter>();
	UHumanCombatComponent* CombatComp = HumanActor->GetCombat();
	UHumanDataComponent* Data = HumanActor->GetData();

	int Index = Data->FindEmptyHotbarSlot(Slot);
	if (Index == -1) return false;
	EHotbarSlotLocation SlotLocation = Data->GetHotbarSlots()[Index].SlotLocation;

	AWeapon* WeaponActor = CombatComp->SetupWeapon(WeaponBlueprint, SlotLocation, this);
	if (!WeaponActor) return false;

	FHotbarSlot HSlot =
		FHotbarSlot(-1, this, WeaponActor, SlotLocation);

	if (!Data->EquipHotbarSlot(HSlot)) return false;

	// Updating Amount
	PhItem->SetAmount(PhItem->GetAmount() - 1);
	if (!PhItem->GetAmount())
		PhItem->Destroy();

	return true;
}

bool UItem_Weapon::EquipAt(APController* PlayerController, int ID, ESlotType SlotType, EHotbarSlotLocation SlotLocation)
{
	AHumanCharacter* HumanActor = PlayerController->GetPawn<AHumanCharacter>();
	UHumanCombatComponent* CombatComp = HumanActor->GetCombat();

	AWeapon* WeaponActor = CombatComp->SetupWeapon(WeaponBlueprint, SlotLocation, this);
	if (!WeaponActor) return false;

	UHumanDataComponent* Data = HumanActor->GetData();
	APhysicalItem* PhItem = nullptr;
	switch (SlotType)
	{
	case ESlotType::Inventory:
		PhItem = Data->GetInventorySlotByID(ID).Item->GetPhysicalItem();
		break;

	case ESlotType::Proximity:
		PhItem = Data->GetProximitySlotByID(ID).PhItem;
		break;
	}

	FHotbarSlot HSlot = *Data->GetHotbarSlot(SlotLocation);
	HSlot.Item = this;
	HSlot.WeaponActor = WeaponActor;

	if (!Data->EquipHotbarSlot(HSlot)) return false;

	switch (SlotType)
	{
	case ESlotType::Inventory:
		Data->UpdateInventorySlot(ID, -1);
		break;

	case ESlotType::Proximity:
		Data->UpdateProximitySlot(ID, -1);
		break;
	}

	return true;
}