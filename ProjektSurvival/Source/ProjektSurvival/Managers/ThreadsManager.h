// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ThreadsManager.generated.h"

class FCustomRunnable;

enum ERunPriority : int8
{
	Low = 0,
	Normal = 1,
	High = 2
};

struct FQueueNode
{
	FCustomRunnable* Thread;
	const TCHAR* ThreadName;
	ERunPriority Priority;

	FQueueNode* Next;

	FQueueNode()
	{
		Thread = nullptr, ThreadName = TEXT("NONE"), Next = nullptr, Priority = ERunPriority::Normal;
	}

	FQueueNode(FCustomRunnable* InThread, const TCHAR* InThreadName, ERunPriority InPriority)
	{
		Thread = InThread, ThreadName = InThreadName, Next = nullptr, Priority = InPriority;
	}
};

struct FThreadNode
{
	// instance of the thread
	FRunnableThread* Thread;
	// thread runnable
	FCustomRunnable* Runnable;
	ERunPriority Priority;

	FThreadNode* Next;
	FThreadNode* Prev;

	FThreadNode()
	{
		Thread = nullptr, Runnable = nullptr, Next = nullptr, Prev = nullptr, Priority = ERunPriority::Normal;
	}

	FThreadNode(FRunnableThread* InThread, FCustomRunnable* InRunnable, ERunPriority InPriority)
	{
		Thread = InThread, Runnable = InRunnable, Priority = InPriority, Next = nullptr, Prev = nullptr;
	}
};

/**
 * Threads Pool + Managing
 *
 * All threads are controlled in here
 * If all possible threads be in use then
 * it will queue the thread and wait till a thread is over.
 *
 */
UCLASS()
class PROJEKTSURVIVAL_API UThreadsManager : public UObject
{
	GENERATED_BODY()

public:

	UThreadsManager(const FObjectInitializer& ObjectInitializer);

	// It will close and kill all the active threads on end play
	virtual void BeginDestroy() override;

	// Call this to create the thread and start it going
	// It may queue the thread!! if there was no idle cores available
	bool RunThread(FCustomRunnable* InThread, const TCHAR* ThreadName = TEXT("NONE"), ERunPriority Priority = ERunPriority::Normal);

	// Calls when each thread finished (exited)
	void OnThreadFinished(uint32 ID);

	bool bPrintDebug = true;

private:

	// Active Threads Head | Linked List
	FThreadNode* ThreadsHead = nullptr;
	void CreateThread(FCustomRunnable* InThread, const TCHAR* ThreadName, ERunPriority Priority);
	void RemoveThread(uint32 ID);	

	// Queue Head | Linked List
	FQueueNode* QueueHead;
	void PushQueue(FQueueNode* InThread);
	FQueueNode PopQueue();	

	// Number of CPU Logical Cores
	int32 MaxCores;
	// In use threads of cpu count
	int8 ActiveThreads = 0;
};