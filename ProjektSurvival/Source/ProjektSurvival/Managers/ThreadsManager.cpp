// Fill out your copyright notice in the Description page of Project Settings.

#include "ThreadsManager.h"
#include "HAL/RunnableThread.h"
#include "../Threading/CustomRunnable.h"

UThreadsManager::UThreadsManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	MaxCores = FPlatformMisc::NumberOfCoresIncludingHyperthreads();
}

// Call this to create the thread and start it going
// It may queue the thread!! if there was no idle cores available
bool UThreadsManager::RunThread(FCustomRunnable* InThread, const TCHAR* ThreadName, ERunPriority Priority)
{
	if (!FPlatformProcess::SupportsMultithreading()) return false;

	// If platform didnt have enough logical cores (<= 2)
	if (MaxCores < 3)
	{
		// Runs thread codes on Game Thread!
		FRunnableThread::Create(InThread, ThreadName)->Kill(true);

		if (bPrintDebug)
			UE_LOG(LogTemp, Error, TEXT("%s thread started on game thread due to lack of logical CPU cores."), ThreadName);
	}
	// Uses 50% of the CPU logical cores
	else if (MaxCores / 2 >= ActiveThreads)
		CreateThread(InThread, ThreadName, Priority);
	// if there was no idle cores then Queue the Thread
	else
		PushQueue(new FQueueNode(InThread, ThreadName, Priority));

	return true;
}

void UThreadsManager::CreateThread(FCustomRunnable* InThread, const TCHAR* ThreadName, ERunPriority Priority)
{
	// Creating the Thread
	FRunnableThread* NewThread = FRunnableThread::Create(InThread, ThreadName);
	InThread->SetID(NewThread->GetThreadID());

	// Managing Threads Linked list
	FThreadNode* NewThreadNode = new FThreadNode(NewThread, InThread, Priority);
	if (ThreadsHead)
		NewThreadNode->Next = ThreadsHead, ThreadsHead->Prev = NewThreadNode;
	ThreadsHead = NewThreadNode;

	++ActiveThreads;

	if (bPrintDebug)
		UE_LOG(LogTemp, Warning, TEXT("=> %s Thread is Created with %d ID and %d Priority. <="), ThreadName, NewThread->GetThreadID(), Priority);
}

void UThreadsManager::RemoveThread(uint32 ID)
{
	if (!ThreadsHead) return;

	--ActiveThreads;

	FThreadNode* Current = ThreadsHead;
	while (1)
	{
		if (Current->Thread->GetThreadID() == ID)
		{
			if (bPrintDebug)
				UE_LOG(LogTemp, Log, TEXT("%s Thread Removed."), *Current->Thread->GetThreadName());

			/* Setting prev and next nodes */
			// LOGIC : Prev -> Current -> Next |=> Prev -> Next
			// - Part 1
			if (Current->Prev)
				Current->Prev->Next = Current->Next;
			// - Part 2
			if (Current->Next) {
				if (Current == ThreadsHead)
					ThreadsHead = ThreadsHead->Next;
				Current->Next->Prev = Current->Prev;
			}

			if (Current == ThreadsHead) ThreadsHead = nullptr;
			delete Current;
			Current = nullptr;

			return;
		}

		if (!Current->Next) break;
		// Moving Current to Forward for the next iteration
		Current = Current->Next;
	}

	UE_LOG(LogTemp, Error, TEXT("!!!--- Remove Thread Error : ID Not Found ---!!!"));
}

void UThreadsManager::PushQueue(FQueueNode* InThread)
{
	if (bPrintDebug)
		UE_LOG(LogTemp, Error, TEXT(" ~ %s Thread Queued!"), InThread->ThreadName);

	// Queues the thread in the right place, at the end of the priority
	if (QueueHead)
	{
		FQueueNode* PushLocation = QueueHead
			, * Prev = nullptr;
		// Finding the push location
		while (PushLocation && PushLocation->Priority >= InThread->Priority)
			Prev = PushLocation, PushLocation = PushLocation->Next;

		// Adding the thread to the queue at the push location
		if (Prev)
			Prev->Next = InThread;
		else
			QueueHead = InThread;

		InThread->Next = PushLocation;
	}
	else
		QueueHead = InThread;	
}

FQueueNode UThreadsManager::PopQueue()
{
	FQueueNode Temp = *QueueHead;

	QueueHead = QueueHead->Next;

	if (bPrintDebug)
		UE_LOG(LogTemp, Error, TEXT(" < %s Thread UnQueued >"), Temp.ThreadName);

	return Temp;
}

// Calls when each thread finished (exited)
void UThreadsManager::OnThreadFinished(uint32 ID)
{
	RemoveThread(ID);

	if (QueueHead)
	{
		FQueueNode FirstNode = PopQueue();
		RunThread(FirstNode.Thread, FirstNode.ThreadName, FirstNode.Priority);
	}
}

// It will close and kill all the active threads on end play
void UThreadsManager::BeginDestroy()
{
	while (ThreadsHead)
	{
		ThreadsHead->Thread->Kill(false);
		ThreadsHead->Runnable->ClearThreadsManager();

		FThreadNode* Next = ThreadsHead->Next;
		delete ThreadsHead;
		ThreadsHead = nullptr;
		ThreadsHead = Next;
	}

	Super::BeginDestroy();
}
