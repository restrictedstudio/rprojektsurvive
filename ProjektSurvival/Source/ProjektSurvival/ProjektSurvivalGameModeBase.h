// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjektSurvivalGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJEKTSURVIVAL_API AProjektSurvivalGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
