// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Gun_Rifle.generated.h"

class AStaticMeshActor;

/*
 Assualt Rifle Class
 * Derrived class of Gun
 */
UCLASS()
class PROJEKTSURVIVAL_API AGun_Rifle : public AGun
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGun_Rifle();
		
	void CancelReloading() override;

protected:

	void Timer() override;	

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* MagazineMesh;

	// The hand bone name that magazine attaches to	
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		FName HandBoneName = TEXT("LeftHand");
	// the time that player removed the old magazine
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float MagazineDetachTimeFromGun = 0.2;
	// The time that player would release the old magazine
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float MagazineThrowAwayTime = 0.4;
	// The time that player would grab the magazine from her/him pocket and etc...
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float NewMagazineSpawnTime = 0.6;
	// The time that the new magazine would replace the old one (reload ended)
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float NewMagazineReplaceTime = 0.8;

private:

	// Reloading Stuff - #Notes
	/*  0 Nothing
	 1 Magazine Detached
	 2 Magazine Thrown Away
	 3 Magazine Spawned
	*/
	int ReloadStage = 0;
	// Magazine Location and Rotation on Removing the old magazine
	FVector MagazineDefaultLocation;
	FRotator MagazineDefaultRotation;
	// Uses for reloading process
	UStaticMeshComponent* OldMagazineStaticMesh = nullptr;
	UStaticMeshComponent* NewMagazineStaticMesh = nullptr;

};
