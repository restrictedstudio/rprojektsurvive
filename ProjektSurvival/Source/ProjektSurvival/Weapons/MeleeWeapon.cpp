// Fill out your copyright notice in the Description page of Project Settings.

#include "MeleeWeapon.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanCombatComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Animation/AnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"

// Sets default values
AMeleeWeapon::AMeleeWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	BladedBodyCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BladedBodyCollision"));
	BladedBodyCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	BladedBodyCollision->SetGenerateOverlapEvents(false);
	BladedBodyCollision->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	SetRootComponent(BladedBodyCollision);

	BluntBodyCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BluntBodyCollision"));
	BluntBodyCollision->SetCollisionProfileName(TEXT("MeleeWeapon"));
	BluntBodyCollision->SetGenerateOverlapEvents(false);
	BluntBodyCollision->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	BluntBodyCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bladed Weapon Mesh"));
	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));

	BDSparkParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bladed SparkParticle"));
	BDSparkParticleSystemComponent->bAutoActivate = false;
	BDSparkParticleSystemComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BTSparkParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Blunt SparkParticle"));
	BTSparkParticleSystemComponent->bAutoActivate = false;
	BTSparkParticleSystemComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	MainTimerRefreshInterval = 0.05;
}

// Called when the game starts or when spawned
void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (!ensure(GetWorld())) return;

	World = GetWorld();

	// Settings
	if (bIsBladed)
	{
		BladedBodyCollision->SetNotifyRigidBodyCollision(true);
		BladedBodyCollision->OnComponentHit.AddDynamic(this, &AMeleeWeapon::OnBladedPartHit);
	}

	if (bIsBlunt)
	{
		BluntBodyCollision->SetNotifyRigidBodyCollision(true);
		BluntBodyCollision->OnComponentHit.AddDynamic(this, &AMeleeWeapon::OnBluntPartHit);
	}
}

// Called every frame
void AMeleeWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ExecuteLineTraces();

	// Detecting One hit on single attack + Hit blockage
	if ((bSingleAttackMode && bDidHit) || bHitBlocked)
	{
		bHitBlocked = false;
		bSingleAttackMode = false;
		Player.Combat->PlayMontage(ECombatState::Blocked, AnimationData.AnimMontage, AnimationData.AttackEndSectionName);
	}

}

// line traces
void AMeleeWeapon::ExecuteLineTraces()
{
	if (!ensure(World) || !bLineTrace) return;

	// Bladed Hit simulation ----
	FHitResult RHit, LHit, Hit;
	FVector StartLocation, EndLocation;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Melee Weapon", true, Player.Character);
	QueryParams.AddIgnoredActor(this);

	// if weapon has bladed part
	if (bIsBladed)
	{
		StartLocation =
			BladedBodyCollision->GetComponentLocation()
			- BladedBodyCollision->GetForwardVector().GetSafeNormal() * (BladedBodyCollision->GetScaledBoxExtent().X);

		// Moving the start location to blade! (Right Edge of the blade)
		StartLocation += BladedBodyCollision->GetRightVector().GetSafeNormal() * (BladedBodyCollision->GetScaledBoxExtent().Y);

		EndLocation =
			StartLocation
			+ BladedBodyCollision->GetForwardVector().GetSafeNormal() * (2 * BladedBodyCollision->GetScaledBoxExtent().X);

		World->LineTraceSingleByChannel(RHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::Red, false, -1, '\000', 1);

		// Left edge of the blade trace
		StartLocation =
			BladedBodyCollision->GetComponentLocation()
			- BladedBodyCollision->GetForwardVector().GetSafeNormal() * (BladedBodyCollision->GetScaledBoxExtent().X);

		StartLocation -= BladedBodyCollision->GetRightVector().GetSafeNormal() * (BladedBodyCollision->GetScaledBoxExtent().Y);

		EndLocation =
			StartLocation
			+ BladedBodyCollision->GetForwardVector().GetSafeNormal() * (2 * BladedBodyCollision->GetScaledBoxExtent().X);

		World->LineTraceSingleByChannel(LHit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

		if (bDrawDebugHelpers)
			DrawDebugLine(World, StartLocation, EndLocation, FColor::Orange, false, -1, '\000', 1);

		if (RHit.bBlockingHit || LHit.bBlockingHit)
		{
			Hit = RHit.bBlockingHit ? RHit : LHit;

			// Hitting Human
			if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>() && !HitHumen.Contains(Hit.Actor))
			{
				HitHumen.Add(Hit.Actor);

				USkeletalMeshComponent* HumanMesh = Hit.Actor->FindComponentByClass<USkeletalMeshComponent>();
				UHumanCombatComponent* HumanCombatComponent = Hit.Actor->FindComponentByClass<UHumanCombatComponent>();

				FVector AttackForward =
					UKismetMathLibrary::FindLookAtRotation(Player.Character->GetActorLocation(), Hit.Actor->GetActorLocation()).Vector();

				// Ragdoll state activates if player was moving over a certain speed (MinHitSpeedToGoToRagdoll in Human Class)
				if (HumanMesh->GetPhysicsLinearVelocity(TEXT("Hips")).Size() > HumanCombatComponent->GetMinHitSpeedToGoToRagdoll() / 2)
					HumanCombatComponent->GoRagdoll();
				else
				{
					// find the hit direction
					FName HitDirectionLetter = HumanCombatComponent->FindHitDirection(
						AttackForward
						, Hit.Actor->GetActorForwardVector());
					// Plays hit reaction montage
					HumanCombatComponent->PlayHitReactionMontage(HitDirectionLetter);
				}

				bDidHit = true;

				HitBone = Hit.BoneName;
				HumanCombatComponent->DealDamage(Damage, HitBone);

				if (HitBone == NAME_None)
					HitBone = TEXT("Spine");

				// Spawns Blood FX
				UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);
			}
			// Not Movable then play end montage in HumanCombatComponent
			else if (!Hit.Actor->IsRootComponentMovable())
			{
				bHitBlocked = true;
				bLineTrace = false;
				bDidHit = true;

				if (bGenerateSparksOnHitForBladedPart && Hit.Actor->ActorHasTag(TEXT("GenerateSparks")))
				{
					BDSparkParticleSystemComponent->Activate();
					ElapsedTimeSinceBDSparkActivated = 0;
				}
			}
		}

	}

	// Returns if weapon wasnt blunt!
	if (!bIsBlunt) return;

	// Blunt Hit Simulation ----	
	StartLocation =
		BluntBodyCollision->GetComponentLocation()
		- BluntBodyCollision->GetForwardVector().GetSafeNormal() * (BluntBodyCollision->GetScaledBoxExtent().X);
	EndLocation =
		StartLocation
		+ BluntBodyCollision->GetForwardVector().GetSafeNormal() * (2 * BluntBodyCollision->GetScaledBoxExtent().X);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
	{
		// Hitting Human
		if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>() && !HitHumen.Contains(Hit.Actor))
		{
			HitHumen.Add(Hit.Actor);

			USkeletalMeshComponent* HumanMesh = Cast<USkeletalMeshComponent>(Hit.Component);
			UHumanCombatComponent* HumanCombatComponent = Hit.Actor->FindComponentByClass<UHumanCombatComponent>();

			FVector AttackForward =
				UKismetMathLibrary::FindLookAtRotation(Player.Character->GetActorLocation(), Hit.Actor->GetActorLocation()).Vector();

			// Ragdoll state activates if player was moving over a certain speed (MinHitSpeedToGoToRagdoll in Human Class)
			if (HumanMesh->GetPhysicsLinearVelocity(TEXT("Hips")).Size() > HumanCombatComponent->GetMinHitSpeedToGoToRagdoll())
				HumanCombatComponent->GoRagdoll();
			else
			{
				// find the hit direction
				FName HitDirectionLetter = HumanCombatComponent->FindHitDirection(
					AttackForward
					, Hit.Actor->GetActorForwardVector());
				// Plays hit reaction montage
				HumanCombatComponent->PlayHitReactionMontage(HitDirectionLetter);
			}

			bDidHit = true;

			HitBone = Hit.BoneName;
			HumanCombatComponent->DealDamage(Damage, HitBone);

			if (HitBone == NAME_None)
				HitBone = TEXT("Spine");

			// Spawns Blood FX
			if (bGenerateBloodForBluntPart || HitBone == HeadBoneName || HitBone == NeckBoneName)
				UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);
		}
		// Not Movable then play end montage in HumanCombatComponent
		else if (!Hit.Actor->IsRootComponentMovable())
		{
			bHitBlocked = true;
			bLineTrace = false;
			bDidHit = true;

			if (bGenerateSparksOnHitForBluntPart && Hit.Actor->ActorHasTag(TEXT("GenerateSparks")))
			{
				BTSparkParticleSystemComponent->Activate();
				ElapsedTimeSinceBTSparkActivated = 0;
			}
		}
	}

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Blue, false, -1, '\000', 1);
}

void AMeleeWeapon::Timer()
{
	float DeltaTime = MainTimerRefreshInterval;

	// Decativting Spark Particle System for Bladed part
	if (ElapsedTimeSinceBDSparkActivated < 5)
	{
		ElapsedTimeSinceBDSparkActivated += DeltaTime;

		if (ElapsedTimeSinceBDSparkActivated > 0.05 && BDSparkParticleSystemComponent->IsActive())
			BDSparkParticleSystemComponent->Deactivate();
	}

	// Decativting Spark Particle System for blunt part
	if (ElapsedTimeSinceBTSparkActivated < 5)
	{
		ElapsedTimeSinceBTSparkActivated += DeltaTime;

		if (ElapsedTimeSinceBTSparkActivated > 0.05 && BTSparkParticleSystemComponent->IsActive())
			BTSparkParticleSystemComponent->Deactivate();
	}

	// hit delay
	if (HitDelayTimer < HitDelay)
	{
		HitDelayTimer += DeltaTime;

		if (HitDelayTimer >= HitDelay)
			bCanHit = true;
	}

	// Hit validation
	if (ValidHitTimer < AnimationData.ValidSingleHitNotifier)
	{
		ValidHitTimer += DeltaTime;

		if (ValidHitTimer >= AnimationData.ValidSingleHitNotifier)
		{
			bIsHitValid = true;
			SetCollision(true);
		}
	}
}

// when MeleeWeapon sharp part hits sth
void AMeleeWeapon::OnBladedPartHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!bIsHitValid) return;

	bDidHit = true;

	if (bGenerateSparksOnHitForBladedPart && Hit.Actor->ActorHasTag(TEXT("GenerateSparks")))
	{
		BDSparkParticleSystemComponent->Activate();
		ElapsedTimeSinceBDSparkActivated = 0;
	}
}

// when MeleeWeapon blunt part hits sth
void AMeleeWeapon::OnBluntPartHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!bIsHitValid) return;

	bDidHit = true;

	if (bGenerateSparksOnHitForBluntPart && Hit.Actor->ActorHasTag(TEXT("GenerateSparks")))
	{
		BTSparkParticleSystemComponent->Activate();
		ElapsedTimeSinceBTSparkActivated = 0;
	}
}

void AMeleeWeapon::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	SetCollision(false);
	//bSingleAttackMode = false;
}

void AMeleeWeapon::Setup(FPlayerData InPlayer)
{
	Super::Setup(InPlayer);

	// Binding Event
	Player.AnimInst->OnMontageBlendingOut.AddDynamic(this, &AMeleeWeapon::OnMontageBlendingOut);
}

void AMeleeWeapon::Reset()
{
	bSingleAttackMode = false;
	bDidHit = false;
	bHitBlocked = false;
	HitBone = NAME_None;
	HitHumen.Empty();
}

void AMeleeWeapon::SetCollision(bool bEnabled)
{
	if (bEnabled)
	{
		if (bIsBladed)
			BladedBodyCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		if (bIsBlunt)
			BluntBodyCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		bLineTrace = true;
	}
	else
	{
		if (bIsBladed)
			BladedBodyCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		if (bIsBlunt)
			BluntBodyCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		bLineTrace = false;
	}
}

void AMeleeWeapon::Attack()
{
	if (!bCanHit) return;

	bCanHit = false;
	HitDelayTimer = 0;

	// Decides which Melee Weapon montage to play (area/single)
	TArray<FHitResult> Hits;
	FVector StartLocation, EndLocation;
	FCollisionQueryParams QueryParams = FCollisionQueryParams("Enemy Count Finder", false, Player.Character);
	QueryParams.AddIgnoredActor(this);
	FCollisionShape BoxShape = FCollisionShape::MakeBox(FVector(35, 60, 30));

	StartLocation = Player.Character->GetActorLocation() + Player.Character->GetActorForwardVector().GetSafeNormal() * 35 + Player.Character->GetActorUpVector().GetSafeNormal() * 45;
	EndLocation = StartLocation + Player.Character->GetActorForwardVector().GetSafeNormal() * 10;

	World->SweepMultiByChannel(Hits, StartLocation, EndLocation, Player.Character->GetActorQuat(), ECollisionChannel::ECC_Visibility, BoxShape, QueryParams);

	if (bDrawDebugHelpers)
		DrawDebugBox(World, StartLocation, BoxShape.GetExtent(), FColor::Turquoise, false, 2, '\000', 1);

	// Adding hit actors in order to count them!
	TArray<TWeakObjectPtr<AActor>> HitActors;
	for (FHitResult Hit : Hits)
		if (!HitActors.Contains(Hit.Actor))
			HitActors.Add(Hit.Actor);

	Reset();
	if (HitActors.Num() > 1)
	{
		Player.Combat->PlayMontage(ECombatState::MeleeAttacking, AnimationData.AnimMontage, AnimationData.AttackAreaSectionName);

		bIsHitValid = true;
		SetCollision(true);
	}
	else
	{
		FMath::RandBool() ?
			Player.Combat->PlayMontage(ECombatState::MeleeAttacking, AnimationData.AnimMontage, AnimationData.AttackSingleSectionName)
			: Player.Combat->PlayMontage(ECombatState::MeleeAttacking, AnimationData.AnimMontage, AnimationData.AttackAreaSectionName);

		bIsHitValid = false;
		bSingleAttackMode = true;
		ValidHitTimer = 0;
	}

}

void AMeleeWeapon::UnEquip(UItem_Weapon* Item, FName AttachSocketName)
{
	Super::UnEquip(Item, AttachSocketName);

	SetCollision(false);
}
