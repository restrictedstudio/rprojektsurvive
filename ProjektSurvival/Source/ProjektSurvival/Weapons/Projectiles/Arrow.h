// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Structs.h"
#include "Arrow.generated.h"

class UProjectileMovementComponent;
class UBoxComponent;

/*
Arrow projectile class
NOTE : Arrow will go through actors with "Soft" tag otherwise will simulate physics on hit.
*/
UCLASS()
class PROJEKTSURVIVAL_API AArrow : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AArrow();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void LaunchProjectile(float Speed, FRotator ArrowDefaultRotation, bool bAlignAlongCrosshairAxis = false, float AlignRange = 200);

	void SetOwnerActor(AActor* Actor);	

	void SetDamageData(FDamage DamageData) { Damage = DamageData; }

	void SetTargetLocation(FVector Target, FVector LineStart, FVector LineEnd);

	// Starts simulating physics
	void ActivatePhysicsMode();

	FVector GetArrowTipLocation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UProjectileMovementComponent* ProjectileMovement = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		USkeletalMeshComponent* Mesh = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp = nullptr;

	UPROPERTY(EditAnywhere, Category = "Debuging")
		bool bHideDebugLines = false;

	// in seconds
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float ArrowLifeTime = 600;
	// The force which will apply on hit (Multiplies by 100)
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float HitForceForMovableObjects = 50;
	// The force which will apply on hit to SkeletonMeshes (Multiplies by 100)
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float BonesHitForce = 70; 
	// Morph Target name for vibration and other stuff
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		FName MorphTargetName = TEXT("Curve");	

private:
	// all of the line traces
	void ExecuteLineTraces();
	// Vibrates Arrow using mesh morph tagets
	void VibrateArrow();
	// Align the arrow with the crosshair axis
	void AlignWithCrosshairAxis();
	// Sync MESH rotation with target for better graphics
	void SyncMeshRotationWithTargetLoc();	

	UWorld* World = nullptr;
	FName HitBone;
	AActor* OwnerActor = nullptr;
	FDamage Damage;

	//These variables are in use by the class for different stuff (interps, randomizing, etc..)
	// the depth of penetration
	float DepthVar = 1;
	int VibrateStage = 0;
	float VibrateRange = 0;
	float CurveMorphTargetValue = 0;
	float LaunchSpeed = 0;
	bool bCurveUp = true;
	bool bLineTrace = false;
	bool bVibrate = false;
	bool bDidHit = false;
	bool bGoThrough = false;
	bool bIsRotationAdjusted = false;

	// Location of crosshair hit that player aimed for
	FVector TargetLocation;
	// Crosshair line trace from camera start/end
	FVector CrosshairTraceLineStart, CrosshairTraceLineEnd;
	// The distance from arrow to crosshair axis
	float ActualDistance = 0;
	bool bAlignAlongCrosshair = false;
	FVector AlignDirection;
	float MaxAlignRange; // Sets by bow class
	float TraveledDistance = 0; // total amount of arrow travel
	FVector ExLocation = FVector(0);
	FRotator DefaultMeshRotation; // Sets by bow class
};
