// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Arrow.h"
#include "PenetrableProjectileComponent.h"
#include "../../Characters/HumanCharacter.h"
#include "../../Characters/HumanCombatComponent.h"
#include "../../Items/PhysicalItem.h"
#include "../../Items/Item.h"

// Sets default values
ABullet::ABullet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	//SetActorTickEnabled(false);

	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComp->SetCollisionProfileName(TEXT("Bullet"));
	RootComp->SetNotifyRigidBodyCollision(true);
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->SetBoxExtent(FVector(5, 1, 1));
	SetRootComponent(RootComp);

	ProjectileMovement = CreateDefaultSubobject<UPenetrableProjectileComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->bSweepCollision = false;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->ProjectileGravityScale = 0;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	BulletMesh->SetCollisionProfileName(TEXT("NoCollision"));

	BulletTrail = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Trail"));
	BulletTrail->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	BulletTrail->SetCollisionProfileName(TEXT("NoCollision"));
	BulletTrail->SetVisibility(false);

	BulletTrailParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet Trail Particle System"));
	BulletTrailParticleSystem->bAutoActivate = true;
	BulletTrailParticleSystem->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	// Shell Stuff
	ShellCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Shell Collision"));
	ShellCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ShellCollision->SetCapsuleSize(1, 5);
	ShellCollision->SetRelativeLocation(FVector(-20, 0, 0));
	ShellCollision->SetRelativeRotation(FRotator(-90, 0, 0));
	ShellCollision->SetMassOverrideInKg(NAME_None, 1);
	ShellCollision->SetLinearDamping(1);
	ShellCollision->SetGenerateOverlapEvents(false);
	ShellCollision->SetNotifyRigidBodyCollision(false);
	ShellCollision->SetCollisionProfileName(TEXT("NoCollision"));

	ShellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Shell Mesh"));
	ShellMesh->AttachToComponent(ShellCollision, FAttachmentTransformRules::KeepRelativeTransform);
	ShellMesh->SetCollisionProfileName(TEXT("NoCollision"));
}

/*
* This method needs to be called after spawning the bullet or bullet won't work at all
*
* @param OwnerActor : is the shooter!
* @param Deceleration : the deceleration after effective range.
* @param MaxHitRange : Bullet will be destroyed after exceeds this range.
* @param bCloseRangeShot : if true then bullet will go straight to Target and Start aligning with crosshair direction after first hit.
* @param bSpawnShell : Whether spawn shell or not
* @param bWillShellBeThrownToTheRight : Shell will velocity will depend on this.
*/
void ABullet::Setup(FDamage DamageData, AActor* OwnerActorArg, int LaunchSpeedArg, int EffectiveRangeArg, int MaxHitRangeArg, float DecelerationArg, bool bInCloseRangeShot, float AlignRangeArg, bool bSpawnShellArg, FVector ShellSpawnLocationArg, bool bWillShellBeThrownToTheRightArg)
{
	Damage = DamageData;
	OwnerActor = OwnerActorArg;
	LaunchSpeed = LaunchSpeedArg;
	EffectiveRange = EffectiveRangeArg;
	Deceleration = DecelerationArg;
	MaxHitRange = MaxHitRangeArg;
	ShellSpawnLocation = ShellSpawnLocationArg;
	bSpawnShell = bSpawnShellArg;
	bWillShellSpawnToTheRight = bWillShellBeThrownToTheRightArg;
	bCloseRangeShot = bInCloseRangeShot;
	bAlignAlongCrosshair = !bCloseRangeShot;
	MaxAlignRange = AlignRangeArg;

	if (!bSpawnShell)
		ShellMesh->DestroyComponent(), ShellCollision->DestroyComponent(), bIsShellDestroyed = true;

	Initialize();

	if (bAlignAlongCrosshair)
	{
		FVector Loc = GetActorLocation();
		Loc.Z = 0;
		ActualDistance = UKismetMathLibrary::GetPointDistanceToLine(Loc, CrosshairTraceLineStart, (CrosshairTraceLineEnd - CrosshairTraceLineStart).GetSafeNormal());
	}
}

void ABullet::SetTarget(FRotator Rotation, FVector LineStart, FVector LineEnd)
{
	TargetRotation = Rotation;
	CrosshairTraceLineStart = LineStart;
	CrosshairTraceLineEnd = LineEnd;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();

	MinSpeedForPenetration *= 100;
	HitForceForMovableObjects *= 100;

	World = GetWorld();
	RootComp->OnComponentHit.AddDynamic(this, &ABullet::OnHit);

	ExLocation = GetActorLocation();
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//DrawDebugPoint(GetWorld(), GetActorLocation(), 7, FColor::Blue, false, 2);

	SetBulletSpeed();

	AlignWithCrosshairAxis();

	TraveledDistance += (GetActorLocation() - ExLocation).Size();	

	if (TraveledDistance > EffectiveRange)
		ProjectileMovement->MaxSpeed = FMath::FInterpTo(ProjectileMovement->MaxSpeed, ProjectileMovement->MaxSpeed - 100, World->DeltaTimeSeconds, Deceleration);

	// Start Destroying
	if (TraveledDistance > MaxHitRange)
		StartDestruction();

	// Effective range finishes and gravity starts taking effect
	if (EffectiveRange > 0 && TraveledDistance > EffectiveRange)
		ProjectileMovement->ProjectileGravityScale = 1, EffectiveRange = -1;

	BulletPenetrationTick();

	ExLocation = GetActorLocation();
}

// Sets CurrentSpeed, LastSpeed and Detects bullet stucking...
void ABullet::SetBulletSpeed()
{
	if (PauseSpeedVariablesUpdate) return;

	constexpr int8 StuckDetectorFrame = 12;
	const float BulletSpeed = ProjectileMovement->Velocity.Size();
		
	// If bullet was not moving for StuckDetectorFrame then we realize it's stuck
	if (GetActorLocation() == ExLocation)
	{
		if (StuckDetector < StuckDetectorFrame + 2)
			++StuckDetector;

		// Bullet's Stuck Event
		if (StuckDetector == StuckDetectorFrame)
		{
			UE_LOG(LogTemp, Error, TEXT("Bullet was stuck!"));
			StartDestruction();
		}
	}
	// Because bullet has PenetratingSafeSpeed during penetration and current speed shouldnt be updated.
	else if (!bIsPenetrating)
	{
		CurrentSpeed = BulletSpeed;

		if (CurrentSpeed > MinAllowedSpeed)
			LastSpeed = ProjectileMovement->Velocity;
		else
			StartDestruction(); // Destroys the bullet if the speed was lower than MinAllowedSpeed		

		StuckDetector = 0;
	}
}

void ABullet::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Hit.Actor == LastHitActor) return;
	LastHitActor = Hit.Actor;

	PauseSpeedVariablesUpdate = true;
	World->GetTimerManager().ClearTimer(SpeedCheckerPauseTimer);
	World->GetTimerManager().SetTimer(SpeedCheckerPauseTimer, this, &ABullet::ResumeSpeedVariablesUpdate, 0.2);

	if (bCloseRangeShot)
	{
		FVector Loc = GetActorLocation();
		Loc.Z = 0;
		ActualDistance = UKismetMathLibrary::GetPointDistanceToLine(Loc, CrosshairTraceLineStart, (CrosshairTraceLineEnd - CrosshairTraceLineStart).GetSafeNormal());
		bAlignAlongCrosshair = true;
		SetBulletRotationToFaceTarget();

		bCloseRangeShot = false;
	}

	// Hitting Human
	if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>())
	{
		FName HitBone = Hit.BoneName;
		if (HitBone == NAME_None) return;

		UHumanCombatComponent* HumanCombatComponent = Hit.Actor->FindComponentByClass<UHumanCombatComponent>();

		// Penetration Decision
		if (CurrentSpeed < MinSpeedForPenetration || HumanCombatComponent->IsRagdoll())
			StartDestruction();
		else
			SetPenetrating(true, LastHitActor);

		USkeletalMeshComponent* HumanMesh = Cast<USkeletalMeshComponent>(Hit.Component);

		HumanCombatComponent->ApplyPhysicalAnimation(Cast<AHumanCharacter>(Hit.Actor), HumanMesh, HitBone);
		HumanCombatComponent->DealDamage(Damage, HitBone);

		// Spawning Blood FX
		UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);

		// Adding Impulse to simulate human reaction
		HumanMesh->AddImpulseToAllBodiesBelow(GetActorForwardVector().GetSafeNormal() * 5000 /*EXPERIMENTAL to get the best result for bullet penetertation*/, HitBone);
	}
	// Hitting Non Human
	else
	{
		if (Hit.Actor->GetClass()->IsChildOf<APhysicalItem>())
		{
			APhysicalItem* PhItem = Cast<APhysicalItem>(Hit.GetActor());

			if (PhItem->GetItem()->GetItemType() == EItemType::Consumable)
				PhItem->Destroy();
			// Bullet adds force to movable objects			
			else
				PhItem->ActivatePhysics()->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects / 10, Hit.Location);

			// Destroy the current bullet
			StartDestruction();
		}
		// Ricochet System & Hitting Arrow With Bullet
		else if (Hit.Actor->ActorHasTag(TEXT("Ricochet")) || Hit.Actor->GetClass()->IsChildOf<AArrow>())
		{
			// Finding the angle between hit point normal and the bullet backward			
			float DeltaAngle = Hit.Actor->ActorHasTag(TEXT("Ricochet")) ?
				FindDeltaAngle(Hit.ImpactNormal, -1 * GetActorForwardVector())
				: FMath::RandRange(-60, 60);

			ProjectileMovement->bRotationFollowsVelocity = false;
			AddActorWorldRotation(FRotator(0, (-2 * DeltaAngle) + 180, 0));
			AddActorWorldOffset(GetActorForwardVector() * 10);
			ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * LastSpeed.Size());
			ProjectileMovement->bRotationFollowsVelocity = true;

			float MaxRange = FMath::Clamp<float>(MaxHitRange / 10, 100, 300);
			EffectiveRange = TraveledDistance + MaxRange / 2;
			MaxHitRange = TraveledDistance + MaxRange;

			// Spawning Bullet Effect Decal
			if (Hit.Actor->ActorHasTag(TEXT("Ricochet")))
			{
				FRotator DecalRotation = Hit.ImpactNormal.Rotation();
				DecalRotation.Roll -= GetActorRotation().Pitch;
				SpawnBulletHole(Hit.GetComponent(), Hit.Location, DecalRotation, FVector(3.5, FMath::Abs(DeltaAngle / 90) * 20, 3.5), false);
			}

		}
		// Bullet penetration for non human stuff
		else if (Hit.Actor->ActorHasTag(TEXT("BulletPenetration")))
		{
			// Spawning Bullet Hole Decal
			SpawnBulletHole(Hit.GetComponent(), Hit.Location, Hit.Normal.Rotation());

			// Bullet adds force to movable objects
			if (Hit.Actor->IsRootComponentMovable() && Hit.Actor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>() && Hit.Actor->GetRootComponent()->IsSimulatingPhysics())
				Cast<UPrimitiveComponent>(Hit.Actor->GetRootComponent())->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects, Hit.Location);

			// Checking whether can penetrate or not
			if (CurrentSpeed < MinSpeedForPenetration)
				StartDestruction();
			else
				SetPenetrating(true, LastHitActor);

		}
		// if hit another bullet or ground just destory.
		else if (Hit.Actor->GetClass()->IsChildOf<ABullet>() || Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
			StartDestruction();
		else
		{
			// Bullet adds force to movable objects
			if (Hit.Actor->IsRootComponentMovable() && Hit.Actor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>() && Hit.Actor->GetRootComponent()->IsSimulatingPhysics())
				Cast<UPrimitiveComponent>(Hit.Actor->GetRootComponent())->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects, Hit.Location);

			// Spawning Bullet Hole decal
			SpawnBulletHole(Hit.GetComponent(), Hit.Location, Hit.Normal.Rotation());

			// Trigger Destroying process...
			StartDestruction();
		}
	}
}

void ABullet::SetBulletRotationToFaceTarget()
{
	ProjectileMovement->bRotationFollowsVelocity = false;
	SetActorRotation(TargetRotation);	
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * CurrentSpeed);
	ProjectileMovement->bRotationFollowsVelocity = true;
}

float ABullet::FindDeltaAngle(FVector Vec1, FVector Vec2)
{
	// No Z Offset	
	Vec1.Z = 0;
	Vec2.Z = 0;

	// Normalizing
	Vec1.Normalize();
	Vec2.Normalize();

	float DeltaAngle =
		/* 0.Gets the angle between Vec1 & Vec2 */
		UKismetMathLibrary::DegAcos(FVector::DotProduct(
			Vec1,
			Vec2
		));

	// 2.Figuring out the sign of DeltaAngle (+/-)	
	if (FVector::CrossProduct(Vec1, Vec2).Z < 0) DeltaAngle = -DeltaAngle;

	return DeltaAngle;
}

void ABullet::SpawnBulletHole(USceneComponent* ComponentToAttach, FVector Location, FRotator Rotation, FVector Scale, bool bRandomizeRoll)
{
	if (!BulletHoleDecal)
	{
		UE_LOG(LogTemp, Error, TEXT("BulletHole Decal is not set!"));
		return;
	}

	const float LifeTime = 60;

	if (bRandomizeRoll)
		Rotation.Roll += FMath::RandRange(0, 180);

	if (FVector(0).Equals(Scale))
		Scale = FVector(FMath::RandRange(6, 8));

	UDecalComponent* UDC = UGameplayStatics::SpawnDecalAttached(BulletHoleDecal, FVector(Scale), ComponentToAttach, NAME_None, Location, Rotation, EAttachLocation::KeepWorldPosition, LifeTime);
	UDC->SetFadeScreenSize(0.001);
}

void ABullet::Initialize()
{
	SpawnTimer = 0;

	// Shell stuff
	if (bSpawnShell)
	{
		ShellCollision->SetCollisionProfileName(TEXT("BulletShell"));
		ShellCollision->SetSimulatePhysics(true);
		int Random = FMath::RandRange(25, 50);

		// Generating random impulse for shell
		FVector Force = (GetActorUpVector().GetSafeNormal() * Random)
			+ (GetActorRightVector().GetSafeNormal() * (bWillShellSpawnToTheRight ? 1 : -1) * Random)
			+ (GetActorForwardVector().GetSafeNormal() * -Random);

		// Applying Angular and Linear force
		ShellCollision->AddAngularImpulseInDegrees(Force * 5);
		ShellCollision->AddImpulse(Force * 5);
	}

	// Activates Projectile
	ProjectileMovement->bSweepCollision = true;
	ProjectileMovement->MaxSpeed = (float)LaunchSpeed;
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * LaunchSpeed);
	CurrentSpeed = LaunchSpeed;
	LastSpeed = ProjectileMovement->Velocity;
	ProjectileMovement->Activate();

	// Starts the main timer.
	World->GetTimerManager().SetTimer(MainTimer, this, &ABullet::Timer, MainTimerRefreshInterval, true);

	SetActorTickEnabled(true);
	// MAX Life of a bullet
	SetLifeSpan(MaxBulletLifetime);
}

// On Actor's Destory
void ABullet::DestroyBullet()
{
	if (bIsShellDestroyed)
		Destroy();
	else
	{
		SetRootComponent(ShellCollision);
		if (ensure(RootComp))
			RootComp->DestroyComponent(), RootComp = nullptr;
	}
}

void ABullet::Timer()
{
	SpawnTimer += MainTimerRefreshInterval;

	// Destroying Process
	if (DestroyTimer < DestroyDelay)
	{
		DestroyTimer += MainTimerRefreshInterval;

		if (DestroyTimer >= DestroyDelay)
			DestroyBullet();
	}
	//TODO Clean this up
	else if (SpawnTimer > 0.1 && BulletTrail && !BulletTrail->IsVisible())
		BulletTrail->SetVisibility(true);

	// Shell Destroying Process
	if (!bIsShellDestroyed && SpawnTimer > BulletShellLifetime)
		if (RootComp)
			Destroy();
		else
			ShellMesh->DestroyComponent(), ShellCollision->DestroyComponent(), bIsShellDestroyed = true;
}

void ABullet::AlignWithCrosshairAxis()
{
	if (!bAlignAlongCrosshair) return;

	// Aligning Bullet with Target direction					
	float Offset = -((ActualDistance / 2) - (75 * (LaunchSpeed / 10000)) * World->DeltaTimeSeconds);
	if (ActualDistance + Offset > 3 && TraveledDistance < MaxAlignRange)
	{		
		AddActorLocalOffset(FVector(0, Offset, 0), !bIsPenetrating);
		ActualDistance += Offset;
	}
	else
	{
		AddActorLocalOffset(FVector(0, -ActualDistance, 0), !bIsPenetrating);
		bAlignAlongCrosshair = false;
		if (bIsPenetrating)
			bInvalidLocation = true;
		//DrawDebugPoint(GetWorld(), GetActorLocation(), 10, FColor::Orange, false, 5);
	}
}

void ABullet::StartDestruction()
{
	DestroyTimer = 0;

	BulletTrailParticleSystem->Deactivate();
	ProjectileMovement->Deactivate();
	RootComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	BulletTrail->DestroyComponent();
	BulletTrail = nullptr;

	if (BulletMesh)
		BulletMesh->DestroyComponent(), BulletMesh = nullptr;

	SetActorTickEnabled(false);
}

// Starts and Stops penetrating mode
void ABullet::SetPenetrating(bool bEnabled, TWeakObjectPtr<AActor> InPenetratingActor)
{
	ProjectileMovement->bSweepCollision = !bEnabled;
	RootComp->SetCollisionEnabled(bEnabled ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);

	if (!bEnabled)
		OnPenetrationEnded();
	else
	{
		float PenetrationSpeed = PenetrationSafeSpeed < (CurrentSpeed / 2) ? PenetrationSafeSpeed : CurrentSpeed / 2;
		ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * PenetrationSpeed);
		InPenetratingActor->GetActorBounds(true, PenetratingActor.Origin, PenetratingActor.Bounds);
		PenetratingActor.Bounds += FVector(RootComp->GetScaledBoxExtent().X * 2);
		//DrawDebugBox(World, PenetratingActor.Origin, PenetratingActor.Bounds, FColor::Red, false, 5, '\000', 1);
	}

	bIsPenetrating = bEnabled;
}

// Check whether the bullet is in actor or not and detects penetration's ending
void ABullet::BulletPenetrationTick()
{
	if (!bIsPenetrating) return;

	if (!bAlignAlongCrosshair && !IsBulletInPenetratingActor())
		if (bInvalidLocation)
			bInvalidLocation = false;
		else
			SetPenetrating(false);
	//DrawDebugPoint(GetWorld(), GetActorLocation(), 10, FColor::Green, false, 5);
}

//TODO Move to thread
// Is bullet within actor bounds
bool ABullet::IsBulletInPenetratingActor()
{
	float BulletLength = RootComp->GetScaledBoxExtent().X;
	// Bullet Location
	FVector ActorForward = GetActorForwardVector().GetSafeNormal()
		, BulletTip = GetActorLocation() + ActorForward * BulletLength
		, BulletTail = BulletTip + ActorForward * -2 * BulletLength;

	FVector MinRange = PenetratingActor.Origin - PenetratingActor.Bounds
		, MaxRange = PenetratingActor.Origin + PenetratingActor.Bounds;

	bool bTipInXRange = BulletTip.X >= MinRange.X && BulletTip.X <= MaxRange.X;
	bool bTailInXRange = BulletTail.X >= MinRange.X && BulletTail.X <= MaxRange.X;

	bool bTipInYRange = BulletTip.Y >= MinRange.Y && BulletTip.Y <= MaxRange.Y;
	bool bTailInYRange = BulletTail.Y >= MinRange.Y && BulletTail.Y <= MaxRange.Y;

	bool bTipInZRange = BulletTip.Z >= MinRange.Z && BulletTip.Z <= MaxRange.Z;
	bool bTailInZRange = BulletTail.Z >= MinRange.Z && BulletTail.Z <= MaxRange.Z;

	if ((bTipInXRange || bTailInXRange)
		&&
		(bTipInYRange || bTailInYRange)
		&&
		(bTipInZRange || bTailInZRange))
		return true;

	return false;
}

void ABullet::OnPenetrationEnded()
{
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * (CurrentSpeed / 2));
	EffectiveRange = 1;
	MaxHitRange = TraveledDistance + (MaxHitRange - TraveledDistance) / 2;

	if (LastHitActor->GetClass()->IsChildOf<AHumanCharacter>()) return;

	FVector StartLocation = GetActorLocation() + GetActorForwardVector() * 10
		, EndLocation = StartLocation + GetActorForwardVector() * -150;

	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, true /* Complexity */, this /* Actor To Ignore */);

	//DrawDebugLine(World, StartLocation, EndLocation, FColor::Purple, false, 5, '\000', 3);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams))
		// Spawning Bullet Hole Decal at the exit location
		SpawnBulletHole(Hit.GetComponent(), Hit.Location, Hit.Normal.Rotation());
}