// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Structs.h"
#include "Bullet.generated.h"

class UPenetrableProjectileComponent;
class UBoxComponent;
class UCapsuleComponent;
class UMaterialInterface;
class UParticleSystemComponent;

USTRUCT()
struct FActorBounds
{
	GENERATED_BODY()

public:
	FVector Origin;
	FVector Bounds;

	FActorBounds()
	{
		Origin = FVector(0);
		Bounds = FVector(0);
	}
};

/*
Bullet Class

Behaviour :
 Has EffectiveRange which bullet will go straight forward and after that will start falling

Tags :
	"BulletPenetration" tag for Things will let the bullet to penetrate
	"Ricochet" tag for the objects that will make the bullet ricochet

*/
UCLASS()
class PROJEKTSURVIVAL_API ABullet : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABullet();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*
	* This method needs to be called after spawning the bullet or bullet won't work at all
	*
	* @param OwnerActor : is the shooter!
	* @param Deceleration : the deceleration after effective range.
	* @param MaxHitRange : Bullet will be destroyed after exceeds this range.
	* @param bInCloseRangeShot : if true then bullet will go straight to Target and Start aligning with crosshair direction after first hit.
	* @param bSpawnShell : Whether spawn shell or not
	* @param bWillShellBeThrownToTheRight : Shell will velocity will depend on this.
	*/
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Setup(FDamage DamageData, AActor* OwnerActorArg, int LaunchSpeedArg, int EffectiveRangeArg, int MaxHitRangeArg, float DecelerattionArg, bool bInCloseRangeShot = false, float AlignRangeArg = 200
			, bool bSpawnShellArg = false, FVector ShellSpawnLocationArg = FVector::ZeroVector, bool bWillShellBeThrownToTheRightArg = true);

	void SetTarget(FRotator Rotation, FVector LineStart, FVector LineEnd);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UPenetrableProjectileComponent* ProjectileMovement = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* BulletMesh = nullptr;

	//TODO Remove this after you added a proper particle system
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* BulletTrail = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UParticleSystemComponent* BulletTrailParticleSystem = nullptr;

	// Shell
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* ShellCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* ShellMesh = nullptr;

	// Bullet Hole Decal
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		UMaterialInterface* BulletHoleDecal;
	// Bullet will get destoryed if it's speed was lower than this value
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float MinAllowedSpeed = 500;
	// Bullet will be destoryed automatically after this time elapsed
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float MaxBulletLifetime = 10;
	// in Seconds
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float BulletShellLifetime = 3.5;
	// will penetrate if speed was more than this (Multiplies by 100) CM/S
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float MinSpeedForPenetration = 20;
	// The force which will apply on hit | Watchout while changing this... Peneteration may get glitchy (Multiplies by 100) CM/S
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float HitForceForMovableObjects = 50;
	// Think out FPS penetration may get super glitchy in low frame rates
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float PenetrationSafeSpeed = 4000;

private:

	void Initialize();
	void DestroyBullet();
	// Align the bullet with the crosshair axis
	void AlignWithCrosshairAxis();

	bool PauseSpeedVariablesUpdate = false;
	FTimerHandle SpeedCheckerPauseTimer;
	void ResumeSpeedVariablesUpdate() { PauseSpeedVariablesUpdate = false; }

	void SetBulletRotationToFaceTarget();

	// Sets CurrentSpeed, LastSpeed and Detects bullet stucking...
	void SetBulletSpeed();

	float FindDeltaAngle(FVector Vec1, FVector Vec2);

	// * @param Scale will be randomized autoamatically
	void SpawnBulletHole(USceneComponent* ComponentToAttach, FVector Location, FRotator Rotation, FVector Scale = FVector(0), bool bRandomizeRoll = true);

	const float MainTimerRefreshInterval = 0.2;
	FTimerHandle MainTimer;
	void Timer();

	UWorld* World = nullptr;
	// the Shooter
	AActor* OwnerActor = nullptr;
	TWeakObjectPtr<AActor> LastHitActor;
	FDamage Damage;

	// Starts destruction process
	void StartDestruction();
	bool bIsDestroyed = false;
	
	int8 StuckDetector = 0;

	bool bIsPenetrating = false;
	// Starts and Stops penetrating mode
	void SetPenetrating(bool bEnabled, TWeakObjectPtr<AActor> InPenetratingActor = nullptr);		
	// This will be true when aligning completes during penetration
	// and it will disable penetration out of the box checker for one frame.
	bool bInvalidLocation = false;	
	void OnPenetrationEnded();
	// Check whether the bullet is in actor or not and detects penetration's ending
	void BulletPenetrationTick();
	// Is bullet within actor bounds
	bool IsBulletInPenetratingActor();
	// bounds and origin of penetrating actor
	FActorBounds PenetratingActor = FActorBounds();

	// last non-zero speed
	FVector LastSpeed;
	FVector ShellSpawnLocation;
	// Ex Location of bullet to calculate the TraveledDistance of the bullet
	FVector ExLocation;

	// Location of crosshair hti that player aimed for
	FRotator TargetRotation;
	// Crosshair line trace from camera start/end
	FVector CrosshairTraceLineStart, CrosshairTraceLineEnd;

	// The distance from bullet to crosshair axis
	float ActualDistance = 0;
	bool bAlignAlongCrosshair = false;
	float MaxAlignRange;
	bool bCloseRangeShot = false;

	int LaunchSpeed;
	// the range that bullet will go straight forward
	int EffectiveRange;
	int MaxHitRange;
	// the deceleration after effective range
	float Deceleration = 2.5;
	float TraveledDistance = 0;
	// Elapsed Time since projectile spawned
	float SpawnTimer = 0;
	float CurrentSpeed = 0;
	float DestroyTimer = 100;
	float DestroyDelay = 0.5;

	// Left or Right Vector
	bool bWillShellSpawnToTheRight;
	// Launches when this bool becomes true
	bool bIsSet = false;
	bool bIsShellDestroyed = false;	
	bool bSpawnShell = false;
};