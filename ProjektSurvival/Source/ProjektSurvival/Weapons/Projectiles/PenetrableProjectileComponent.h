// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PenetrableProjectileComponent.generated.h"

/**
 * Inhertied projectile movement that will let projectile to penetrate through objects
 * actually this projectile movement wont stop moving after blocking hit.
 */
UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent), ShowCategories = (Velocity))
class PROJEKTSURVIVAL_API UPenetrableProjectileComponent : public UProjectileMovementComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	UPenetrableProjectileComponent();

protected:

	EHandleBlockingHitResult HandleBlockingHit(const FHitResult& Hit, float TimeTick, const FVector& MoveDelta, float& SubTickTimeRemaining) override { return EHandleBlockingHitResult::AdvanceNextSubstep; }

};
