// Fill out your copyright notice in the Description page of Project Settings.

#include "Arrow.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "../../Characters/HumanCharacter.h"
#include "../../Characters/HumanCombatComponent.h"
#include "../../Items/Item.h"
#include "../../Items/PhysicalItem.h"

// Sets default values
AArrow::AArrow()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->SetNotifyRigidBodyCollision(false);
	SetRootComponent(RootComp);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->bAutoActivate = false;
	ProjectileMovement->bSweepCollision = false;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->MaxSpeed = 0;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Arrow Mesh"));
	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AArrow::BeginPlay()
{
	Super::BeginPlay();

	HitForceForMovableObjects *= 100;
	BonesHitForce *= 100;

	SetLifeSpan(ArrowLifeTime);

	World = GetWorld();
	RootComp->OnComponentHit.AddDynamic(this, &AArrow::OnHit);
}

// Called every frame
void AArrow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bVibrate)
		VibrateArrow();

	// Makes arrow go through objects with "Soft" tag
	if (bGoThrough && bDidHit && DepthVar)
	{
		DepthVar = FMath::FInterpTo(DepthVar, DepthVar - 1, World->DeltaTimeSeconds, 60);
		if (DepthVar < 0.1)
			DepthVar = 0;

		RootComp->AddWorldOffset(GetRootComponent()->GetForwardVector() * World->DeltaTimeSeconds * 999);
	}

	// If arrow was colliding with sth that it shouldnt has (e.g Archer, etc...), the arrow will be moved to a point that can be launched from there!
	if (ensure(ProjectileMovement) && ProjectileMovement->IsActive() && ProjectileMovement->Velocity.Size() == 0 && !bDidHit)
	{
		RootComp->AddWorldOffset(GetRootComponent()->GetForwardVector() * World->DeltaTimeSeconds * 999, false);
		ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * LaunchSpeed);
	}

	if (!ProjectileMovement->IsActive()) return;

	ExecuteLineTraces();
	AlignWithCrosshairAxis();
	SyncMeshRotationWithTargetLoc();

	TraveledDistance += (GetActorLocation() - ExLocation).Size();
	ExLocation = GetActorLocation();
}

// Vibrates Arrow using mesh morph tagets
void AArrow::VibrateArrow()
{
	// Generates a random vibration ratio to get a realistic vibration for each arrow.
	if (!VibrateRange && !VibrateStage)
		VibrateRange = FMath::FRandRange(0.5, 1);

	// Stop Vibrating if VibrateRange was small
	if (VibrateRange <= 0.05 || (RootComp->IsSimulatingPhysics() && VibrateStage % 2 != 0))
	{
		CurveMorphTargetValue = FMath::FInterpTo(CurveMorphTargetValue, 0, World->DeltaTimeSeconds, 10);
		Mesh->SetMorphTarget(MorphTargetName, FMath::Clamp<float>(CurveMorphTargetValue, -1, 1));
		if (CurveMorphTargetValue == 0)
			bVibrate = false;

		return;
	}

	// Changes the curve direction after reaching max or min morph value
	if ((CurveMorphTargetValue >= VibrateRange && bCurveUp) || (CurveMorphTargetValue <= -VibrateRange && !bCurveUp))
	{
		bCurveUp = !bCurveUp, VibrateStage++;

		// After each full Vibration (up/down or left/right)
		if (VibrateStage % 2 == 0)
			VibrateRange /= 2;
	}

	// Interps the morph target value
	if (bCurveUp)
		CurveMorphTargetValue = FMath::FInterpTo(CurveMorphTargetValue, CurveMorphTargetValue + 5, World->DeltaTimeSeconds, 4);
	else
		CurveMorphTargetValue = FMath::FInterpTo(CurveMorphTargetValue, CurveMorphTargetValue - 5, World->DeltaTimeSeconds, 4);

	Mesh->SetMorphTarget(MorphTargetName, FMath::Clamp<float>(CurveMorphTargetValue, -1, 1));
}

void AArrow::AlignWithCrosshairAxis()
{
	if (!bAlignAlongCrosshair) return;

	// Aligning With with Target direction				
	FVector DeltaLocation = GetActorLocation();
	float OffsetDamper = 75 * (LaunchSpeed / 4000);
	float Offset = (ActualDistance / 2) - (OffsetDamper < ActualDistance / 2 ? (75 * (LaunchSpeed / 4000)) : 0) * World->DeltaTimeSeconds;

	if (ActualDistance - Offset > 3 && TraveledDistance < MaxAlignRange)
	{
		RootComp->AddWorldOffset(AlignDirection * FMath::Abs(Offset), true);
		DeltaLocation -= GetActorLocation();
		ActualDistance -= DeltaLocation.Size();
	}
	else
	{
		RootComp->AddWorldOffset(AlignDirection * ActualDistance, true);
		DeltaLocation -= GetActorLocation();
		ActualDistance -= DeltaLocation.Size();

		if (FMath::TruncToInt(ActualDistance) == 0)
		{
			bAlignAlongCrosshair = false;
			ProjectileMovement->bSweepCollision = true;
			RootComp->SetNotifyRigidBodyCollision(true);
		}
	}

}

void AArrow::SyncMeshRotationWithTargetLoc()
{
	if (bIsRotationAdjusted) return;

	FVector ActorLoc = GetActorLocation();

	// Seting mesh rotation to face the target location
	FVector TargetLookAt = (TargetLocation - ActorLoc).GetSafeNormal();
	TargetLookAt.Z = 0;
	FVector MeshForward = Mesh->GetForwardVector().GetSafeNormal();
	MeshForward.Z = 0;

	int DeltaAngle = FMath::TruncToInt( // 1.Rounds down DeltaAngle		
		UKismetMathLibrary::DegAcos(FVector::DotProduct(
			MeshForward
			, TargetLookAt
		)));

	if (DeltaAngle > 1 && (TargetLocation - ActorLoc).Size() > 50)
	{
		FRotator MeshRotation = Mesh->GetComponentRotation();
		if (FVector::CrossProduct(MeshForward, TargetLookAt).Z < 0) DeltaAngle = -DeltaAngle;

		Mesh->SetWorldRotation(FRotator(MeshRotation.Pitch,
			FMath::FInterpTo(MeshRotation.Yaw, MeshRotation.Yaw + DeltaAngle, World->DeltaTimeSeconds, 16)
			, MeshRotation.Roll));
	}
	else
	{
		Mesh->AddWorldRotation(FRotator(0, DeltaAngle, 0));
		bIsRotationAdjusted = true;
	}
}

//TODO if arrow was not inside the hit mesh then simulate physics! (but how?)
void AArrow::ExecuteLineTraces()
{
	if (!bLineTrace || bDidHit) return;

	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, true, OwnerActor);
	QueryParams.AddIgnoredActor(this);
	FVector StartLocation, EndLocation;
	StartLocation = RootComp->GetComponentLocation() + RootComp->GetForwardVector().GetSafeNormal() * (RootComp->GetScaledBoxExtent().X / 2);
	EndLocation = StartLocation + RootComp->GetForwardVector().GetSafeNormal() * ((RootComp->GetScaledBoxExtent().X / 2) + 15);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel4 /* Projectile Trace */, QueryParams))
	{
		// If Actor had no "Soft" TAG then physics will be simulated before the hit!		
		if (!Hit.Actor->ActorHasTag(TEXT("Soft")))
		{
			// Arrow effect on Physical Items (Pickable items)
			if (Hit.Actor->GetClass()->IsChildOf<APhysicalItem>())
			{
				APhysicalItem* PhItem = Cast<APhysicalItem>(Hit.GetActor());
				// Destroying ConsumableItem
				if (PhItem->GetItem()->GetItemType() == EItemType::Consumable)
					PhItem->Destroy();
				// Adding Force
				else
					PhItem->ActivatePhysics()->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects / 10, Hit.Location);
			}

			RootComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			RootComp->SetSimulatePhysics(true);
			bCurveUp = FMath::RandBool(), bVibrate = true;
			bLineTrace = false;
			ProjectileMovement->SetVelocityInLocalSpace(FVector(0));
			ProjectileMovement->Deactivate();
		}
		// Hitting Landscape simulations!
		else if (Hit.Actor->GetClass()->GetName() == TEXT("Landscape"))
		{
			bDidHit = true;
			bGoThrough = true;
			DepthVar += FMath::FRandRange(0.5, 1.5);
			RootComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			bLineTrace = false;
			ProjectileMovement->SetVelocityInLocalSpace(FVector(0));
			ProjectileMovement->Deactivate();
		}
		// hitting "Soft" actors
		else if (Hit.Actor->ActorHasTag(TEXT("Soft")))
		{
			// Hitting Human
			if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>())
			{
				HitBone = Hit.BoneName;
				if (HitBone == NAME_None) return;

				DepthVar += FMath::FRandRange(0, 0.5);

				USkeletalMeshComponent* HumanMesh = Cast<USkeletalMeshComponent>(Hit.Component);
				RootComp->AttachToComponent(HumanMesh, FAttachmentTransformRules::KeepWorldTransform, HitBone);

				UHumanCombatComponent* HumanCombatComponent = Hit.Actor->FindComponentByClass<UHumanCombatComponent>();
				HumanCombatComponent->ApplyPhysicalAnimation(Cast<AHumanCharacter>(Hit.Actor), HumanMesh, HitBone);
				HumanCombatComponent->DealDamage(Damage, HitBone);
				// Spawning Blood FX
				UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);

				// Adding Impulse to simulate human reaction
				HumanMesh->AddImpulseToAllBodiesBelow(RootComp->GetForwardVector().GetSafeNormal() * BonesHitForce, HitBone);
			}
			// Hitting Non Human
			else
			{
				if (Hit.Actor->IsRootComponentMovable() && Hit.Actor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>() && Hit.Actor->GetRootComponent()->IsSimulatingPhysics())
					Cast<UPrimitiveComponent>(Hit.Actor->GetRootComponent())->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects, Hit.Location);

				RootComp->SetSimulatePhysics(false);
				RootComp->AttachToComponent(Hit.Actor->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform, NAME_None);
				DepthVar += FMath::FRandRange(-0.5, 0.75);
				bCurveUp = FMath::RandBool();
				bVibrate = true;
			}

			RootComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			ProjectileMovement->SetVelocityInLocalSpace(FVector(0));
			ProjectileMovement->Deactivate();
			bLineTrace = false;
			bDidHit = true;
			bGoThrough = true;
		}
	}
}

void AArrow::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (bDidHit || OwnerActor == OtherActor || (Hit.Actor->GetClass()->IsChildOf<AArrow>() && Hit.Actor->GetVelocity().Size() < 10)) return;

	bDidHit = true;

	if (!bHideDebugLines)
		DrawDebugPoint(World, Hit.Location, 10, FColor::Green, false, 5);
	//NOTE : these are the exact same code in line trace, the reason behind this is insurance... we dont want arrow to miss anything!
	// Hitting actors that arrow goes through them and stuck!
	if (OtherActor->ActorHasTag(TEXT("Soft")))
	{
		// Hitting humen
		if (OtherActor->GetClass()->IsChildOf<AHumanCharacter>())
		{
			HitBone = Hit.BoneName;
			if (HitBone == NAME_None)
			{
				bDidHit = false;
				return;
			}

			USkeletalMeshComponent* HumanMesh = Cast<USkeletalMeshComponent>(OtherComp);
			RootComp->AttachToComponent(HumanMesh, FAttachmentTransformRules::KeepWorldTransform, HitBone);

			UHumanCombatComponent* HumanCombatComponent = OtherActor->FindComponentByClass<UHumanCombatComponent>();
			HumanCombatComponent->ApplyPhysicalAnimation(Cast<AHumanCharacter>(OtherActor), HumanMesh, HitBone);
			HumanCombatComponent->DealDamage(Damage, HitBone);
			// Spawning Blood FX
			UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);
			// Adding Impulse to simulate human reaction
			HumanMesh->AddImpulseToAllBodiesBelow(RootComp->GetForwardVector().GetSafeNormal() * BonesHitForce, HitBone);

			DepthVar += FMath::FRandRange(0, 0.5);
			bLineTrace = false;
		}
		// hitting non humen
		else
		{
			if (OtherActor->IsRootComponentMovable() && OtherActor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>() && Hit.Actor->GetRootComponent()->IsSimulatingPhysics())
				Cast<UPrimitiveComponent>(OtherActor->GetRootComponent())->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects, Hit.Location);
			// Arrow effect on Physical Items (Pickable items)
			else if (Hit.Actor->GetClass()->IsChildOf<APhysicalItem>())
			{
				APhysicalItem* PhItem = Cast<APhysicalItem>(Hit.GetActor());
				// Destroying ConsumableItem
				if (PhItem->GetItem()->GetItemType() == EItemType::Consumable)
					PhItem->Destroy();
				// Adding Force
				else
					PhItem->ActivatePhysics()->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -HitForceForMovableObjects / 10, Hit.Location);
			}

			RootComp->SetSimulatePhysics(false);
			RootComp->AttachToComponent(OtherActor->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform, NAME_None);

			DepthVar += FMath::FRandRange(-0.5, 0.75);
			bCurveUp = FMath::RandBool();
			bVibrate = true;
		}

		RootComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		bGoThrough = true;
	}
	else
	{
		RootComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		RootComp->SetSimulatePhysics(true);
		bCurveUp = FMath::RandBool(), bVibrate = true;
	}
	ProjectileMovement->SetVelocityInLocalSpace(FVector(0));
	ProjectileMovement->Deactivate();
}

void AArrow::LaunchProjectile(float Speed, FRotator ArrowDefaultRotation, bool bAlignAlongCrosshairAxis, float AlignRange)
{
	DefaultMeshRotation = ArrowDefaultRotation;
	Mesh->SetWorldRotation(DefaultMeshRotation);

	ExLocation = GetActorLocation();
	LaunchSpeed = Speed;
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * Speed);

	// Setting up align vars
	if (bAlignAlongCrosshairAxis)
	{
		FVector Intersection;
		float T;
		// the Intersection of crosshair trace line and the arrow forward plane (Z Plane)
		UKismetMathLibrary::LinePlaneIntersection_OriginNormal(CrosshairTraceLineStart + (CrosshairTraceLineStart - CrosshairTraceLineEnd).GetSafeNormal() * 500, CrosshairTraceLineStart + (CrosshairTraceLineStart - CrosshairTraceLineEnd).GetSafeNormal() * -500, GetActorLocation(), (CrosshairTraceLineStart - CrosshairTraceLineEnd).GetSafeNormal(), T, Intersection);

		FVector Loc = GetActorLocation();
		Loc.Z = 0;
		ActualDistance = (Intersection - Loc).Size();

		AlignDirection = (Intersection - Loc).GetSafeNormal();

		if (!bHideDebugLines)
		{
			DrawDebugLine(World, CrosshairTraceLineStart + (CrosshairTraceLineStart - CrosshairTraceLineEnd).GetSafeNormal() * 300, CrosshairTraceLineStart + (CrosshairTraceLineStart - CrosshairTraceLineEnd).GetSafeNormal() * -300, FColor::MakeRandomColor(), true, 0, 0, 4);
			DrawDebugDirectionalArrow(World, GetActorLocation(), GetActorLocation() + AlignDirection * 100, 15, FColor::MakeRandomColor(), true, 0, 0, 1);
			DrawDebugPoint(World, Intersection, 20, FColor::Purple, true);
		}
	}

	MaxAlignRange = AlignRange;
	bAlignAlongCrosshair = bAlignAlongCrosshairAxis;

	ProjectileMovement->Activate();
	bLineTrace = true;
}

// Sets the owner actor (for ignoring the owner).
void AArrow::SetOwnerActor(AActor* Actor)
{
	OwnerActor = Actor;
}

void AArrow::ActivatePhysicsMode()
{
	bLineTrace = false;
	bDidHit = true;
	RootComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	RootComp->SetSimulatePhysics(true);
	bCurveUp = FMath::RandBool(), bVibrate = true;
	ProjectileMovement->SetVelocityInLocalSpace(FVector(0));
	ProjectileMovement->Deactivate();
}

void AArrow::SetTargetLocation(FVector Target, FVector LineStart, FVector LineEnd)
{
	TargetLocation = Target;
	this->CrosshairTraceLineStart = LineStart;
	this->CrosshairTraceLineEnd = LineEnd;
}

FVector AArrow::GetArrowTipLocation()
{
	return  GetActorLocation() + GetActorForwardVector().GetSafeNormal() * (RootComp->GetScaledBoxExtent().X + 3);
}
