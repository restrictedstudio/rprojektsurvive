// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "Throwable.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Components/BoxComponent.h"
#include "Particles/Emitter.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanMovementComponent.h"
#include "../Characters/HumanCameraManagerComponent.h"
#include "../Characters/HumanCombatComponent.h"
#include "../Characters/HumanDataComponent.h"
#include "../Controllers/PController.h"
#include "Projectiles/Arrow.h"
#include "Projectiles/Bullet.h"
#include "../Items/PhysicalItem.h"
#include "../Items/Item.h"
#include "../Items/Item_Weapon.h"
#include "../UI/PlayerHUDWidget.h"
#include "../UI/InventoryWidget.h"

// Sets default values
AThrowable::AThrowable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// Root component initilizing
	RootComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Root"));
	RootComp->SetCollisionProfileName(TEXT("NoCollision"));
	RootComp->SetNotifyRigidBodyCollision(true);
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->SetCapsuleSize(5, 8);
	SetRootComponent(RootComp);

	// static mesh initilizing
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));

	// projectile component initilizing
	ProjectileComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileComponent->bAutoActivate = false;
	ProjectileComponent->bSweepCollision = false;
	ProjectileComponent->bRotationFollowsVelocity = false;
	ProjectileComponent->MaxSpeed = 0; // No Limit

	MainTimerRefreshInterval = 0.05;
}

// Called when the game starts or when spawned
void AThrowable::BeginPlay()
{
	Super::BeginPlay();

	// Event
	RootComp->OnComponentHit.AddDynamic(this, &AThrowable::OnHit);

	World = GetWorld();
	MeshDefaultRotation = StaticMesh->GetRelativeRotation();

	// Bounce Settings
	ProjectileComponent->bShouldBounce = bIsBouncy;
	ProjectileComponent->bBounceAngleAffectsFriction = bIsBouncy;
	ProjectileComponent->Bounciness = Bounciness;
	ProjectileComponent->Friction = FrictionRestitution;

	// Activating Follow Velocity
	if (bRotateInAir) ProjectileComponent->bRotationFollowsVelocity = true;

	// Sorting Array of Particle Systems
	if (ScheduledParticles.Num() > 1) SortScheduledParticles();
}

// Called every frame
void AThrowable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Rotation simulating
	InAirRotationManager(DeltaTime);
	AfterHitRotationManager(DeltaTime);

	// Moves the actor to start point or syncs the location with the hand and etc...
	ActorPositioningManager();

	if (bIsAiming)
		DrawProjectileTrajectory();

	CurrentSpeed = ProjectileComponent->Velocity.Size();
}

void AThrowable::LaunchProjectile()
{
	// Setting Collision
	RootComp->SetCollisionProfileName(TEXT("Throwable"));
	// Detaching from hand
	this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	// setting up projectile
	ProjectileComponent->bSweepCollision = true;
	ProjectileComponent->Velocity = ThrowingDirection * (LaunchSpeed + 500 * MovementMultiplier);
	ProjectileComponent->Activate();

	if (bRotateInAir)
		bLineTrace = true;

	Player.Combat->SetAttackType(EAttackType::NONE);
	Player.Combat->SetCombatState(ECombatState::NONE);
}

// Bubble Sort
void AThrowable::SortScheduledParticles()
{
	int Size = ScheduledParticles.Num();

	for (int i = 0; i < Size - 1; i++)
		for (int j = 0; j < Size - i - 1; j++)
			if (ScheduledParticles[j].SpawnTime > ScheduledParticles[j + 1].SpawnTime)
				ScheduledParticles.Swap(j, j + 1);
}

void AThrowable::ExplosionEffect()
{
	//TODO do this in a thread!!	

	RootComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	TArray<FHitResult> OutHits;
	FVector StartLocation = GetActorLocation()
		, EndLocation = StartLocation + GetActorUpVector().GetSafeNormal() * 5;

	FCollisionQueryParams QueryParams = FCollisionQueryParams("Throwable Explosion", false, this);
	FCollisionObjectQueryParams ObjectParams = FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel3 /* Melee Weapons (other throwables) */);

	FCollisionShape SphereShape = FCollisionShape::MakeSphere(ExplosionRadius);

	if (bDrawDebugHelpers)
		DrawDebugSphere(World, StartLocation, ExplosionRadius, 8, FColor::MakeRandomColor(), false, 2, '\000', 1);

	// To Find the same explodable throwables and explode them together [by MeleeWeapon Object Type]	
	if (bAddForceOnExplosion && World->SweepMultiByObjectType(OutHits, StartLocation, EndLocation, FQuat(), ObjectParams, SphereShape, QueryParams))
		// Grenade Booster (Boosts explosion if other explodables were available)
		for (int i = 0; i < OutHits.Num(); ++i)
			if (OutHits[i].Actor->GetClass()->IsChildOf<AThrowable>())
			{
				// Checking if sth is on the way of explosion or not
				FHitResult ExplosionHit;
				if (World->LineTraceSingleByChannel(ExplosionHit, GetActorLocation(), OutHits[i].Actor->GetActorLocation(), ECollisionChannel::ECC_Visibility) && OutHits[i].Actor != ExplosionHit.Actor)
					continue;

				AThrowable* Throwable = Cast<AThrowable>(OutHits[i].GetActor());
				if (Throwable->bIsExplodable)
					// if throwables in range were the same! add them up and explode as a single one
					if (Throwable->StaticMesh->GetStaticMesh() == StaticMesh->GetStaticMesh() && (Throwable->GetActorLocation() - GetActorLocation()).Size() < ExplosionRadius * EffectiveRadiusRatio)
					{
						// Values are experimental
						ExplosionScale *= 1.3;
						ExplosionDecalSize *= 1.3;
						RagdollExplosionForce *= 1.3;
						ExplosionStrength *= 1.4;
						ExplosionRadius *= 1.2;
						ExplosionDamage *= 2;
						Throwable->bIsExplodable = false;
						Throwable->SelfDestruct();
					}
			}

	// Sphere trace to find all the objects in explosion range. [Explosive Trace Channel]
	if (World->SweepMultiByChannel(OutHits, StartLocation, EndLocation, FQuat(), ECollisionChannel::ECC_GameTraceChannel8, SphereShape, QueryParams))
	{
		TArray<TWeakObjectPtr<AActor>> HitActors;
		HitActors.Empty(OutHits.Num());

		for (int i = 0; i < OutHits.Num(); ++i)
		{
			// Avoids Repeating Hits...
			if (HitActors.Contains(OutHits[i].Actor)) return;
			HitActors.Add(OutHits[i].Actor);

			if (OutHits[i].Actor->IsRootComponentMovable() && OutHits[i].Actor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>())
			{
				FHitResult Hit = OutHits[i], ExplosionHit;
				if (bDrawDebugHelpers)
					DrawDebugLine(World, GetActorLocation(), Hit.Actor->GetActorLocation(), FColor::MakeRandomColor(), false, 5, '\000', 2);

				// If sth was on the way then will go to next hit object and nothing would apply for exoplosion to the following actor
				if (World->LineTraceSingleByChannel(ExplosionHit, GetActorLocation(), Hit.Actor->GetActorLocation(), ECollisionChannel::ECC_Visibility) && Hit.Actor != ExplosionHit.Actor)
					continue;

				if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>())
				{
					AHumanCharacter* HitHuman = Cast<AHumanCharacter>(Hit.GetActor());

					if (bAddForceOnExplosion && RagdollExplosionForce && (Hit.Actor->GetActorLocation() - GetActorLocation()).Size() < ExplosionRadius * EffectiveRadiusRatio)
					{
						HitHuman->ActivateRagdoll();
						HitHuman->GetMesh()->AddRadialImpulse(GetActorLocation(), ExplosionRadius, RagdollExplosionForce, ERadialImpulseFalloff::RIF_Linear, true);
					}

					// Dealing Damage
					HitHuman->GetCombat()->DealDamage(GetActorLocation(), ExplosionRadius, ExplosionDamage);

					// Goes to next loop
					if (!bAddForceOnExplosion)
						continue;
				}
				else if (Hit.Actor->GetClass()->IsChildOf<ABullet>())
				{
					ABullet* Bullet = Cast<ABullet>(Hit.GetActor());
					Bullet->Destroy();
				}
				// Effect on Arrows
				else if (Hit.Actor->GetClass()->IsChildOf<AArrow>())
				{
					// Simulating Physics
					AArrow* Arrow = Cast<AArrow>(Hit.GetActor());
					Arrow->ActivatePhysicsMode();
					// Adding Impulse
					Cast<UPrimitiveComponent>(Hit.Actor->GetRootComponent())->
						AddRadialImpulse(GetActorLocation(), ExplosionRadius, ExplosionStrength, ERadialImpulseFalloff::RIF_Linear, true);
				}
				else if (Hit.Actor->GetClass()->IsChildOf<AThrowable>())
				{
					AThrowable* Throwable = Cast<AThrowable>(Hit.GetActor());
					if (Throwable && Throwable->bIsExplodable)
						Throwable->SelfDestruct();
				}
				else if (Hit.Actor->GetClass()->IsChildOf<APhysicalItem>())
				{
					APhysicalItem* PhItem = Cast<APhysicalItem>(Hit.GetActor());
					// Destroying ConsumableItem
					if (PhItem->GetItem()->GetItemType() == EItemType::Consumable)
						PhItem->Destroy();
					// Adding Force
					else
						PhItem->ActivatePhysics()->AddRadialImpulse(GetActorLocation(), ExplosionRadius, ExplosionStrength, ERadialImpulseFalloff::RIF_Linear, true);
				}
				else
				{
					UPrimitiveComponent* HitRoot = Cast<UPrimitiveComponent>(Hit.Actor->GetRootComponent());
					HitRoot->AddRadialImpulse(GetActorLocation(), ExplosionRadius, ExplosionStrength, ERadialImpulseFalloff::RIF_Linear, true);
				}
			}
		}
	}


	// Spawning Explosion Particel System
	if (bSpawnExplosionParticleSystem && ensure(ExplosionParticleSystem))
	{
		if (ExplosionDuration)
			SpawnedParticleSystemComponents.Add(
				UGameplayStatics::SpawnEmitterAttached(ExplosionParticleSystem, RootComp, NAME_None, RootComp->GetComponentLocation(), FVector::UpVector.Rotation(), ExplosionScale, EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease));
		else
			SpawnedParticleSystemComponents.Add(
				UGameplayStatics::SpawnEmitterAtLocation(World, ExplosionParticleSystem, RootComp->GetComponentLocation(), FVector::UpVector.Rotation(), ExplosionScale, true, EPSCPoolMethod::AutoRelease));

		bSpawnExplosionParticleSystem = false;
	}
}

void AThrowable::GoCloseRangeMode()
{
	bCloseRangeMode = true;
	LaunchSpeed = CRLaunchSpeed;
	RightOffset = CRRightOffset;
	ForwardOffset = CRForwardOffset;
	UpOffset = CRUpOffset;
}

void AThrowable::InAirRotationManager(float DeltaTime)
{
	if (!bRotateInAir) return;

	if (!bAdjustRotationForHit && bLineTrace && !bIsBouncy && CurrentSpeed > 5)
	{
		FHitResult Hit;
		FVector StartLocation = GetActorLocation() + GetActorForwardVector().GetSafeNormal() * RootComp->GetScaledCapsuleRadius() * 2;
		FVector EndLocation = StartLocation + ProjectileComponent->Velocity.GetSafeNormal() * 100;
		// Line traces ahead to realize whether should change rotation back to default or not...
		if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility))
		{
			RandomPitchForRotation = FMath::RandRange(-10, 10);
			MeshLastPitch = StaticMesh->GetRelativeRotation().Pitch;
			MeshLastPitch += RandomPitchForRotation;
			bAdjustRotationForHit = true;
		}
		// Debuging
		if (bDrawDebugHelpers)
		{
			DrawDebugPoint(World, Hit.Location, 20, FColor::Green, false, 10);
			DrawDebugLine(World, StartLocation, EndLocation, FColor::Red);
		}
		// Rotating the mesh each frame
		StaticMesh->AddLocalRotation(FRotator(RotationSpeed * DeltaTime * -100, 0, 0));
	}
	// Turning rotation back to default
	if ((bAdjustRotationForHit && MeshRotationAlpha != 1) || CurrentSpeed <= 5)
	{
		MeshRotationAlpha = FMath::Clamp<float>(FMath::FInterpTo(MeshRotationAlpha, MeshRotationAlpha + 1, World->DeltaTimeSeconds, 7), 0, 1);
		StaticMesh->SetRelativeRotation(FRotator(FMath::Lerp<float>(MeshLastPitch, MeshDefaultRotation.Pitch, MeshRotationAlpha), MeshDefaultRotation.Yaw, MeshDefaultRotation.Roll));
	}
}

void AThrowable::AfterHitRotationManager(float DeltaTime)
{
	if (bDidHit && CurrentSpeed > 0)
		StaticMesh->AddWorldRotation((RootComp->GetRightVector().Rotation() * DeltaTime * 15 + RootComp->GetForwardVector().Rotation() * DeltaTime * 15) * (FMath::Clamp<float>(CurrentSpeed, 0, 250) / 250));
}

// Moves the actor to start point or syncs the location with the hand and etc...
void AThrowable::ActorPositioningManager()
{
	if (!Player.Character) return;

	//TODO This may cause a bug!! watchout
	float PawnSpeed = Player.Movement->GetSpeed();
	if (!ProjectileComponent->IsActive())
		// Updating the Start Point Position
		if (!MoveThrowableToStartPoint || PawnSpeed > 0)
		{
			// Setting Start Point
			StartPoint = Player.Character->GetActorLocation()
				+ Player.Character->GetActorRightVector().GetSafeNormal() * RightOffset * 10
				+ Player.Character->GetActorForwardVector().GetSafeNormal() * ForwardOffset * 10
				+ Player.Character->GetActorUpVector().GetSafeNormal() * UpOffset * 10;

			// Distance from start point to this actor
			DistanceToHand = (StartPoint - GetActorLocation()).Size();

			// Updating the location to start point while player was moving
			if (MoveThrowableToStartPoint && PawnSpeed > 0)
			{
				MoveToStartPointAlpha = FMath::Clamp<float>(FMath::FInterpTo(MoveToStartPointAlpha, MoveToStartPointAlpha + 1, World->DeltaTimeSeconds, LaunchSpeed / (DistanceToHand * 2)), 0, 1);
				RootComp->SetWorldLocation(FMath::Lerp<FVector>(ThrowableLocationOnThrow, StartPoint, MoveToStartPointAlpha));

				if (MoveToStartPointAlpha == 1)
					LaunchProjectile();
			}
		}
		else
		{
			// Updating the location to start point while player was NOT moving
			MoveToStartPointAlpha = FMath::Clamp<float>(FMath::FInterpTo(MoveToStartPointAlpha, MoveToStartPointAlpha + 1, World->DeltaTimeSeconds, LaunchSpeed / (DistanceToHand * 2)), 0, 1);
			RootComp->SetWorldLocation(FMath::Lerp<FVector>(ThrowableLocationOnThrow, StartPoint, MoveToStartPointAlpha));

			if (MoveToStartPointAlpha == 1)
				LaunchProjectile();
		}
}

// Calculates and draws the projectile trajectory + set throwing direction 
void AThrowable::DrawProjectileTrajectory()
{
	FRotator ControlRotation = Player.Character->GetControlRotation();
	FVector AimingForward = ControlRotation.Vector();
	ControlRotation.Pitch += 90;
	FVector AimingUp = ControlRotation.Vector();

	// Setting throwing direction
	ThrowingDirection = AimingForward + AimingUp / 5;

	// Clearing and Destroying Previous trajectory segments
	for (AEmitter* Segment : TrajectorySegments)
		Segment->Destroy();
	TrajectorySegments.Empty(TrajectoryArcQuality);

	// Calculating movement multiplier
	MovementMultiplier = FMath::Clamp<float>(FMath::FInterpTo(MovementMultiplier,
		(FMath::Clamp<float>(Player.Movement->GetSpeed(), 0, 200) / 200) + (Player.Movement->IsFalling() ? 0.5 : 0),
		World->DeltaTimeSeconds, 10), 0, 1);

	// Trace Vars
	FHitResult ProjectileHit;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(Player.Character);
	FVector LastTrace;
	FOccluderVertexArray PathPositions;
	FVector LaunchVelocity = ThrowingDirection * (LaunchSpeed + 500 * MovementMultiplier);

	// Predicting Projectile Trajectory
	UGameplayStatics::Blueprint_PredictProjectilePath_ByTraceChannel(World, ProjectileHit, PathPositions, LastTrace,
		StartPoint, LaunchVelocity, false, 5
		, TEnumAsByte<ECollisionChannel>(ECollisionChannel::ECC_Visibility), true
		, ActorsToIgnore, EDrawDebugTrace::None, 0, TrajectoryArcQuality, TrajectoryMaxRenderDistance);

	// Iterating for Segments
	for (int i = 0; i < PathPositions.Num() - 1; i++)
	{
		// Line tracing in between each 2 path positions
		FVector StartLocation = PathPositions[i];
		FVector EndLocation = PathPositions[i + 1];
		FHitResult Hit;
		FCollisionQueryParams QueryParams = FCollisionQueryParams("Projectile Trajectory", true, Player.Character);
		World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility, QueryParams);

		// Spawning the segment and setting the particle system variables
		AEmitter* Segment = World->SpawnActor<AEmitter>();
		Segment->SetTemplate(TrajectoryBeamParticle);
		Segment->GetParticleSystemComponent()->SetBeamSourcePoint(0, StartLocation, 0);
		Segment->GetParticleSystemComponent()->SetBeamTargetPoint(0, Hit.bBlockingHit ? Hit.Location : EndLocation, 0);
		TrajectorySegments.Add(Segment);

		if (Hit.bBlockingHit)
		{
			// Drawing last segment on line trace hit
			AEmitter* LastSegment = World->SpawnActor<AEmitter>(Hit.Location + (Hit.Normal * 5), Hit.Normal.Rotation());
			LastSegment->SetTemplate(TrajectoryHitParticle);
			TrajectorySegments.Add(LastSegment);

			// Draws till hit sth or simulation stops
			break;
		}
	}
}

void AThrowable::SelfDestruct()
{
	// Deactivating spawned particle systems
	if (SpawnedParticleSystemComponents.Num())
		for (UParticleSystemComponent* Particle : SpawnedParticleSystemComponents)
			Particle->DeactivateSystem();

	if (bIsExplodable)
	{
		if (!ExplosionDuration)
			ExplosionEffect();

		FHitResult LandHit;
		if (bSpawnDecalImpactOnSurface && ensure(ExplosionImpactDecal) && World->LineTraceSingleByObjectType(LandHit, GetActorLocation(), GetActorLocation() + FVector::UpVector * -ExplosionRadius, FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldStatic)))
		{
			// Spawning Explosion Decal on the Ground
			FRotator DecalRotation = LandHit.Normal.Rotation();
			DecalRotation.Roll += FMath::RandRange(0, 180);
			float Scale = ExplosionDecalSize;

			UDecalComponent* UDC = UGameplayStatics::SpawnDecalAttached(ExplosionImpactDecal, FVector(1, Scale, Scale), LandHit.GetComponent(), NAME_None, LandHit.Location, DecalRotation, EAttachLocation::KeepWorldPosition, DecalLifeTime);
			UDC->SetFadeScreenSize(0.01);
		}
	}

	Destroy();
}

void AThrowable::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (GetVelocity().Size() < 500 && bSimulatePhysicsOnHit) return;

	// Checking Physics Stuff
	if (bSimulatePhysicsOnHit && !bDidHit)
	{
		RootComp->SetPhysicsLinearVelocity(ProjectileComponent->Velocity);
		ProjectileComponent->StopMovementImmediately();
		RootComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		RootComp->SetSimulatePhysics(true);
	}

	if (!bIsBouncy && !bSimulatePhysicsOnHit)
	{
		RootComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		AttachToComponent(Hit.GetComponent(), FAttachmentTransformRules::KeepWorldTransform, Hit.BoneName);
	}

	// Breaking stuff
	if (bIsMeshBreakable)
	{
		StaticMesh->SetVisibility(false);
		RootComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		if (!ExplosionDuration && !bIsExplodable)
			SelfDestruct();
	}

	bDidHit = true;

	if (bRagdollOnHeadShot && OtherActor->GetClass()->IsChildOf<AHumanCharacter>() && Hit.BoneName == TEXT("Head"))
		Cast<AHumanCharacter>(OtherActor)->ActivateRagdoll();

	if (LifeTime == 0)
	{
		if (ExplosionDuration && bIsExplodable)
		{
			RootComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			ExplosionEffectTimer = 0;
			bExecuteExplosionEffects = true;
		}
		else
			SelfDestruct();

		return;
	}

	// Hitting Human
	if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>())
	{
		AHumanCharacter* HitHuman = Cast<AHumanCharacter>(Hit.GetActor());
		USkeletalMeshComponent* HumanMesh = HitHuman->GetMesh();
		UHumanCombatComponent* HumanCombatComponent = HitHuman->GetCombat();

		if (!HumanCombatComponent->IsRagdoll())
			HumanCombatComponent->ApplyPhysicalAnimation(Cast<AHumanCharacter>(Hit.Actor), HumanMesh, Hit.BoneName);
		// Spawning Blood FX
		if (bSpawnBloodOnHit || Hit.BoneName == "Head")
			UGameplayStatics::SpawnEmitterAttached(HumanCombatComponent->BloodPS, HumanMesh, Hit.BoneName, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);

		// Adding Impulse to simulate human reaction
		HumanMesh->AddImpulseToAllBodiesBelow(Hit.Normal * -HumanBodyHitForce, Hit.BoneName);

		// Dealing Damage
		if (bIsExplodable)
			HumanCombatComponent->DealDamage(FDamage(5), Hit.BoneName);
		else
			HumanCombatComponent->DealDamage(HitDamage, Hit.BoneName);
	}
	// Adding Force on simple hit
	else if (OtherActor->IsRootComponentMovable() && OtherActor->GetRootComponent()->IsSimulatingPhysics() && OtherActor->GetRootComponent()->GetClass()->IsChildOf<UPrimitiveComponent>())
	{
		float VelocityAlpha = ProjectileComponent->Velocity.Size() / LaunchSpeed;
		Cast<UPrimitiveComponent>(OtherActor->GetRootComponent())->AddImpulseAtLocation(Hit.ImpactNormal.GetSafeNormal() * -ForceOnHit * 100 * VelocityAlpha, Hit.Location);
	}
}

void AThrowable::Prepare(bool bIsCloseRange)
{
	if (!ensure(AnimationData.AnimMontage))
		return;

	// Selecting the correct animation
	if (bIsCloseRange)
		GoCloseRangeMode()
		, Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.CloseRangePreparingSectionName);
	else
		Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.PreparingSectionName);

	bIsAiming = true;

	if (!bCalculateLifeTimeAfterThrowing)
	{
		FTimerHandle SelfDestructTimer;
		World->GetTimerManager().SetTimer(SelfDestructTimer, this, &AThrowable::SelfDestruct, LifeTime);
	}
}

void AThrowable::Throw(bool bIsCloseRange)
{
	if (!ensure(AnimationData.AnimMontage) || !bIsAiming)
		return;

	// Reticle UI
	Player.Controller->GetHUD()->ShowReticle(EReticleType::Dot);

	// Selecting the correct animation
	if (bIsCloseRange)
		Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.CloseRangeThrowingSectionName);
	else
		Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.ThrowingSectionName);

	// Starting Timers
	ThrowingTimer = 0;
	ThrowTimer = 0;
	bIsAiming = false;

	// Clearing projectile trajectory segments
	for (AEmitter* Segment : TrajectorySegments)
		Segment->Destroy();
	TrajectorySegments.Empty(TrajectoryArcQuality);
}

void AThrowable::Attack()
{
	Player.Combat->SetUseAimPose(false);
	Player.Combat->SetCombatState(ECombatState::Aiming);
	bIsPreparedToThrow = true;
	Prepare();
}

bool AThrowable::Aim()
{
	if (bHasCloseRangeMode)
	{
		bIsPreparedToThrow = true;
		Player.Combat->SetUseAimPose(false);
		Prepare(true);
	}

	return true;
}

bool AThrowable::UnAim()
{
	if (bHasCloseRangeMode)
	{
		Throw(true);

		bIsPreparedToThrow = false;

		Player.Character->GetData()->RemoveHotbarSlot(Player.Combat->GetActiveSlot()->ID, true);
		Player.Controller->GetInventoryUI()->SelectHotbarSlot(EHotbarSlotLocation::NONE);
		Player.Combat->ClearActiveSlot();
	}

	return true;
}

void AThrowable::Equip()
{
	Super::Equip();

	Player.Controller->GetHUD()->ShowReticle(EReticleType::NONE);
	Player.Combat->SetAttackType(EAttackType::Throwable);
	Player.Combat->SetUseAimPose(false);
}

void AThrowable::UnEquip(UItem_Weapon* Item, FName AttachSocketName)
{
	if (ThrowTimer < 0)
		Super::UnEquip(Item, AttachSocketName);
	else
	{
		Player.Combat->SetAttackType(EAttackType::NONE);
		Player.Controller->GetHUD()->ShowReticle(EReticleType::Dot);		

		// Stopping OwnerMovement Montage	
		Player.Combat->SetMovementAnimationData(FMovementAnimation());

		Player.Combat->SetAimAdjuster(0);
		Player.Combat->SetBlockedWidget(false);
	}
}

void AThrowable::OnAttackKeyRelease()
{
	Throw();

	bIsPreparedToThrow = false;
	Player.Character->GetData()->RemoveHotbarSlot(Player.Combat->GetActiveSlot()->ID, true);
	Player.Controller->GetInventoryUI()->SelectHotbarSlot(EHotbarSlotLocation::NONE);
	Player.Combat->ClearActiveSlot();
}

void AThrowable::Timer()
{
	float DeltaTime = MainTimerRefreshInterval;

	if (ThrowTimer >= 0)
	{
		ThrowTimer += DeltaTime;

		if (ExplosionEffectIntervalTimer < ExplosionEffectsInterval)
			ExplosionEffectIntervalTimer += DeltaTime;

		// Explosion with Durations stuff
		if (LifeTime == 0 && ExplosionEffectTimer < ExplosionDuration)
		{
			ExplosionEffectTimer += DeltaTime;

			if (ExplosionEffectTimer >= ExplosionDuration)
				SelfDestruct();
		}

		if (LifeTime != 0 && ThrowTimer >= LifeTime - ExplosionDuration)
			bExecuteExplosionEffects = true;

		// Explosion effects
		if (ExplosionDuration && bExecuteExplosionEffects && ExplosionEffectIntervalTimer >= ExplosionEffectsInterval)
			ExplosionEffect(), ExplosionEffectIntervalTimer = 0;

		// Spawning Scheduled Particles
		if (ScheduledParticles.Num() && ThrowTimer >= ScheduledParticles[0].SpawnTime)
		{
			if (ensure(ScheduledParticles[0].Template))
			{
				UParticleSystemComponent* Particle;

				if (ScheduledParticles[0].bIsAttachedToOwner)
					Particle = UGameplayStatics::SpawnEmitterAttached(ScheduledParticles[0].Template
						, RootComp, NAME_None, RootComp->GetComponentLocation()
						, ScheduledParticles[0].bUseRootRotation ? RootComp->GetComponentRotation() : ScheduledParticles[0].Rotation
						, ScheduledParticles[0].Scale, EAttachLocation::Type::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);
				else
					Particle = UGameplayStatics::SpawnEmitterAtLocation(World, ScheduledParticles[0].Template
						, RootComp->GetComponentLocation()
						, ScheduledParticles[0].bUseRootRotation ? RootComp->GetComponentRotation() : ScheduledParticles[0].Rotation
						, ScheduledParticles[0].Scale, true, EPSCPoolMethod::AutoRelease);

				SpawnedParticleSystemComponents.Add(Particle);
			}

			ScheduledParticles.RemoveAt(0);
		}
	}

	// Throwing notifier (throw animation ended)
	if (ThrowingTimer <= (bCloseRangeMode ? AnimationData.CloseRangeThrowingTimeNotifier : AnimationData.ThrowingTimeNotifier))
	{
		ThrowingTimer += DeltaTime;

		if (ThrowingTimer > (bCloseRangeMode ? AnimationData.CloseRangeThrowingTimeNotifier : AnimationData.ThrowingTimeNotifier))
		{
			//Player.Combat->ResetThrowableTimer();

			if (bCalculateLifeTimeAfterThrowing)
			{
				FTimerHandle SelfDestructTimer;
				World->GetTimerManager().SetTimer(SelfDestructTimer, this, &AThrowable::SelfDestruct, LifeTime);
			}

			UnAimingTimer = 0;

			ThrowableLocationOnThrow = RootComp->GetComponentLocation();
			MoveThrowableToStartPoint = true;
			bHasThrown = true;
		}
	}

	// NO NEED.
	// UnAiming delay to maintain the rotation of player to avoid causing self hitting bugs
	/*if (UnAimingTimer <= UnAimDelay && false)
	{
		UnAimingTimer += DeltaTime;

		if (UnAimingTimer > UnAimDelay)
			Player.Combat->UnAim();
	}*/

}