// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Gun_Pistol.generated.h"

/**
 * 
 */
UCLASS()
class PROJEKTSURVIVAL_API AGun_Pistol : public AGun
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AGun_Pistol();
	
	void CancelReloading() override;

protected:

	void Timer() override;	

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* MagazineMesh;

	// The hand bone name that magazine attaches to	
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		FName HandBoneName = TEXT("LeftHand");
	// the time that player removed the old magazine
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float MagazineDetachTimeFromGun = 0.2;	
	// The time that player would grab the magazine from her/him pocket and etc...
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float NewMagazineSpawnTime = 0.4;
	// The time that the new magazine would replace the old one (reload ended)
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		float NewMagazineReplaceTime = 0.6;
	
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		FVector NewMagazineLocationRelativeToHand;
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Reloading")
		FRotator NewMagazineRotationRelativeToHand;

private:

	// Reloading Stuff -
	/*  0 Nothing	 
	 1 Magazine Thrown Away
	 2 Magazine Spawned
	*/
	int ReloadStage = 0;	
	// Uses for reloading process
	UStaticMeshComponent* OldMagazineStaticMesh = nullptr;
	UStaticMeshComponent* NewMagazineStaticMesh = nullptr;

};
