// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "Gun_Pistol.h"
#include "../Characters/HumanCharacter.h"
//#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"
#include "Components/SkeletalMeshComponent.h"

AGun_Pistol::AGun_Pistol()
{
	// Magazine of the Gun
	MagazineMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Magazine Mesh"));
	MagazineMesh->AttachToComponent(SKMesh, FAttachmentTransformRules::KeepRelativeTransform);
	MagazineMesh->SetCollisionProfileName("NoCollision");
	MagazineMesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
}

void AGun_Pistol::Timer()
{
	Super::Timer();
	
	float DeltaTime = MainTimerRefreshInterval;

	if (ReloadTimer <= ReloadTime)
	{
		// the time that player removed the old magazine
		if (ReloadStage == 0 && ReloadTimer >= MagazineDetachTimeFromGun)
		{
			USkeletalMeshComponent* HumanSKMesh = Player.Character->GetMesh();

			// Faking the old magazine
			AStaticMeshActor* OldMagazineActor =
				World->SpawnActor<AStaticMeshActor>();
			OldMagazineActor->SetActorLabel(FString("Gun Old Magazine"));
			OldMagazineActor->SetMobility(EComponentMobility::Movable);
			OldMagazineActor->SetLifeSpan(5);

			OldMagazineStaticMesh = OldMagazineActor->GetStaticMeshComponent();
			OldMagazineStaticMesh->SetMobility(EComponentMobility::Movable);
			OldMagazineStaticMesh->SetWorldTransform(MagazineMesh->GetComponentTransform());
			OldMagazineStaticMesh->SetCollisionProfileName(TEXT("GunMagazine"));
			OldMagazineStaticMesh->SetStaticMesh(MagazineMesh->GetStaticMesh());
			OldMagazineStaticMesh->SetSimulatePhysics(true);

			MagazineMesh->SetVisibility(false);

			++ReloadStage;
		}
		// The time that player would grab the magazine from her/him pocket and etc...
		else if (ReloadStage == 1 && ReloadTimer >= NewMagazineSpawnTime)
		{
			USkeletalMeshComponent* HumanSKMesh = Player.Character->GetMesh();

			// Spawning actual new magazine actor and setting it's static mesh
			AStaticMeshActor* NewMagazineActor =
				World->SpawnActor<AStaticMeshActor>(HumanSKMesh->GetBoneLocation(HandBoneName), HumanSKMesh->GetBoneQuaternion(HandBoneName).Rotator());
			NewMagazineActor->SetActorLabel(FString("Gun New Magazine"));
			NewMagazineActor->SetMobility(EComponentMobility::Movable);
			NewMagazineActor->AttachToComponent(HumanSKMesh, FAttachmentTransformRules::KeepRelativeTransform, HandBoneName);

			NewMagazineStaticMesh = NewMagazineActor->GetStaticMeshComponent();
			NewMagazineStaticMesh->SetMobility(EComponentMobility::Movable);
			NewMagazineStaticMesh->SetRelativeLocation(NewMagazineLocationRelativeToHand);
			NewMagazineStaticMesh->SetRelativeRotation(NewMagazineRotationRelativeToHand);
			NewMagazineStaticMesh->SetWorldScale3D(MagazineMesh->GetComponentScale());
			NewMagazineStaticMesh->SetCollisionProfileName(TEXT("GunMagazine"));
			NewMagazineStaticMesh->SetStaticMesh(MagazineMesh->GetStaticMesh());

			++ReloadStage;
		}
		// The time that the new magazine would replace the old one (reload ended)
		else if (ReloadStage == 2 && ReloadTimer >= NewMagazineReplaceTime)
		{
			NewMagazineStaticMesh->GetOwner()->Destroy();
			MagazineMesh->SetVisibility(true);

			++ReloadStage;
		}
	}
	else
		ReloadStage = 0;
}

void AGun_Pistol::CancelReloading()
{
	Super::CancelReloading();

	if (NewMagazineStaticMesh)
	{
		if (OldMagazineStaticMesh)
			OldMagazineStaticMesh->GetOwner()->Destroy();
		NewMagazineStaticMesh->GetOwner()->Destroy();
		MagazineMesh->SetVisibility(true);
	}
}
