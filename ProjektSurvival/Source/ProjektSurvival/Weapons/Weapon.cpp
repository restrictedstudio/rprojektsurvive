// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanCombatComponent.h"
#include "../Items/Item_Weapon.h"
#include "../UI/PlayerHUDWidget.h"
#include "../Controllers/PController.h"

// Sets default values
AWeapon::AWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AWeapon::Setup(FPlayerData InPlayer)
{
	Player = InPlayer;
}

void AWeapon::Equip()
{
	SetActorTickEnabled(true);
	AttachToComponent(Player.Character->GetMesh(), FAttachmentTransformRules::KeepWorldTransform, AttachBoneName);
	SetActorRelativeTransform(GetDefaultTransformForHand());
	// Starts the main timer.
	GetWorld()->GetTimerManager().SetTimer(MainTimer, this, &AWeapon::Timer, MainTimerRefreshInterval, true);

	Player.Combat->SetMovementAnimationData(MovementAnimationData);
}

void AWeapon::UnEquip(UItem_Weapon* Item, FName AttachSocketName)
{
	SetActorTickEnabled(false);
	// Stopping the main timer
	GetWorld()->GetTimerManager().ClearTimer(MainTimer);
	Player.Combat->SetAttackType(EAttackType::NONE);
	Player.Controller->GetHUD()->ShowReticle(EReticleType::Dot);

	AttachToComponent(Player.Character->GetMesh(), FAttachmentTransformRules::KeepWorldTransform, AttachSocketName);
	SetActorRelativeTransform(Item->GetSocketTransform());

	// Stopping OwnerMovement Montage	
	Player.Combat->SetMovementAnimationData(FMovementAnimation());	
	Player.Combat->StopMontage();

	Player.Combat->SetAimAdjuster(0);
	Player.Combat->SetBlockedWidget(false);
}

// Main timer that handles any sorta timings for weapons like reloading timer, cooldown and etc...
// you can set the refresh interval with MainTimerRefreshInterval variable
void AWeapon::Timer()
{
	// OVERRIDE
}
