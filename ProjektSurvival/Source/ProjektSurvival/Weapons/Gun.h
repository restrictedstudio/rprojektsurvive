// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "Gun.generated.h"

class ABullet;
class APController;
class UHumanCombatComponent;
class AHumanCharacter;
class UBoxComponent;
class UParticleSystem;
class UAnimInstance;
class UPlayerHUDWidget;
enum class EAimingType : uint8;

UENUM(BlueprintType)
enum class EAmmunitionType : uint8 {
	Bullet22LR	UMETA(DisplayName = ".22 LR"),
	Bullet12Gauge	UMETA(DisplayName = "12-Gauge"),
	Bullet9mm	UMETA(DisplayName = ".9mm"),
	Bullet45ACP	UMETA(DisplayName = ".45 ACP"),
	Bullet308	UMETA(DisplayName = ".308"),
	Bullet762mm	UMETA(DisplayName = ".7.62mm"),
	Bullet223	UMETA(DisplayName = ".223"),
	Bullet50BMG	UMETA(DisplayName = ".50 BMG")
};

UENUM(BlueprintType)
enum class EGunFireMode : uint8 {
	SemiAuto, // Most Commong
	Auto, // Fires contunuisly
	Burst // Fires 3 Bullets with each pulling the trigger
};

USTRUCT(BlueprintType)
struct FGunAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName LoadSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName ReloadSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName MeleeAttackSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName EquipSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName UnEquipSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName AimingSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName QuickAimingSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName BlockedSectionName;

	FGunAnimation()
	{
		AnimMontage = nullptr;
		LoadSectionName = TEXT("Load");
		ReloadSectionName = TEXT("Reload");
		MeleeAttackSectionName = TEXT("MeleeAttack");
		EquipSectionName = TEXT("Use");
		UnEquipSectionName = TEXT("UnEquip");
		BlockedSectionName = TEXT("Blocked");
		AimingSectionName = TEXT("Aiming");
		QuickAimingSectionName = TEXT("QuickAiming");
	}
};

USTRUCT(BlueprintType)
struct FGunKickBackAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName KickBackSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName QuickKickBackSectionName;

	FGunKickBackAnimation()
	{
		AnimMontage = nullptr;
		KickBackSectionName = TEXT("KickBack");
		QuickKickBackSectionName = TEXT("QuickKickBack");
	}
};

/*
* All of the Projectile based guns should use this class.

 ** CAUTION : DO NOT CHANGE GUN TIP'S ROLL! IT WILL BREAK THE ALIGNING PROCESS. **
*/
UCLASS()
class PROJEKTSURVIVAL_API AGun : public AWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGun();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Sets default varaibles of gun parent
	virtual void Setup(FPlayerData InPlayer) override;

	// Firing Proccess of Gun
	virtual void Fire();

	// event attack key pressed
	void Attack() override;	

	bool Aim() override;

	bool UnAim() override;

	void Equip() override;

	// Melee Attack with the Gun
	virtual bool MeleeAttack();

	// Resets LoadedBullets, Plays Animation
	virtual bool Reload() override;

	// Reloading End Event
	virtual void OnReloadingEnded();

	// If Gun is Ready to Shoot (Recoil, Reloading, Bullets, etc...)
	virtual bool IsLoaded();

	virtual void CancelReloading();

	// Checks if the bullet would go to the crosshair (where the player actually aimed at) or not
	virtual void IsBulletBlocked(float TraceLength);

	// Checks if sth is on the way of camera (Behind the player!)
	bool IsAimingValid();

	// @return bIsReloading
	virtual bool IsReloading() { return bIsReloading; }

	// @return bIsMeleeAttacking
	virtual bool IsMeleeAttacking() { return MeleeAttackTimer <= MeleeAttackActivationDelay + 0.1 || bIsMeleeAttacking; }

	// Changes the CurrentFiringMode depend on the gun settings
	virtual void ChangeGunFiringMode();	

	USceneComponent* GetFPPCamDriver() { return FPPAimingCamComponent; }

	FGunAnimation GetAnimData() { return AnimationData; }	

	// First Hit look at rotation after aiming
	FRotator CrosshairFirstHitLookAt;
	// Aligns Gun with the crosshair trace hit point
	bool bFPSFirstAlign = false;

	// Iron Sight aiming
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		EAimingType AimingType;
	// hips aiming
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		EAimingType QuickAimingType;	

protected:
	// Evenet
	UFUNCTION()
		void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	

	// Line Traces for Melee Attack
	virtual void ExecuteLineTraces();

	// Aligns gun's rotation with crosshair to hit percisely while aiming in First Person mode
	virtual void AlignWeaponRotationWithCrosshair();

	// Crosshair OwnerMovement to up/down due to kickback codes
	virtual void CrosshairKickBackManager(float DeltaTime);

	void SetCollision(bool bEnabled);

	void ApplyInAccuracy(FRotator& Rotation);

	// Main Timer
	virtual void Timer() override;

	// Ticks and calculates in accuracy due to different elements
	void CalculateInAccuracy();

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USkeletalMeshComponent* SKMesh;	
	// Tip of Gun's Barrel
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* BarrelTipComponent;
	// The Location where shells would spawn at
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* ShellSpawner;
	// Camera Location in First Person Mode for Aiming
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* FPPAimingCamComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		bool bDrawDebugHelpers = false;
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		float T1 = 0.1;
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		float T2 = 0.2;

	// Adjusts the angle of gun in order to align with crosshiar!
	// possitive values would lead to move the gun barrel lower and vice versa
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		float AimAdjustOffset = 0;
	// Gun Animations Data
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Player")
		FGunAnimation AnimationData;
	// Gun Kickback Animations Data
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Player")
		FGunKickBackAnimation KBAnimationData;	

	// Before this time if hit anything, won't do anything
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Melee")
		float MeleeAttackActivationDelay = 0.15;		

	// Damage to other Players
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		FDamage Damage;
	// Size of each magazine
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		int MagazineSize = 30;
	// The Time That Gun Takes in Order to Get Ready After Firing to Fire Again In Seconds
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		float RecoilTime = 0.2;
	// Time That Takes Gun to Reload In Seconds
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		float ReloadTime = 1;
	// Auto Reload Delay
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		float AutoReloadDelay = 0.5;
	// Enter In Seconds
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		float LoadTime = 0.5;
	// Cooldown After Each Melee Attack Hit In Seconds
	UPROPERTY(EditDefaultsOnly, Category = "Settings|General")
		float MeleeAttackCooldown = 0.6;	

	// Max offset (cm) that will crosshair go up on firing
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float MaxCrosshairKickBackOffset = 3;
	// FPS INDEPENDENT | This value will be multiplied by delta time
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float CrosshairGettingBackToDefaulOffset = 0.6;
	// FPS INDEPENDENT | This value will be multiplied by delta time
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float CrosshairMovingUpOffsetOnFire = 1;
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float MinAccuracyRatio = 0.1;
	// Gun's maximum inaccuracy due to different stuff like 1.PlayerVelocity 2.Jumping 3.Crouching 5.Spraying 6.Aiming
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float MaxInaccuracyRatio = 2;
	// Applies to the crosshair with each firing...
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float InAccuracyIncreasingRate = 0.2;
	// Accuracy decrease gradually while spraying
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy", meta = (ClampMin = "0", ClampMax = "1"))
		float AccuracyRecoverSpeedAfterSpraying = 0.5;
	// idle time after each shot to turn on super cooler which will decrease the in accuracy super fast
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float SuperCoolerDelay = 0.35;
	// Mutiplies with MaximumInAccuracyRatio
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float JumpingInAccuracyMultiplier = 1;

	// Morph Target name for recoil
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Gun")
		FName MorphTargetName = TEXT("Recoil");
	// Max roll rotation of gun on moving to sideways
	UPROPERTY(EditDefaultsOnly, Category = "Animation|Gun")
		float GunMaxRollOnSideMovement = 10;	

	// Firing Modes
	// Does gun have semi-auto firing mode? (one bullet each shot)
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Fire Modes")
		bool bHasSemiAuto = true;
	// Does gun have fully automatic mode? (firing bullets one after another)
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Fire Modes")
		bool bHasFullAuto = false;
	// Does gun have burst firing mode? (3 bullets each shot)
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Fire Modes")
		bool bHasBurst = false;
	// recoil of burst (those 3 rounds... the time between them)
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Fire Modes", meta = (EditCondition = "bHasBurst"))
		float BurstRecoilTime = 0.1;
	// The Delay after each burst shot
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Fire Modes", meta = (EditCondition = "bHasBurst"))
		float BurstDelay = 0.3;

	// The Particle System of Firing
	UPROPERTY(EditDefaultsOnly, Category = "Particle Systems")
		UParticleSystem* FirePS = nullptr;

	//// ** Bullet Settings ** \\\\
	
	// Bullet Blueprint
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings", NoClear)
		TSubclassOf<ABullet> BulletType;
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings")
		EAmmunitionType AmmunitionType; // This should be used for pickup
	// Bullet Launch Speed (don't know the unit!)
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings", meta = (ClampMin = "5", ClampMax = "500"))
		int LaunchSpeed = 75;
	// The Range that Bullet Would Go Straight Forward In Meters
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings")
		int EffectiveRange = 10;
	// Max Travel Distance of the Bullet In Meters
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings")
		int MaxHitRange = 20;
	// Deceleration after exceeded the effective range
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings")
		float Deceleration = 1;
	// Bullet spawns shell?!
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings")
		bool bSpawnShell = true;
	// Spawns shell to the right?!
	UPROPERTY(EditDefaultsOnly, Category = "Bullet Settings", meta = (EditCondition = "bSpawnShell"))
		bool bWillShellSpawnToTheRight = false;	

	UWorld* World = nullptr;	
	UPlayerHUDWidget* HUD = nullptr;
	
	// Uses for First Person Mode aiming and stuff...
	FRotator GunDefaultWorldRotation;

	// Visibility of the Blocked Widget
	bool bIsBlockedWidgetVisible = false;
	
	// While Reloading gun, it's true!
	bool bIsReloading = false;	

	// To revert rotation mode after reloading
	bool bRevertRotationMode = false;
	
	// Number of bullets in the current magazine
	int LoadedBullets;
	
	// Morph Target Value
	float MorphTagetValue = 0;
	float RecoilTimer = 50;
	float ReloadTimer = 50;
	float AutoReloadTimer = 50;
	float LoadTimer = 50;

	// 0 means totaly recovered, effects accuracy of weapon after firing non-stop
	float InAccuracyAlpha = 0;
	float LastAccuracyRatio = 0;

	// Gets big while player not shoot for a while
	float AccuracySuperCooler = 1;

	// Increases the Accuracy recover speed
	float SuperCoolerTimer = 0;
	bool bDidAutoReload = false;

	// -- Melee Attack Vars
	float MeleeAttackTimer = 50;
	bool bCanMeleeAttack = true;
	bool bIsMeleeAttacking = false;

	// -- Firing Mode Vars	
	// Current Firing Mode
	EGunFireMode CurrentFiringMode;
	// in order to switch between burst recoil and the semi-auto or auto
	float CurrentRecoilTime = RecoilTime;
	// For Burst shot delay
	float BurstTimer = 50;
	int BurstCounter = 0;

	// becomes 0 after a while that player stopped shooting
	float FireOutAlpha = 0;
	// Total amount of added offset to camera because of shooting +/- Mouse Y
	float CrosshairAddedOffset = 0;
	// Crosshair AddedOffset without MouseY effect
	float PureCrosshairAddedOffset = 0;	
	float AimInAccuracy = 0;
};
