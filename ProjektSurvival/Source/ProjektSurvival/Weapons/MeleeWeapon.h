// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "MeleeWeapon.generated.h"

class UBoxComponent;
class UParticleSystemComponent;
class AHumanCharacter;
class UAnimInstance;
class UHumanCombatComponent;

USTRUCT(BlueprintType)
struct FMeleeWeaponAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName AttackSingleSectionName;
	UPROPERTY(EditDefaultsOnly)
		float ValidSingleHitNotifier;
	UPROPERTY(EditDefaultsOnly)
		FName AttackAreaSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName AttackEndSectionName;

	FMeleeWeaponAnimation()
	{
		ValidSingleHitNotifier = 0.2;
		AnimMontage = nullptr;
		AttackSingleSectionName = TEXT("SingleAttack");
		AttackEndSectionName = TEXT("AttackEnd");
		AttackAreaSectionName = TEXT("AreaAttack");
	}
};

/*
	All types of melee weapons can be made with this class.

	If actor had "GenerateSparks" tag then the MeleeWeapon would generate sparks.

	Blunt Weapons Blood spawning system :
	  bGenerateBloodForBluntPart(TRUE) : Spawns blood on hit
	  bGenerateBloodForBluntPart(FALSE) : Spawns blood if hit an alive creatures Head!
	** Bladed Weapons spawn blood fx on every hit of alive creature. **

	CAUTION : BladedBodyCollision and BluntBodyCollision must not overlap each other!
*/
UCLASS()
class PROJEKTSURVIVAL_API AMeleeWeapon : public AWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMeleeWeapon();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void Setup(FPlayerData Player) override;

	void SetCollision(bool bEnabled);

	// event attack key pressed
	void Attack() override;	

	void UnEquip(UItem_Weapon* Item, FName AttachSocketName);

	///* @return DefaultLocationForHand and DefaultRotationForHand */
	//FTransform GetDefaultTransformForHand() { return FTransform(DefaultRotationForHand, DefaultLocationForHand, FVector(1)); }
	//FMovementAnimation GetMovementAnimData() { return MovementAnimationData; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Events
	/**/
	UFUNCTION()
		void OnBladedPartHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void OnBluntPartHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);
	
	UPROPERTY(EditAnywhere, Category = "Debuging")
		bool bDrawDebugHelpers = false;

	// You can set it if the weapon has blunt part like Axe! (make sure to activate collision)
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* BluntBodyCollision = nullptr;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* Mesh = nullptr;
	// Bladed part collision
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* BladedBodyCollision = nullptr;
	// Spark particle for bladed part
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UParticleSystemComponent* BDSparkParticleSystemComponent = nullptr;
	// Spark particle for blunt part
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UParticleSystemComponent* BTSparkParticleSystemComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		FMeleeWeaponAnimation AnimationData = FMeleeWeaponAnimation();	

		// Damage to other Players
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FDamage Damage;
	// Delay After each hit
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float HitDelay = 1;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FName HeadBoneName = TEXT("Head");
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FName NeckBoneName = TEXT("Neck");	

	UPROPERTY(EditAnywhere, Category = "Weapon")
		bool bIsBladed;
	// Does Bladed part generate sparks while hitting objects with "GenerateSparks" tag?
	UPROPERTY(EditAnywhere, Category = "Weapon", meta = (EditCondition = "bIsBladed"))
		bool bGenerateSparksOnHitForBladedPart = true;

	UPROPERTY(EditAnywhere, Category = "Weapon")
		bool bIsBlunt;
	// Does Blunt part generate sparks while hitting objects with "GenerateSparks" tag?
	UPROPERTY(EditAnywhere, Category = "Weapon", meta = (EditCondition = "bIsBlunt"))
		bool bGenerateSparksOnHitForBluntPart;
	// is blunt part sharp enough to generate blood on every hit?
	UPROPERTY(EditAnywhere, Category = "Weapon", meta = (EditCondition = "bIsBlunt"))
		bool bGenerateBloodForBluntPart;

private:

	// Main Timer
	void Timer() override;

	// all of the line traces
	void ExecuteLineTraces();		

	// Resets all the variable and refreshes the MeleeWeapon
	void Reset();														

	UWorld* World = nullptr;
	FName HitBone;		

	// hit humen will be ignored for more than one hit!
	TArray<TWeakObjectPtr<AActor>> HitHumen;

	bool bDidHit = false;
	bool bHitBlocked = false;
	bool bLineTrace = false;
	bool bCanHit = true;
	bool bIsHitValid = false;
	bool bSingleAttackMode = false;

	float ElapsedTimeSinceBDSparkActivated = 50;
	float ElapsedTimeSinceBTSparkActivated = 50;
	float HitDelayTimer = 50;
	float ValidHitTimer = 50;
};
