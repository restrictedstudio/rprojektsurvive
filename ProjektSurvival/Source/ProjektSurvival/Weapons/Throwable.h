// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "Throwable.generated.h"

class UProjectileMovementComponent;
class UCapsuleComponent;
class UParticleSystem;
class UHumanCombatComponent;
class UParticleSystemComponent;
class AHumanCharacter;
class AEmitter;
class UItem_Weapon;

USTRUCT(BlueprintType)
struct FThrowableAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName EquipSectionName;
	// The time that player would grab the weapon to throw
	UPROPERTY(EditDefaultsOnly)
		float WeaponAttachTimeNotifier;
	UPROPERTY(EditDefaultsOnly)
		FName PreparingSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName ThrowingSectionName;
	// The time that player would throw the weapon, Works like notifiers but with numbers!
	UPROPERTY(EditDefaultsOnly)
		float ThrowingTimeNotifier;

	UPROPERTY(EditDefaultsOnly)
		bool bHasCloseRangeMode = false;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bHasCloseRangeMode"))
		FName CloseRangePreparingSectionName;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bHasCloseRangeMode"))
		FName CloseRangeThrowingSectionName;
	// The time that player would throw the weapon, Works like notifiers but with numbers!
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bHasCloseRangeMode"))
		float CloseRangeThrowingTimeNotifier;

	FThrowableAnimation()
	{
		AnimMontage = nullptr;
		EquipSectionName = TEXT("Equip");
		PreparingSectionName = TEXT("Preparing");
		ThrowingSectionName = TEXT("Throwing");
		ThrowingTimeNotifier = 0.2;
		CloseRangePreparingSectionName = TEXT("CloseRangePreparing");
		CloseRangeThrowingSectionName = TEXT("CloseRangeThrowing");
		CloseRangeThrowingTimeNotifier = 0.2;
	}
};

USTRUCT(BlueprintType)
struct FParticleSpawner
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* Template;
	UPROPERTY(EditDefaultsOnly)
		bool bUseRootRotation;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseRootRotation"))
		FRotator Rotation;
	UPROPERTY(EditDefaultsOnly, meta = (AllowPreserveRatio))
		FVector Scale;
	UPROPERTY(EditDefaultsOnly)
		float SpawnTime;
	UPROPERTY(EditDefaultsOnly)
		bool bIsAttachedToOwner;

	FParticleSpawner()
	{
		Template = nullptr;
		SpawnTime = 0.0;
		bIsAttachedToOwner = true;
		bUseRootRotation = true;
		Rotation = FRotator(0);
		Scale = FVector(1);
	}
};

/*
Any type of throwable weapons use this base class.

NOTE : Explosion effects all trigger on actor destroying.
*/
UCLASS()
class PROJEKTSURVIVAL_API AThrowable : public AWeapon
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AThrowable();

	// Called every frame
	virtual void Tick(float DeltaTime) override;	

	// Plays Preparing Montage and sets the variable
	void Prepare(bool bIsCloseRange = false);

	// Plays throwing anim and trigger projectile launching
	void Throw(bool bIsCloseRange = false);

	// event attack key pressed
	void Attack() override;

	bool Aim() override;

	bool UnAim() override;	

	void Equip() override;

	void UnEquip(UItem_Weapon* Item, FName AttachSocketName) override;

	// event attack key released
	void OnAttackKeyRelease() override;	
	
	// Getters --
	/* */	
	//TODO Remove it laters!
	FName GetHandBoneName() { return TEXT("RightHand"); }
	float GetNextThrowingDelay() { return NextThrowingDelay; }
	bool HasCloseRange() { return bHasCloseRangeMode; }
	FThrowableAnimation GetAnimData() { return AnimationData; }		

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Events */	
	// Starts destruction process and it's effects...
	void SelfDestruct();
	// On Collision Hit
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);		

	// Launches Projectile with Given Settings
	void LaunchProjectile();

	//Bubble Sorts particle spawner by time
	void SortScheduledParticles();

	// Calls when actor explodes and adds force, spawns explosion particle and etc...
	void ExplosionEffect();

	// Switches the aiming mode to close range
	void GoCloseRangeMode();	

	// COMPONENTS --

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UCapsuleComponent* RootComp;
	// Should Face X Axis and Z Up for best experience
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UProjectileMovementComponent* ProjectileComponent;

	// VARIABLES --

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		FThrowableAnimation AnimationData;		

	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Long Range Mode")
		float UpOffset = 9;
	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Long Range Mode")
		float RightOffset = 2;
	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Long Range Mode")
		float ForwardOffset = 6;
	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Close Range Mode", meta = (EditCondition = "bHasCloseRangeMode"))
		float CRUpOffset = 1;
	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Close Range Mode", meta = (EditCondition = "bHasCloseRangeMode"))
		float CRRightOffset = 2;
	// Relative to Player capsule (Value multiplies by 10)
	UPROPERTY(EditDefaultsOnly, Category = "Attachment|Start Position|Close Range Mode", meta = (EditCondition = "bHasCloseRangeMode"))
		float CRForwardOffset = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Timing")
		float UnAimDelay = 0.5;
	// Delay after each throwing...
	UPROPERTY(EditDefaultsOnly, Category = "Timing")
		float NextThrowingDelay = 1;		
	// if false then the timer will begin when player started aiming
	UPROPERTY(EditDefaultsOnly, Category = "Timing")
		bool bCalculateLifeTimeAfterThrowing = true;

	// Close Range Mode Variables --

	UPROPERTY(EditDefaultsOnly, Category = "Close Range Mode")
		bool bHasCloseRangeMode = false;
	// Close range mode launch speed
	UPROPERTY(EditDefaultsOnly, Category = "Close Range Mode", meta = (EditCondition = "bHasCloseRangeMode"))
		float CRLaunchSpeed = 500;	
	
	// Damage for non-explosives
	UPROPERTY(EditDefaultsOnly, Category = "General", meta = (EditCondition = "!bIsExplodable"))
		FDamage HitDamage;
	UPROPERTY(EditDefaultsOnly, Category = "General")
		float LaunchSpeed = 1000;
	// 0 means never dies until sth happen, mostly use 0 for breakable throwables
	UPROPERTY(EditDefaultsOnly, Category = "General", meta = (ClampMin = "0"))
		float LifeTime = 10;
	// Destroys on hit if didnt have explosion duration
	UPROPERTY(EditDefaultsOnly, Category = "General")
		bool bIsMeshBreakable = false;

	// would rotate toward forward axis? like knives, ninja blades, etc...
	UPROPERTY(EditDefaultsOnly, Category = "General")
		bool bRotateInAir = false;
	UPROPERTY(EditDefaultsOnly, Category = "General", meta = (EditCondition = "bRotateInAir"))
		float RotationSpeed = 10;
	// Hit player would go to ragdoll if had head shoted
	UPROPERTY(EditDefaultsOnly, Category = "General")
		bool bRagdollOnHeadShot = false;
	// Rotates along Z (Up) axis like grenades...
	UPROPERTY(EditDefaultsOnly, Category = "General")
		bool bWouldRotateAfterHit = true;

	UPROPERTY(EditDefaultsOnly, Category = "General|Physics")
		bool bSimulatePhysicsOnHit = false;
	UPROPERTY(EditDefaultsOnly, Category = "General|Physics", meta = (EditCondition = "!bSimulatePhysicsOnHit"))
		bool bIsBouncy = false;
	UPROPERTY(EditDefaultsOnly, Category = "General|Physics", meta = (EditCondition = "bIsBouncy && !bSimulatePhysicsOnHit", ClampMin = "0", ClampMax = "1"))
		float Bounciness = 0.4;
	// The amount of sliding
	UPROPERTY(EditDefaultsOnly, Category = "General|Physics", meta = (EditCondition = "bIsBouncy && !bSimulatePhysicsOnHit", ClampMin = "0", ClampMax = "1"))
		float FrictionRestitution = 0.5;		

	// explodes after life time is over
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion")
		bool bIsExplodable = true;
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable"))
		float ExplosionDamage = 100;	
	/* explodes in a duration, molotovs should have it
	 IF LIFE TIME > 0
	 0 means explosion effect happen in one frame and on destoying
	 > 0 means explosion begin at LifeTime - ExplosionDuration.
	 IF LIFE TIME = 0
	 Then will destory on hit and explosion effects remain for ExplosionDuration */
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (ClampMin = "0", EditCondition = "bIsExplodable"))
		float ExplosionDuration = 0;
	// add angular force on explosion if enabled
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable"))
		bool bAddForceOnExplosion = false;
	// 0 means explosion effects will happen evey frame which is so expensive in performance
	// it's up to you!
	// WORKS ONLY WITH EXPLOSION DURATION!
	// ** WARNING SHOULD BE MORE THAN MAIN_REFRESH_INTERVAL WHICH IS 0.05 NOW **
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (ClampMin = "0", EditCondition = "ExplosionDuration > 0 && bAddForceOnExplosion && bIsExplodable"))
		float ExplosionEffectsInterval = 0.5;
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bAddForceOnExplosion"))
		float ExplosionStrength = 1000;
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bAddForceOnExplosion"))
		float ExplosionRadius = 250;
	// (EffectiveRadiusRatio * ExplosionRaidus) super effective range of the explosion radius (e.g makes player go to ragdoll state, same explodable throwables will be added together and explode as a single unit...)
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bAddForceOnExplosion", ClampMin = "0", ClampMax = "1"))
		float EffectiveRadiusRatio = 0.75;
	// Force that applies to ragdolls
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bAddForceOnExplosion"))
		float RagdollExplosionForce = 750;

	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable"))
		bool bSpawnExplosionParticleSystem = true;
	// Activates on destroying
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bSpawnExplosionParticleSystem"))
		UParticleSystem* ExplosionParticleSystem;
	// Scale of the explosion particle system
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable  && bSpawnExplosionParticleSystem", AllowPreserveRatio))
		FVector ExplosionScale = FVector(1);
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable"))
		bool bSpawnDecalImpactOnSurface = true;
	/* Explosion Impact on surface */
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bSpawnDecalImpactOnSurface"))
		UMaterialInterface* ExplosionImpactDecal;
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable  && bSpawnDecalImpactOnSurface"))
		float ExplosionDecalSize = 1;
	UPROPERTY(EditDefaultsOnly, Category = "General|Explosion", meta = (EditCondition = "bIsExplodable && bSpawnDecalImpactOnSurface"))
		float DecalLifeTime = 30;

	/* Times start calculating after throwable object was thrown!
	 Make sure particle systems have life time or they will last forever!!*/
	UPROPERTY(EditDefaultsOnly, Category = "Particle Systems")
		TArray<FParticleSpawner> ScheduledParticles;	
	
	// Default Force that would apply to other objects on hit + object's velocity ( Multiplies by 100 )
	UPROPERTY(EditDefaultsOnly, Category = "On Hitting")
		float ForceOnHit = 99;	
	UPROPERTY(EditDefaultsOnly, Category = "On Hitting")
		bool bSpawnBloodOnHit = true;
	// the amount of force that will be added to the hit bone
	UPROPERTY(EditDefaultsOnly, Category = "On Hitting")
		float HumanBodyHitForce = 5000;

	UPROPERTY(EditDefaultsOnly, Category = "Aiming Trajectory")
		UParticleSystem* TrajectoryBeamParticle;
	UPROPERTY(EditDefaultsOnly, Category = "Aiming Trajectory")
		UParticleSystem* TrajectoryHitParticle;
	// Higher values cost more performance!
	UPROPERTY(EditDefaultsOnly, Category = "Aiming Trajectory")
		int TrajectoryArcQuality = 20;
	// Higher values cost more performance!
	UPROPERTY(EditDefaultsOnly, Category = "Aiming Trajectory")
		float TrajectoryMaxRenderDistance = 1.25;	

	UPROPERTY(EditDefaultsOnly, Category = "Debuging")
		bool bDrawDebugHelpers = false;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T1 = 1;
	UPROPERTY(EditAnywhere, Category = "Debuging")
		float T2 = 1;

	UWorld* World = nullptr;

	// Main Timer
	void Timer() override;

	// Ticking Functions --
	/* Roation stuff */
	void InAirRotationManager(float DeltaTime);
	void AfterHitRotationManager(float DeltaTime);
	
	// Moves the actor to start point or syncs the location with the hand and etc...
	void ActorPositioningManager();
	
	// Calculates and draws the projectile trajectory + set throwing direction 
	void DrawProjectileTrajectory();
		
	// Line traces if rotate in air was active
	bool bLineTrace = false;
	bool bIsAiming = false;
	bool bHasThrown = false;
	// Transfers object to start point given in the settings
	bool MoveThrowableToStartPoint = false;
	// did hit anything...
	bool bDidHit = false;
	// activates close range mode
	bool bCloseRangeMode = false;

	bool bIsPreparedToThrow = false;

	// Start Point (Close Range or Long Range)
	FVector StartPoint;
	// Uses For Lerps
	float MoveToStartPointAlpha = 0;
	FVector ThrowableLocationOnThrow;
	FVector ThrowingDirection;

	// Segments of drawing object trajectory (trajectory line is consist of many line traces and emitters...)
	TArray<AEmitter*> TrajectorySegments;
	// all of the spawned particle system (will be deactivated on actor destruction)
	TArray<UParticleSystemComponent*> SpawnedParticleSystemComponents;
	
	// For rotating in air stuff
	float MeshLastPitch;
	FRotator MeshDefaultRotation;	
	// to Maniplute launch speed on movement
	float MovementMultiplier = 0;
	// Uses For Lerps | don't touch it
	float DistanceToHand = 1;
	// Adjusts static mesh's rotation to default on hit
	bool bAdjustRotationForHit = false;
	float RandomPitchForRotation;
	// Uses For Lerps
	float MeshRotationAlpha = 0;
	// Add Explosion effect on interval
	bool bExecuteExplosionEffects = false;

	float CurrentSpeed = 0;

	// Timers
	float ThrowTimer = -50;
	float ThrowingTimer = 50;
	float UnAimingTimer = 50;
	float ExplosionEffectIntervalTimer = 50;
	float ExplosionEffectTimer = 50;	
};