// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include "Projectiles/Bullet.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"
#include "../Controllers/PController.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanCombatComponent.h"
#include "../Characters/HumanCameraManagerComponent.h"
#include "../Characters/HumanMovementComponent.h"
#include "../UI/PlayerHUDWidget.h"

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// Setting up the Root Component
	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComp->SetCollisionProfileName(TEXT("NoCollision"));
	RootComp->SetNotifyRigidBodyCollision(false);
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	RootComp->SetBoxExtent(FVector(40, 20, 20));
	SetRootComponent(RootComp);

	// Skeletal Mesh of the Gun
	SKMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun Mesh"));
	SKMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// Barrel Tip Component
	BarrelTipComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Barrel Tip Location"));
	BarrelTipComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// Shell Spawner
	ShellSpawner = CreateDefaultSubobject<USceneComponent>(TEXT("Shell Spawner"));
	ShellSpawner->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// First Person Prespective Aiming Camera Component
	FPPAimingCamComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Aim Camera Location"));
	FPPAimingCamComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	MainTimerRefreshInterval = 0.05;
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();

	LaunchSpeed *= 100;
	EffectiveRange *= 100;
	MaxHitRange *= 100;
	LoadedBullets = MagazineSize;
	World = GetWorld();

	// Setting up Firing Mode vars
	if (bHasFullAuto)
		CurrentFiringMode = EGunFireMode::Auto;
	else if (bHasBurst)
		CurrentRecoilTime = BurstRecoilTime
		, CurrentFiringMode = EGunFireMode::Burst
		, BurstDelay += 3 * BurstRecoilTime;
	else if (bHasSemiAuto)
		CurrentFiringMode = EGunFireMode::SemiAuto;
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CrosshairKickBackManager(DeltaTime);

	if (bIsMeleeAttacking) // Melee Attacking
		ExecuteLineTraces();

	CalculateInAccuracy();

	// For FirstPerson Mode | We dont have first person mode yet
	//AlignWeaponRotationWithCrosshair();

	// Checking Gun Blockage and Shows Blocked UI	
	IsBulletBlocked(Player.Combat->GetProjectileAligningRange());

	// Firing Modes
	if (/* For Auto Mode */ (CurrentFiringMode == EGunFireMode::Auto && Player.Combat->IsTriggerPulled())
		/* For Burst Mode*/ || (CurrentFiringMode == EGunFireMode::Burst && BurstCounter > 0))
		Fire();
}

// Firing Proccess of Gun
void AGun::Fire()
{
	// Check for Reload	
	if (LoadedBullets <= 0 && AutoReloadTimer > AutoReloadDelay && !bIsReloading)
	{
		bDidAutoReload = true;
		AutoReloadTimer = 0;
		return;
	}

	// Burst Mode
	if (CurrentFiringMode == EGunFireMode::Burst && BurstTimer > BurstDelay && BurstCounter == 0)
		BurstCounter = 3, BurstTimer = 0;

	if (!IsLoaded() || bIsReloading) return;

	// Aligining With Crosshair Process
	float MinRangForAligning = Player.Combat->GetProjectileAligningRange();
	FVector CrosshairHitLocation = Player.Controller->GetSightRayHitLocation();

	float HitDistance = (RootComp->GetComponentLocation() - CrosshairHitLocation).Size();

	bool bCloseRangeShot = HitDistance < MinRangForAligning * 2;

	if (!IsAimingValid())
	{
		Player.Character->GetData()->ShowMessage(FText::FromString("BLOCKED | Something is on the way!"));
		return;
	}

	// Spawn Bullet and Shell
	ABullet* Bullet =
		World->SpawnActor<ABullet>(
			BulletType,
			BarrelTipComponent->GetComponentTransform());
	Bullet->SetActorLabel(FString("Bullet_Projectile"));

	// Calculates rotation of bullet to travel to the center of crosshair	
	FRotator BulletCrosshairRotation = UKismetMathLibrary::FindLookAtRotation(Bullet->GetActorLocation(), CrosshairHitLocation);

	// getting the actual line of linetrace for the crosshair and pass it to bullet in order 
	//to calculate the distance between bullet and actual shooting position which is the camera position
	FVector LineStart, LineEnd;
	Player.Controller->GetTraceLine(LineStart, LineEnd);
	LineStart.Z = 0;
	LineEnd.Z = 0;
	float LineRotationYaw = (LineStart - LineEnd).Rotation().Yaw;

	SuperCoolerTimer = 0;
	InAccuracyAlpha += InAccuracyIncreasingRate;

	ApplyInAccuracy(BulletCrosshairRotation);
	LineRotationYaw += FMath::RandRange(-AimInAccuracy, AimInAccuracy);

	// If bAlignWithCrosshair was true then the bullet will go straight then get aligned along the crosshair direction
	// Else bullet rotation will be set to go to the exact point of the crosshair hit
	Bullet->SetActorRotation(
		FRotator(BulletCrosshairRotation.Pitch,
			bCloseRangeShot ? BulletCrosshairRotation.Yaw : LineRotationYaw,
			BulletCrosshairRotation.Roll));

	FRotator TargetRotation = FRotator(0);
	if (bCloseRangeShot)
		TargetRotation = (CrosshairHitLocation - Player.Camera->GetViewportLocation()).Rotation(),
		ApplyInAccuracy(TargetRotation);

	Bullet->SetTarget(TargetRotation, LineStart, LineEnd);
	Bullet->Setup(Damage, Player.Combat->GetOwner(), LaunchSpeed, EffectiveRange, MaxHitRange, Deceleration, bCloseRangeShot, Player.Combat->GetProjectileAligningRange(), bSpawnShell, ShellSpawner->GetComponentLocation(), bWillShellSpawnToTheRight);

	// Playing Fire Animation (Particle)
	float RandomScale = FMath::RandRange((float)0.05, (float)0.125);
	UGameplayStatics::SpawnEmitterAttached(FirePS, BarrelTipComponent, NAME_None, BarrelTipComponent->GetComponentLocation(), BarrelTipComponent->GetComponentRotation(), FVector(RandomScale), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);

	// Playing Kickback Animation
	if (Player.Combat->GetCombatState() == ECombatState::Aiming)
		Player.Combat->PlayMontage(ECombatState::Aiming, KBAnimationData.AnimMontage, KBAnimationData.KickBackSectionName);
	else
		Player.Combat->PlayMontage(ECombatState::QuickAiming, KBAnimationData.AnimMontage, KBAnimationData.QuickKickBackSectionName);

	// Updating Vars
	--LoadedBullets;
	RecoilTimer = 0;

	// Burst Mode
	if (CurrentFiringMode == EGunFireMode::Burst && BurstCounter > 0)
		--BurstCounter;

	// Check for Reload	
	if (LoadedBullets <= 0)
		AutoReloadTimer = 0, bDidAutoReload = true;

	// For Crosshair Kickback stuff (Moving up/down)
	FireOutAlpha = 1;
}

// Sets default varaibles of gun parent
void AGun::Setup(FPlayerData InPlayer)
{
	Super::Setup(InPlayer);

	// Binding Event
	Player.AnimInst->OnMontageBlendingOut.AddDynamic(this, &AGun::OnMontageBlendingOut);

	HUD = Player.Controller->GetHUD();
}

void AGun::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	FName SectionName = Player.Combat->GetPlayingMontageSection();

	if (SectionName == AnimationData.MeleeAttackSectionName)
		SetCollision(false)
		, bIsMeleeAttacking = false
		, Player.Combat->CheckAiming()
		, Player.Combat->StartQuickAiming();
}

bool AGun::MeleeAttack()
{
	if (!bCanMeleeAttack)
		return false;

	if (Player.Combat->GetCombatState() == ECombatState::Aiming)
		Player.Combat->UnAim();
	else
		Player.Combat->SetCombatMode(true);

	MeleeAttackTimer = 0;
	// Play Anim
	Player.Combat->PlayMontage(ECombatState::MeleeAttacking, AnimationData.AnimMontage, AnimationData.MeleeAttackSectionName);

	return true;
}

bool AGun::Reload()
{
	if (!Player.Combat || (LoadedBullets == MagazineSize || ReloadTimer < ReloadTime || LoadTimer < LoadTime))
		return false;

	if (Player.Combat->GetCombatState() == ECombatState::Aiming)
		Player.Combat->UnAim();

	bIsReloading = true;
	ReloadTimer = 0;
	AutoReloadTimer = 50;
	Player.Combat->PlayMontage(ECombatState::Reloading, AnimationData.AnimMontage, AnimationData.ReloadSectionName);

	if (Player.Combat->GetCombatMode())
	{
		// Holding Combat Mode
		Player.Combat->PauseCombatModeTimer();

		if (!Player.Combat->IsAimingKeyDown()) // Means Ironsight aiming			
			Player.Movement->SetRotationMode(ERotationMode::OrientToMovement)
			, bRevertRotationMode = true;
	}

	if (bDidAutoReload)
		bDidAutoReload = false;

	BurstCounter = 0;

	return true;
}

void AGun::OnReloadingEnded()
{
	if (Player.Combat->GetCombatMode())
	{
		// Quick Aiming | It fixes a small bug aw, dont touch it! :)
		Player.Combat->PlayMontage(ECombatState::QuickAiming, AnimationData.AnimMontage, AnimationData.QuickAimingSectionName);

		if (Player.Combat->IsAimingKeyDown())
			Player.Combat->Aim();
		else if (bRevertRotationMode)
			Player.Movement->RevertRotationMode()
			, bRevertRotationMode = false;

		Player.Combat->ResetCombatModeTimer();
	}
	else
	{
		Player.Combat->PlayMovementMontage(Player.Movement->GetMovementState());
	}
}

// Timer Manager
void AGun::Timer()
{
	float DeltaTime = MainTimerRefreshInterval;

	// Recoil
	if (RecoilTimer < CurrentRecoilTime)
	{
		RecoilTimer += DeltaTime;

		// Recooil Morph Target settings for gun mesh
		float HalfOfRecoilTime = CurrentRecoilTime / 2;
		float MorphTargetValue;

		if (RecoilTimer < CurrentRecoilTime)
			if (RecoilTimer <= HalfOfRecoilTime) // Morph Target Increasing
				MorphTargetValue = RecoilTimer / HalfOfRecoilTime;
			else
				MorphTargetValue = 1 - ((RecoilTimer - HalfOfRecoilTime) / HalfOfRecoilTime);
		else
			MorphTargetValue = 0;

		SKMesh->SetMorphTarget(MorphTargetName, MorphTargetValue);
	}

	// Load
	if (LoadTimer < LoadTime)
	{
		LoadTimer += DeltaTime;

		//// Triggers aiming back on after reloading
		//if (LoadTimer >= LoadTime && Player.CombatComponent->bTriggerAimingIfPossible)
		//	Player.CombatComponent->Aim();

		if (LoadTimer >= LoadTime)
		{
			bIsReloading = false;

			OnReloadingEnded();
		}

		// Recooil Morph Target settings for gun mesh
		float HalfOfRecoilTime = LoadTime / 2;
		float MorphTargetValue;

		if (LoadTimer < LoadTime)
			if (LoadTimer <= HalfOfRecoilTime) // Morph Target Increasing
				MorphTargetValue = LoadTimer / HalfOfRecoilTime;
			else
				MorphTargetValue = 1 - ((LoadTimer - HalfOfRecoilTime) / HalfOfRecoilTime);
		else
			MorphTargetValue = 0;

		SKMesh->SetMorphTarget(MorphTargetName, MorphTargetValue);
	}

	// Reload
	if (ReloadTimer <= ReloadTime)
	{
		ReloadTimer += DeltaTime;

		if (ReloadTimer > ReloadTime)
		{
			Player.Combat->PlayMontage(ECombatState::Reloading, AnimationData.AnimMontage, AnimationData.LoadSectionName);
			LoadTimer = 0;
			LoadedBullets = MagazineSize;
		}
	}

	// Auto Reload
	if (AutoReloadTimer <= AutoReloadDelay)
	{
		AutoReloadTimer += DeltaTime;

		if (AutoReloadTimer > AutoReloadDelay)
			Player.Combat->Reload();
	}

	// Accuracy Cooldown
	if (SuperCoolerTimer <= SuperCoolerDelay)
	{
		AccuracySuperCooler = 1;
		SuperCoolerTimer += DeltaTime;
		if (SuperCoolerTimer > SuperCoolerDelay)
			AccuracySuperCooler = 10; // EXPERIMENTAL					

		// In order to stop updating aim offset on kick back anim
		if (SuperCoolerTimer < 0.2)
			Player.Camera->bUpdateAimPitch = false;
		else
			Player.Camera->bUpdateAimPitch = true;
	}

	// Melee Attack
	if (MeleeAttackTimer <= MeleeAttackActivationDelay)
	{
		MeleeAttackTimer += DeltaTime;

		// Trigger line traces for melee hit (Activate Melee Attack)
		if (MeleeAttackTimer > MeleeAttackActivationDelay)
			SetCollision(true)
			, bIsMeleeAttacking = true;
	}
	else if (MeleeAttackTimer <= MeleeAttackCooldown)
	{
		bCanMeleeAttack = false;
		MeleeAttackTimer += DeltaTime;

		if (MeleeAttackTimer > MeleeAttackCooldown)
			bCanMeleeAttack = true;
	}

	// Burst
	if (BurstTimer <= BurstDelay)
		BurstTimer += DeltaTime;
}

// Checks if the bullet would go to the crosshair (where the player actually aimed at) or not
void AGun::IsBulletBlocked(float TraceLength)
{
	ECombatState State = Player.Combat->GetCombatState();
	if (!Player.Controller || !IsLoaded() || (State != ECombatState::Aiming && State != ECombatState::QuickAiming))
	{
		if (bIsBlockedWidgetVisible)
			Player.Combat->SetBlockedWidget(false), bIsBlockedWidgetVisible = false;
		return;
	}

	FHitResult Hit;
	FVector StartLocation, EndLocation;
	FVector RayHitLoc = Player.Controller->GetSightRayHitLocation(Player.Combat->GetProjectileAligningRange() * 2),
		GunBarrelTipLocation = BarrelTipComponent->GetComponentLocation();

	StartLocation = GunBarrelTipLocation;
	// Calculates rotation of bullet to travel to the center of crosshair	
	FRotator BulletCrosshairRotation = UKismetMathLibrary::FindLookAtRotation(GunBarrelTipLocation, RayHitLoc);
	EndLocation = StartLocation + BulletCrosshairRotation.Vector() * TraceLength;

	if (bDrawDebugHelpers)
		DrawDebugPoint(World, RayHitLoc, 15, FColor::Green, false, -1),
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Red, false, -1, '\000', 0.75);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility) && !Hit.Location.Equals(RayHitLoc, 5))
		Player.Combat->SetBlockedWidget(true, Hit.Location + Hit.Normal * 5), bIsBlockedWidgetVisible = true;
	else
		Player.Combat->SetBlockedWidget(false), bIsBlockedWidgetVisible = false;
}

// Checks if sth is on the way of camera (Behind the player!)
bool AGun::IsAimingValid()
{
	FVector RayHitLoc = Player.Controller->GetSightRayHitLocation(),
		GunBarrelTipLocation = BarrelTipComponent->GetComponentLocation();

	// If player was actually hitting behind! (sth was on the way of camera behind the character)
	if (FVector::DotProduct(BarrelTipComponent->GetForwardVector().GetSafeNormal(),
		(RayHitLoc - GunBarrelTipLocation).GetSafeNormal()) < 0)
		return false;

	return true;
}

void AGun::ExecuteLineTraces()
{
	// Hit Simulation ----		
	FHitResult Hit;
	FCollisionQueryParams QueryParams = FCollisionQueryParams(NAME_None, true, Player.Character);
	QueryParams.AddIgnoredActor(this);

	FVector StartLocation =
		RootComp->GetComponentLocation()
		- RootComp->GetForwardVector().GetSafeNormal() * (RootComp->GetScaledBoxExtent().X / 2);
	FVector EndLocation =
		StartLocation
		+ RootComp->GetForwardVector().GetSafeNormal() * (RootComp->GetScaledBoxExtent().X + 5);

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Yellow, false, -1, '\000', 1);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel4 /* Projectile Trace */, QueryParams))
	{
		// Hitting Human
		if (Hit.Actor->GetClass()->IsChildOf<AHumanCharacter>())
		{
			USkeletalMeshComponent* HumanMesh = Cast<USkeletalMeshComponent>(Hit.Component);
			UHumanCombatComponent* HitHumanCombatComponent = Hit.Actor->FindComponentByClass<UHumanCombatComponent>();

			FVector AttackForward =
				UKismetMathLibrary::FindLookAtRotation(Player.Character->GetActorLocation(), Hit.Actor->GetActorLocation()).Vector();

			// Ragdoll state activates if player was moving over a certain speed (MinHitSpeedToGoToRagdoll in Human Class)
			if (HumanMesh->GetPhysicsLinearVelocity(TEXT("Hips")).Size() > Player.Combat->GetMinHitSpeedToGoToRagdoll())
				HitHumanCombatComponent->GoRagdoll();
			else
			{
				// find the hit direction
				FName HitDirectionLetter = Player.Combat->FindHitDirection(
					AttackForward
					, Hit.Actor->GetActorForwardVector());
				// Plays hit reaction montage
				HitHumanCombatComponent->PlayHitReactionMontage(HitDirectionLetter);
				// Dealing damage
				HitHumanCombatComponent->DealDamage(FDamage(20), TEXT("Head"));
			}

			// Only one hit is valid
			bIsMeleeAttacking = false;
			// Montage
			Player.Combat->StopMontage();

			FName HitBone = Hit.BoneName;
			if (HitBone == NAME_None)
				HitBone = TEXT("Spine");

			// Spawns Blood FX
			if (HitBone == TEXT("Head") || HitBone == TEXT("Neck"))
				UGameplayStatics::SpawnEmitterAttached(Player.Combat->BloodPS, HumanMesh, HitBone, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, true, EPSCPoolMethod::AutoRelease);
		}
		// Not Movable then play end montage in HumanCombatComponent
		else if (!Hit.Actor->IsRootComponentMovable())
		{
			// Only one hit is valid
			bIsMeleeAttacking = false;
			// Montage
			Player.Combat->StopMontage();
		}
	}
}

// Aligns gun's rotation with crosshair to hit percisely while aiming in First Person mode
void AGun::AlignWeaponRotationWithCrosshair()
{
	// Updating Gun Rotation to aim at the crosshair location
	if (Player.Combat->GetCombatState() == ECombatState::Aiming && Player.Camera->IsFirstPerson() && !Player.Combat->IsGunBlocked() && !bIsReloading)
	{
		FRotator CrosshairHitLookAt = UKismetMathLibrary::FindLookAtRotation(RootComp->GetComponentLocation(), Player.Controller->GetSightRayHitLocation());

		if (Player.Movement->GetMoveRightValue())
			bFPSFirstAlign = false;

		CrosshairHitLookAt.Roll -= Player.Movement->GetMoveRightValue() * GunMaxRollOnSideMovement;
		// Add Roll on moving to sides
		if (!Player.AnimInst->Montage_IsPlaying(KBAnimationData.AnimMontage) && IsLoaded())
			RootComp->SetWorldRotation(FMath::RInterpTo(RootComp->GetComponentRotation(), bFPSFirstAlign ? CrosshairFirstHitLookAt : CrosshairHitLookAt, World->DeltaTimeSeconds, 20));
	}

	/*if (Player.Combat->GetCombatState() != ECombatState::Aiming || bIsReloading || Player.Combat->IsGunBlocked())
		RootComp->SetRelativeRotation(FMath::RInterpTo(RootComp->GetRelativeRotation(), AttachBoneRotation, World->DeltaTimeSeconds, 10));*/
}

// Crosshair OwnerMovement to up/down due to kickback codes
void AGun::CrosshairKickBackManager(float DeltaTime)
{
	if (!CrosshairAddedOffset && !FireOutAlpha)
	{
		PureCrosshairAddedOffset = 0;
		return;
	}

	// If player was aiming then the max crosshair kickback offsets will be divided by 2
	float MaxKickBackOffset = this->MaxCrosshairKickBackOffset
		, GettingBackToDefaulOffset = this->CrosshairGettingBackToDefaulOffset
		, MovingUpOffsetOnFire = this->CrosshairMovingUpOffsetOnFire;

	if (Player.Combat->GetCombatState() == ECombatState::Aiming)
		MaxKickBackOffset /= 2,
		GettingBackToDefaulOffset /= 2
		, MovingUpOffsetOnFire /= 2;

	// Clamping
	CrosshairAddedOffset = FMath::Clamp<float>(CrosshairAddedOffset, 0, MaxKickBackOffset * 2);
	// Interping + Clamping
	FireOutAlpha = FMath::Clamp<float>(FMath::FInterpTo(FireOutAlpha, FireOutAlpha - 1, World->DeltaTimeSeconds, 2), 0, 1);

	// Canceling the crosshair turn back on moving the crosshair with mouse!
	if (Player.Camera->MouseYValue < 0)
		CrosshairAddedOffset += Player.Camera->MouseYValue;

	constexpr float DeltaTimeCoefficient = 10;

	// Checking whether player is still shooting or not
	if (FireOutAlpha == 0)
	{
		// The amount of offset that should add each frame in order to get crosshair back to its default position
		const float Value = DeltaTime * DeltaTimeCoefficient * GettingBackToDefaulOffset;
		// If remaining crosshair offset to default location was less than the value then we add it all! 
		// we do this to make sure crosshair will go back to it's default location
		const float OffsetToAdd = CrosshairAddedOffset > Value ? Value : CrosshairAddedOffset;
		Player.Camera->AddCamOffset(OffsetToAdd);
		CrosshairAddedOffset -= OffsetToAdd;
		PureCrosshairAddedOffset -= OffsetToAdd;
	}
	else
	{
		// After the crosshair max kickback exceeded then it will add (Offset x 0.01) offset up each frame...
		const float Value = DeltaTime * DeltaTimeCoefficient * MovingUpOffsetOnFire * (PureCrosshairAddedOffset < MaxKickBackOffset ? 1 : 0.01);
		Player.Camera->AddCamOffset(-Value);
		CrosshairAddedOffset += Value;
		PureCrosshairAddedOffset += Value;
	}
}

// Changes the CurrentFiringMode depend on the gun settings
void AGun::ChangeGunFiringMode()
{
	TArray<EGunFireMode> FireModes;

	// Adding All of the Fire Modes
	if (bHasFullAuto) FireModes.Add(EGunFireMode::Auto);
	if (bHasBurst) FireModes.Add(EGunFireMode::Burst);
	if (bHasSemiAuto) FireModes.Add(EGunFireMode::SemiAuto);

	int ArraySize = FireModes.Num();
	// Then there is no other fire mode to swtich between
	if (ArraySize == 1) return;

	// Finds the index of current firing mode
	int Index = FireModes.Find(CurrentFiringMode);

	// setting the next index
	Index++;
	if (Index >= ArraySize)
		Index = 0;

	// Changin Gun Fire Mode
	CurrentFiringMode = FireModes[Index];

	// Setting up Firing Mode vars
	if (CurrentFiringMode == EGunFireMode::Burst)
		CurrentRecoilTime = BurstRecoilTime;
	else if (CurrentFiringMode == EGunFireMode::SemiAuto || CurrentFiringMode == EGunFireMode::Auto)
		CurrentRecoilTime = RecoilTime, BurstCounter = 0;
}


void AGun::SetCollision(bool bEnabled)
{
	if (bEnabled)
	{
		RootComp->SetCollisionProfileName(TEXT("MeleeWeapon"));
		RootComp->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	}
	else
		RootComp->SetCollisionProfileName(TEXT("NoCollision"));
}

void AGun::ApplyInAccuracy(FRotator& Rotation)
{
	Rotation.Yaw += FMath::RandRange(-AimInAccuracy, AimInAccuracy);
	Rotation.Pitch += FMath::RandRange(-AimInAccuracy, AimInAccuracy);
	Rotation.Roll += FMath::RandRange(-AimInAccuracy, AimInAccuracy);
}

// Ticks and calculates in accuracy due to different elements
void AGun::CalculateInAccuracy()
{
	// Accuracy Recovery
	if (InAccuracyAlpha > 0)
		InAccuracyAlpha = FMath::FInterpTo(InAccuracyAlpha, -1, World->DeltaTimeSeconds, AccuracySuperCooler * AccuracyRecoverSpeedAfterSpraying * 0.2);

	// Applying accuracy stuff | 1.PlayerVelocity 2.Jumping 3.Crouching 5.Spraying 6.Aiming 
	//TODO 4.Proning	
	InAccuracyAlpha = FMath::Clamp<float>(InAccuracyAlpha, 0, 1);
	float InAccuracy = MaxInaccuracyRatio * JumpingInAccuracyMultiplier;
	if (!Player.Movement->IsFalling())
	{
		InAccuracy = MaxInaccuracyRatio;

		float Multiplier = 0;
		const float MinEffectiveSpeedOnInAccuracy = 50;
		const float MaxEffectiveSpeedOnInAccuracy = 270;
		Multiplier += (FMath::Clamp<float>(FMath::Abs(Player.Movement->GetSpeed()), MinEffectiveSpeedOnInAccuracy, MaxEffectiveSpeedOnInAccuracy) - MinEffectiveSpeedOnInAccuracy) / (MaxEffectiveSpeedOnInAccuracy - MinEffectiveSpeedOnInAccuracy);

		if (Player.Combat->GetCombatState() != ECombatState::Aiming)
			Multiplier += 0.3;

		if (Player.Movement->IsCrouching())
			Multiplier -= 0.3;

		Multiplier += InAccuracyAlpha;
		InAccuracy *= FMath::Clamp<float>(Multiplier, 0, 1);

		InAccuracy += MinAccuracyRatio;
	}

	// Updating UI
	HUD->SetCrosshairGap(InAccuracy);

	AimInAccuracy = InAccuracy;
}

void AGun::Attack()
{
	if (Player.Combat->GetCombatState() != ECombatState::Aiming && IsLoaded())
	{
		// just to make sure player ain't running
		Player.Movement->UnSprint();

		Player.Combat->PlayMontage(ECombatState::QuickAiming, AnimationData.AnimMontage, AnimationData.QuickAimingSectionName);
		Player.Combat->SetFireTimer(0);
		Player.Combat->SetFireAfterDelay(true);
	}

	if (Player.Combat->GetFireTimer() >= Player.Combat->GetFirstFireDelay())
		Fire();
	else
		Player.Combat->SetFireAfterDelay(true);
}

bool AGun::Aim()
{
	if (Player.Combat->GetCombatState() == ECombatState::Reloading || IsMeleeAttacking() || Player.Combat->GetIsWeaponBlocked())
		return false;
	else
		Player.Combat->SetFireTimer(0);

	Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.AimingSectionName);

	// First Person Codes for Setting the aiming camera
	if (!Player.Combat->IsAimingKeyDown())
	{
		CrosshairFirstHitLookAt = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Player.Controller->GetSightRayHitLocation());
		bFPSFirstAlign = true;
	}

	if (Player.Camera->IsFirstPerson())
		HUD->ShowReticle(EReticleType::NONE);

	Player.Combat->SetUseAimPose(true);
	Player.Camera->bUpdateAimPitch = true;
	Player.Camera->SetFPCameraDriver(true, FPPAimingCamComponent);

	return true;
}

bool AGun::UnAim()
{
	if (IsReloading() || Player.Combat->GetIsWeaponBlocked() || Player.Movement->IsRunning())
		return false;

	Player.Combat->PlayMontage(ECombatState::QuickAiming, AnimationData.AnimMontage, AnimationData.QuickAimingSectionName);

	HUD->ShowReticle(EReticleType::Crosshair);

	// First Person Aiming Cam
	Player.Combat->ResumeCurrentMontage(); // Pauses anim on aiming in first person
	Player.Camera->SetFPCameraDriver(false);

	return true;
}

void AGun::Equip()
{
	Super::Equip();

	Player.Combat->SetAimTypes(AimingType, QuickAimingType);
	Player.Combat->SetAttackType(EAttackType::Gun);
	Player.Combat->SetAimAdjuster(AimAdjustOffset);

	HUD->ShowReticle(EReticleType::Crosshair);
	HUD->SetCrosshairGap(MinAccuracyRatio);

	// Starts the main timer.
	World->GetTimerManager().SetTimer(MainTimer, this, &AGun::Timer, MainTimerRefreshInterval, true);
}

// If Gun is Ready to Shoot (Recoil, Reloading, Bullets, etc...)
bool AGun::IsLoaded()
{
	// had Ammo	&& Recoil was finished && Reloading was finished => TRUE
	if (ReloadTimer >= ReloadTime && LoadedBullets > 0
		&& RecoilTimer >= CurrentRecoilTime && LoadTimer >= LoadTime
		&& !(CurrentFiringMode == EGunFireMode::Burst && BurstTimer < BurstDelay && BurstCounter <= 0))
		return true;
	return false;
}

void AGun::CancelReloading()
{
	bIsReloading = false;
	ReloadTimer = 50;
	LoadTimer = 50;
}