// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "Bow.h"
#include "Projectiles/Arrow.h"
#include "../Characters/HumanCharacter.h"
#include "../Characters/HumanCombatComponent.h"
#include "../Characters/HumanCameraManagerComponent.h"
#include "../Characters/HumanMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Controllers/PController.h"

// Sets default values
ABow::ABow()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// Setting up the Root Component
	RootComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	RootComp->SetCollisionProfileName(TEXT("NoCollision"));
	RootComp->SetNotifyRigidBodyCollision(false);
	RootComp->SetGenerateOverlapEvents(false);
	RootComp->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	RootComp->SetBoxExtent(FVector(10, 5, 40));
	SetRootComponent(RootComp);

	// Skeletal Mesh of the Gun
	SKMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Bow Mesh"));
	SKMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// First Person Prespective Aiming Camera Component
	FPPAimingCamComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Aim Camera Location"));
	FPPAimingCamComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	MainTimerRefreshInterval = 0.05;
}

// Called when the game starts or when spawned
void ABow::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();	
}

void ABow::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	FName SectionName = Player.Combat->GetPlayingMontageSection();

	if (SectionName == AnimationData.LoadArrowSectionName)
		Draw();
	else if (SectionName == AnimationData.UnLoadBowSectionName)
		RestoreArrow();
}

void ABow::Timer()
{
	float DeltaTime = MainTimerRefreshInterval;

	// Drawing the Bow
	if (DrawTimer < AnimationData.StringPullingNotifier)
	{
		DrawTimer += DeltaTime;

		if (DrawTimer >= AnimationData.StringPullingNotifier)
		{
			bCurveBow = true;
			if (!bHasArrow)
				Reload();
		}
	}

	// UnLoading bow
	if (UnLoadTimer < AnimationData.StringReleaseNotifier)
	{
		UnLoadTimer += DeltaTime;

		if (UnLoadTimer >= AnimationData.StringReleaseNotifier)
			bCurveBow = false;
	}

	// Reload Delay
	if (ReloadDelayTimer < ReloadingDelay)
	{
		ReloadDelayTimer += DeltaTime;

		if (ReloadDelayTimer >= ReloadingDelay)
			Reload();
	}

	// Spawning Arrow Event
	if (ReloadTimer < AnimationData.ArrowAttachmentNotifier)
	{
		ReloadTimer += DeltaTime;

		if (ReloadTimer >= AnimationData.ArrowAttachmentNotifier)
		{
			bHasArrow = true;

			Arrow =
				World->SpawnActor<AArrow>(ArrowBlueprint);

			Arrow->SetActorLabel(FString("Arrow_Projectile"));
			Arrow->AttachToComponent(Player.Character->GetMesh(), FAttachmentTransformRules::KeepWorldTransform, TEXT("RightHand"));
			Arrow->SetActorRelativeTransform(FTransform(ArrowRelativeRotationForHand, ArrowRelativeLocationForHand));
			Arrow->SetOwnerActor(Player.Character);
			Arrow->SetDamageData(Damage);
		}
	}

	// Restore Arrow | has Arrow?!
	if (bHasArrow && RestoreTimer < AnimationData.ArrowDetachmentNotifier)
	{
		RestoreTimer += DeltaTime;

		if (RestoreTimer >= AnimationData.ArrowDetachmentNotifier)
		{
			Arrow->Destroy();
			bHasArrow = false;
		}
	}

}

// Called every frame
void ABow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	

	BowCurveSynchronizer();

	bIsBowReadyToShoot = bHasArrow && (BowCurveMorphTargetValue == 1); //Player.AnimInst->Montage_GetCurrentSection(AnimationData.AnimMontage) == AnimationData.AimLoopSectionName;
}

// Morph Target Stuff
void ABow::BowCurveSynchronizer()
{
	if (bCurveBow)
		BowCurveMorphTargetValue = FMath::Clamp<float>(FMath::FInterpTo(BowCurveMorphTargetValue, BowCurveMorphTargetValue + 1, World->DeltaTimeSeconds, MorphTargetsInterpolationSpeed), 0, 1);
	else
		BowCurveMorphTargetValue = FMath::Clamp<float>(FMath::FInterpTo(BowCurveMorphTargetValue, BowCurveMorphTargetValue - 1, World->DeltaTimeSeconds, MorphTargetsInterpolationSpeed * 2), 0, 1);
	SKMesh->SetMorphTarget(TEXT("StringCurve"), BowCurveMorphTargetValue);
	SKMesh->SetMorphTarget(TEXT("WoodCurve"), BowCurveMorphTargetValue);
}

void ABow::Setup(FPlayerData InPlayer)
{
	Super::Setup(InPlayer);

	// Binding Event
	Player.AnimInst->OnMontageBlendingOut.AddDynamic(this, &ABow::OnMontageBlendingOut);
}

bool ABow::Aim()
{
	if (!bIsBowReadyToShoot)
		Draw();
	else // For Blockage Situation
		Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.AimLoopSectionName);

	Player.Combat->SetUseAimPose(true);
	Player.Camera->SetFPCameraDriver(true, FPPAimingCamComponent);

	return true;
}

bool ABow::UnAim()
{
	UnLoadBow();

	// First Person Aiming Cam
	Player.Camera->SetFPCameraDriver(false);

	return true;
}

void ABow::Equip()
{
	Super::Equip();

	Player.Combat->SetAimTypes(EAimingType::Bow, EAimingType::Bow);
	Player.Combat->SetAttackType(EAttackType::Bow);
	Player.Combat->SetAimAdjuster(AimAdjustOffset);
}

void ABow::Release()
{
	if (!bIsBowReadyToShoot) return;

	// Aligining With Crosshair Process
	float MinRangForAligning = Player.Combat->GetProjectileAligningRange();
	FVector CrosshairHitLocation = Player.Controller->GetSightRayHitLocation();

	float HitDistance = (RootComp->GetComponentLocation() - CrosshairHitLocation).Size();

	bool bAlignWithCrosshair = HitDistance > MinRangForAligning;

	// Checking Bow Blockage
	if (IsArrowBlocked(bAlignWithCrosshair ? MinRangForAligning : Player.Combat->GetProjectileAligningRange()))
		return;

	FVector ArrowTipLocation = Arrow->GetArrowTipLocation();

	// Calculates rotation of Arrow to travel to the center of crosshair
	FRotator ArrowCrosshairRotation = UKismetMathLibrary::FindLookAtRotation(ArrowTipLocation, CrosshairHitLocation);
	ArrowCrosshairRotation.Pitch = Arrow->GetActorRotation().Pitch;

	// getting the actual line of linetrace for the crosshair and pass it to Arrow in order
	//to calculate the distance between Arrow and actual shooting position which is the camera position
	FVector LineStart, LineEnd;
	Player.Controller->GetTraceLine(LineStart, LineEnd);
	LineStart.Z = 0;
	LineEnd.Z = 0;
	float LineRotationYaw = (LineStart - LineEnd).Rotation().Yaw;

	AccuracyRecovery += 0.1;
	AccuracyRecovery = FMath::Clamp<float>(AccuracyRecovery, 0, 1);
	float InAccuracy = MaxInaccuracyRatio * JumpingInAccuracyMultiplier;
	if (!Player.Movement->IsFalling())
	{
		InAccuracy = MaxInaccuracyRatio;

		float Multiplier = 0;
		const float MinEffectiveSpeedOnInAccuracy = 50;
		const float MaxEffectiveSpeedOnInAccuracy = 270;
		Multiplier += (FMath::Clamp<float>(Player.Movement->GetSpeed(), MinEffectiveSpeedOnInAccuracy, MaxEffectiveSpeedOnInAccuracy) - MinEffectiveSpeedOnInAccuracy) / (MaxEffectiveSpeedOnInAccuracy - MinEffectiveSpeedOnInAccuracy);

		if (Player.Combat->GetCombatState() != ECombatState::Aiming)
			Multiplier += 0.3;

		if (Player.Movement->IsCrouching())
			Multiplier -= 0.3;

		Multiplier += AccuracyRecovery;
		InAccuracy *= FMath::Clamp<float>(Multiplier, 0, 1);
	}

	ArrowCrosshairRotation.Yaw += FMath::RandRange(-InAccuracy, InAccuracy);
	ArrowCrosshairRotation.Pitch += FMath::RandRange(-InAccuracy, InAccuracy);
	ArrowCrosshairRotation.Roll += FMath::RandRange(-InAccuracy, InAccuracy);
	LineRotationYaw += FMath::RandRange(-InAccuracy, InAccuracy);

	FRotator ArrowRotation = Arrow->GetActorRotation();
	// If bAlignWithCrosshair was true then the Arrow will go straight then get aligned along the crosshair direction
	// Else Arrow rotation will be set to go to the exact point of the crosshair hit
	Arrow->SetActorRotation(
		FRotator(ArrowCrosshairRotation.Pitch,
			bAlignWithCrosshair ? LineRotationYaw : ArrowCrosshairRotation.Yaw,
			ArrowCrosshairRotation.Roll));

	Arrow->SetTargetLocation(CrosshairHitLocation, LineStart, LineEnd);

	Arrow->LaunchProjectile(ArrowLaunchSpeed, ArrowRotation, bAlignWithCrosshair, Player.Combat->GetProjectileAligningRange());
	Arrow->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	bCurveBow = false, bHasArrow = false;
	// Activates the arrow reload delay
	ReloadDelayTimer = 0;
}

void ABow::Draw()
{
	if (bHasArrow)
	{
		Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.DrawBowSectionName);
		Player.Camera->bUpdateAimPitch = true;
		DrawTimer = 0;
		RestoreTimer = 50;
		UnLoadTimer = 50;
	}
	else
		Reload();
}

void ABow::UnLoadBow()
{
	Player.Camera->bUpdateAimPitch = false;
	if (bHasArrow)
	{
		UE_LOG(LogTemp, Warning, TEXT("Un Loading..."));
		Player.Combat->PlayMontage(ECombatState::NONE, AnimationData.AnimMontage, AnimationData.UnLoadBowSectionName);
		UnLoadTimer = 0;
	}
	else
	{
		bCurveBow = false;
		Player.Combat->StopMontage();
		DrawTimer = 50;
		UnLoadTimer = 50;
		ReloadDelayTimer = 50;
		ReloadTimer = 50;
		RestoreTimer = 50;
	}
}

void ABow::RestoreArrow()
{
	if (!bHasArrow) return;

	Player.Camera->bUpdateAimPitch = false;
	bCurveBow = false;
	Player.Combat->PlayMontage(ECombatState::NONE, AnimationData.AnimMontage, AnimationData.RestoreArrowSectionName);
	RestoreTimer = 0;
}

bool ABow::Reload()
{
	Player.Combat->PlayMontage(ECombatState::Aiming, AnimationData.AnimMontage, AnimationData.LoadArrowSectionName);
	ReloadTimer = 0;
	Player.Camera->bUpdateAimPitch = false;

	return true;
}

void ABow::Attack()
{
	Release();
}

void ABow::MeleeAttack(bool& Success)
{

}

void ABow::CancelActions()
{

}

// Checks if the Arrow would go to the crosshair (where the player actually aimed at) or not
bool ABow::IsArrowBlocked(float TraceLength)
{
	//TODO Add UI for it and dont stop shooting...
	if (!ensure(Player.Controller)) return false;

	FHitResult Hit;
	FVector RayHitLoc = Player.Controller->GetSightRayHitLocation(),
		StartLocation = Arrow->GetArrowTipLocation(), EndLocation;

	if (bDrawDebugHelpers)
	{
		DrawDebugPoint(World, RayHitLoc, 20, FColor::Green, false, 3);
		DrawDebugPoint(World, StartLocation, 20, FColor::Red, false, 3);
	}

	// If player was actually hitting behind! (sth was on the way of camera behind the character)
	if (FVector::DotProduct(Arrow->GetActorForwardVector().GetSafeNormal(),
		(RayHitLoc - StartLocation).GetSafeNormal()) < 0)
		return true;

	// Calculates rotation of Arrow to travel to the center of crosshair	
	FRotator ArrowCrosshairRotation = UKismetMathLibrary::FindLookAtRotation(StartLocation, RayHitLoc);
	EndLocation = StartLocation + ArrowCrosshairRotation.Vector() * TraceLength;

	if (bDrawDebugHelpers)
		DrawDebugLine(World, StartLocation, EndLocation, FColor::Red, false, 3, '\000', 1.5);

	if (World->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Visibility) && !Hit.Location.Equals(RayHitLoc, 5))
	{
		if (bDrawDebugHelpers)
			DrawDebugPoint(World, Hit.Location, 20, FColor::Yellow, false, 3);
		return true;
	}

	return false;
}