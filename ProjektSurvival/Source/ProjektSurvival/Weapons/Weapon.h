// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Structs.h"
#include "Weapon.generated.h"

class APController;
class UHumanCombatComponent;
class UHumanCameraManagerComponent;
class UHumanMovementComponent;
class AHumanCharacter;
class UAnimInstance;
class UItem_Weapon;

USTRUCT()
struct FPlayerData
{
	GENERATED_BODY()

public:

	// Owner of the Weapon
	AHumanCharacter* Character;

	// CombatComponent of Owner(Human)
	UHumanCombatComponent* Combat;

	// CameraManagerComponent of Owner(Human)
	UHumanCameraManagerComponent* Camera;

	// MovementComponent of Owner(Human)
	UHumanMovementComponent* Movement;

	// Animation instance to manage anim montages
	UAnimInstance* AnimInst;

	// InPlayer Controller
	APController* Controller;

	FPlayerData()
	{
		Character = nullptr, Combat = nullptr, Camera = nullptr
			, Movement = nullptr, AnimInst = nullptr, Controller = nullptr;
	}

	FPlayerData(AHumanCharacter* InCharacter, UHumanCombatComponent* InCombatComp, UHumanCameraManagerComponent* InCameraManager
		, UHumanMovementComponent* InMovementComp, UAnimInstance* InAnimInst, APController* InController)
	{
		Character = InCharacter, Combat = InCombatComp, Camera = InCameraManager
			, Movement = InMovementComp, AnimInst = InAnimInst, Controller = InController;
	}
};

UCLASS()
class PROJEKTSURVIVAL_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon();

	//TODO Rename this fuck shit
	virtual FTransform GetDefaultTransformForHand() { return FTransform(AttachBoneRotation, AttachBoneLocation, FVector(1)); }
	virtual FMovementAnimation GetMovementAnimData() { return MovementAnimationData; }

	virtual void Setup(FPlayerData InPlayer);

	virtual void Attack() { return; }
	virtual void OnAttackKeyRelease() { return; }
	virtual bool Reload() { return false; }
	virtual bool Aim() { return false; }
	virtual bool UnAim() { return false; }

	virtual void Equip();
	virtual void UnEquip(UItem_Weapon* Item, FName AttachSocketName);

protected:

	// OwnerMovement Animations
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		FMovementAnimation MovementAnimationData;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FName AttachBoneName;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FVector AttachBoneLocation;
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		FRotator AttachBoneRotation;

	FPlayerData Player;

	// Main timer that handles any sorta timings for weapons like reloading timer, cooldown and etc...
	// you can set the refresh interval with MainTimerRefreshInterval variable
	virtual void Timer();
	// refresh intervarl of the "void Timer()" method
	float MainTimerRefreshInterval = 0.05;
	// Timer handle to enable/disable main timer
	FTimerHandle MainTimer;
};
