// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "Bow.generated.h"

class APController;
class UHumanCombatComponent;
class AHumanCharacter;
class UBoxComponent;
class AArrow;
class UAnimInstance;


USTRUCT(BlueprintType)
struct FBowAnimation
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* AnimMontage;
	UPROPERTY(EditDefaultsOnly)
		FName DrawBowSectionName;
	UPROPERTY(EditDefaultsOnly)
		float StringPullingNotifier;

	UPROPERTY(EditDefaultsOnly)
		FName UnLoadBowSectionName;
	UPROPERTY(EditDefaultsOnly)
		float StringReleaseNotifier;

	UPROPERTY(EditDefaultsOnly)
		FName LoadArrowSectionName;
	UPROPERTY(EditDefaultsOnly)
		float ArrowAttachmentNotifier;

	UPROPERTY(EditDefaultsOnly)
		FName RestoreArrowSectionName;
	UPROPERTY(EditDefaultsOnly)
		float ArrowDetachmentNotifier;

	UPROPERTY(EditDefaultsOnly)
		FName AimLoopSectionName;

	UPROPERTY(EditDefaultsOnly)
		FName MeleeAttackingSectionName;
	UPROPERTY(EditDefaultsOnly)
		FName BlockedSectionName;

	FBowAnimation()
	{
		AnimMontage = nullptr;
		DrawBowSectionName = TEXT("DrawBow");
		UnLoadBowSectionName = TEXT("UnLoadBow");
		LoadArrowSectionName = TEXT("LoadArrow");
		RestoreArrowSectionName = TEXT("RestoreArrow");
		MeleeAttackingSectionName = TEXT("MeleeAttack");
		BlockedSectionName = TEXT("Blocked");
		AimLoopSectionName = TEXT("AimLoop");
	}
};

UCLASS()
class PROJEKTSURVIVAL_API ABow : public AWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABow();

	// Called every frame
	virtual void Tick(float DeltaTime) override;	

	// Sets default varaibles of Bow parent
	void Setup(FPlayerData InPlayer) override;

	bool Aim() override;

	bool UnAim() override;

	void Equip() override;

	// Firing Proccess of Bow
	void Release();

	// Pulls back the string and goes to aiming mode
	void Draw();

	// Pulls back the string and goes to aiming mode
	void UnLoadBow();

	// puts arrow back to its holster
	void RestoreArrow();

	// Plays Animation and set reloading timer
	bool Reload() override;

	// event attack key pressed
	void Attack() override;			

	// Melee Attack with the Bow
	void MeleeAttack(bool& Success);

	void CancelActions();

	//USceneComponent* GetFPPCamDriver();	
	FBowAnimation GetAnimData() { return AnimationData; }	
	USceneComponent* GetFPPCamDriver() { return FPPAimingCamComponent; }	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);

	UPROPERTY(VisibleAnywhere, Category = "Component")
		UBoxComponent* RootComp;
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USkeletalMeshComponent* SKMesh;
	// Camera Location in First Person Mode for Aiming
	UPROPERTY(VisibleAnywhere, Category = "Component")
		USceneComponent* FPPAimingCamComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		bool bDrawDebugHelpers = false;
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		float T1 = 0.1;
	UPROPERTY(EditDefaultsOnly, Category = "Debugging")
		float T2 = 0.2;
		
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		FBowAnimation AnimationData;
	// Adjusts the angle of gun in order to align with crosshiar!
	// possitive values would lead to move the gun barrel lower and vice versa
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		float AimAdjustOffset = 0;

	// Damage to other Players
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		FDamage Damage;
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float ReloadingDelay = 0.2;			
	// Location of arrow relative to the hand of player
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		FVector ArrowRelativeLocationForHand = FVector(1.1, -45.3, 3.5);
	// Rotation of arrow relative to the hand of player
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		FRotator ArrowRelativeRotationForHand = FRotator(4.5, -89.5, 4.3);
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float MorphTargetsInterpolationSpeed = 10;
	// Bow's maximum inaccuracy due to different stuff like 1.PlayerVelocity 2.Jumping 3.Crouching 5.Spraying 6.Aiming
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float MaxInaccuracyRatio = 2;		
	// Mutiplies with MaximumInAccuracyRatio
	UPROPERTY(EditDefaultsOnly, Category = "Settings|Accuracy")
		float JumpingInAccuracyMultiplier = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Arrow", NoClear)
		TSubclassOf<AArrow> ArrowBlueprint;
	UPROPERTY(EditDefaultsOnly, Category = "Arrow")
		float ArrowLaunchSpeed = 2500;	
	
private:
	void Timer() override;

	// Checks if the bullet would go to the crosshair (where the player actually aimed at) or not
	bool IsArrowBlocked(float TraceLength);

	// Morph Target Stuff
	void BowCurveSynchronizer();

	AArrow* Arrow = nullptr;	
	UWorld* World = nullptr;			
	// While Reloading Bow, it's true!
	bool bIsReloading = false;
	// Morph Target Value
	float MorphTargetValue = 0;		
	// 0 means totaly recovered, effects accuracy of weapon after firing non-stop
	float AccuracyRecovery = 0;
	// Gets big while player not shoot for a while
	float AccuracySuperCooler = 1;
	// Increases the Accuracy recover speed
	float SuperCoolerTimer = 0;	
	bool bIsMeleeAttacking = false;				

	bool bHasArrow = false;
	bool bIsBowReadyToShoot = false;
	bool bCurveBow = false;
	float BowCurveMorphTargetValue = 0;	

	float DrawTimer = 50;
	float UnLoadTimer = 50;
	float ReloadDelayTimer = 50;
	float ReloadTimer = 50;
	float RestoreTimer = 50;
};
