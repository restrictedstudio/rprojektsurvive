// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUDWidget.generated.h"

class UTextBlock;
class UImage;
class UProgressBar;
class UCanvasPanel;
class UHorizontalBox;
class UOverlay;
class UHumanDataComponent;

enum class EReticleType {
	NONE,
	Dot,
	Crosshair,
	ShotgunCrosshair
};

/**
 * Player HUD widget class. Waiting bar , crosshair, life qualities, hint and messages are here.
 */
UCLASS()
class PROJEKTSURVIVAL_API UPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void NativeConstruct() override;
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void SetScreenMessage(FText Text);
	void SetHint(FText Text);
	void Setup(UHumanDataComponent* DataComp) { Data = DataComp, bTick = true; }

	void SetupWaitBar(float WaitTime);
	void HideWaitBar();

	void ShowReticle(EReticleType Type);
	void SetCrosshairGap(float Gap);

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Life Params")
		float LifeParamsProgressBarsInterpSpeed = 1;

	// Dot Reticle
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* Dot_Reticle;
	// Crosshiar
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UCanvasPanel* Crosshair_Reticle;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* U_Crosshair;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* D_Crosshair;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* R_Crosshair;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* L_Crosshair;

	// Life Qualities
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UHorizontalBox* LifeQualities;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* HealthBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* HealthBarPercent;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* ThirstBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* ThirstBarPercent;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* HungerBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* HungerBarPercent;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* StaminaBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* StaminaPercent;

	// Wait Bar
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UOverlay* WaitingBarOverlay;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UProgressBar* WaitBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtWaitTime;

	// Hint
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* Hint;

	// Screen Message
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* ScreenMessage;

private:
	UHumanDataComponent* Data;

	// Ticks
	void SyncLifeParams(float DeltaTime);
	void SyncWaitBar(float DeltaTime);	
	void SyncCrosshairGap(float DeltaTime);	

	// Waiting Bar	
	float InitialWaitTime = -1;
	float RemainingWaitTime = -1;
	bool bUpdateWaitBar = false;

	float CrosshairGapTarget = 0, CrosshairGap = 0;	
	EReticleType ReticleType = EReticleType::Dot;

	bool bTick = false;		
};
