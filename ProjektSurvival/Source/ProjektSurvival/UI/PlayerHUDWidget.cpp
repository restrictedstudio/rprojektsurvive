// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "PlayerHUDWidget.h"
#include "TimerManager.h"
#include "../Characters/HumanDataComponent.h"
#include "../Controllers/PController.h"
#include "Components/TextBlock.h"
#include "Components/HorizontalBox.h"
#include "Components/CanvasPanel.h"
#include "Components/ProgressBar.h"
#include "Components/Overlay.h"
#include "Components/Image.h"

void UPlayerHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (Crosshair_Reticle)
		Crosshair_Reticle->SetVisibility(ESlateVisibility::Collapsed);
}

void UPlayerHUDWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	if (!bTick) return;

	SyncLifeParams(InDeltaTime);
	SyncWaitBar(InDeltaTime);

	if (ReticleType == EReticleType::Crosshair)
		SyncCrosshairGap(InDeltaTime);
}

void UPlayerHUDWidget::SetScreenMessage(FText Text)
{	
	ScreenMessage->SetText(Text);
	ScreenMessage->SetVisibility(
		Text.IsEmpty() ? ESlateVisibility::Collapsed : ESlateVisibility::Visible);
}

void UPlayerHUDWidget::SetHint(FText Text)
{
	Hint->SetText(Text);
	Hint->SetVisibility(
		Text.IsEmpty() ? ESlateVisibility::Collapsed : ESlateVisibility::Visible);
}

void UPlayerHUDWidget::SyncLifeParams(float DeltaTime)
{
	// Health Bar		
	HealthBar->SetPercent(
		FMath::FInterpTo(HealthBar->Percent, Data->GetHealth(), DeltaTime, LifeParamsProgressBarsInterpSpeed));
	HealthBarPercent->SetText(FText::FromString(FString::FromInt(FMath::CeilToInt(HealthBar->Percent * 100))));

	// Hunger Bar
	HungerBar->SetPercent(
		FMath::FInterpTo(HungerBar->Percent, Data->GetHunger(), DeltaTime, LifeParamsProgressBarsInterpSpeed));
	HungerBarPercent->SetText(FText::FromString(FString::SanitizeFloat(FMath::RoundToInt(HungerBar->Percent * 100), 0)));

	// Thirst Bar
	ThirstBar->SetPercent(
		FMath::FInterpTo(ThirstBar->Percent, Data->GetThirst(), DeltaTime, LifeParamsProgressBarsInterpSpeed));
	ThirstBarPercent->SetText(FText::FromString(FString::SanitizeFloat(FMath::RoundToInt(ThirstBar->Percent * 100), 0)));

	// Stamina Bar
	StaminaBar->SetPercent(
		FMath::FInterpTo(StaminaBar->Percent, Data->GetStamina(), DeltaTime, LifeParamsProgressBarsInterpSpeed));
	StaminaPercent->SetText(FText::FromString(FString::FromInt((int)(StaminaBar->Percent * 100))));
}

void UPlayerHUDWidget::SyncWaitBar(float DeltaTime)
{
	if (!bUpdateWaitBar) return;

	// Progress Bar
	WaitBar->SetPercent(RemainingWaitTime / InitialWaitTime);
	// Text
	float RemainingTime = (float)((int)(RemainingWaitTime * 10)) / 10;
	txtWaitTime->SetText(FText::FromString(FString::SanitizeFloat(RemainingTime, 1)));

	RemainingWaitTime -= DeltaTime;
}

void UPlayerHUDWidget::SyncCrosshairGap(float DeltaTime)
{
	CrosshairGap = FMath::FInterpTo(CrosshairGap, CrosshairGapTarget, DeltaTime, 10);

	L_Crosshair->SetRenderTranslation(FVector2D(-CrosshairGap, 0));
	R_Crosshair->SetRenderTranslation(FVector2D(CrosshairGap, 0));
	U_Crosshair->SetRenderTranslation(FVector2D(0, -CrosshairGap));
	D_Crosshair->SetRenderTranslation(FVector2D(0, CrosshairGap));				
}

void UPlayerHUDWidget::ShowReticle(EReticleType Type)
{
	if (ReticleType == Type) return;

	// Hiding Ex Reticle
	switch (ReticleType)
	{
	case EReticleType::Dot:
		Dot_Reticle->SetVisibility(ESlateVisibility::Collapsed);
		break;

	case EReticleType::Crosshair:
		Crosshair_Reticle->SetVisibility(ESlateVisibility::Collapsed);
		break;

	case EReticleType::ShotgunCrosshair:
		break;
	}

	ReticleType = Type;

	// Showing Desired Reticle
	switch (Type)
	{
	case EReticleType::Dot:
		Dot_Reticle->SetVisibility(ESlateVisibility::Visible);
		break;

	case EReticleType::Crosshair:
		Crosshair_Reticle->SetVisibility(ESlateVisibility::Visible);
		break;

	case EReticleType::ShotgunCrosshair:
		break;
	}
}

void UPlayerHUDWidget::SetCrosshairGap(float Gap)
{
	Gap *= 20; // Experimental and Weird!! but it just works fine :|
	CrosshairGapTarget = Gap;
}

void UPlayerHUDWidget::SetupWaitBar(float WaitTime)
{
	InitialWaitTime = WaitTime;
	RemainingWaitTime = WaitTime;
	bUpdateWaitBar = true;

	WaitingBarOverlay->SetVisibility(ESlateVisibility::Visible);
}

void UPlayerHUDWidget::HideWaitBar()
{
	bUpdateWaitBar = false;

	WaitingBarOverlay->SetVisibility(ESlateVisibility::Collapsed);
	WaitBar->SetPercent(1);
	txtWaitTime->SetText(FText());
}
