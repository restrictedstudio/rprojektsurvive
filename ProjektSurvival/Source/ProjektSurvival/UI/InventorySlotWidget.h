// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventorySlotWidget.generated.h"

class UTextBlock;
class UImage;
class UBorder;
class UItem;
class UInventoryWidget;
enum class ESlotType;

/**
 * Each slot in the inventory uses this class.
 */
UCLASS()
class PROJEKTSURVIVAL_API UInventorySlotWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	
	// Mouse Clicks
	FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	FReply NativeOnMouseButtonDoubleClick(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;	
	// Drag & Drop
	void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation) override;
	void NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;

	void Setup(UInventoryWidget* InventoryRef, int IDToSet, UItem* ItemToSet, int AmountToSet, ESlotType SlotTypeToSet);
	void SetAmount(int AmountToSet);
	int GetAmount() { return Amount; }
	int GetID() { return ID; }
	UItem* GetItem() { return Item; }
	ESlotType GetSlotType() { return SlotType; }	
	void ClearAmountText();

	// Updates Slot's UI (basically the background color)
	/**/
	void SetSelected(bool bEnabled);	

protected:

	UPROPERTY(EditDefaultsOnly)
		FLinearColor SelectedBackgroundColor;
	UPROPERTY(EditDefaultsOnly)
		FLinearColor DefaultBackgroundColor;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* Icon;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtAmount;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtID;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UBorder* Background;

private:			

	// PROPERTIES
	/**/
	int ID = -1;
	UItem* Item = nullptr;
	int Amount = -1;
	ESlotType SlotType;
	UInventoryWidget* InventoryUI;

	bool bRightClicked = false;

};
