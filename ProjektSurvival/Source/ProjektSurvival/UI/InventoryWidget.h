// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "../Structs.h"
#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"

class APController;
class UHumanDataComponent;
class UGridPanel;
class UUniformGridPanel;
class UTextBlock;
class UEditableTextBox;
class UBackgroundBlur;
class UInventorySlotWidget;
class UInventoryContextMenuWidget;
class UVerticalBox;
class UBorder;
class UButton;
class UCanvasPanel;
class UImage;
enum class EWearableSlot : uint8;

USTRUCT(BlueprintType)
struct FPie
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bEnabled;

	FPie()
	{
		Name = TEXT("PieName");
		Icon = nullptr;
		bEnabled = true;
	}
};

USTRUCT(BlueprintType)
struct FPieSettings
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor ActiveColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor SelectedColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor DisabledColor;
	// better be power of 2
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MenuSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.5", ClampMax = "1"))
		float PieRadius;
	// the space between pies
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MarginAngle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector2D IconsSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIconsFaceCenter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bShowSelectedName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bShowSelectedName"))
		int SelectedPieFontSize;

	FPieSettings()
	{
		ActiveColor = FLinearColor(0.25, 0.25, 0.25, 0.5);
		SelectedColor = FLinearColor(0.85, 0.85, 0.85, 0.75);;
		DisabledColor = FLinearColor(0.08, 0.08, 0.08, 0.3);;
		MenuSize = 512;
		PieRadius = 0.75;
		MarginAngle = 0.001;
		IconsSize = FVector2D(64, 64);
		bIconsFaceCenter = false;
		bShowSelectedName = true;
		SelectedPieFontSize = 35;
	}
};

/**
 *
 */
UCLASS()
class PROJEKTSURVIVAL_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual void NativeConstruct() override;
	virtual void NativePreConstruct() override;
	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;	

	UFUNCTION(BlueprintCallable, Category = "Animation")
		void ToggleInventoryView();

	// STACK
	UFUNCTION()
		void CloseStackFrame();
	UFUNCTION()
		void OpenStackFrame();

	UInventoryContextMenuWidget* GetContextMenu() { return ContextMenu; }

	void AddProximitySlot(APController* PlayerController, FProximitySlot PSlot);
	void RemoveProximitySlot(APController* PlayerController, int ID, bool bSort = true);
	void SortProximitySlots();
	// Basically just updates item's amount!
	void UpdateProximitySlot(APController* PlayerController, FProximitySlot PSlot);

	// Basically just updates item's amount and removes the item from inventory if amount was 0!
	void UpdateInventorySlot(UHumanDataComponent* Data, APController* PlayerController, FInventorySlot ISlot);
	void AddInventorySlot(UHumanDataComponent* Data, APController* PlayerController, FInventorySlot ISlot);

	void SetSelectedSlot(UInventorySlotWidget* SlotWidget);
	void SelectHotbarSlot(EHotbarSlotLocation SlotLocation);

	void SetupWearableSlot(UHumanDataComponent* Data, EWearableSlot SlotType, UInventorySlotWidget* SlotWidget, UImage* SlotImage);
	void SetupHotbarSlot(UHumanDataComponent* Data, EHotbarSlotLocation SlotLocation, UInventorySlotWidget* SlotWidget, UImage* SlotImage);

	void LoadInventorySlots(UHumanDataComponent* Data, APController* PlayerController);
	void LoadProximitySlots(UHumanDataComponent* Data, APController* PlayerController);
	void LoadWearedSlots(UHumanDataComponent* Data, APController* PlayerController);
	void LoadHotbarSlots(UHumanDataComponent* Data, APController* PlayerController);	

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float HotbarCloseDelay = 3;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UVerticalBox* LeftBar;

	// Contatining player items in inventory
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UGridPanel* ItemsGrid;
	// For Drag & Drop
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UVerticalBox* InventoryAndCraftingBox;

	// For Future
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UGridPanel* CraftingGrid;

	// Any item in proximity area should be shown in this grid
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UGridPanel* LootGrid;
	// For Drag & Drop
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UBorder* LootBackground;

	// Any item in proximity area should be shown in this grid
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UBackgroundBlur* LeftBarBlur;

	/* WEARABLES */
	// Contatining player weared items
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UUniformGridPanel* WearablePanel;
	// Weared Slots
	/* HEAD */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* HeadSlotImage;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* HeadSlotWidget;
	/* PANTS */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* PantsSlotImage;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* PantsSlotWidget;
	// ...


	/* HOTBAR */
	// Contatining player weapons
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UCanvasPanel* HotbarPanel;
	/* Hotbar Slots	*/
	// PRIMARY 1
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* PrimarySlot1Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* PrimarySlot1Widget;
	// PRIMARY 2
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* PrimarySlot2Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* PrimarySlot2Widget;
	// SECONDARY 1
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* SecondarySlot1Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* SecondarySlot1Widget;
	// SECONDARY 2
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* SecondarySlot2Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* SecondarySlot2Widget;
	// THROWABLE 1
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* ThrowableSlot1Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* ThrowableSlot1Widget;
	// THROWABLE 2
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* ThrowableSlot2Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* ThrowableSlot2Widget;
	// THROWABLE 3
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* ThrowableSlot3Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* ThrowableSlot3Widget;
	// THROWABLE 4
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UImage* ThrowableSlot4Image;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UInventorySlotWidget* ThrowableSlot4Widget;

	// INFO BOX
	// The title of info box (selected item)
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtInfoTitle;
	// Descreyption of the selected item in the info box
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtInfoContent;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UTextBlock* txtInventoryCapacity;

	// STACK
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UBorder* StackBackground;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UEditableTextBox* txtStackAmount;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnSubmitStack;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnCloseStack;

	UPROPERTY(BlueprintReadWrite, Category = "Animations", meta = (BindWidgetAnim))
		UWidgetAnimation* ShowInventoryAnimation;

	void UpdateInfoBox();

	bool bIsInventoryOpen = false;
	const int32 ColumnsCount = 5;

private:

	FTimerHandle InventoryCleanupTimer;
	void CleanupInventory();

	FTimerHandle HotbarTimer;
	void HideHotbar();

	APController* Controller = nullptr;

	// Stack | events
	UFUNCTION()
		void ExecuteStackAction();
	UFUNCTION()
		void txtStackAmountOnTextChanged(const FText& Text);
	UFUNCTION()
		void txtStackAmountOnTextCommitted(const FText& Text, ETextCommit::Type CommitMethod);


	UInventoryContextMenuWidget* ContextMenu;

	// Takes a widget and depend on mouse location realizes whether it's hovering or not
	bool IsMouseHovering(UWidget* Widget, FPointerEvent PointerEvent);

	// @return EHotbarSlotLocation::NONE if not found.
	EHotbarSlotLocation GetHoveredHotbarSlot(UItem_Weapon* Item, FPointerEvent PointerEvent);

	UInventorySlotWidget* SelectedSlot = nullptr;
	UInventorySlotWidget* SelectedStackSlot = nullptr;	

	TArray<UInventorySlotWidget*> InventorySlotsWidget;
	TArray<UInventorySlotWidget*> ProximitySlotsWidget;
};