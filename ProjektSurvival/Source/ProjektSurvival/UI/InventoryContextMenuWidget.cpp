// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "InventoryContextMenuWidget.h"
#include "../Items/Item.h"
#include "../Controllers/PController.h"
#include "../Characters/HumanDataComponent.h"
#include "../UI/InventoryWidget.h"
#include "Components/GridPanel.h"
#include "Components/GridSlot.h"
#include "Components/Button.h"

void UInventoryContextMenuWidget::NativeConstruct()
{
	Controller = GetOwningPlayer<APController>();

	Close();

	// Binding on click events
	btnTake->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Take);
	btnCarry->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Carry);
	btnEquip->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Use);
	btnDrop->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Drop);	
	btnConsume->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Use);
	btnStack->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Stack);
	btnThrow->OnClicked.AddDynamic(this, &UInventoryContextMenuWidget::Throw);
}

void UInventoryContextMenuWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Close();
}

void UInventoryContextMenuWidget::Setup(int IDToSet, UItem* ItemToSet, int AmountToSet, ESlotType SlotTypeToSet)
{
	ChildsCount = ButtonsGrid->GetChildrenCount();

	ID = IDToSet;
	Item = ItemToSet;
	Amount = AmountToSet;
	SlotType = SlotTypeToSet;

	APhysicalItem* PhItem = Item->GetPhysicalItem();

	// SETTING UP BUTTONS

	btnTake->SetVisibility(ESlateVisibility::Visible);
	btnCarry->SetVisibility(ESlateVisibility::Visible);
	btnThrow->SetVisibility(ESlateVisibility::Visible);
	btnEquip->SetVisibility(ESlateVisibility::Visible);
	btnDrop->SetVisibility(ESlateVisibility::Visible);
	btnConsume->SetVisibility(ESlateVisibility::Visible);

	if (!PhItem->bIsCarriable)
	{
		btnCarry->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;
		btnThrow->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;
	}
	else
		if (!PhItem->GetAnimData().bIsThrowable)
			btnThrow->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;

	if (!PhItem->bIsConsumable)
		btnConsume->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;
	if (!PhItem->bIsEquipable)
		btnEquip->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;

	if (SlotType == ESlotType::Proximity)
		btnDrop->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;
	else
		btnTake->SetVisibility(ESlateVisibility::Collapsed), --ChildsCount;
}

void UInventoryContextMenuWidget::Open()
{
	SortGrid();
	SetPositionInViewport(FindBestPosition());
	SetVisibility(ESlateVisibility::Visible);
}

void UInventoryContextMenuWidget::Close()
{
	SetVisibility(ESlateVisibility::Collapsed);
}

// Sorts Grid Childs in Squar shape possibaly
void UInventoryContextMenuWidget::SortGrid()
{
	int ColumnsCount = 2;
	if (ChildsCount > 4)
		ColumnsCount = 3;

	int Index = 0;
	for (int i = 0; i < ButtonsGrid->GetChildrenCount(); ++i)
	{
		// Re-Sorting Childs
		UGridSlot* Child = Cast<UGridSlot>(ButtonsGrid->GetChildAt(i)->Slot);

		if (ButtonsGrid->GetChildAt(i)->IsVisible())
		{
			Child->SetRow(Index / ColumnsCount);
			Child->SetColumn(Index % ColumnsCount);

			++Index;
		}
	}
}

// Finds the best position relative to mouse position
FVector2D UInventoryContextMenuWidget::FindBestPosition()
{
	int ViewportX, ViewportY;
	GetOwningPlayer()->GetViewportSize(ViewportX, ViewportY);

	float MouseX, MouseY;
	GetOwningPlayer()->GetMousePosition(MouseX, MouseY);

	// The Offset the menu is outta of screen
	float YOffsetAdjuster = (GetDesiredSize().Y * 0.5 + MouseY) - ViewportY;

	return FVector2D(MouseX, MouseY - (YOffsetAdjuster > 0 ? YOffsetAdjuster : 0));
}


void UInventoryContextMenuWidget::Take()
{
	Item->Take(Controller, ID, SlotType);

	Close();
}

void UInventoryContextMenuWidget::Drop()
{
	Item->Drop(Controller, ID, SlotType);

	Close();
}

void UInventoryContextMenuWidget::Use()
{
	Item->Use(Controller, ID, SlotType);

	Close();
}

void UInventoryContextMenuWidget::Carry()
{
	Item->Carry(Controller, ID, SlotType);

	Close();
}

void UInventoryContextMenuWidget::Throw()
{	
	Item->Throw(Controller, ID, SlotType);

	Close();
}

void UInventoryContextMenuWidget::Stack()
{
	Controller->GetInventoryUI()->OpenStackFrame();

	Close();
}