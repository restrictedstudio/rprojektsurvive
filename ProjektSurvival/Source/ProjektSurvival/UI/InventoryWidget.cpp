// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#include "InventoryWidget.h"
#include "InventorySlotWidget.h"
#include "InventoryContextMenuWidget.h"
#include "../Characters/HumanDataComponent.h"
#include "../Characters/HumanCharacter.h"
#include "../Items/Item.h"
#include "../Items/Item_Wearable.h"
#include "../Items/Item_Weapon.h"
#include "../Controllers/PController.h"
#include "TimerManager.h"
#include "Blueprint/DragDropOperation.h"

#include "Components/GridPanel.h"
#include "Components/GridSlot.h"
#include "Components/UniformGridPanel.h"
#include "Components/TextBlock.h"
#include "Components/BackgroundBlur.h"
#include "Components/VerticalBox.h"
#include "Components/Border.h"
#include "Components/EditableTextBox.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"
#include "Components/Image.h"

void UInventoryWidget::NativePreConstruct()
{
	if (txtStackAmount)
		txtStackAmount->SelectAllTextWhenFocused = true;

	if (LeftBar)
		LeftBar->SetVisibility(ESlateVisibility::Collapsed);
	if (WearablePanel)
		WearablePanel->SetVisibility(ESlateVisibility::Collapsed);
	if (HotbarPanel)
		HotbarPanel->SetVisibility(ESlateVisibility::Collapsed);

	if (HeadSlotWidget)
		HeadSlotWidget->SetVisibility(ESlateVisibility::Collapsed);
	if (PantsSlotWidget)
		PantsSlotWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void UInventoryWidget::NativeConstruct()
{
	// Call Blueprint Event Construct node
	Super::NativeConstruct();

	Controller = GetOwningPlayer<APController>();

	// STACK	
	btnCloseStack->OnClicked.AddDynamic(this, &UInventoryWidget::CloseStackFrame);
	btnSubmitStack->OnClicked.AddDynamic(this, &UInventoryWidget::ExecuteStackAction);
	txtStackAmount->OnTextChanged.AddDynamic(this, &UInventoryWidget::txtStackAmountOnTextChanged);
	txtStackAmount->OnTextCommitted.AddDynamic(this, &UInventoryWidget::txtStackAmountOnTextCommitted);

	if (Controller->InventoryContextMenuBP)
	{
		ContextMenu = CreateWidget<UInventoryContextMenuWidget>(Controller, Controller->InventoryContextMenuBP, TEXT("Context Menu"));
		ContextMenu->AddToViewport(99);
	}
}

bool UInventoryWidget::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	//if (!InOperation->Payload->GetClass()->IsChildOf<UInventorySlotWidget>()) return false;
	UE_LOG(LogTemp, Warning, TEXT("Fok"));
	UInventorySlotWidget* DraggedSlot = Cast<UInventorySlotWidget>(InOperation->Payload);

	if (DraggedSlot->GetSlotType() == ESlotType::Inventory || DraggedSlot->GetSlotType() == ESlotType::Proximity)
		DraggedSlot->SetVisibility(ESlateVisibility::Visible);

	// Drop on Inventory
	if (IsMouseHovering(InventoryAndCraftingBox, InDragDropEvent))
	{
		if (DraggedSlot->GetSlotType() == ESlotType::Inventory) return false;

		// Stack drag and drop
		if (InOperation->Tag.Equals(TEXT("StackDrop")))
		{
			SetSelectedSlot(DraggedSlot);
			OpenStackFrame();
			return true;
		}

		DraggedSlot->GetItem()->Take(Controller, DraggedSlot->GetID(), DraggedSlot->GetSlotType());

		return true;
	}
	// Drop on Wearables
	else if (IsMouseHovering(WearablePanel, InDragDropEvent))
	{
		if (DraggedSlot->GetSlotType() == ESlotType::Wearables
			|| !DraggedSlot->GetItem()->GetClass()->IsChildOf<UItem_Wearable>())
			return false;

		DraggedSlot->GetItem()->Use(Controller, DraggedSlot->GetID(), DraggedSlot->GetSlotType());

		return true;
	}
	// Drop on Hotbar
	else if (IsMouseHovering(HotbarPanel, InDragDropEvent))
	{
		UE_LOG(LogTemp, Warning, TEXT("oops"));
		if (!DraggedSlot->GetItem()->GetClass()->IsChildOf<UItem_Weapon>())
			return false;

		UItem_Weapon* Item = Cast<UItem_Weapon>(DraggedSlot->GetItem());
		EHotbarSlotLocation SlotLocation = GetHoveredHotbarSlot(Item, InDragDropEvent);

		if (DraggedSlot->GetSlotType() == ESlotType::Hotbar && SlotLocation != EHotbarSlotLocation::NONE)
		{
			UHumanDataComponent* DataComp = Controller->GetPawn<AHumanCharacter>()->GetData();
			if (!DataComp->SwapHotbarSlots(SlotLocation, DraggedSlot->GetID()))
				DraggedSlot->SetVisibility(ESlateVisibility::Visible); // If item was dragged on itself!
		}
		else
		{
			// Hovered on Correct Slot
			if (SlotLocation != EHotbarSlotLocation::NONE)
				Item->EquipAt(Controller, DraggedSlot->GetID(), DraggedSlot->GetSlotType(), SlotLocation);
			// Wasnt from hotbar
			else if (DraggedSlot->GetSlotType() != ESlotType::Hotbar)
				Item->Use(Controller, DraggedSlot->GetID(), DraggedSlot->GetSlotType());
			// Othervise | Nothing Happens
			else
				DraggedSlot->SetVisibility(ESlateVisibility::Visible);
		}

		return true;
	}
	// Drop on Proximity
	else //if (IsMouseHovering(LootBackground, InDragDropEvent))
	{
		if (DraggedSlot->GetSlotType() == ESlotType::Proximity) return false;

		// Stack drag and drop
		if (InOperation->Tag.Equals(TEXT("StackDrop")))
		{
			SetSelectedSlot(DraggedSlot);
			OpenStackFrame();
			return true;
		}

		DraggedSlot->GetItem()->Drop(Controller, DraggedSlot->GetID(), DraggedSlot->GetSlotType());

		switch (DraggedSlot->GetSlotType())
		{
		case ESlotType::Hotbar:
			Controller->GetPawn<AHumanCharacter>()->GetData() // Getting Data
				->RemoveHotbarSlot(DraggedSlot->GetID());
			break;

		case ESlotType::Wearables:
			Controller->GetPawn<AHumanCharacter>()->GetData() // Getting Data
				->RemoveWearableSlot(Cast<UItem_Wearable>(DraggedSlot->GetItem())->GetCompatibleSlot()); // UnEquiping Wearable
			break;
		}
	}

	return true;
}

void UInventoryWidget::LoadInventorySlots(UHumanDataComponent* Data, APController* PlayerController)
{
	if (!PlayerController) return;

	ItemsGrid->ClearChildren();
	InventorySlotsWidget.Empty(Data->GetInventorySlots().Num());

	for (int i = 0; i < Data->GetInventorySlots().Num(); ++i)
	{
		FInventorySlot InvSlot = Data->GetInventorySlots()[i];

		UInventorySlotWidget* SlotWidget =
			CreateWidget<UInventorySlotWidget>(PlayerController, PlayerController->InventorySlotWidgetBP);
		SlotWidget->Setup(this, InvSlot.ID, InvSlot.Item, InvSlot.Amount, ESlotType::Inventory);
		InventorySlotsWidget.Add(SlotWidget);

		ItemsGrid->AddChildToGrid(SlotWidget, i / ColumnsCount, i % ColumnsCount);
	}

	FFormatNamedArguments Args;
	Args.Add("MaxCapacity", Data->GetInventoryMaxCapacity());
	Args.Add("UsedCapacity", Data->GetInventoryUsedCapacity());

	txtInventoryCapacity->SetText(FText::Format(
		NSLOCTEXT("Inventory", "Capacity", "{MaxCapacity} / {UsedCapacity}"),
		Args
	));
}

void UInventoryWidget::LoadProximitySlots(UHumanDataComponent* Data, APController* PlayerController)
{
	if (!PlayerController) return;

	LootGrid->ClearChildren();
	ProximitySlotsWidget.Empty(Data->GetProximitySlots().Num());

	// Trace to find proximity items.
	Data->DetectProximitySlots();
}

void UInventoryWidget::LoadWearedSlots(UHumanDataComponent* Data, APController* PlayerController)
{
	if (!PlayerController) return;

	// Head
	SetupWearableSlot(Data, EWearableSlot::Head, HeadSlotWidget, HeadSlotImage);
	// Pants
	SetupWearableSlot(Data, EWearableSlot::Pants, PantsSlotWidget, PantsSlotImage);
}

void UInventoryWidget::LoadHotbarSlots(UHumanDataComponent* Data, APController* PlayerController)
{
	if (!PlayerController) return;

	// Panel Visibility
	if (!bIsInventoryOpen)
	{
		if (!HotbarPanel->IsVisible())
			HotbarPanel->SetVisibility(ESlateVisibility::Visible);
		GetWorld()->GetTimerManager().SetTimer(HotbarTimer, this, &UInventoryWidget::HideHotbar, HotbarCloseDelay, false);
	}

	// Primary
	SetupHotbarSlot(Data, EHotbarSlotLocation::HeavyWeapon1, PrimarySlot1Widget, PrimarySlot1Image);
	SetupHotbarSlot(Data, EHotbarSlotLocation::HeavyWeapon2, PrimarySlot2Widget, PrimarySlot2Image);
	// Secondary
	SetupHotbarSlot(Data, EHotbarSlotLocation::LightWeapon1, SecondarySlot1Widget, SecondarySlot1Image);
	SetupHotbarSlot(Data, EHotbarSlotLocation::LightWeapon2, SecondarySlot2Widget, SecondarySlot2Image);
	// Throwable
	SetupHotbarSlot(Data, EHotbarSlotLocation::Throwable1, ThrowableSlot1Widget, ThrowableSlot1Image);
	SetupHotbarSlot(Data, EHotbarSlotLocation::Throwable2, ThrowableSlot2Widget, ThrowableSlot2Image);
	SetupHotbarSlot(Data, EHotbarSlotLocation::Throwable3, ThrowableSlot3Widget, ThrowableSlot3Image);
	SetupHotbarSlot(Data, EHotbarSlotLocation::Throwable4, ThrowableSlot4Widget, ThrowableSlot4Image);
}

void UInventoryWidget::AddProximitySlot(APController* PlayerController, FProximitySlot PSlot)
{
	if (!PlayerController) return;

	UInventorySlotWidget* SlotWidget =
		CreateWidget<UInventorySlotWidget>(PlayerController, PlayerController->InventorySlotWidgetBP);

	SlotWidget->Setup(this, PSlot.ID, PSlot.Item, PSlot.Amount, ESlotType::Proximity);

	ProximitySlotsWidget.Add(SlotWidget);

	LootGrid->AddChildToGrid(SlotWidget, LootGrid->GetChildrenCount() / ColumnsCount, LootGrid->GetChildrenCount() % ColumnsCount);
}

void UInventoryWidget::RemoveProximitySlot(APController* PlayerController, int ID, bool bSort)
{
	int ChildIndex = 0;

	int IndexToRemove = -1;
	for (int i = 0; i < ProximitySlotsWidget.Num(); ++i)
	{
		if (ProximitySlotsWidget[i]->GetID() == ID)
			IndexToRemove = i;
		else if (bSort)
		{
			// Re-Sorting Childs
			UGridSlot* Child = Cast<UGridSlot>(LootGrid->GetChildAt(i)->Slot);
			Child->SetRow(ChildIndex / ColumnsCount);
			Child->SetColumn(ChildIndex % ColumnsCount);

			++ChildIndex;
		}
	}

	if (IndexToRemove != -1)
	{
		ProximitySlotsWidget[IndexToRemove]->RemoveFromParent();
		ProximitySlotsWidget.RemoveAt(IndexToRemove);
	}
}

void UInventoryWidget::SortProximitySlots()
{
	for (int i = 0; i < ProximitySlotsWidget.Num(); ++i)
	{
		UGridSlot* Child = Cast<UGridSlot>(LootGrid->GetChildAt(i)->Slot);
		Child->SetRow(i / ColumnsCount);
		Child->SetColumn(i % ColumnsCount);
	}
}

void UInventoryWidget::UpdateProximitySlot(APController* PlayerController, FProximitySlot PSlot)
{
	int ChildIndex = 0;
	for (int i = 0; i < ProximitySlotsWidget.Num(); ++i)
	{
		if (ProximitySlotsWidget[i]->GetID() == PSlot.ID)
			ProximitySlotsWidget[i]->SetAmount(PSlot.Amount);

		// Re-Sorting Childs
		UGridSlot* Child = Cast<UGridSlot>(LootGrid->GetChildAt(i)->Slot);
		Child->SetRow(ChildIndex / ColumnsCount);
		Child->SetColumn(ChildIndex % ColumnsCount);

		++ChildIndex;
	}
}

void UInventoryWidget::SetSelectedSlot(UInventorySlotWidget* SlotWidget)
{
	/*if (SelectedSlot)
		SelectedSlot->SetSelected(false);*/

	SelectedSlot = SlotWidget;
	//SelectedSlot->SetSelected(true);

	UpdateInfoBox();
}

void UInventoryWidget::SelectHotbarSlot(EHotbarSlotLocation SlotLocation)
{
	// Panel Visibility
	if (!bIsInventoryOpen)
	{
		if (!HotbarPanel->IsVisible())
			HotbarPanel->SetVisibility(ESlateVisibility::Visible);
		GetWorld()->GetTimerManager().SetTimer(HotbarTimer, this, &UInventoryWidget::HideHotbar, HotbarCloseDelay, false);
	}

	// Selection | Deselecting All
	PrimarySlot1Widget->SetSelected(false);
	PrimarySlot2Widget->SetSelected(false);
	SecondarySlot1Widget->SetSelected(false);
	SecondarySlot2Widget->SetSelected(false);
	ThrowableSlot1Widget->SetSelected(false);
	ThrowableSlot2Widget->SetSelected(false);
	ThrowableSlot3Widget->SetSelected(false);
	ThrowableSlot4Widget->SetSelected(false);

	// Selection | Selecting the equiped slot
	switch (SlotLocation)
	{
	case EHotbarSlotLocation::HeavyWeapon1: PrimarySlot1Widget->SetSelected(true); break;
	case EHotbarSlotLocation::HeavyWeapon2: PrimarySlot2Widget->SetSelected(true); break;
	case EHotbarSlotLocation::LightWeapon1: SecondarySlot1Widget->SetSelected(true); break;
	case EHotbarSlotLocation::LightWeapon2: SecondarySlot2Widget->SetSelected(true); break;
	case EHotbarSlotLocation::Throwable1: ThrowableSlot1Widget->SetSelected(true); break;
	case EHotbarSlotLocation::Throwable2: ThrowableSlot2Widget->SetSelected(true); break;
	case EHotbarSlotLocation::Throwable3: ThrowableSlot3Widget->SetSelected(true); break;
	case EHotbarSlotLocation::Throwable4: ThrowableSlot4Widget->SetSelected(true); break;
	}
}

void UInventoryWidget::SetupWearableSlot(UHumanDataComponent* Data, EWearableSlot SlotType, UInventorySlotWidget* SlotWidget, UImage* SlotImage)
{
	int SlotIndex = Data->GetWearableSlotIndex(SlotType);
	if (SlotIndex == -1)
		SlotWidget->SetVisibility(ESlateVisibility::Collapsed);
	else if (SlotIndex != -1 && (!SlotWidget->IsVisible() || Data->GetWearedSlots()[SlotIndex].Item != SlotWidget->GetItem()))
	{
		FWearableSlot WSlot = Data->GetWearedSlots()[SlotIndex];
		SlotWidget->Setup(this, WSlot.ID, WSlot.Item, 1, ESlotType::Wearables);
		SlotWidget->ClearAmountText();
		SlotWidget->SetVisibility(ESlateVisibility::Visible);
	}
}

void UInventoryWidget::SetupHotbarSlot(UHumanDataComponent* Data, EHotbarSlotLocation SlotLocation, UInventorySlotWidget* SlotWidget, UImage* SlotImage)
{
	FHotbarSlot* HSlot = Data->GetHotbarSlot(SlotLocation);
	//if (SlotWidget->GetItem() == HSlot->Item) return;

	if (HSlot->Item)
	{
		SlotWidget->Setup(this, HSlot->ID, HSlot->Item, 1, ESlotType::Hotbar);
		SlotWidget->ClearAmountText();
		SlotWidget->SetVisibility(ESlateVisibility::Visible);
	}
	else
		SlotWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void UInventoryWidget::UpdateInventorySlot(UHumanDataComponent* Data, APController* PlayerController, FInventorySlot ISlot)
{
	int ChildIndex = 0;

	bool ReSort = false;
	if (!ISlot.Amount)
		ReSort = true;

	int IndexToRemove = -1;
	for (int i = 0; i < InventorySlotsWidget.Num(); ++i)
	{
		if (InventorySlotsWidget[i]->GetID() == ISlot.ID)
			if (!ISlot.Amount)
				IndexToRemove = i;
			else
			{
				InventorySlotsWidget[i]->SetAmount(ISlot.Amount);
				return;
			}
		else if (ReSort)
		{
			// Re-Sorting Childs
			UGridSlot* Child = Cast<UGridSlot>(ItemsGrid->GetChildAt(i)->Slot);
			Child->SetRow(ChildIndex / ColumnsCount);
			Child->SetColumn(ChildIndex % ColumnsCount);

			++ChildIndex;
		}
	}

	if (IndexToRemove != -1)
	{
		InventorySlotsWidget[IndexToRemove]->RemoveFromParent();
		InventorySlotsWidget.RemoveAt(IndexToRemove);
	}

	FFormatNamedArguments Args;
	Args.Add("MaxCapacity", Data->GetInventoryMaxCapacity());
	Args.Add("UsedCapacity", Data->GetInventoryUsedCapacity());

	txtInventoryCapacity->SetText(FText::Format(
		NSLOCTEXT("Inventory", "Capacity", "{MaxCapacity} / {UsedCapacity}"),
		Args
	));
}

void UInventoryWidget::AddInventorySlot(UHumanDataComponent* Data, APController* PlayerController, FInventorySlot ISlot)
{
	if (!PlayerController) return;

	UInventorySlotWidget* SlotWidget =
		CreateWidget<UInventorySlotWidget>(PlayerController, PlayerController->InventorySlotWidgetBP);
	SlotWidget->Setup(this, ISlot.ID, ISlot.Item, ISlot.Amount, ESlotType::Inventory);

	InventorySlotsWidget.Add(SlotWidget);

	ItemsGrid->AddChildToGrid(SlotWidget, ItemsGrid->GetChildrenCount() / ColumnsCount, ItemsGrid->GetChildrenCount() % ColumnsCount);

	FFormatNamedArguments Args;
	Args.Add("MaxCapacity", Data->GetInventoryMaxCapacity());
	Args.Add("UsedCapacity", Data->GetInventoryUsedCapacity());

	txtInventoryCapacity->SetText(FText::Format(
		NSLOCTEXT("Inventory", "Capacity", "{MaxCapacity} / {UsedCapacity}"),
		Args
	));
}

void UInventoryWidget::ToggleInventoryView()
{
	float StartTime = GetAnimationCurrentTime(ShowInventoryAnimation);
	// Gonna Close Inventory
	if (bIsInventoryOpen)
	{
		// Finding the animation play time inverse!
		StartTime = IsAnimationPlaying(ShowInventoryAnimation) ?
			ShowInventoryAnimation->GetEndTime() - GetAnimationCurrentTime(ShowInventoryAnimation) : 0;

		GetWorld()->GetTimerManager().SetTimer(InventoryCleanupTimer, this, &UInventoryWidget::CleanupInventory, ShowInventoryAnimation->GetEndTime(), false);
		CloseStackFrame();
	}
	else
	{
		if (!LeftBar->IsVisible())
		{
			WearablePanel->SetVisibility(ESlateVisibility::Visible);
			HotbarPanel->SetVisibility(ESlateVisibility::Visible);
			LeftBar->SetVisibility(ESlateVisibility::Visible);
			LeftBarBlur->SetBlurStrength(7);
		}

		GetWorld()->GetTimerManager().ClearTimer(InventoryCleanupTimer);
		GetWorld()->GetTimerManager().ClearTimer(HotbarTimer);
	}

	PlayAnimation(ShowInventoryAnimation, StartTime, 1, bIsInventoryOpen ? EUMGSequencePlayMode::Reverse : EUMGSequencePlayMode::Forward, 2);

	bIsInventoryOpen = !bIsInventoryOpen;
}

void UInventoryWidget::UpdateInfoBox()
{
	if (!SelectedSlot) return;

	UItem* Item = SelectedSlot->GetItem();
	txtInfoTitle->SetText(FText::FromName(Item->GetName()));
	txtInfoContent->SetText(Item->GetDescryption());
}

void UInventoryWidget::CloseStackFrame()
{
	StackBackground->SetVisibility(ESlateVisibility::Collapsed);
}

void UInventoryWidget::OpenStackFrame()
{
	if (!SelectedSlot) return;

	SelectedStackSlot = SelectedSlot;

	StackBackground->SetVisibility(ESlateVisibility::Visible);

	// Setting half of item amount as default stack value.
	txtStackAmount->SetText(FText::FromString(FString::FromInt(SelectedStackSlot->GetAmount() / 2)));
	txtStackAmount->SetUserFocus(Controller);
}

void UInventoryWidget::CleanupInventory()
{
	LeftBar->SetVisibility(ESlateVisibility::Collapsed);
	WearablePanel->SetVisibility(ESlateVisibility::Collapsed);
	HotbarPanel->SetVisibility(ESlateVisibility::Collapsed);

	LeftBarBlur->SetBlurStrength(0);
	ProximitySlotsWidget.Empty(0);

	// Grids
	LootGrid->ClearChildren();
	ItemsGrid->ClearChildren();

	// Info Box
	txtInfoContent->SetText(FText::FromString("Select an Item to see its info."));
	txtInfoTitle->SetText(FText::FromString("INFO"));
}

void UInventoryWidget::HideHotbar()
{
	HotbarPanel->SetVisibility(ESlateVisibility::Collapsed);
}

void UInventoryWidget::ExecuteStackAction()
{
	CloseStackFrame();

	if (!SelectedStackSlot) return;

	int SelectedAmount = FCString::Atoi(*txtStackAmount->GetText().ToString());
	if (!SelectedAmount) return;

	switch (SelectedStackSlot->GetSlotType())
	{
	case ESlotType::Inventory:
		SelectedStackSlot->GetItem()->Drop(
			Controller, SelectedStackSlot->GetID(), ESlotType::Inventory, SelectedAmount);
		return;

	case ESlotType::Proximity:
		SelectedStackSlot->GetItem()->Take(
			Controller, SelectedStackSlot->GetID(), ESlotType::Proximity, SelectedAmount);
		return;

	}
}

void UInventoryWidget::txtStackAmountOnTextCommitted(const FText& Text, ETextCommit::Type CommitMethod)
{
	if (!SelectedStackSlot) return;

	txtStackAmount->SetText(FText::FromString(
		FString::FromInt(
			FMath::Clamp<int>(FCString::Atoi(*Text.ToString()), 0, SelectedStackSlot->GetAmount())
		)
	));

	if (CommitMethod == ETextCommit::OnEnter)
		ExecuteStackAction();
}

void UInventoryWidget::txtStackAmountOnTextChanged(const FText& Text)
{
	txtStackAmount->SetText(FText::FromString(
		FString::FromInt(
			FCString::Atoi(*Text.ToString())
		)
	));
}

// Takes a widget and depend on mouse location realizes whether it's hovering or not
bool UInventoryWidget::IsMouseHovering(UWidget* Widget, FPointerEvent PointerEvent)
{
	// Relative to Widget Location
	FVector2D MouseRelativeLocation = Widget->GetCachedGeometry().AbsoluteToLocal(PointerEvent.GetScreenSpacePosition());
	// Widget Size
	FVector2D WidgetSize = Widget->GetCachedGeometry().GetLocalSize();

	// Is In Range (in widget's size range)
	if ((MouseRelativeLocation.X > 0 && MouseRelativeLocation.X < WidgetSize.X)
		&& (MouseRelativeLocation.Y > 0 && MouseRelativeLocation.Y < WidgetSize.Y))
		return true;

	return false;
}

EHotbarSlotLocation UInventoryWidget::GetHoveredHotbarSlot(UItem_Weapon* Item, FPointerEvent PointerEvent)
{
	switch (Item->GetCompatibleSlot())
	{
	case EWeaponSlotType::HeavyWeapon:
		if (IsMouseHovering(PrimarySlot1Image, PointerEvent)) return EHotbarSlotLocation::HeavyWeapon1;
		else if (IsMouseHovering(PrimarySlot2Image, PointerEvent)) return EHotbarSlotLocation::HeavyWeapon2;
		break;

	case EWeaponSlotType::LightWeapon:
		if (IsMouseHovering(SecondarySlot1Image, PointerEvent)) return EHotbarSlotLocation::LightWeapon1;
		else if (IsMouseHovering(SecondarySlot2Image, PointerEvent)) return EHotbarSlotLocation::LightWeapon2;
		break;

	case EWeaponSlotType::Throwable:
		if (IsMouseHovering(ThrowableSlot1Image, PointerEvent)) return EHotbarSlotLocation::Throwable1;
		else if (IsMouseHovering(ThrowableSlot2Image, PointerEvent)) return EHotbarSlotLocation::Throwable2;
		else if (IsMouseHovering(ThrowableSlot3Image, PointerEvent)) return EHotbarSlotLocation::Throwable3;
		else if (IsMouseHovering(ThrowableSlot4Image, PointerEvent)) return EHotbarSlotLocation::Throwable4;
		break;
	}
	return EHotbarSlotLocation::NONE;
}
