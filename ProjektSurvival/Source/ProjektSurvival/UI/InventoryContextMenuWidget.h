// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryContextMenuWidget.generated.h"

class UButton;
class UGridPanel;
class UItem;
class APController;
enum class ESlotType;

/**
 *
 */
UCLASS()
class PROJEKTSURVIVAL_API UInventoryContextMenuWidget : public UUserWidget
{
	GENERATED_BODY()


public:
	void NativeConstruct() override;
	void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;

	void Setup(int IDToSet, UItem* ItemToSet, int AmountToSet, ESlotType SlotTypeToSet);
	void Open();
	void Close();

	// Sorts Grid Childs in Squar shape possibaly
	void SortGrid();

	// Finds the best position relative to mouse position
	FVector2D FindBestPosition();

protected:

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UGridPanel* ButtonsGrid;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnEquip;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnDrop;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnTake;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnConsume;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnCarry;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnStack;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UButton* btnThrow;

private:

	APController* Controller = nullptr;

	// Buttons Events
	/**/
	UFUNCTION()
		void Take();
	UFUNCTION()
		void Drop();
	UFUNCTION()
		void Use();
	UFUNCTION()
		void Carry();
	UFUNCTION()
		void Throw();
	UFUNCTION()
		void Stack();

	// PROPERTIES
	/**/
	int ID = -1;
	UItem* Item = nullptr;
	int Amount = -1;
	ESlotType SlotType;

	int ChildsCount;
};