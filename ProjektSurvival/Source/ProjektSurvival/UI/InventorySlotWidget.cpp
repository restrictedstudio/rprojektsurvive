// Copyright Restricted Studio 2018 by Koorosh Torabi All Rights Reserved


#include "InventorySlotWidget.h"
#include "InventoryWidget.h"
#include "InventoryContextMenuWidget.h"
#include "../Items/Item.h"
#include "../Controllers/PController.h"
#include "../Characters/HumanDataComponent.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Blueprint/DragDropOperation.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"

FReply UInventorySlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (InMouseEvent.IsMouseButtonDown(FKey(TEXT("RightMouseButton"))))
	{
		// Context Menu
		if (SlotType == ESlotType::Inventory || SlotType == ESlotType::Proximity)
		{
			UInventoryContextMenuWidget* Menu = InventoryUI->GetContextMenu();
			Menu->Setup(ID, Item, Amount, SlotType);
			Menu->Open();
		}

		InventoryUI->SetSelectedSlot(this);

		// Stack Drag
		return UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent, this, FKey(TEXT("RightMouseButton"))).NativeReply;
	}
	else if (InMouseEvent.IsMouseButtonDown(FKey(TEXT("LeftMouseButton"))))
	{
		InventoryUI->SetSelectedSlot(this);
		// Normal Drag
		return UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent, this, FKey(TEXT("LeftMouseButton"))).NativeReply;
	}

	return FReply::Handled();
}

FReply UInventorySlotWidget::NativeOnMouseButtonDoubleClick(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	switch (SlotType)
	{
	case ESlotType::Inventory:
		Item->Use(GetOwningPlayer<APController>(), ID, SlotType);
		break;

	default:
		Item->Take(GetOwningPlayer<APController>(), ID, SlotType);		
		break;

	/*case ESlotType::Proximity:
		Item->Take(GetOwningPlayer<APController>(), ID, SlotType);
		break;

	case ESlotType::Hotbar:
		Item->Take(GetOwningPlayer<APController>(), ID, SlotType);
		break;

	case ESlotType::Wearables:
		Item->Take(GetOwningPlayer<APController>(), ID, SlotType);
		break;*/
	}

	return FReply::Handled();
}

void UInventorySlotWidget::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation)
{
	InventoryUI->SetSelectedSlot(this);
	InventoryUI->GetContextMenu()->Close();

	SetVisibility(ESlateVisibility::Hidden);

	// Setting up Operation
	OutOperation = NewObject<UDragDropOperation>();
	OutOperation->Payload = this;
	OutOperation->Offset = FVector2D(-0.15, 0);
	OutOperation->Pivot = EDragPivot::CenterCenter;

	// Setting up Drag Visual	
	UInventorySlotWidget* DragVisual = CreateWidget<UInventorySlotWidget>(GetOwningPlayer(), GetClass(), FName("Drag Visual"));
	DragVisual->Setup(nullptr, -1, Item, Amount, ESlotType::Inventory);
	DragVisual->SetSelected(true);

	if (InMouseEvent.IsMouseButtonDown(FKey(TEXT("RightMouseButton"))))
	{
		DragVisual->txtAmount->SetText(FText::FromString("?"));
		OutOperation->Tag = TEXT("StackDrop");
	}

	OutOperation->DefaultDragVisual = DragVisual;
}

void UInventorySlotWidget::NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	SetVisibility(ESlateVisibility::Visible);
}

void UInventorySlotWidget::Setup(UInventoryWidget* InventoryRef, int IDToSet, UItem* ItemToSet, int AmountToSet, ESlotType SlotTypeToSet)
{
	InventoryUI = InventoryRef;
	ID = IDToSet;
	Item = ItemToSet;
	Amount = AmountToSet;
	SlotType = SlotTypeToSet;	

	Icon->SetBrushFromTexture(Item->GetThumbnail());
	txtAmount->SetText(FText::FromString(FString::FromInt(Amount)));
	txtID->SetText(FText::FromString(FString::FromInt(ID)));	
}

void UInventorySlotWidget::SetAmount(int AmountToSet)
{
	Amount = AmountToSet;

	txtAmount->SetText(FText::FromString(FString::FromInt(Amount)));
}

void UInventorySlotWidget::ClearAmountText()
{
	txtAmount->SetText(FText());
}

void UInventorySlotWidget::SetSelected(bool bEnabled)
{
	//return;

	// CAUSES FUCKING CRASHES DON'T KNOW WHY
	if (Background)
		Background->SetBrushColor(bEnabled ? SelectedBackgroundColor : DefaultBackgroundColor);
}