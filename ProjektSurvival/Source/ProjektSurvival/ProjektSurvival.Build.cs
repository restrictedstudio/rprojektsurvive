// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class ProjektSurvival : ModuleRules
{
	public ProjektSurvival(ReadOnlyTargetRules Target) : base(Target)
	{
        //always create a precompiled header
        MinFilesUsingPrecompiledHeaderOverride = 1;

        //This will turn off the combining of source files
        bUseUnity = false;        

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG", "Slate", "SlateCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });        

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
