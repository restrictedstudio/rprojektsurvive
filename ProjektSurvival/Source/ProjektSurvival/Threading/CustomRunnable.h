// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HAL/Runnable.h"

/**
 * A Custom FRunnable Class
 * It has ID and ThreadsManager
 */
class FCustomRunnable : public FRunnable
{
public:

	// Works
	virtual uint32 Run() = 0;

	// Calls on Thread End
	virtual void Exit() = 0;

	// Calls on termination
	virtual void Stop() { }

	void SetID(uint32 InID) { ID = InID; }

	void ClearThreadsManager() { ThreadsManager = nullptr; }

protected:

	uint32 ID = 0;

	class UThreadsManager* ThreadsManager;

};
