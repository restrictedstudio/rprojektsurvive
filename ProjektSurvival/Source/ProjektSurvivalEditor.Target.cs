// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class ProjektSurvivalEditorTarget : TargetRules
{
	public ProjektSurvivalEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
        
        ExtraModuleNames.AddRange( new string[] { "ProjektSurvival" } );

		DefaultBuildSettings = BuildSettingsVersion.V2;

	}
}
